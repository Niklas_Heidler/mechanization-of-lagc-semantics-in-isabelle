theory LAGC_WL_Examples
  imports LAGC_WL
begin


subsection \<open>While Language (WL)\<close>

text \<open>\<open>SKIP\<close>\<close>
definition
  stmt\<^sub>0 :: stmt where
  "stmt\<^sub>0 \<equiv> SKIP"

text \<open>\<open>x1 := 2\<close>\<close>
definition
  stmt\<^sub>1 :: stmt where
  "stmt\<^sub>1 \<equiv> ''x1'' := Num 2"

text \<open>\<open>IF True THEN SKIP FI\<close>\<close>
definition
  stmt\<^sub>2 :: stmt where
  "stmt\<^sub>2 \<equiv> IF (Bool True) THEN SKIP FI"

text \<open>\<open>IF False THEN WHILE (False) DO SKIP OD FI;; z1 := 0\<close>\<close>
definition
  stmt\<^sub>3 :: stmt where
  "stmt\<^sub>3 \<equiv> IF (Bool False) THEN WHILE (Bool False) DO SKIP OD FI;; ''z1'' := Num 0"

text \<open>\<open>WHILE True DO SKIP OD\<close>\<close>
definition
  stmt\<^sub>4 :: stmt where
  "stmt\<^sub>4 \<equiv> WHILE (Bool True) DO SKIP OD"

text \<open>\<open>SKIP; WHILE False DO x0 := 5; SKIP OD; SKIP\<close>\<close>
definition
  stmt\<^sub>5 :: stmt where
  "stmt\<^sub>5 \<equiv> (SKIP;;WHILE (Bool False) DO ''x0'' := Num 5;;SKIP OD);;SKIP"

text \<open>\<open>SKIP; SKIP\<close>\<close>
definition
  stmt\<^sub>6 :: stmt where
  "stmt\<^sub>6 \<equiv> SKIP;;SKIP"

text \<open>\<open>z0 := 2 + z0; x0 := 2\<close>\<close>
definition
  stmt\<^sub>7 :: stmt where
  "stmt\<^sub>7 \<equiv> ''z0'' := (Num 2) \<^sub>Aadd (Var ''z0'');; ''x0'' := Num 2"

text \<open>\<open>SKIP; SKIP; SKIP\<close>\<close>
definition
  stmt\<^sub>8 :: stmt where
  "stmt\<^sub>8 \<equiv> (SKIP;;SKIP);;SKIP"

text \<open>\<open>x := 0; x := 1; x := 2\<close>\<close>
definition
  stmt\<^sub>9 :: stmt where
  "stmt\<^sub>9 \<equiv> (''x'' := Num 0;;''x'' := Num 1);;''x'' := Num 2"

text \<open>\<open>\<sigma>\<^sub>A = [x \<mapsto> 0][y \<mapsto> 0]\<close>\<close>
definition 
  \<sigma>\<^sub>A :: \<Sigma> where
  "\<sigma>\<^sub>A \<equiv> \<sigma>\<^sub>I ((''x'' := Num 1;; ''y'' := (Var ''x'') \<^sub>Aadd (Num 1)))"

lemma "vars stmt\<^sub>0 = {}"
  by (simp add: stmt\<^sub>0_def)

lemma "vars stmt\<^sub>1 = {''x1''}"
  by (simp add: stmt\<^sub>1_def)

lemma "vars stmt\<^sub>2 = {}"
  by (simp add: stmt\<^sub>2_def)

lemma "vars stmt\<^sub>3 = {''z1''}"
  by (simp add: stmt\<^sub>3_def)

lemma "vars stmt\<^sub>4 = {}"
  by (simp add: stmt\<^sub>4_def)

lemma "vars stmt\<^sub>5 = {''x0''}"
  by (simp add: stmt\<^sub>5_def)

lemma "vars stmt\<^sub>6 = {}"
  by (simp add: stmt\<^sub>6_def)

lemma "vars stmt\<^sub>7 = {''x0'', ''z0''}"
  by (simp add: stmt\<^sub>7_def)

lemma "vars stmt\<^sub>8 = {}"
  by (simp add: stmt\<^sub>8_def)

lemma "vars stmt\<^sub>9 = {''x''}"
  by (simp add: stmt\<^sub>9_def)

lemma "\<sigma>\<^sub>I stmt\<^sub>0 = fm []"
  by (simp add: stmt\<^sub>0_def)

lemma "\<sigma>\<^sub>I stmt\<^sub>1 = fm [(''x1'', Exp (Num 0))]"
  by (simp add: stmt\<^sub>1_def)

lemma "\<sigma>\<^sub>I stmt\<^sub>2 = fm []"
  by (simp add: stmt\<^sub>2_def)

lemma "\<sigma>\<^sub>I stmt\<^sub>3 = fm [(''z1'', Exp (Num 0))]"
  by (simp add: stmt\<^sub>3_def)

lemma "\<sigma>\<^sub>I stmt\<^sub>4 = fm []"
  by (simp add: stmt\<^sub>4_def)

lemma "\<sigma>\<^sub>I stmt\<^sub>5 = fm [(''x0'', Exp (Num 0))]"
  by (simp add: stmt\<^sub>5_def)

lemma "\<sigma>\<^sub>I stmt\<^sub>6 = fm []"
  by (simp add: stmt\<^sub>6_def)

lemma "\<sigma>\<^sub>I stmt\<^sub>7 = fm [(''z0'', Exp (Num 0)), (''x0'', Exp (Num 0))]"
  by (simp add: stmt\<^sub>7_def)

lemma "\<sigma>\<^sub>I stmt\<^sub>8 = fm []"
  by (simp add: stmt\<^sub>8_def)

lemma "\<sigma>\<^sub>I stmt\<^sub>9 = fm [(''x'', Exp (Num 0))]"
  by (simp add: stmt\<^sub>9_def)



subsection \<open>Local Evaluation\<close>

text \<open>\<open>SKIP\<close>\<close>
lemma "val SKIP \<sigma> 
      = { {} \<triangleright> \<langle>\<sigma>\<rangle> \<^item> \<lambda>[\<nabla>] }"
  by simp

text \<open>\<open>x := 1\<close>\<close>
lemma "val (''x'' := Num 1) \<sigma> 
      = { {} \<triangleright> \<langle>\<sigma>\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 1)] \<sigma>\<rrangle> \<^item> \<lambda>[\<nabla>] }"
  by simp

text \<open>\<open>If True THEN x := 1 FI\<close>\<close>
lemma "val (IF (Bool True) THEN ''x'' := Num 1 FI) \<sigma> 
      = { {Bool True} \<triangleright> \<langle>\<sigma>\<rangle> \<^item> \<lambda>[''x'' := Num 1], 
          {Bool False} \<triangleright> \<langle>\<sigma>\<rangle> \<^item> \<lambda>[\<nabla>] }"
  by simp

text \<open>\<open>WHILE True DO SKIP OD\<close>\<close>
lemma "val (WHILE (Bool True) DO SKIP OD) \<sigma> 
      = { {Bool True} \<triangleright> \<langle>\<sigma>\<rangle> \<^item> \<lambda>[SKIP;;WHILE (Bool True) DO SKIP OD], 
          {Bool False} \<triangleright> \<langle>\<sigma>\<rangle> \<^item> \<lambda>[\<nabla>] }"
  by simp

text \<open>\<open>SKIP; x := 1\<close>\<close>
lemma "val (SKIP;;''x'' := Num 1) \<sigma> 
      = { {} \<triangleright> \<langle>\<sigma>\<rangle> \<^item> \<lambda>[''x'' := Num 1] }"
  by simp

text \<open>\<open>x := 1; y := x + 1\<close> [\<open>Example 3.1\<close>]\<close>
lemma "val (''x'' := Num 1;; ''y'' := (Var ''x'') \<^sub>Aadd (Num 1)) \<sigma> 
      = { {} \<triangleright> \<langle>\<sigma>\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 1)] \<sigma>\<rrangle> \<^item> \<lambda>[''y'' := (Var ''x'') \<^sub>Aadd (Num 1)] }"
  by simp



subsection \<open>Trace Composition\<close>

text \<open>\<open>SKIP\<close>\<close>
lemma "\<delta> (\<langle>\<circle>\<rangle>, \<lambda>[SKIP]) = {(\<langle>\<circle>\<rangle>, \<lambda>[\<nabla>])}" 
  using \<delta>_system by simp

text \<open>\<open>SKIP;SKIP\<close>\<close>
lemma "\<delta> (\<langle>\<circle>\<rangle>, \<lambda>[SKIP;;SKIP]) = {(\<langle>\<circle>\<rangle>, \<lambda>[SKIP])}" 
  using \<delta>_system by simp

text \<open>\<open>x := 1\<close>\<close>
lemma "\<delta> (\<langle>\<circle>\<rangle>, \<lambda>[''x'' := Num 1]) =
       {(\<langle>\<circle>\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 1)] \<circle>\<rrangle>, \<lambda>[\<nabla>])}" 
  using \<delta>_system by simp

text \<open>\<open>IF True THEN x := 1 FI\<close>\<close>
lemma "\<delta> (\<langle>\<circle>\<rangle>, \<lambda>[IF (Bool True) THEN ''x'' := Num 1 FI]) =
       {(\<langle>\<circle>\<rangle>, \<lambda>[''x'' := Num 1])}" 
  using \<delta>_system by simp

text \<open>\<open>IF False THEN x := 1 FI\<close>\<close>
lemma "\<delta> (\<langle>\<circle>\<rangle>, \<lambda>[IF (Bool False) THEN ''x'' := Num 1 FI]) =
       {(\<langle>\<circle>\<rangle>, \<lambda>[\<nabla>])}" 
  using \<delta>_system by simp

text \<open>\<open>WHILE True DO x := 2 OD\<close>\<close>
lemma "\<delta> (\<langle>\<circle>\<rangle>, \<lambda>[WHILE (Bool True) DO ''x'' := Num 2 OD]) =
       {(\<langle>\<circle>\<rangle>, \<lambda>[''x'' := (Num 2);;WHILE (Bool True) DO ''x'' := Num 2 OD])}" 
  using \<delta>_system by simp

text \<open>\<open>WHILE False DO x := 2 OD\<close>\<close>
lemma "\<delta> (\<langle>\<circle>\<rangle>, \<lambda>[WHILE (Bool False) DO ''x'' := Num 2 OD]) =
       {(\<langle>\<circle>\<rangle>, \<lambda>[\<nabla>])}" 
  using \<delta>_system by simp

text \<open>\<open>x := 1; y := x + 1\<close>\<close>
lemma "\<delta> (\<langle>\<circle>\<rangle>, \<lambda>[''x'' := Num 1;; ''y'' := (Var ''x'') \<^sub>Aadd (Num 1)]) =
       {(\<langle>\<circle>\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 1)] \<circle>\<rrangle>, \<lambda>[''y'' := (Var ''x'') \<^sub>Aadd (Num 1)])}" 
  using \<delta>_system by simp



subsection \<open>Global Trace Semantics\<close>

context notes [simp] = Traces\<^sub>N_def begin

text \<open>\<open>SKIP\<close>\<close>
lemma "\<Delta>\<^sub>N 1000 (\<langle>\<circle>\<rangle>, \<lambda>[SKIP]) = {(\<langle>\<circle>\<rangle>, \<lambda>[\<nabla>])}"
  by (simp add: \<Delta>\<^sub>N_system) 

text \<open>\<open>SKIP; x := 2\<close>\<close>
lemma "\<Delta>\<^sub>N 1000 (\<langle>\<circle>\<rangle>, \<lambda>[SKIP;;''x'' := Num 2]) = {(\<langle>\<circle>\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 2)] \<circle>\<rrangle>, \<lambda>[\<nabla>])}"
  by (simp add: \<delta>_system \<Delta>\<^sub>N_system)

text \<open>\<open>SKIP; SKIP; SKIP\<close>\<close>
lemma "\<Delta>\<^sub>N 1000 (\<langle>\<circle>\<rangle>, \<lambda>[SKIP;;(SKIP;;SKIP)]) = {(\<langle>\<circle>\<rangle>, \<lambda>[\<nabla>])}"
  by (simp add: \<delta>_system \<Delta>\<^sub>N_system)

text \<open>\<open>x := 1; x := x + 1\<close>\<close>
lemma "\<Delta>\<^sub>N 1000 (\<langle>\<circle>\<rangle>, \<lambda>[''x'' := Num 1;; ''x'' := (Var ''x'') \<^sub>Aadd (Num 1)]) =
       {((\<langle>\<circle>\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 1)] \<circle>\<rrangle>) \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 2)] \<circle>\<rrangle>, \<lambda>[\<nabla>])}" 
  by (simp add: \<delta>_system \<Delta>\<^sub>N_system)

text \<open>\<open>x := 1; y := x + 1\<close> [\<open>Example 3.4\<close>]\<close>
lemma "\<Delta>\<^sub>N 1000 (\<langle>\<sigma>\<^sub>A\<rangle>, \<lambda>[''x'' := Num 1;; ''y'' := (Var ''x'') \<^sub>Aadd (Num 1)]) =
       {((\<langle>\<sigma>\<^sub>A\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 1)] \<sigma>\<^sub>A\<rrangle>) \<leadsto> State\<llangle>[''y'' \<longmapsto> Exp (Num 2)] [''x'' \<longmapsto> Exp (Num 1)] \<sigma>\<^sub>A\<rrangle>, \<lambda>[\<nabla>])}" 
  by (simp add: \<delta>_system \<Delta>\<^sub>N_system)

text \<open>\<open>x := 0; IF (x == 0) THEN x := 2 FI\<close>\<close>
lemma "\<Delta>\<^sub>N 1000 (\<langle>\<circle>\<rangle>, \<lambda>[''x'' := Num 0;;IF ((Var ''x'') \<^sub>Req (Num 0)) THEN ''x'' := Num 2 FI]) =
       {((\<langle>\<circle>\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 0)] \<circle>\<rrangle>) \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 2)] \<circle>\<rrangle>, \<lambda>[\<nabla>])}"
  by (simp add: \<delta>_system \<Delta>\<^sub>N_system)

text \<open>\<open>x := 0; WHILE (x == 0) DO x := 2 OD\<close>\<close>
lemma "\<Delta>\<^sub>N 1000 (\<langle>\<circle>\<rangle>, \<lambda>[''x'' := Num 0;;WHILE ((Var ''x'') \<^sub>Req (Num 0)) DO ''x'' := Num 2 OD]) =
       {((\<langle>\<circle>\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 0)] \<circle>\<rrangle>) \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 2)] \<circle>\<rrangle>, \<lambda>[\<nabla>])}"
  by (simp add: \<delta>_system \<Delta>\<^sub>N_system)

text \<open>\<open>x := 7;; y := 1;; WHILE (x \<ge> 2) DO y := y * x;; x := x - 1 OD\<close>\<close>
lemma "\<Delta>\<^sub>N 1000 (\<langle>\<circle>\<rangle>, \<lambda>[(''x'' := Num 7;; ''y'' := Num 1);; 
              WHILE ((Var ''x'') \<^sub>Rgeq (Num 2)) 
                 DO ''y'' := (Var ''y'') \<^sub>Amul (Var ''x'');; 
                    ''x'' := (Var ''x'') \<^sub>Asub (Num 1) 
                 OD]) =
      {((((((((((((((\<langle>fm []\<rangle> \<leadsto> State \<llangle>fm [(''x'', Exp (Num 7))]\<rrangle>) \<leadsto>
                  State \<llangle>fm [(''x'', Exp (Num 7)), (''y'', Exp (Num 1))]\<rrangle>) \<leadsto>
                 State \<llangle>fm [(''x'', Exp (Num 7)), (''y'', Exp (Num 7))]\<rrangle>) \<leadsto>
                State \<llangle>fm [(''x'', Exp (Num 6)), (''y'', Exp (Num 7))]\<rrangle>) \<leadsto>
               State \<llangle>fm [(''x'', Exp (Num 6)), (''y'', Exp (Num 42))]\<rrangle>) \<leadsto>
              State \<llangle>fm [(''x'', Exp (Num 5)), (''y'', Exp (Num 42))]\<rrangle>) \<leadsto>
             State \<llangle>fm [(''x'', Exp (Num 5)), (''y'', Exp (Num 210))]\<rrangle>) \<leadsto>
            State \<llangle>fm [(''x'', Exp (Num 4)), (''y'', Exp (Num 210))]\<rrangle>) \<leadsto>
           State \<llangle>fm [(''x'', Exp (Num 4)), (''y'', Exp (Num 840))]\<rrangle>) \<leadsto>
          State \<llangle>fm [(''x'', Exp (Num 3)), (''y'', Exp (Num 840))]\<rrangle>) \<leadsto>
         State \<llangle>fm [(''x'', Exp (Num 3)), (''y'', Exp (Num 2520))]\<rrangle>) \<leadsto>
        State \<llangle>fm [(''x'', Exp (Num 2)), (''y'', Exp (Num 2520))]\<rrangle>) \<leadsto>
       State \<llangle>fm [(''x'', Exp (Num 2)), (''y'', Exp (Num 5040))]\<rrangle>) \<leadsto>
      State \<llangle>fm [(''x'', Exp (Num 1)), (''y'', Exp (Num 5040))]\<rrangle>, \<lambda>[\<nabla>])}"
  by (simp add: \<delta>_system \<Delta>\<^sub>N_system)
                          
text \<open>\<open>SKIP\<close>\<close>
lemma "Tr\<^sub>N SKIP \<circle> 1000 = {\<langle>\<circle>\<rangle>}"
  by (simp add: \<delta>_system \<Delta>\<^sub>N_system)

text \<open>\<open>SKIP; x := 2\<close>\<close>
lemma "Tr\<^sub>N (SKIP;;''x'' := Num 2) \<circle> 1000 = {\<langle>\<circle>\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 2)] \<circle>\<rrangle>}"
  by (simp add: \<delta>_system \<Delta>\<^sub>N_system)

text \<open>\<open>SKIP; SKIP; SKIP\<close>\<close>
lemma "Tr\<^sub>N (SKIP;;(SKIP;;SKIP)) \<circle> 1000 = {\<langle>\<circle>\<rangle>}"
  by (simp add: \<delta>_system \<Delta>\<^sub>N_system)

text \<open>\<open>x := 1; x := x + 1\<close>\<close>
lemma "Tr\<^sub>N (''x'' := Num 1;; ''x'' := (Var ''x'') \<^sub>Aadd (Num 1)) \<circle> 1000 =
       {(\<langle>\<circle>\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 1)] \<circle>\<rrangle>) \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 2)] \<circle>\<rrangle>}" 
  by (simp add: \<delta>_system \<Delta>\<^sub>N_system)

text \<open>\<open>x := 1; y := x + 1\<close> [\<open>Example 3.4\<close>]\<close>
lemma "Tr\<^sub>N (''x'' := Num 1;; ''y'' := (Var ''x'') \<^sub>Aadd (Num 1)) \<sigma>\<^sub>A 1000 =
       {(\<langle>\<sigma>\<^sub>A\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 1)] \<sigma>\<^sub>A\<rrangle>) \<leadsto> State\<llangle>[''y'' \<longmapsto> Exp (Num 2)] [''x'' \<longmapsto> Exp (Num 1)] \<sigma>\<^sub>A\<rrangle>}" 
  by (simp add: \<delta>_system \<Delta>\<^sub>N_system)

text \<open>\<open>x := 0; IF (x == 0) THEN x := 2 FI\<close>\<close>
lemma "Tr\<^sub>N (''x'' := Num 0;;IF ((Var ''x'') \<^sub>Req (Num 0)) THEN ''x'' := Num 2 FI) \<circle> 1000 =
       {(\<langle>\<circle>\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 0)] \<circle>\<rrangle>) \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 2)] \<circle>\<rrangle>}"
  by (simp add: \<delta>_system \<Delta>\<^sub>N_system)

text \<open>\<open>x := 0; WHILE (x == 0) DO x := 2 OD\<close>\<close>
lemma "Tr\<^sub>N (''x'' := Num 0;;WHILE ((Var ''x'') \<^sub>Req (Num 0)) DO ''x'' := Num 2 OD) \<circle> 1000 =
       {(\<langle>\<circle>\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 0)] \<circle>\<rrangle>) \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 2)] \<circle>\<rrangle>}"
  by (simp add: \<delta>_system \<Delta>\<^sub>N_system)

text \<open>\<open>x := 7;; y := 1;; WHILE (x \<ge> 2) DO y := y * x;; x := x - 1 OD\<close>\<close>
lemma "Tr\<^sub>N ((''x'' := Num 7;; ''y'' := Num 1);; 
              WHILE ((Var ''x'') \<^sub>Rgeq (Num 2)) 
                 DO ''y'' := (Var ''y'') \<^sub>Amul (Var ''x'');; 
                    ''x'' := (Var ''x'') \<^sub>Asub (Num 1) 
                 OD) \<circle> 1000 =
      {(((((((((((((\<langle>fm []\<rangle> \<leadsto> State \<llangle>fm [(''x'', Exp (Num 7))]\<rrangle>) \<leadsto>
                 State \<llangle>fm [(''x'', Exp (Num 7)), (''y'', Exp (Num 1))]\<rrangle>) \<leadsto>
                State \<llangle>fm [(''x'', Exp (Num 7)), (''y'', Exp (Num 7))]\<rrangle>) \<leadsto>
               State \<llangle>fm [(''x'', Exp (Num 6)), (''y'', Exp (Num 7))]\<rrangle>) \<leadsto>
              State \<llangle>fm [(''x'', Exp (Num 6)), (''y'', Exp (Num 42))]\<rrangle>) \<leadsto>
             State \<llangle>fm [(''x'', Exp (Num 5)), (''y'', Exp (Num 42))]\<rrangle>) \<leadsto>
            State \<llangle>fm [(''x'', Exp (Num 5)), (''y'', Exp (Num 210))]\<rrangle>) \<leadsto>
           State \<llangle>fm [(''x'', Exp (Num 4)), (''y'', Exp (Num 210))]\<rrangle>) \<leadsto>
          State \<llangle>fm [(''x'', Exp (Num 4)), (''y'', Exp (Num 840))]\<rrangle>) \<leadsto>
         State \<llangle>fm [(''x'', Exp (Num 3)), (''y'', Exp (Num 840))]\<rrangle>) \<leadsto>
        State \<llangle>fm [(''x'', Exp (Num 3)), (''y'', Exp (Num 2520))]\<rrangle>) \<leadsto>
       State \<llangle>fm [(''x'', Exp (Num 2)), (''y'', Exp (Num 2520))]\<rrangle>) \<leadsto>
      State \<llangle>fm [(''x'', Exp (Num 2)), (''y'', Exp (Num 5040))]\<rrangle>) \<leadsto>
     State \<llangle>fm [(''x'', Exp (Num 1)), (''y'', Exp (Num 5040))]\<rrangle>}"
  by (simp add: \<delta>_system \<Delta>\<^sub>N_system)

end

context notes [simp] = Traces_def begin

text \<open>\<open>SKIP\<close>\<close>
lemma "Tr SKIP \<circle> = {\<langle>\<circle>\<rangle>}"
  by (simp add: WL_derivation_system)

text \<open>\<open>SKIP; x := 2\<close>\<close>
lemma "Tr (SKIP;;''x'' := Num 2) \<circle> = {\<langle>\<circle>\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 2)] \<circle>\<rrangle>}"
  by (simp add: WL_derivation_system)

text \<open>\<open>SKIP; SKIP; SKIP\<close>\<close>
lemma "Tr (SKIP;;(SKIP;;SKIP)) \<circle> = {\<langle>\<circle>\<rangle>}"
  by (simp add: WL_derivation_system)

text \<open>\<open>x := 1; x := x + 1\<close>\<close>
lemma "Tr (''x'' := Num 1;; ''x'' := (Var ''x'') \<^sub>Aadd (Num 1)) \<circle> =
       {(\<langle>\<circle>\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 1)] \<circle>\<rrangle>) \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 2)] \<circle>\<rrangle>}" 
  by (simp add: WL_derivation_system)

text \<open>\<open>x := 1; y := x + 1\<close> [\<open>Example 3.4\<close>]\<close>
lemma "Tr (''x'' := Num 1;; ''y'' := (Var ''x'') \<^sub>Aadd (Num 1)) \<sigma>\<^sub>A =
       {(\<langle>\<sigma>\<^sub>A\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 1)] \<sigma>\<^sub>A\<rrangle>) \<leadsto> State\<llangle>[''y'' \<longmapsto> Exp (Num 2)] [''x'' \<longmapsto> Exp (Num 1)] \<sigma>\<^sub>A\<rrangle>}" 
  by (simp add: WL_derivation_system)

text \<open>\<open>x := 0; IF (x == 0) THEN x := 2 FI\<close>\<close>
lemma "Tr (''x'' := Num 0;;IF ((Var ''x'') \<^sub>Req (Num 0)) THEN ''x'' := Num 2 FI) \<circle> =
       {(\<langle>\<circle>\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 0)] \<circle>\<rrangle>) \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 2)] \<circle>\<rrangle>}"
  by (simp add: WL_derivation_system)

text \<open>\<open>IF (x == 0) THEN x := 2 FI;; SKIP\<close>\<close>
lemma "Tr (IF ((Var ''x'') \<^sub>Req (Num 0)) THEN ''x'' := Num 2 FI;; SKIP) ([''x'' \<longmapsto> \<^emph>] \<circle>) = {}"
  by (simp add: WL_derivation_system)

text \<open>\<open>x := 0; WHILE (x == 0) DO x := 2 OD\<close>\<close>
lemma "Tr (''x'' := Num 0;;WHILE ((Var ''x'') \<^sub>Req (Num 0)) DO ''x'' := Num 2 OD) \<circle> =
       {(\<langle>\<circle>\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 0)] \<circle>\<rrangle>) \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 2)] \<circle>\<rrangle>}"
  by (simp add: WL_derivation_system)

text \<open>\<open>x := 7;; y := 1;; WHILE (x \<ge> 2) DO y := y * x;; x := x - 1 OD\<close>\<close>
lemma "Tr ((''x'' := Num 7;; ''y'' := Num 1);; 
              WHILE ((Var ''x'') \<^sub>Rgeq (Num 2)) 
                 DO ''y'' := (Var ''y'') \<^sub>Amul (Var ''x'');; 
                    ''x'' := (Var ''x'') \<^sub>Asub (Num 1) 
                 OD) \<circle> =
      {(((((((((((((\<langle>fm []\<rangle> \<leadsto> State \<llangle>fm [(''x'', Exp (Num 7))]\<rrangle>) \<leadsto>
                 State \<llangle>fm [(''x'', Exp (Num 7)), (''y'', Exp (Num 1))]\<rrangle>) \<leadsto>
                State \<llangle>fm [(''x'', Exp (Num 7)), (''y'', Exp (Num 7))]\<rrangle>) \<leadsto>
               State \<llangle>fm [(''x'', Exp (Num 6)), (''y'', Exp (Num 7))]\<rrangle>) \<leadsto>
              State \<llangle>fm [(''x'', Exp (Num 6)), (''y'', Exp (Num 42))]\<rrangle>) \<leadsto>
             State \<llangle>fm [(''x'', Exp (Num 5)), (''y'', Exp (Num 42))]\<rrangle>) \<leadsto>
            State \<llangle>fm [(''x'', Exp (Num 5)), (''y'', Exp (Num 210))]\<rrangle>) \<leadsto>
           State \<llangle>fm [(''x'', Exp (Num 4)), (''y'', Exp (Num 210))]\<rrangle>) \<leadsto>
          State \<llangle>fm [(''x'', Exp (Num 4)), (''y'', Exp (Num 840))]\<rrangle>) \<leadsto>
         State \<llangle>fm [(''x'', Exp (Num 3)), (''y'', Exp (Num 840))]\<rrangle>) \<leadsto>
        State \<llangle>fm [(''x'', Exp (Num 3)), (''y'', Exp (Num 2520))]\<rrangle>) \<leadsto>
       State \<llangle>fm [(''x'', Exp (Num 2)), (''y'', Exp (Num 2520))]\<rrangle>) \<leadsto>
      State \<llangle>fm [(''x'', Exp (Num 2)), (''y'', Exp (Num 5040))]\<rrangle>) \<leadsto>
     State \<llangle>fm [(''x'', Exp (Num 1)), (''y'', Exp (Num 5040))]\<rrangle>}"
  by (simp add: WL_derivation_system)

end



subsection \<open>Code Generation\<close>

text \<open>\<open>SKIP;; x := 2\<close> \<open>[1 step]\<close>\<close>
value "Tr\<^sub>N (SKIP;;''x'' := Num 2) \<circle> 1"

text \<open>\<open>SKIP;; x := 2\<close> \<open>[2 steps]\<close>\<close>
value "Tr\<^sub>N (SKIP;;''x'' := Num 2) \<circle> 2"

text \<open>\<open>x := 0;; WHILE (x \<le> 10) DO x := x + 2 OD\<close> \<open>[5 steps]\<close>\<close>
value "Tr\<^sub>N (''x'' := Num 0;;WHILE ((Var ''x'') \<^sub>Rleq (Num 10)) DO ''x'' := (Var ''x'') \<^sub>Aadd (Num 2) OD) \<circle> 5"

text \<open>\<open>x := 0;; WHILE (x \<le> 10) DO x := x + 2 OD\<close> \<open>[1000 steps]\<close>\<close>
value "Tr\<^sub>N (''x'' := Num 0;;WHILE ((Var ''x'') \<^sub>Rleq (Num 10)) DO ''x'' := (Var ''x'') \<^sub>Aadd (Num 2) OD) \<circle> 1000"

text \<open>\<open>SKIP\<close>\<close>
value "Tr SKIP \<circle>"

text \<open>\<open>SKIP;; x := 2\<close>\<close>
value "Tr (SKIP;;''x'' := Num 2) \<circle>" 

text \<open>\<open>SKIP;; SKIP;; SKIP\<close>\<close>
value "Tr (SKIP;;(SKIP;;SKIP)) \<circle>"

text \<open>\<open>x := 1;; x := x + 1\<close>\<close>
value "Tr (''x'' := Num 1;; ''x'' := (Var ''x'') \<^sub>Aadd (Num 1)) \<circle>"

text \<open>\<open>x := 1; y := x + 1\<close> [\<open>Example 3.4\<close>]\<close>
value "Tr (''x'' := Num 1;; ''y'' := (Var ''x'') \<^sub>Aadd (Num 1)) \<sigma>\<^sub>A"

text \<open>\<open>x := 0;; IF (x == 0) THEN x := 2 FI\<close>\<close>
value "Tr (''x'' := Num 0;;IF ((Var ''x'') \<^sub>Req (Num 0)) THEN ''x'' := Num 2 FI) \<circle>"

text \<open>\<open>x := 0;; WHILE (x \<le> 10) DO x := x + 2 OD\<close>\<close>
value "Tr (''x'' := Num 0;;WHILE ((Var ''x'') \<^sub>Rleq (Num 10)) DO ''x'' := (Var ''x'') \<^sub>Aadd (Num 2) OD) \<circle>"

text \<open>\<open>x := 0;; x := 1;; x := 2;; x := 3;; y := 4;; x := 5;; x := 6;; x := 7;; x := 8\<close>\<close>
value "Tr ((((((((''x'' := Num 0;; ''x'' := Num 1);; ''x'' := Num 2);; ''x'' := Num 3);;
              ''y'' := Num 4);; ''x'' := Num 5);; ''x'' := Num 6);; ''x'' := Num 7);; ''x'' := Num 8) \<circle>"

text \<open>\<open>x := 6;; y := 1;; WHILE (x \<ge> 2) DO y := y * x;; x := x - 1 OD\<close>\<close>
value "Tr ((''x'' := Num 6;; ''y'' := Num 1);; 
              WHILE ((Var ''x'') \<^sub>Rgeq (Num 2)) 
                 DO ''y'' := (Var ''y'') \<^sub>Amul (Var ''x'');; 
                    ''x'' := (Var ''x'') \<^sub>Asub (Num 1) 
                 OD) \<circle>"



subsection \<open>Trace Equivalence\<close>

text \<open>SKIP \<sim> (SKIP;;SKIP)\<close>
lemma "SKIP \<sim> (SKIP;;SKIP) [\<sigma>]"
  using tequivalence by (simp add: Traces_def WL_derivation_system)

text \<open>(IF (True) THEN x := 2 FI) \<sim> (x := 2;; SKIP)\<close>
lemma "(IF (Bool True) THEN ''x'' := Num 2 FI) \<sim> (''x'' := Num 2;;SKIP) [\<sigma>]"
  using tequivalence by (simp add: Traces_def WL_derivation_system)

text \<open>(IF (False) THEN x := 2 FI) \<sim> (x := 2;; SKIP)\<close>
lemma "\<not>((IF (Bool False) THEN ''x'' := Num 2 FI) \<sim> (''x'' := Num 2;;SKIP) [\<sigma>])"
  using tequivalence by (simp add: Traces_def WL_derivation_system)

text \<open>(IF x == 1 THEN x := 0) \<sim> (x := 0)\<close>
lemma "\<not>((IF ((Var ''x'') \<^sub>Req (Num 1)) THEN ''x'' := Num 0 FI) \<sim> (''x'' := Num 0) [[''x'' \<longmapsto> Exp (Num 0)] \<circle>])"
  using tequivalence by (simp add: Traces_def WL_derivation_system)

text \<open>(IF x == 1 THEN x := 0) \<sim> (x := 0)\<close>
lemma "(IF ((Var ''x'') \<^sub>Req (Num 1)) THEN ''x'' := Num 0 FI) \<sim> (''x'' := Num 0) [[''x'' \<longmapsto> Exp (Num 1)] \<circle>]"
  using tequivalence by (simp add: Traces_def WL_derivation_system)

text \<open>(x := 4;; WHILE (x \<ge> 1) DO x := x - 1 OD) \<sim> (x := 4;; x := 3;; x := 2;; x := 1;; x := 0)\<close>
lemma "(''x'' := Num 4;; WHILE ((Var ''x'') \<^sub>Rgeq (Num 1)) DO ''x'' := (Var ''x'') \<^sub>Asub (Num 1) OD) 
        \<sim> ((((''x'' := Num 4;;''x'' := Num 3);; ''x'' := Num 2);; ''x'' := Num 1);; ''x'' := Num 0) [\<sigma>]"
  using tequivalence by (simp add: Traces_def WL_derivation_system)

value "SKIP \<sim> (SKIP;;SKIP) [\<circle>]"
value "(IF (Bool True) THEN ''x'' := Num 2 FI) \<sim> (''x'' := Num 2;;SKIP) [\<circle>]"
value "(IF (Bool False) THEN ''x'' := Num 2 FI) \<sim> (''x'' := Num 2;;SKIP) [\<circle>]"
value "(IF ((Var ''x'') \<^sub>Req (Num 1)) THEN ''x'' := Num 0 FI) \<sim> (''x'' := Num 0) [[''x'' \<longmapsto> Exp (Num 0)] \<circle>]"
value "(IF ((Var ''x'') \<^sub>Req (Num 1)) THEN ''x'' := Num 0 FI) \<sim> (''x'' := Num 0) [[''x'' \<longmapsto> Exp (Num 1)] \<circle>]"
value "(''x'' := Num 4;; WHILE ((Var ''x'') \<^sub>Rgeq (Num 1)) DO ''x'' := (Var ''x'') \<^sub>Asub (Num 1) OD) 
      \<sim> ((((''x'' := Num 4;;''x'' := Num 3);; ''x'' := Num 2);; ''x'' := Num 1);; ''x'' := Num 0) [\<circle>]"

end