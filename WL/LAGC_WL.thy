chapter \<open>LAGC Semantics for WL\<close>
theory LAGC_WL
  imports "../basics/LAGC_Base"
begin

text \<open>In this chapter, we formalize the LAGC semantics for the standard
  While Language \<open>WL\<close> as described in section~3 of the original paper.
  One of our main objectives is establishing a proof automation system for
  the trace construction, implying that we will later be able to systematically 
  derive the set of traces generated for any specific program within Isabelle. The
  second goal is setting up an efficient code generation for the construction of
  traces, thereby ensuring that we can dynamically output the set of traces for any
  particular program in the~console.\<close>

section \<open>While Language (WL)\<close>

subsection \<open>Syntax\<close>

text \<open>We begin by defining the syntax of the standard imperative While Language (WL), 
  which we will later use as the underlying programming language of our LAGC
  semantics. Statements of WL can be categorized~as~follows: \begin{description}
  \item[Skip Statement] This command performs no operation.
  \item[Assignment] This command assigns a variable the value of an 
      arithmetic~expression. Note that a variable cannot be assigned to a Boolean 
      value, thereby aligning with the definition~of~states.
  \item[If-Branch] An If-Branch hides a statement behind a Boolean guard. If the
      Boolean guard evaluates to true, the statement is executed. Otherwise,
      the statement is~skipped.
  \item[While-Loop] A While-Loop consists of a Boolean guard and a statement. If the
      Boolean guard evaluates to true, the statement is executed and the loop
      preserved. Otherwise, nothing happens and the loop~exits.
  \item[Sequential Statement] This command executes two statements in~sequence.
  \end{description}\<close>
  datatype stmt = 
      SKIP \<comment> \<open>No-Op\<close>
    | Assign var aexp \<comment> \<open>Assignment of a variable\<close>
    | If bexp stmt \<comment> \<open>If-Branch\<close>
    | While bexp stmt \<comment> \<open>While-Loop\<close>
    | Seq stmt stmt \<comment> \<open>Sequential Statement\<close>

text \<open>We also add a minimal concrete syntax for WL statements, so as to
  improve the readability of programs.\<close>
  notation Assign (infix ":=" 61)
  notation If ("(IF _/ THEN _/ FI)" [1000, 0] 61)
  notation While ("(WHILE _/ DO _/ OD)" [1000, 0] 61)
  notation Seq (infix ";;" 60)

text \<open>Using our previously defined grammar, it is now possible to derive 
  syntactically correct WL statements. We demonstrate this by presenting short 
  examples for programs utilizing our minimal concrete syntax. The first presented 
  program switches the program variables x and y, whilst the second program computes 
  the factorial~of~6.\<close>
  definition
    WL_ex\<^sub>1 :: stmt where
    "WL_ex\<^sub>1 \<equiv> IF (not ((Var ''x'') \<^sub>Req (Var ''y'')))
                          THEN (''z'' := Var ''y'';; ''y'' := Var ''x'');; ''x'' := Var ''z'' 
                    FI"

  definition
    WL_ex\<^sub>2 :: stmt where
    "WL_ex\<^sub>2 \<equiv> (''x'' := Num 6;; ''y'' := Num 1);; 
                    WHILE ((Var ''x'') \<^sub>Rgeq (Num 2)) DO 
                          ''y'' := (Var ''y'') \<^sub>Amul (Var ''x'');; 
                          ''x'' := (Var ''x'') \<^sub>Asub (Num 1) 
                    OD"


subsection \<open>Variable Mappings\<close>

text \<open>We now introduce variable mappings for WL programs mapping specific
  statements to a set of their enclosed free variables. The definition of the 
  function is~straightforward.\<close>
  fun
    vars :: "stmt \<Rightarrow> var set" where
    "vars SKIP = {}" |
    "vars (Assign x a) = {x} \<union> vars\<^sub>A(a)" |
    "vars (If b S) = vars\<^sub>B(b) \<union> vars(S)" |
    "vars (While b S) = vars\<^sub>B(b) \<union> vars(S)" |
    "vars (Seq S\<^sub>1 S\<^sub>2) = vars(S\<^sub>1) \<union> vars(S\<^sub>2)"

text \<open>Similar to expressions, we also provide a variable occurrence function
  that maps specific statements onto a list of all variables occurring in the
  statement. Mapping onto a finite list instead of a (theoretically) infinite set 
  ensures that all variables of programs can be systematically traversed, thereby 
  allowing us to later establish a notion of initial program states. Note that the 
  returned list can consist of duplicate variables, depending on the number of 
  variable occurrences in the actual~program.\<close>
  fun
    occ :: "stmt \<Rightarrow> var list" where
    "occ SKIP = []" |
    "occ (Assign x a) = x # occ\<^sub>A(a)" |
    "occ (If b S) = occ\<^sub>B(b) @ occ(S)" |
    "occ (While b S) = occ\<^sub>B(b) @ occ(S)" |
    "occ (Seq S\<^sub>1 S\<^sub>2) = occ(S\<^sub>1) @ occ(S\<^sub>2)"

text \<open>We can now take another look at our earlier program examples and analyze
  the result of applying a variable mapping and variable occurrence~function.\<close>
  lemma "vars WL_ex\<^sub>1 = {''x'', ''y'', ''z''}"
    by (auto simp add: WL_ex\<^sub>1_def)

  lemma "vars WL_ex\<^sub>2 = {''x'', ''y''}"
    by (auto simp add: WL_ex\<^sub>2_def)

  lemma "occ WL_ex\<^sub>1 = [''x'', ''y'', ''z'', ''y'', ''y'', ''x'', ''x'', ''z'']"
    by (auto simp add: WL_ex\<^sub>1_def)

  lemma "occ WL_ex\<^sub>2 = [''x'', ''y'', ''x'', ''y'', ''y'', ''x'', ''x'', ''x'']"
    by (auto simp add: WL_ex\<^sub>2_def)


subsection \<open>Initial States\<close>

text \<open>An initial state for a program is a state that maps all variables occurring
  in the program to the arithmetic numeral 0. Contrary to the paper, we decide to 
  formalize the construction of initial program states in an explicit manner, so as to 
  ensure that we can automatically create them instead of having to provide 
  them manually for each program. We abbreviate the initial state of a  
  program~with~\<open>\<sigma>\<^sub>I\<close>. \par
  Initial program states are constructed as follows: We first apply the variable
  occurrence function on the given program in order to receive a list of all 
  its program variables. We can then traverse this variable list due to its finite 
  nature and construct the desired initial state. Note that this is the 
  exact purpose of the previously defined \<open>get_initial\<^sub>\<Sigma>\<close>~function.\<close>
  fun
    initial :: "stmt \<Rightarrow> \<Sigma>" ("\<sigma>\<^sub>I") where
    "initial S = get_initial\<^sub>\<Sigma> (occ S)"

text \<open>We can now provide an example for the construction of an initial program state 
  using one of our earlier~programs.\<close>
  lemma "\<sigma>\<^sub>I WL_ex\<^sub>1 = fm[(''x'', Exp (Num 0)), (''y'', Exp (Num 0)), (''z'', Exp (Num 0))]"
    by (simp add: WL_ex\<^sub>1_def fmupd_reorder_neq)


section \<open>Continuations\<close>

subsection \<open>Continuation Markers\<close>

text \<open>In order to later faithfully capture the local evaluation of our While Language, 
  we will need to define a valuation function that evaluates a statement in a given 
  state and returns the set of all conditioned symbolic traces that can be constructed
  until the next scheduling point is reached. However, composite statements have 
  multiple scheduling points, as they consist of several constituent parts. We 
  therefore need to keep track of the statements that are still left to be evaluated 
  (i.e. all statements after the next scheduling point). This is the exact purpose 
  of a continuation marker. A notion of continuation markers is therefore a crucial 
  prerequisite for setting up the local evaluation of our LAGC~semantics. \par
  A continuation marker has one of the following two forms: \<open>\<lambda>[S]\<close> denotes that 
  statement S still needs to be evaluated under the valuation function. \<open>\<lambda>[\<nabla>]\<close> 
  refers to the empty continuation, implying that the computation of the program
  has already been fully~completed.\<close>
  datatype cont_marker = 
      Lambda stmt ("\<lambda>[_]") \<comment> \<open>Non-Empty Continuation Marker\<close>
    | Empty ("\<lambda>[\<nabla>]") \<comment> \<open>Empty Continuation Marker\<close>

text \<open>In contrast to the original paper, we propose an additional variable mapping for 
  continuation markers mapping a non-empty continuation marker to all free variables
  occurring in its encased statement. Note that the empty continuation marker contains 
  no free~variables.\<close>
  fun
    mvars :: "cont_marker \<Rightarrow> var set" where
    "mvars \<lambda>[\<nabla>] = {}" |
    "mvars \<lambda>[S] = vars(S)"


subsection \<open>Continuation Traces\<close>

text \<open>Analogous to the original paper, we can now define a continuation trace as a 
  conditioned symbolic trace with an additional appended continuation~marker.\<close>
  datatype cont_trace = 
      Cont \<CC>\<T> cont_marker (infix "\<^item>" 55)

text \<open>In order to ease the handling of continuation traces, we propose additional
  projections which map a continuation trace onto its encased~components.\<close>
  fun
    proj_pc :: "cont_trace \<Rightarrow> path_condition" ("\<down>\<^sub>p") where
    "\<down>\<^sub>p (pc \<triangleright> \<tau> \<^item> cm) = pc" 

  fun
    proj_\<tau> :: "cont_trace \<Rightarrow> \<T>" ("\<down>\<^sub>\<tau>") where
    "\<down>\<^sub>\<tau> (pc \<triangleright> \<tau> \<^item> cm) = \<tau>" 

  fun
    proj_cont :: "cont_trace \<Rightarrow> cont_marker" ("\<down>\<^sub>\<lambda>") where
    "\<down>\<^sub>\<lambda> (cont \<^item> cm) = cm" 



section \<open>Local Evaluation\<close>

text \<open>We can now start with formalizing the local evaluation of our semantics,
  referring to the construction of traces for arbitrary programs in a local
  environment. Our evaluation rules will take a program and a possibly symbolic state 
  as arguments, subsequently returning a set of all possible continuation traces that 
  can be constructed until the next scheduling point~is~reached. \par
  Aiming to setup the local evaluation, we first introduce a helper function, which 
  modifies a given continuation marker by sequentially appending another statement 
  onto the command inside the continuation marker. If the continuation marker is 
  empty, we simply insert the provided~statement.\<close>
  fun
    cont_append :: "cont_marker \<Rightarrow> stmt \<Rightarrow> cont_marker" where
    "cont_append \<lambda>[S\<^sub>1'] S\<^sub>2 = \<lambda>[S\<^sub>1';;S\<^sub>2]" |
    "cont_append \<lambda>[\<nabla>] S\<^sub>2 = \<lambda>[S\<^sub>2]" 

text \<open>We now have sufficient means to establish the valuation function. Our
  formalization adheres to the following core concepts: \begin{description}
  \item[Skip Statement] The Skip statement called in state \<open>\<sigma>\<close> only generates 
    a singular continuation trace. Its path condition is empty, because the Skip
    statement can be executed in any situation. Its symbolic trace only contains \<open>\<sigma>\<close>,
    as no state changes occur. Considering that Skip is an atomic statement, the 
    continuation marker will also~be~empty. 
  \item[Assignment] An assignment called in state \<open>\<sigma>\<close> generates exactly one
    continuation trace, consisting of an empty path condition and a symbolic trace 
    transiting from \<open>\<sigma>\<close> into an updated version of \<open>\<sigma>\<close>. Note that its continuation 
    marker is again empty, as the whole assignment is evaluated in one singular 
    evaluation~step.
  \item[If-Branch] The conditional statement called in state \<open>\<sigma>\<close> generates two 
    distinct continuation traces. The first one (true-case) can only be taken if 
    the Boolean guard evaluates to true, indicated by the path condition. While its 
    symbolic trace contains only the original state \<open>\<sigma>\<close>, its continuation marker 
    encases the statement S, suggesting that S is still left to be evaluated. Note 
    that this implies a scheduling point right after the evaluation of the Boolean 
    expression. Guard and statement are therefore never evaluated in the same 
    evaluation step. The second continuation trace (false-case) can only be chosen 
    if the Boolean guard evaluates to false, subsequently skipping the encased
    statement. This causes the symbolic trace to only contain \<open>\<sigma>\<close>, and the 
    continuation marker~to~be~empty. \par
    The formalization of the path conditions slightly deviates from the original
    paper. Whilst the original paper compares the evaluation of the guard with
    Boolean truth values, our approach simply evaluates the guard in its 
    normal/negated form. This design choice ensures that our formalization is 
    easier to read, as too long expressions would bloat up our trace~specifications.
  \item[While-Loop] The While-Loop called in state \<open>\<sigma>\<close> also generates two continuation 
    traces, which closely resemble the traces generated by the conditional statement. 
    Their only difference lies in the continuation marker of the true-case. Whilst the 
    If-Branch ensures the subsequent evaluation of its encased statement, the 
    While-Loop additionally enforces the execution of another loop~repetition. \par
    Contrary to the paper, we have decided against rewriting the While-Loop
    as a conditional statement, because Isabelle, in this case, fails at establishing 
    a corresponding termination~argument.
  \item[Sequential Statement] The rule for the sequential statement \<open>S\<^sub>1;;S\<^sub>2\<close> is 
    simple. We collect all possible continuation traces generated by \<open>S\<^sub>1\<close>, and
    append statement \<open>S\<^sub>2\<close> onto all their continuation~markers. \par
    Considering that we have to adhere to Isabelle syntax, our formalization gets 
    slightly more complex than the definition of the original paper. We first apply 
    \<open>(val S\<^sub>1 \<sigma>)\<close> to compute the set of all continuation traces generated by \<open>S\<^sub>1\<close> called 
    in \<open>\<sigma>\<close>. We then apply the predefined operator \<open>`\<close> in order to compute the image of 
    this set under a function, which appends \<open>S\<^sub>2\<close> onto all their continuation markers. 
    For this purpose, we utilize our earlier defined helper~function. \par
    Note that it is not possible to generate code for particular functions, if 
    quantifications over infinite types (e.g. states, traces) take place. We circumvent 
    this problem in our formalization by using the element-wise operation \<open>`\<close> instead 
    of explicit trace quantifications, thus ensuring that Isabelle can automatically 
    generate corresponding~code.
  \end{description} Due to its construction, each application of the valuation 
  function results in only finitely many continuation~traces.\<close>
  primrec
    val :: "stmt \<Rightarrow> \<Sigma> \<Rightarrow> cont_trace set" where
    "val SKIP \<sigma> = { {} \<triangleright> \<langle>\<sigma>\<rangle> \<^item> \<lambda>[\<nabla>] }" |
    "val (x := a) \<sigma> = { {} \<triangleright> \<langle>\<sigma>\<rangle> \<leadsto> State\<llangle>[x \<longmapsto> Exp (val\<^sub>A a \<sigma>)] \<sigma>\<rrangle> \<^item> \<lambda>[\<nabla>] }" |
    "val (IF b THEN S FI) \<sigma> = {
        {val\<^sub>B b \<sigma>} \<triangleright> \<langle>\<sigma>\<rangle> \<^item> \<lambda>[S],
        {val\<^sub>B (Not b) \<sigma>} \<triangleright> \<langle>\<sigma>\<rangle> \<^item> \<lambda>[\<nabla>]
      }" |
    "val (WHILE b DO S OD) \<sigma> = {
        {val\<^sub>B b \<sigma>} \<triangleright> \<langle>\<sigma>\<rangle> \<^item> \<lambda>[S;;WHILE b DO S OD],
        {val\<^sub>B (Not b) \<sigma>} \<triangleright> \<langle>\<sigma>\<rangle> \<^item> \<lambda>[\<nabla>]
      }" |
    "val (S\<^sub>1;;S\<^sub>2) \<sigma> = (%c. (\<down>\<^sub>p c) \<triangleright> (\<down>\<^sub>\<tau> c) \<^item> cont_append (\<down>\<^sub>\<lambda> c) S\<^sub>2) ` (val S\<^sub>1 \<sigma>)"

text \<open>Considering that we have established the local evaluation of our semantics, 
  we can now take a look at several examples of valuation function~applications.\<close>
  lemma "val (''x'' := Num 2) \<sigma>\<^sub>1 = { 
                  {} \<triangleright> \<langle>\<sigma>\<^sub>1\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 2)] \<sigma>\<^sub>1\<rrangle> \<^item> \<lambda>[\<nabla>] 
              }" 
    by simp

  lemma "val WL_ex\<^sub>1 \<sigma>\<^sub>2 = { 
                  {Bool True} \<triangleright> \<langle>\<sigma>\<^sub>2\<rangle> \<^item> \<lambda>[(''z'' := Var ''y'';; ''y'' := Var ''x'');; ''x'' := Var ''z''],
                  {Bool False} \<triangleright> \<langle>\<sigma>\<^sub>2\<rangle> \<^item> \<lambda>[\<nabla>]
              }"
    by (simp add: \<sigma>\<^sub>2_def WL_ex\<^sub>1_def)



section \<open>Trace Composition\<close>

subsection \<open>Configurations\<close>

text \<open>Similar to the original paper, we introduce program configurations as 
  tuples of symbolic traces and continuation markers. We call a configuration
  terminal iff the corresponding continuation marker is~empty.\<close>
  type_synonym config = "\<T> * cont_marker" 


subsection \<open>\<open>\<delta>\<close>-function\<close>

text \<open>The previously defined valuation function already enables us to
  construct traces in a local environment. However, these local traces still
  need to be composed into concrete, global traces. This motivates the notion of 
  trace~compositions. \par
  During a trace composition, the valuation function will be repeatedly applied
  on a given program, until it has been fully evaluated. After each application,
  the constructed traces will be stitched together, and the scheduler will choose 
  which process to execute next. Considering that WL supports no method calls, 
  the scheduler will only be able to select the main process. As WL is even strictly 
  deterministic, we will not be able to generate more than one global trace for any 
  arbitrary~program. \par
  In the original paper, the composition rule is formalized using an inductive
  definition. The rule describes that \<open>(sh \<^emph>\<^emph> \<tau>, \<lambda>[S'])\<close> is a successor configuration
  of \<open>(sh, \<lambda>[S])\<close> iff the continuation trace \<open>pc \<triangleright> \<tau> \<^item> \<lambda>[S']\<close> with consistent path 
  condition \<open>pc\<close> can be generated from \<open>S\<close> called in the last state of \<open>sh\<close>. Such an 
  inductive definition could also be provided in Isabelle. However, its
  transitive closure would later need to have the following~form: \par
  \begin{center} \<open>{\<tau>. (\<langle>\<sigma>\<^sub>I\<rangle>, \<lambda>[S]) \<longrightarrow>\<^sup>\<star> (\<tau>, \<lambda>[\<nabla>])}\<close> \end{center} \par
  Note that this greatly impedes the automatic code generation in Isabelle, as
  we are quantifying over infinitely many traces \<open>\<tau>\<close>. We therefore need to provide an 
  alternative formalization, which does not entail explicit trace quantifications, 
  thus greatly deviating from the rule denoted in the original~paper. \par
  Instead of an inductively defined relation between successor configurations, we 
  decide to formalize a deterministic successor function (\<open>\<delta>\<close>-function) that maps 
  a configuration onto all possible successor configurations reachable in 
  one evaluation step. Using this approach, we can later denote the transitive
  closure with a recursive function, mapping a configuration onto all 
  reachable terminal configurations. This in turn can be formalized without 
  using trace quantifications, thereby solving the earlier~problem. \par
  We formalize the \<open>\<delta>\<close>-function as follows: We first collect all continuation
  traces with consistent path conditions that can be generated by S called in \<open>\<sigma>\<close>. 
  Note that the cardinality of this set is finite. We then use the 
  predefined image operator \<open>`\<close> in order to translate all these continuation traces 
  into corresponding configurations, whilst concatenating the previous trace \<open>sh\<close>
  with the newly generated trace \<open>\<tau>\<close>. Note that this is a partial function,
  as it is undefined if the symbolic trace in the configuration ends
  with an event. However, we will not encounter this situation, as long as we ensure
  that the composition preserves the wellformedness of symbolic~traces.\<close>
  fun
    successors :: "config \<Rightarrow> config set" ("\<delta>")  where
    "\<delta> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S]) = 
        (%c. (sh \<cdot> (\<down>\<^sub>\<tau> c), \<down>\<^sub>\<lambda> c)) ` {cont \<in> (val S \<sigma>). consistent(\<down>\<^sub>p cont)}" |
    "\<delta> _ = undefined"


subsection \<open>Proof Automation\<close>

text \<open>We strongly desire to establish an automated proof system for the
  construction of global traces in our LAGC semantics. The \<open>\<delta>\<close>-function plays
  a crucial role in this construction. However, its definition is slightly complex, 
  considering that it builds on top of the valuation function. This greatly impedes
  automatic proofs, causing a need for additional simplification~lemmas. \par
  We will therefore provide a general simplification lemma for each statement of WL,
  thereby covering all application scenarios of the \<open>\<delta>\<close>-function. This will lay the 
  groundwork for efficient proof derivations, as we can later simply utilize our 
  simplification lemmas when deriving global traces, hence avoiding having to deal 
  with the underlying valuation function. Note that all applications of the \<open>\<delta>\<close>-function 
  will return singleton configuration sets, considering the determinism~of~WL.\<close>

  context notes [simp] = consistent_def begin

  lemma \<delta>_Skip:
    "\<delta> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[SKIP]) = {(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[\<nabla>])}"
    by (simp add: image_constant_conv)

  lemma \<delta>_Assign:
    "\<delta> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[x := a]) = {((sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<leadsto> State\<llangle>[x \<longmapsto> Exp (val\<^sub>A a \<sigma>)] \<sigma>\<rrangle>, \<lambda>[\<nabla>])}"
    by (simp add: image_constant_conv)

text \<open>Applying the \<open>\<delta>\<close>-function on the If-Branch and the While-Loop can cause
  three distinct results. If the path condition containing the evaluated Boolean
  guard is consistent, the statement enters the true-case. If the path condition
  consisting of the evaluated negated Boolean guard is consistent, the statement will
  enter the false-case. The results of both of these cases are~straightforward. \par
  However, it is also possible that the Boolean guard cannot be fully evaluated
  (e.g. due to symbolic variables in the guard expression), thereby differing from 
  the previous cases. Note that the \<open>\<delta>\<close>-function will return the empty set in this
  case, implying that no successor configuration exists. Such a situation is not 
  supposed to transpire during a trace composition, and will not occur, so long as 
  we start our composition in a concrete~state.\<close>
  lemma \<delta>_If\<^sub>T:
    assumes "consistent {(val\<^sub>B b \<sigma>)}"
    shows "\<delta> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[IF b THEN S FI]) = {(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S])}"
    using assms by force

  lemma \<delta>_If\<^sub>F:
    assumes "consistent {(val\<^sub>B (Not b) \<sigma>)}"
    shows "\<delta> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[IF b THEN S FI]) = {(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[\<nabla>])}"
    using assms by force

  lemma \<delta>_If\<^sub>E:
    assumes "\<not>(consistent {(val\<^sub>B b \<sigma>)} \<or> consistent {(val\<^sub>B (Not b) \<sigma>)})"
    shows "\<delta> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[IF b THEN S FI]) = {}"
    using assms by auto

  lemma \<delta>_While\<^sub>T:
    assumes "consistent {(val\<^sub>B b \<sigma>)}"
    shows "\<delta> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[WHILE b DO S OD]) = {(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S;;WHILE b DO S OD])}"
    using assms by force

  lemma \<delta>_While\<^sub>F:
    assumes "consistent {(val\<^sub>B (Not b) \<sigma>)}"
    shows "\<delta> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[WHILE b DO S OD]) = {(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[\<nabla>])}"
    using assms by force

  lemma \<delta>_While\<^sub>E:
    assumes "\<not>(consistent {(val\<^sub>B b \<sigma>)} \<or> consistent {(val\<^sub>B (Not b) \<sigma>)})"
    shows "\<delta> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[WHILE b DO S OD]) = {}"
    using assms by auto

  end

text \<open>The simplification lemma for the sequential command is slightly more complex. 
  We establish that the successor configurations of any sequential statement \<open>S\<^sub>1;;S\<^sub>2\<close> 
  match the successor configurations of \<open>S\<^sub>1\<close> with \<open>S\<^sub>2\<close> appended on all their 
  continuation markers. This allows us to simplify \<open>\<delta>\<close>-function applications on 
  sequential statements to applications on only its first constituent. We infer 
  this equality in Isabelle by proving both subset~relations.\<close>
  lemma \<delta>_Seq\<^sub>1:
    "\<delta> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1;;S\<^sub>2]) \<subseteq> (%c. ((fst c), cont_append (snd c) S\<^sub>2)) ` \<delta> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1])"
  proof (subst subset_iff)
    show "\<forall>c. c \<in> \<delta> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1;;S\<^sub>2]) 
              \<longrightarrow> c \<in> (%c. ((fst c), cont_append (snd c) S\<^sub>2)) ` \<delta> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1])"
    \<comment> \<open>We first use the subset-iff rule in order to rewrite the subset relation into 
       a semantically equivalent~implication.\<close>
    proof (rule allI, rule impI)
      fix c
      \<comment> \<open>We assume \<open>c\<close> to be an arbitrary, but fixed configuration.\<close>
      assume "c \<in> \<delta> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1;;S\<^sub>2])"
      \<comment> \<open>We assume that the premise holds, implying that \<open>c\<close> is a successor 
         configuration~of~\<open>S\<^sub>1;;S\<^sub>2\<close>.\<close>
      then obtain \<pi> where assm\<^sub>\<pi>: 
        "c = (sh \<cdot> (\<down>\<^sub>\<tau> \<pi>), \<down>\<^sub>\<lambda> \<pi>) \<and> \<pi> \<in> val (S\<^sub>1;;S\<^sub>2) \<sigma> \<and> consistent(\<down>\<^sub>p \<pi>)" by auto
      \<comment> \<open>Due to the definition of the \<open>\<delta>\<close>-function, there must exist a continuation 
         trace \<open>\<pi>\<close> with a consistent path condition generated from \<open>S\<^sub>1;;S\<^sub>2\<close> that can 
         be translated into \<open>c\<close>. We obtain this continuation trace~\<open>\<pi>\<close>.\<close> 
      moreover then obtain \<pi>' where assm\<^sub>\<pi>': 
        "\<pi> = (\<down>\<^sub>p \<pi>') \<triangleright> (\<down>\<^sub>\<tau> \<pi>') \<^item> cont_append (\<down>\<^sub>\<lambda> \<pi>') S\<^sub>2 \<and> \<pi>' \<in> (val S\<^sub>1 \<sigma>)" by auto
      \<comment> \<open>Aligning with the definition of the valuation function, there must also
         exist a continuation trace \<open>\<pi>'\<close> generated from \<open>S\<^sub>1\<close>, which matches
         \<open>\<pi>\<close>, if we appended \<open>S\<^sub>2\<close> onto its continuation~marker.\<close>
      ultimately have connect: 
        "consistent (\<down>\<^sub>p \<pi>') \<and> (\<down>\<^sub>\<tau> \<pi>') = (\<down>\<^sub>\<tau> \<pi>) \<and> (\<down>\<^sub>\<lambda> \<pi>) = cont_append (\<down>\<^sub>\<lambda> \<pi>') S\<^sub>2" by simp
      \<comment> \<open>Considering that the path conditions of \<open>\<pi>\<close> and \<open>\<pi>'\<close> match, both must be 
         consistent. Their symbolic traces also match. The only difference lies
         in the modified continuation~marker.\<close>
      moreover then obtain c' where "c' = (sh \<cdot> (\<down>\<^sub>\<tau> \<pi>'), \<down>\<^sub>\<lambda> \<pi>')" by auto
      \<comment> \<open>We can then obtain the configuration \<open>c'\<close> translated from the 
         continuation trace~\<open>\<pi>'\<close>.\<close>
      moreover then have "fst(c) = fst(c') \<and> snd(c) = cont_append (snd c') S\<^sub>2" 
        by (simp add: assm\<^sub>\<pi> connect)
     \<comment> \<open>Given this information, both \<open>c\<close> and \<open>c'\<close> must match in their symbolic 
         traces. However, \<open>c\<close> additionally appends \<open>S\<^sub>2\<close> onto its continuation~marker.\<close>
      ultimately show "c \<in> (%c. ((fst c), cont_append (snd c) S\<^sub>2)) ` \<delta> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1])"
        using assm\<^sub>\<pi> assm\<^sub>\<pi>' image_iff by fastforce
      \<comment> \<open>Thus \<open>c\<close> must match \<open>c'\<close> with an appended \<open>S\<^sub>2\<close> in its continuation marker,
         closing the proof.\<close>
    qed
  qed

  lemma \<delta>_Seq\<^sub>2:
    "(%c. ((fst c), cont_append (snd c) S\<^sub>2)) ` \<delta> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1]) \<subseteq> \<delta> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1;;S\<^sub>2]) "
  proof (subst subset_iff)
    show "\<forall>c. c \<in> (%c. ((fst c), cont_append (snd c) S\<^sub>2)) ` \<delta> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1]) 
              \<longrightarrow> c \<in> \<delta> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1;;S\<^sub>2])"
    \<comment> \<open>We first use the subset-iff rule in order to rewrite the subset relation into 
       a semantically equivalent~implication.\<close> 
    proof (rule allI, rule impI)
      fix c
      \<comment> \<open>We assume \<open>c\<close> to be an arbitrary, but fixed configuration.\<close>
      assume "c \<in> (%c. ((fst c), cont_append (snd c) S\<^sub>2)) ` \<delta> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1])"
      \<comment> \<open>We assume that the premise holds, implying that \<open>c\<close> is a successor 
         configuration of \<open>S\<^sub>1\<close> with \<open>S\<^sub>2\<close> appended onto its continuation~marker.\<close>
      then obtain c' where assm\<^sub>c': 
        "c' \<in> \<delta> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1]) \<and> fst(c') = fst(c) \<and> cont_append (snd c') S\<^sub>2 = snd(c)" by force
      \<comment> \<open>Then there must exist a configuration \<open>c'\<close> that is also a successor 
         configuration of \<open>S\<^sub>1\<close>, matching with \<open>c\<close> in everything except its
         continuation marker. \<open>c'\<close> with \<open>S\<^sub>2\<close> appended on its continuation marker
         matches~configuration~\<open>c\<close>.\<close>
      moreover then obtain \<pi> where assm\<^sub>\<pi>: 
        "\<pi> \<in> val S\<^sub>1 \<sigma> \<and> consistent(\<down>\<^sub>p \<pi>) \<and> c' = (sh \<cdot> (\<down>\<^sub>\<tau> \<pi>), (\<down>\<^sub>\<lambda> \<pi>))" by auto
      \<comment> \<open>Hence there must exist a continuation trace \<open>\<pi>\<close> with a consistent path 
         condition generated from \<open>S\<^sub>1\<close>, which translates to configuration~\<open>c'\<close>.\<close>
      moreover then obtain \<pi>' where 
        "\<pi>' = (\<down>\<^sub>p \<pi>) \<triangleright> (\<down>\<^sub>\<tau> \<pi>) \<^item> cont_append (\<down>\<^sub>\<lambda> \<pi>) S\<^sub>2" by auto
      \<comment> \<open>We then obtain the continuation trace \<open>\<pi>'\<close> which matches \<open>\<pi>\<close> except
         having \<open>S\<^sub>2\<close> additionally appended onto its continuation~marker.\<close>
      ultimately have connect: "\<pi>' \<in> {c \<in> val (S\<^sub>1;;S\<^sub>2) \<sigma>. consistent (\<down>\<^sub>p c)} \<and> (\<down>\<^sub>\<tau> \<pi>') = (\<down>\<^sub>\<tau> \<pi>) \<and> (\<down>\<^sub>\<lambda> \<pi>') = cont_append (\<down>\<^sub>\<lambda> \<pi>) S\<^sub>2" by auto 
      \<comment> \<open>This implies that \<open>\<pi>'\<close> must be a continuation trace with a consistent
         path condition generated from \<open>S\<^sub>1;;S\<^sub>2\<close>. Note that \<open>\<pi>\<close> matches with \<open>\<pi>'\<close>
         in its symbolic trace, but not in its continuation marker.\<close>
      then obtain c'' where assm\<^sub>c'': "c'' = (sh \<cdot> (\<down>\<^sub>\<tau> \<pi>'), \<down>\<^sub>\<lambda> \<pi>')" by auto
      \<comment> \<open>We then obtain the configuration \<open>c''\<close> translated from \<open>\<pi>'\<close>.\<close>
      hence "c = c''" using assm\<^sub>c' assm\<^sub>\<pi> connect by auto
      \<comment> \<open>We can now derive that \<open>c\<close> and \<open>c''\<close> match in both of~their~elements.\<close>
      thus "c \<in> \<delta> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1;;S\<^sub>2])" 
        using assm\<^sub>c'' connect image_iff by fastforce
      \<comment> \<open>We know that \<open>c''\<close> is a successor configuration of \<open>S\<^sub>1;;S\<^sub>2\<close>. 
         Considering that \<open>c\<close> matches \<open>c''\<close>, we can finally conclude that 
         \<open>c\<close> must also be a successor configuration of \<open>S\<^sub>1;;S\<^sub>2\<close>, which needed to be 
         proven in the first~place.\<close>
    qed
  qed
        
  lemma \<delta>_Seq:
    "\<delta> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1;;S\<^sub>2]) = (%c. ((fst c), cont_append (snd c) S\<^sub>2)) ` \<delta> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1])"
    apply (subst set_eq_subset)
    using \<delta>_Seq\<^sub>1 \<delta>_Seq\<^sub>2 by simp
    \<comment> \<open>We can now use the proof of both subset directions to infer the 
       desired~equality.\<close>

text \<open>In order to finalize the automated proof system of the \<open>\<delta>\<close>-function, we 
  collect all previously proven lemmas in a set, and call it the \<open>\<delta>\<close>-system.
  We additionally remove the normal simplifications of the \<open>\<delta>\<close>-function, thereby 
  ensuring that the Isabelle simplifier will later select the simplification
  lemmas of our system when deriving successor~configurations.\<close>
  lemmas \<delta>_system = 
    \<delta>_Skip \<delta>_Assign \<delta>_If\<^sub>T \<delta>_If\<^sub>F \<delta>_If\<^sub>E \<delta>_While\<^sub>T \<delta>_While\<^sub>F \<delta>_While\<^sub>E \<delta>_Seq consistent_def

  declare successors.simps[simp del]

text \<open>We can now use the program \<open>WL-ex\<^sub>1\<close> in order to provide an example for the
  derivation of successor configurations applying our established~\<open>\<delta>\<close>-system. Note 
  that the Boolean guard of the program evaluates to false in its initial state,  
  which implies that it terminates in one singular evaluation step, also indicated 
  by the derivation~below.\<close>
  lemma "\<delta> (\<langle>(\<sigma>\<^sub>I WL_ex\<^sub>1)\<rangle>, \<lambda>[WL_ex\<^sub>1]) = {(\<langle>(\<sigma>\<^sub>I WL_ex\<^sub>1)\<rangle>, \<lambda>[\<nabla>])}"
    apply (simp add: WL_ex\<^sub>1_def)
    using \<delta>_system by simp



section \<open>Global Trace Semantics\<close>

subsection \<open>Bounded Global Traces\<close>

text \<open>By utilizing the notion of trace compositions, we can finally construct global 
  system traces. A global trace of a given program \<open>S\<close> called in initial state \<open>\<sigma>\<close>
  is a symbolic trace of a terminal configuration reachable from initial 
  configuration \<open>(\<langle>\<sigma>\<rangle>, \<lambda>[S])\<close>. In the original paper, this construction was 
  realized by computing the transitive closure of the inductive composition rule. 
  However, note that we have formalized the trace composition using a deterministic 
  function instead of an inductive relation. We will therefore need to provide an 
  additional function for modeling the transitive closure, hence strongly deviating 
  from the original paper. This function should map an initial configuration 
  onto all symbolic traces of reachable terminal~configurations. \par
  The concept of this transitive closure gives rise to a crucial predicament.
  Note that a standard function in Isabelle requires a corresponding termination
  argument. However, it is possible to construct diverging programs in WL 
  due to the While-Loop command (e.g.~\<open>WHILE (Bool True) DO SKIP OD\<close>).
  This implies that a function modeling the transitive closure of the trace
  composition might never terminate, considering that we cannot ensure that we 
  eventually reach a terminal configuration. A solution to this problem must be found, 
  so as to formalize this concept in~Isabelle. \par
  We therefore first compute n-bounded global traces (\<open>\<Delta>\<^sub>N\<close>-function), halting the 
  transitive closure after a certain bound is reached. The function is provided a 
  bound \<open>n\<close> and an initial configuration \<open>c\<close>. It then returns the set of all 
  terminal configurations reachable from \<open>c\<close> in at most n-steps, as well as all
  configurations reachable in exactly n-steps (regardless of their terminal character). 
  This design choice ensures that we stop the evaluation after a finite number
  of steps, thereby providing the missing termination~argument. \par
  We use Isabelle to formalize this intuitive concept as follows: If a terminal 
  configuration is reached, or the bound is exceeded, then the corresponding 
  configuration is returned in a singleton set. If the bound for a non-terminal 
  configuration is not yet exceeded, we first compute all successor configurations 
  using the \<open>\<delta>\<close>-function. We can then recursively apply the \<open>\<Delta>\<^sub>N\<close>-function with 
  bound \<open>(n-1)\<close> on each of those successor configurations, and then merge the results. 
  Note that this formalization avoids trace quantifications, thereby ensuring that 
  Isabelle can automatically generate corresponding~code.\<close>
  fun
    composition\<^sub>N :: "nat \<Rightarrow> config \<Rightarrow> config set" ("\<Delta>\<^sub>N") where
    "\<Delta>\<^sub>N 0 c = {c}" |
    "\<Delta>\<^sub>N (Suc n) (\<tau>, \<lambda>[\<nabla>]) = {(\<tau>, \<lambda>[\<nabla>])}" |
    "\<Delta>\<^sub>N (Suc n) (\<tau>, \<lambda>[S]) = \<Union>((%c. \<Delta>\<^sub>N n c) ` \<delta>(\<tau>, \<lambda>[S]))"

text \<open>The definition for n-bounded global traces generated from program \<open>S\<close> called 
  in \<open>\<sigma>\<close> is now straightforward. We simply return all symbolic traces of the 
  configurations received by calling the \<open>\<Delta>\<^sub>N\<close>-function in initial configuration 
  \<open>(\<langle>\<sigma>\<rangle>, \<lambda>[S])\<close> with bound n. Note that this implies that the empty continuation 
  markers of the configurations are~discarded.\<close>
  definition
    Traces\<^sub>N :: "stmt \<Rightarrow> \<Sigma> \<Rightarrow> nat \<Rightarrow> \<T> set" ("Tr\<^sub>N") where
    "Tr\<^sub>N S \<sigma> n \<equiv> fst ` \<Delta>\<^sub>N n (\<langle>\<sigma>\<rangle>, \<lambda>[S])"

text \<open>Note that bounded global traces can also be used for debugging purposes, as
  it is possible to return intermediate trace results.\<close>


subsection \<open>Unbounded Global Traces\<close>

text \<open>We are now interested in constructing global traces without having to 
  explicitly provide a bound as an argument. As we have previously established
  however, While-Loops can be used to write diverging programs, which 
  never reach a terminal configuration. This crushes possible termination 
  arguments for our boundless transitive closure. We therefore have to 
  examine alternative formalizations, so as to circumvent this~predicament. \par
  We first desire to construct a boundless function that maps an initial configuration 
  onto all reachable terminal configurations (\<open>\<Delta>\<close>-function). One idea about the
  bound elimination could center around modeling the function explicitly as 
  partial (\<open>\<rightharpoonup>\<close>), thereby ensuring termination by mapping all diverging programs onto 
  \<open>None\<close> (i.e. no function value exists). However, such a formalization is not
  feasible, as we would have to explicitly formalize which programs map onto \<open>None\<close>,
  thereby axiomatizing divergence in WL. This is conceptually not possible, thus 
  eliminating this formalization~option. \par
  We instead decide to construct our transitive closure using the predefined 
  \<open>Partial-Function\<close> theory of the HOL-Library. Using this theory, we remove the 
  need for a corresponding termination argument. Instead, it becomes a necessity 
  to prove the monotonicity of the given function, so as to ensure the existence 
  of a fixpoint that the function may converge~against. \par
  The function is provided a step amount \<open>n\<close> and an initial configuration \<open>c\<close>
  as arguments. It then utilizes the previously defined bounded transitive closure
  (\<open>\<Delta>\<^sub>N-function\<close>) to compute all configurations reachable from \<open>c\<close> in \<open>n\<close>-steps.
  If all returned configurations are terminal, then the evaluation has already 
  finished, implying that we reached a fixpoint. Otherwise, we simply recursively
  call the \<open>\<Delta>\<close>-function with a higher step amount. A continuous increase of
  the evaluation steps will eventually converge in a result, as long as the
  program is not divergent. The step incrementation amount is independent of the
  function result, and can hence be arbitrarily chosen. However, too low numbers will 
  cause an overhead during the computation due to the high amount of recursive 
  calls, thus lowering the performance. We have arbitrarily decided for an increase 
  of 100 steps, taking into account that most example programs will not exceed that 
  boundary. Note that this should be adapted, if large case studies were 
  to~be~analyzed. \par
  We formalize our function in a tailrecursive (\<open>tailrec\<close>) fashion, which is directly
  supported by the \<open>Partial-Function\<close> theory. This ensures that we can use
  the predefined theory for the automatic code generation of our construction.
  Note that the compiled code will only terminate iff a corresponding fixpoint
  is reached during the execution. Otherwise, the execution of the generated 
  code will diverge. The necessary mononoticity proof can be automatically 
  conducted by Isabelle due to the simplicity of the~function. \par\<close>
  partial_function (tailrec) composition :: "nat \<Rightarrow> config \<Rightarrow> config set" ("\<Delta>") where
    [code]: "\<Delta> n c = (if \<forall>c \<in> (\<Delta>\<^sub>N n c). snd(c) = \<lambda>[\<nabla>] then \<Delta>\<^sub>N n c else \<Delta> (n + 100) c)"

text \<open>The construction of unbounded global traces generated from program \<open>S\<close> called 
  in \<open>\<sigma>\<close> is now straightforward. We simply return all symbolic traces of the 
  configurations received by calling the \<open>\<Delta>\<close>-function in initial configuration 
  \<open>(\<langle>\<sigma>\<rangle>, \<lambda>[S])\<close>. Note that this requires the \<open>\<Delta>\<close>-function to converge in a 
  corresponding~fixpoint.\<close>
  definition  
    Traces :: "stmt \<Rightarrow> \<Sigma> \<Rightarrow> \<T> set" ("Tr") where
    "Tr S \<sigma> \<equiv> fst ` \<Delta> 0 (\<langle>\<sigma>\<rangle>, \<lambda>[S])"


subsection \<open>Proof Automation\<close>

text \<open>Similar to the simplification lemmas for the \<open>\<delta>\<close>-function, we now choose to 
  introduce simplification lemmas for the \<open>\<Delta>\<^sub>N\<close>-function. This decision will later 
  ensure a major speedup when deriving bounded global traces in our proof~system. \par 
  We first provide simplifications for situations, in which either the provided bound 
  is exceeded, or a terminal configuration is reached. In these cases, the singular 
  returned configuration matches the configuration provided as the argument. The 
  corresponding proofs are~straightforward.\<close>
  lemma \<Delta>\<^sub>N_Bounded:
    shows "\<Delta>\<^sub>N 0 (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, cm) = {(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, cm)}"
    by (metis composition\<^sub>N.simps(1))

  lemma \<Delta>\<^sub>N_End:
    shows "\<Delta>\<^sub>N n (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[\<nabla>]) = {(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[\<nabla>])}"
    by (metis composition\<^sub>N.simps(2) composition\<^sub>N.elims)

text \<open>We next provide generic simplification lemmas for each statement of WL, 
  allowing us to repeatedly compute successor configurations using the \<open>\<Delta>\<^sub>N\<close>-function. 
  All denoted simplifications apply the \<open>\<delta>\<close>-function on the configuration corresponding 
  to the given statement, whilst also decrementing the step amount for the next 
  recursive call. Similar to the \<open>\<delta>\<close>-function, we also provide simplifications for 
  situations in which the guard of an If-Branch or While-Loop evaluates to neither 
  True nor False (i.e. due to symbolic variables). The program evaluation terminates 
  in this case, returning the empty configuration~set.\<close>
  lemma \<Delta>\<^sub>N_Skip:
    shows "\<Delta>\<^sub>N (Suc n) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[SKIP]) = \<Delta>\<^sub>N n (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[\<nabla>])"
    using \<delta>_Skip by simp

  lemma \<Delta>\<^sub>N_Assign:
    shows "\<Delta>\<^sub>N (Suc n) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[x := a]) = \<Delta>\<^sub>N n ((sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<leadsto> State\<llangle>[x \<longmapsto> Exp (val\<^sub>A a \<sigma>)] \<sigma>\<rrangle>, \<lambda>[\<nabla>])"
    using \<delta>_Assign by simp

  lemma \<Delta>\<^sub>N_If\<^sub>T:
    assumes "consistent {(val\<^sub>B b \<sigma>)}"
    shows "\<Delta>\<^sub>N (Suc n) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[IF b THEN S FI]) = \<Delta>\<^sub>N n (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S])"
    using assms \<delta>_If\<^sub>T by simp

  lemma \<Delta>\<^sub>N_If\<^sub>F:
    assumes "consistent {(val\<^sub>B (Not b) \<sigma>)}"
    shows "\<Delta>\<^sub>N (Suc n) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[IF b THEN S FI]) = \<Delta>\<^sub>N n (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[\<nabla>])"
    using assms \<delta>_If\<^sub>F by simp

  lemma \<Delta>\<^sub>N_If\<^sub>E:
    assumes "\<not>consistent {(val\<^sub>B b \<sigma>)} \<and> \<not>consistent {(val\<^sub>B (Not b) \<sigma>)}"
    shows "\<Delta>\<^sub>N (Suc n) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[IF b THEN S FI]) = {}"
  proof -
    have "\<delta> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[IF b THEN S FI]) = {}" using assms \<delta>_If\<^sub>E by simp
    thus ?thesis using composition\<^sub>N.simps by fastforce
  qed

  lemma \<Delta>\<^sub>N_While\<^sub>T:
    assumes "consistent {(val\<^sub>B b \<sigma>)}"
    shows "\<Delta>\<^sub>N (Suc n) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[WHILE b DO S OD]) = \<Delta>\<^sub>N n (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S;;WHILE b DO S OD])"
    using assms \<delta>_While\<^sub>T by simp

  lemma \<Delta>\<^sub>N_While\<^sub>F:
    assumes "consistent {(val\<^sub>B (Not b) \<sigma>)}"
    shows "\<Delta>\<^sub>N (Suc n) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[WHILE b DO S OD]) = \<Delta>\<^sub>N n (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[\<nabla>])"
    using assms \<delta>_While\<^sub>F by simp

  lemma \<Delta>\<^sub>N_While\<^sub>E:
    assumes "\<not>consistent {(val\<^sub>B b \<sigma>)} \<and> \<not>consistent {(val\<^sub>B (Not b) \<sigma>)}"
    shows "\<Delta>\<^sub>N (Suc n) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[WHILE b DO S OD]) = {}"
  proof -
    have "\<delta> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[WHILE b DO S OD]) = {}" using assms \<delta>_While\<^sub>E by simp
    thus ?thesis using composition\<^sub>N.simps by fastforce
  qed

  lemma \<Delta>\<^sub>N_Seq:
    shows "\<Delta>\<^sub>N (Suc n) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1;;S\<^sub>2]) = \<Union>((%c. \<Delta>\<^sub>N n c) ` \<delta> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1;;S\<^sub>2]))"
    by simp

text \<open>Subsequently, we also provide simplification lemmas for the \<open>\<Delta>\<close>-function. The
  first lemma establishes that the result of the \<open>\<Delta>\<close>-function matches the result of 
  the n-bounded \<open>\<Delta>\<^sub>N\<close>-function iff the program always terminates after at most n 
  evaluation steps, implying that the \<open>\<Delta>\<close>-function has reached a corresponding 
  fixpoint at step amount n. The second lemma infers that the \<open>\<Delta>\<close>-function executes 
  a recursive call with an increased step amount iff the fixpoint has not yet 
  been~reached.\<close>
  lemma \<Delta>_fixpoint_reached:
    assumes "\<forall>c \<in> (\<Delta>\<^sub>N n c). snd(c) = \<lambda>[\<nabla>]"
    shows "\<Delta> n c = \<Delta>\<^sub>N n c"
    using composition.simps by (simp add: assms)

  lemma \<Delta>_iteration:
    assumes "\<not>(\<forall>c \<in> (\<Delta>\<^sub>N n c). snd(c) = \<lambda>[\<nabla>])"
    shows "\<Delta> n c = \<Delta> (n + 100) c"
    using composition.simps by (simp add: assms)

text \<open>In order to finalize the automated proof system of the 
  \<open>\<Delta>\<^sub>N\<close>-function/\<open>\<Delta>\<close>-function, we collect all previously proven lemmas in a set, 
  and call it the \<open>\<Delta>\<^sub>N\<close>-system/\<open>\<Delta>\<close>-system. We additionally add the
  \<open>fmupd-reorder-neq\<close> simplification to our \<open>\<Delta>\<^sub>N\<close>-system, thereby ensuring that
  Isabelle can switch the order of updates when proving the equality of
  finite maps. The \<open>eval-nat-numeral\<close> lemma is also included, such that 
  the simplifier can freely swap between the Suc/Zero notation and the numeral 
  representation when inferring results from the \<open>\<Delta>\<^sub>N\<close>-function. This is necessary, 
  as we use the \<open>Suc\<close> constructor in the function definition, but numerals when
  decrementing~the~bound. \par
  Note that we additionally remove the normal function simplifications of the
  \<open>\<Delta>\<^sub>N\<close>-function, such that the Isabelle simplifier will later select our \<open>\<Delta>\<^sub>N\<close>-system
  when deriving global traces. An explicitly defined \<open>partial-function\<close> does not 
  automatically add its simplifications to the simplifier, hence there are no 
  equations to remove for~the~\<open>\<Delta>\<close>-function.\<close>
  lemmas \<Delta>\<^sub>N_system = 
    \<Delta>\<^sub>N_Bounded \<Delta>\<^sub>N_End \<Delta>\<^sub>N_Skip \<Delta>\<^sub>N_Assign \<Delta>\<^sub>N_If\<^sub>T \<Delta>\<^sub>N_If\<^sub>F \<Delta>\<^sub>N_If\<^sub>E 
    \<Delta>\<^sub>N_While\<^sub>T \<Delta>\<^sub>N_While\<^sub>F \<Delta>\<^sub>N_While\<^sub>E \<Delta>\<^sub>N_Seq fmupd_reorder_neq eval_nat_numeral 

  lemmas \<Delta>_system = 
    \<Delta>_fixpoint_reached \<Delta>_iteration

  declare composition\<^sub>N.simps[simp del]

text \<open>We now collect all our proof systems in one set, and name it the 
  WL-derivation-system. Note that we will later be able to use this system for all 
  global trace derivations~in~WL.\<close>
  lemmas WL_derivation_system = 
    \<delta>_system \<Delta>\<^sub>N_system \<Delta>_system


subsection \<open>Trace Derivation Examples\<close>

text \<open>We can now use our automated proof system to derive global traces for several
  example programs. Note that the trace derivation speed directly depends on the length 
  of the corresponding program evaluation. The derivations are already performant enough 
  for small-scale programs. However, longer programs (e.g. for case studies) may need 
  additional optimizations, which we propose as an idea for further extensions 
  of~the~model. \par
  Program \<open>WL_ex\<^sub>1\<close> corresponds to an If-Branch that switches the contents
  of program variables \<open>x\<close> and \<open>y\<close>. The implementation works as follows:
  If the values of \<open>x\<close> and \<open>y\<close> are different, the conditional statement enters the 
  then-case, which switches the contents of both variables by using an intermediate 
  variable \<open>z\<close>. However, if the value of \<open>x\<close> and \<open>y\<close> already match at the start of the 
  program, no switch needs to occur, thus causing the program to 
  immediately~terminate. \par 
  Note that our program evaluation starts in the initial state induced by \<open>WL_ex\<^sub>1\<close>. This
  implies that all occurring variables are initialized with 0, indicating that the 
  contents of \<open>x\<close> and \<open>y\<close> trivially match. Hence, the program terminates after the 
  evaluation of the Boolean guard, thus resulting in exactly one global trace containing 
  only the initial state \<open>(\<sigma>\<^sub>I WL_ex\<^sub>1)\<close>. We infer this conclusion in~Isabelle.\<close>
  lemma "Tr WL_ex\<^sub>1 (\<sigma>\<^sub>I WL_ex\<^sub>1) 
        = {\<langle>fm [(''y'', Exp (Num 0)), (''x'', Exp (Num 0)), (''z'', Exp (Num 0))]\<rangle>}"
    by (simp add: WL_ex\<^sub>1_def Traces_def WL_derivation_system)

text \<open>Program \<open>WL_ex\<^sub>2\<close> computes the factorial of 6 using a While-Loop. The implementation
  works as follows: In the beginning, variable \<open>x\<close> is assigned to 6, whilst variable 
  \<open>y\<close> is assigned to 1. We then utilize a While-Loop in order to faithfully capture
  the intended functionality. In every iteration of the While-Loop, \<open>y\<close> is updated 
  with the result of the multiplication \<open>x\<close>~*~\<open>y\<close>, afterwards decrementing the variable 
  \<open>x\<close>. When the value stored in \<open>x\<close> drops below 2, the Boolean guard of the While-Loop
  evaluates to false, thus terminating the program. \par
  Note that this exact program behaviour can also be observed in the singular global 
  trace inferred below. The value of variable \<open>y\<close> matches 720 in the final state
  of the trace, thereby directly corresponding to the desired evaluation of \<open>6!\<close>. 
  Due to the determinism of the program, only one global trace can exist.\<close>
  lemma "Tr WL_ex\<^sub>2 (\<sigma>\<^sub>I WL_ex\<^sub>2)
        = {(((((((((((\<langle>fm [(''y'', Exp (Num 0)), (''x'', Exp (Num 0))]\<rangle> \<leadsto>
                State \<llangle>fm [(''y'', Exp (Num 0)), (''x'', Exp (Num 6))]\<rrangle>) \<leadsto>
                State \<llangle>fm [(''y'', Exp (Num 1)), (''x'', Exp (Num 6))]\<rrangle>) \<leadsto>
              State \<llangle>fm [(''y'', Exp (Num 6)), (''x'', Exp (Num 6))]\<rrangle>) \<leadsto>
              State \<llangle>fm [(''y'', Exp (Num 6)), (''x'', Exp (Num 5))]\<rrangle>) \<leadsto>
            State \<llangle>fm [(''y'', Exp (Num 30)), (''x'', Exp (Num 5))]\<rrangle>) \<leadsto>
            State \<llangle>fm [(''y'', Exp (Num 30)), (''x'', Exp (Num 4))]\<rrangle>) \<leadsto>
          State \<llangle>fm [(''y'', Exp (Num 120)), (''x'', Exp (Num 4))]\<rrangle>) \<leadsto>
          State \<llangle>fm [(''y'', Exp (Num 120)), (''x'', Exp (Num 3))]\<rrangle>) \<leadsto>
        State \<llangle>fm [(''y'', Exp (Num 360)), (''x'', Exp (Num 3))]\<rrangle>) \<leadsto>
        State \<llangle>fm [(''y'', Exp (Num 360)), (''x'', Exp (Num 2))]\<rrangle>) \<leadsto>
      State \<llangle>fm [(''y'', Exp (Num 720)), (''x'', Exp (Num 2))]\<rrangle>) \<leadsto>
      State \<llangle>fm [(''y'', Exp (Num 720)), (''x'', Exp (Num 1))]\<rrangle>}"
    by (simp add: WL_ex\<^sub>2_def Traces_def WL_derivation_system)


subsection \<open>Code Generation\<close>

text \<open>The idea of making all function definitions executable finally pays off, 
  as we can now generate code for the construction of global system traces by using 
  the \<open>value\<close> keyword. Note that the code execution itself is very performant, 
  and can therefore be used to compute the global traces for any arbitrary program 
  and arbitrary initial state. We additionally propose further work on exports of
  this code to several other programming languages supported by Isabelle (e.g. Haskell, 
  Scala) as an idea for extending the work of this~thesis.\<close>
  value "Tr WL_ex\<^sub>1 (\<sigma>\<^sub>I WL_ex\<^sub>1)"
  value "Tr WL_ex\<^sub>2 (\<sigma>\<^sub>I WL_ex\<^sub>2)"



section \<open>Trace Equivalence\<close>

text \<open>In contrast to the paper, we additionally propose a notion of equivalence 
  between programs. We call two programs \<open>S\<close> and \<open>S'\<close> of \<open>WL\<close> trace equivalent under a 
  given initial state \<open>\<sigma>\<close> iff \<open>S\<close> and \<open>S'\<close> called in \<open>\<sigma>\<close> generate the exact same 
  set of global traces upon termination. This equivalence property can later be 
  utilized to quickly prove that two programs match in their 
  trace~behaviour. \par
  We formalize this equivalence notion using an inductive predicate. The
  inductive formalization in Isabelle is straightforward, as no step case 
  needs to be considered. Note that we introduce the notation \<open>(S \<sim> S') [\<sigma>]\<close> to 
  denote the trace equivalence of \<open>S\<close> and \<open>S'\<close> under initial~state~\<open>\<sigma>\<close>.\<close>
  inductive
    tequivalent :: "stmt \<Rightarrow> stmt \<Rightarrow> \<Sigma> \<Rightarrow> bool" ("_ \<sim> _ [_]" 80) where
    "\<lbrakk> Tr S \<sigma> = Tr S' \<sigma> \<rbrakk> \<Longrightarrow> S \<sim> S' [\<sigma>]"

text \<open>We can now use the \<open>code-pred\<close> keyword in order to automatically
  generate code for the inductive definition above. This will later ensure
  that we can simply output the result of the inductive predicate in the
  console using the \<open>value\<close>~keyword.\<close>
  code_pred tequivalent .

text \<open>We furthermore automatically generate inductive simplifications for our trace
  equivalence notion using the \<open>inductive-simps\<close> keyword, thereby adding them to the 
  Isabelle simplifier equations. This guarantees that we can later resolve the
  trace equivalence notion \<open>\<sim>\<close>, allowing us to conduct proofs inferring trace
  equivalence.\<close>
  inductive_simps tequivalence: "S \<sim> S' [\<sigma>]"

text \<open>Finally, we derive several trace equivalence conclusions in Isabelle using 
  the following practical~examples.\<close>
  lemma "SKIP \<sim> (SKIP;;SKIP) [\<sigma>]"
    using tequivalence by (simp add: Traces_def WL_derivation_system)

  lemma "(IF ((Var ''x'') \<^sub>Req (Num 1)) THEN ''x'' := Num 0 FI) 
                \<sim> (''x'' := Num 0) [[''x'' \<longmapsto> Exp (Num 1)] \<circle>]"
    using tequivalence by (simp add: Traces_def WL_derivation_system)

end    