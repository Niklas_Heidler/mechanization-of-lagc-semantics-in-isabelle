chapter \<open>Supplementary Proofs\<close>
theory LAGC_Base_Supplementary
  imports "../basics/LAGC_Base"
begin

text \<open>Under the assumption that this convention is used, we can now establish that a 
  generated variable does not match with the original~variable~x.\<close>
  lemma vargen\<^sub>N_distinct: "x \<noteq> vargen\<^sub>N n (prefix @ ''$'' @ x @ suffix)"
    by (induct n arbitrary: prefix; auto; metis Cons_eq_appendI)

  lemma vargen_distinct: "x \<noteq> vargen \<sigma> n m (''$'' @ x @ suffix)"
  proof (induct m arbitrary: n)
  \<comment> \<open>We perform an induction over the bound m.\<close>
    case 0
    thus ?case by simp
    \<comment> \<open>If m is 0, the case can be trivially closed, because the prefix 
       \<open>$BOUND_EXCEEDED::\<close> is attached to the variable.\<close>
  next
    case (Suc m)
    thus ?case
    \<comment> \<open>In the induction step we assume that m is not 0.\<close>
    proof (cases)
    \<comment> \<open>We perform a case distinction over the guard of the if-case.\<close>
      assume "vargen\<^sub>N n (''$'' @ x @ suffix) \<notin> fmdom'(\<sigma>)"
      \<comment> \<open>We first assume that the variable is not in the state domain.\<close>
      moreover then have "x \<noteq> vargen\<^sub>N n ('''' @ ''$'' @ x @ suffix)"
        using vargen\<^sub>N_distinct by blast
      \<comment> \<open>Then we know that the generated variable differs due to the 
         previously proven lemma.\<close>
      ultimately show ?case by fastforce
      \<comment> \<open>Thus the case can be closed.\<close>
    next
      assume "\<not>vargen\<^sub>N n (''$'' @ x @ suffix) \<notin> fmdom'(\<sigma>)"
      \<comment> \<open>We next assume that the variable is in the state domain.\<close>
      thus ?case using Suc.hyps by simp
      \<comment> \<open>Then the case follows from the induction hypothesis.\<close>
    qed
  qed

text \<open>We can now also provide the proof that a generated variable is fresh
   as long as the bound of the generator has not been exceeded.\<close>
  lemma vargen_dom: 
  assumes "(\<forall>v. ''$BOUND_EXCEEDED::'' @ v \<notin> fmdom'(\<sigma>))" 
  shows "vargen \<sigma> n m (''$'' @ x @ suffix) \<notin> fmdom'(\<sigma>)"
  proof (induct m arbitrary: n)
  \<comment> \<open>We perform an induction over the bound m.\<close>
    case 0
    thus ?case using assms by simp
    \<comment> \<open>If m is 0, the case can be trivially closed using the assumptions.\<close>
  next
    case (Suc m)
    thus ?case 
    \<comment> \<open>In the induction step we assume m is not 0.\<close>
    proof (cases)
    \<comment> \<open>We perform a case distinction over the guard of the if-case.\<close>
      assume "vargen\<^sub>N n (''$'' @ x @ suffix) \<notin> fmdom'(\<sigma>)"
      thus ?case by simp
      \<comment> \<open>If the guard is true, the case is trivially closable due to the nature
         of the guard.\<close>
    next
      assume "\<not>vargen\<^sub>N n (''$'' @ x @ suffix) \<notin> fmdom'(\<sigma>)"
      thus ?case using Suc.hyps by simp
      \<comment> \<open>If the guard is false, the case follows from the induction hypothesis.\<close>
    qed
  qed


text \<open>We establish that the consistency of a path condition constructed from an 
  evaluated property $b$ can also be inferred by proofing the inconsistency of the path 
  condition constructed from the evaluated negated property $b$. However, this
  additionally requires the given state to be concrete, as well as all variables occurring
  in $b$ to be located in the domain of~the~state.\<close>
  lemma consistent_negate\<^sub>A:
    assumes "\<not>consistent {val\<^sub>B b \<sigma>} \<and> concrete\<^sub>\<Sigma>(\<sigma>) \<and> vars\<^sub>B(b) \<subseteq> fmdom'(\<sigma>)" 
    shows "consistent {(val\<^sub>B (not b) \<sigma>)}"
    using assms consistent_def concrete_imp\<^sub>B by fastforce

  lemma consistent_negate\<^sub>B:
    assumes "\<not>consistent {val\<^sub>B (not b) \<sigma>} \<and> concrete\<^sub>\<Sigma>(\<sigma>) \<and> vars\<^sub>B(b) \<subseteq> fmdom'(\<sigma>)" 
    shows "consistent {(val\<^sub>B b \<sigma>)}"
    using assms consistent_def concrete_imp\<^sub>B by fastforce

text \<open>Provided we know that all non-symbolic variables of arbitrary trace
  \<open>\<tau>' \<cdot> \<tau>''\<close> are disjunct from the symbolic variables of arbitrary trace \<open>\<tau>\<close>,
  we can deduce that all non-symbolic variables of prefix \<open>\<tau>'\<close> must also be
  disjunct from the symbolic variables of \<open>\<tau>\<close>. The proof is straightforward
  due to the construction of the first wellformedness~condition.\<close>
  lemma wf\<^sub>1_weaken\<^sub>A: "(\<tau>' \<cdot> \<tau>'') \<diamondop>\<diamondop>\<^sub>\<T> symb\<^sub>\<T> \<tau> \<longrightarrow> \<tau>' \<diamondop>\<diamondop>\<^sub>\<T> symb\<^sub>\<T> \<tau>"
  proof (induct \<tau>'')
  \<comment> \<open>We perform a structural induction over the suffix \<open>\<tau>''\<close>.\<close>
    case Epsilon
    show ?case by simp
    \<comment> \<open>If \<open>\<tau>''\<close> is \<open>\<epsilon>\<close>, the case holds by trivial means.\<close>
  next
    case (Transition \<tau>''' ta)
    show ?case
    \<comment> \<open>We next assume \<open>\<tau>''\<close> is \<open>(\<tau>''' \<leadsto> ta)\<close>.\<close>
    proof (rule impI)
      assume "(\<tau>' \<cdot> (\<tau>''' \<leadsto> ta)) \<diamondop>\<diamondop>\<^sub>\<T> symb\<^sub>\<T> \<tau>"
      \<comment> \<open>We assume our premise holds, implying that all non-symbolic variables
         of \<open>(\<tau>' \<cdot> (\<tau>''' \<leadsto> ta))\<close> are disjunct from the symbolic variables in \<open>\<tau>\<close>.\<close>
      hence "(\<tau>' \<cdot> \<tau>''') \<diamondop>\<diamondop>\<^sub>\<T> symb\<^sub>\<T> \<tau>"
        using non_symb_disjunct\<^sub>\<T>.elims(1) by auto
      \<comment> \<open>We can then eliminate the trace atom, because the disjointness is preserved.\<close>
      thus "\<tau>' \<diamondop>\<diamondop>\<^sub>\<T> (symb\<^sub>\<T> \<tau>)"
        using Transition.hyps by simp
      \<comment> \<open>The conclusion can then be inferred by making use of the induction hypothesis.\<close>
    qed
  qed

text \<open>Assuming all non-symbolic variables of arbitrary trace
  \<open>\<tau>\<close> are disjunct from the symbolic variables of arbitrary trace \<open>\<tau>' \<cdot> \<tau>''\<close>,
  we can also derive the conclusion that all non-symbolic variables of \<open>\<tau>\<close> must be
  disjunct from the symbolic variables of the prefix \<open>\<tau>'\<close>.\<close>
  lemma wf\<^sub>1_weaken\<^sub>B: "\<tau> \<diamondop>\<diamondop>\<^sub>\<T> symb\<^sub>\<T> (\<tau>' \<cdot> \<tau>'') \<longrightarrow> \<tau> \<diamondop>\<diamondop>\<^sub>\<T> symb\<^sub>\<T> \<tau>'"
  proof (induct \<tau>)
  \<comment> \<open>We perform a structural induction over trace \<open>\<tau>\<close>.\<close>
    case Epsilon
    show ?case by simp
    \<comment> \<open>If \<open>\<tau>\<close> is \<open>\<epsilon>\<close>, then the case is trivial, because \<open>\<epsilon>\<close> contains no
       non-symbolic variables.\<close>
  next
    case (Transition \<tau>''' ta)
    show ?case
    \<comment> \<open>We next assume \<open>\<tau>\<close> is \<open>(\<tau>''' \<leadsto> ta)\<close>.\<close>
    proof (induct ta)
    \<comment> \<open>We perform a case distinction over trace atom \<open>ta\<close>.\<close>
      case (Event ev' e')
      show ?case using Transition.hyps by simp
      \<comment> \<open>If \<open>ta\<close> is an event, then the case is trivially solvable with our induction 
         hypothesis.\<close>
    next
      case (State \<sigma>)
      show ?case
      \<comment> \<open>We next assume \<open>ta\<close> is a state \<open>\<sigma>\<close>.\<close>
      proof (rule impI)
        assume "(\<tau>''' \<leadsto> State\<llangle>\<sigma>\<rrangle>)  \<diamondop>\<diamondop>\<^sub>\<T> symb\<^sub>\<T> (\<tau>' \<cdot> \<tau>'')"
        \<comment> \<open>We will now assume that the premise holds, meaning that all non-symbolic 
           variables of \<open>(\<tau>''' \<leadsto> \<sigma>)\<close> are disjunct from the symbolic variables of
           \<open>\<tau>' \<cdot> \<tau>''\<close>.\<close>
        hence "(fmdom'(\<sigma>) - symb\<^sub>\<Sigma>(\<sigma>)) \<inter> symb\<^sub>\<T> (\<tau>' \<cdot> \<tau>'') = {} \<and> \<tau>''' \<diamondop>\<diamondop>\<^sub>\<T> symb\<^sub>\<T> (\<tau>' \<cdot> \<tau>'')"
          by simp
        \<comment> \<open>Then all non-symbolic variables of \<open>\<sigma>\<close> and \<open>\<tau>'''\<close> are not 
           in the symbolic variables of \<open>\<tau>' \<cdot> \<tau>''\<close>.\<close>
        hence "(fmdom'(\<sigma>) - symb\<^sub>\<Sigma>(\<sigma>)) \<inter> (symb\<^sub>\<T> \<tau>') = {} \<and> \<tau>''' \<diamondop>\<diamondop>\<^sub>\<T> (symb\<^sub>\<T> \<tau>')"
          using symb_union Transition.hyps by auto
        \<comment> \<open>We can then use our induction hypothesis and the previously
           given lemma to conclude that all non-symbolic variables 
           of \<open>\<sigma>\<close> and \<open>\<tau>'''\<close> are disjunct from the symbolic 
           variables~of~prefix~\<open>\<tau>'\<close>.\<close>
        thus "(\<tau>''' \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<diamondop>\<diamondop>\<^sub>\<T> (symb\<^sub>\<T> \<tau>')"
          by simp
        \<comment> \<open>This in turn is exactly what we needed to prove in the first place.\<close>
      qed
    qed
  qed

text \<open>As all event expressions in concrete traces are not allowed to contain
  variables of any kind, it trivially holds that the variables occurring in
  expressions are a subset of any set. Hence the third wellformedness condition
  is trivially satisfied, which can also be inferred using~Isabelle.\<close>
  lemma conc_events_imp\<^sub>\<T>: "concrete\<^sub>\<T>(\<tau>) \<longrightarrow> \<tau> \<subseteq>\<^sub>E M"
  proof (induct \<tau>)
  \<comment> \<open>We perform a structural induction over the trace \<open>\<tau>\<close>.\<close>
    case Epsilon
    thus ?case by simp
    \<comment> \<open>If \<open>\<tau>\<close> is \<open>\<epsilon>\<close>, the case is trivial, as \<open>\<epsilon>\<close> contains no symbolic variables.\<close>
  next
    case (Transition \<tau>' ta)
    show ?case
     \<comment> \<open>We now assume \<open>\<tau>\<close> is \<open>(\<tau>' \<leadsto> ta)\<close>.\<close>
    proof (induct ta)
    \<comment> \<open>We perform a case distinction over the nature of trace atom \<open>ta\<close>.\<close>
      case (Event ev e)
      show ?case
      \<comment> \<open>We first assume \<open>ta\<close> is an event.\<close>
      proof (rule impI)
        assume "concrete\<^sub>\<T>(\<tau>' \<leadsto> Event\<llangle>ev, e\<rrangle>)"
        \<comment> \<open>We assume the premise holds, meaning that \<open>(\<tau>' \<leadsto> Event\<llangle>ev, e\<rrangle>)\<close> is concrete.\<close>
        moreover then have "lconcrete\<^sub>E e" by simp
        \<comment> \<open>Hence expression e must also be of concrete nature.\<close>
        ultimately show "(\<tau>' \<leadsto> Event\<llangle>ev, e\<rrangle>) \<subseteq>\<^sub>E M"
          using Transition.hyps l_concrete_vars_imp\<^sub>E by auto
        \<comment> \<open>As e therefore cannot contain any variables, we can close the case
           with the help of the induction hypothesis.\<close>
      qed
    next
      case (State \<sigma>')
      thus ?case using Transition.hyps by simp
      \<comment> \<open>If \<open>ta\<close> is a state, the case is trivially closable using the induction
         hypothesis.\<close>
    qed
  qed

text \<open>We prove that the \<open>domain can only be extended\<close> when using a concretization
  mapping\<close>

lemma conc_map_dom_ext\<^sub>\<Sigma>: "fmdom'(\<sigma>) \<subseteq> fmdom'(\<rho> \<bullet> \<sigma>)"
proof (subst subset_iff)
  show "\<forall>v. v \<in> fmdom'(\<sigma>) \<longrightarrow> v \<in> fmdom'(\<rho> \<bullet> \<sigma>)"
  \<comment> \<open>We prove the subset relation by showing that all elements in the first
     set are included in the second set\<close>
  proof (rule allI)
    fix v
    \<comment> \<open>We assume variable v is arbitrary, but fixed\<close>
    show "v \<in> fmdom'(\<sigma>) \<longrightarrow> v \<in> fmdom'(\<rho> \<bullet> \<sigma>)"
    proof (rule impI)
      assume dom: "v \<in> fmdom'(\<sigma>)"
      \<comment> \<open>We assume v is included in the first set\<close>
      thus "v \<in> fmdom'(\<rho> \<bullet> \<sigma>)"
      proof (cases)
        \<comment> \<open>We perform a case distinction over the origin of v\<close>
        assume "v \<in> fmdom'(\<rho>)"
        \<comment> \<open>We first assume v originates from \<open>\<rho>\<close>\<close>
        thus "v \<in> fmdom'(\<rho> \<bullet> \<sigma>)" by simp
        \<comment> \<open>Then the case is obvious\<close>
      next
        assume "\<not>v \<in> fmdom'(\<rho>)"
        \<comment> \<open>We now assume v does not originate from \<open>\<rho>\<close>\<close>
        hence "\<exists>e. fmlookup \<sigma> v = Some e"
          using dom by (meson fmlookup_dom'_iff)
        \<comment> \<open>Due to our previous assumption, we map onto an expression via \<open>\<sigma>\<close>\<close>
        hence "\<exists>e. fmlookup (fmmap_keys (\<lambda>v e. val\<^sub>S e \<rho>) \<sigma>) v = Some e"
          by simp
        \<comment> \<open>Then there must also exist an expression we map onto after
           concretizing\<close>
        hence "v \<in> fmdom'(fmmap_keys (\<lambda>v e. val\<^sub>S e \<rho>) \<sigma>)"
          by (meson fmdom'I)
        \<comment> \<open>Thus v is also included in the domain of the updated state\<close>
        thus "v \<in> fmdom'(\<rho> \<bullet> \<sigma>)" 
          by simp
        \<comment> \<open>Thus the thesis is concluded\<close>
      qed
    qed
  qed
qed

text \<open>We prove that \<open>minimal concretization mappings of states can be composited\<close> in 
  order to create minimal trace concretization mappings\<close>

lemma conc_comp: "\<tau> = \<tau>' \<cdot> \<tau>'' \<longrightarrow> min_conc_map\<^sub>\<T> \<tau> n = (min_conc_map\<^sub>\<T> \<tau>' n) ++\<^sub>f (min_conc_map\<^sub>\<T> \<tau>'' n)"
proof (induct \<tau>'' arbitrary: \<tau>)
  \<comment> \<open>We perform an induction over \<open>\<tau>''\<close>\<close>
  case Epsilon
  \<comment> \<open>We first assume \<open>\<tau>''\<close> is \<open>\<epsilon>\<close>\<close>
  show ?case by simp
  \<comment> \<open>Then the property trivially holds\<close>
next
  case (Transition \<tau>''' ta)
  \<comment> \<open>We next assume \<open>\<tau>''\<close> to be \<open>(\<tau>''' \<leadsto> ta)\<close>\<close>
  show ?case
  proof (rule impI)
    assume "\<tau> = \<tau>' \<cdot> (\<tau>''' \<leadsto> ta)"
    \<comment> \<open>We assume the premise holds\<close>
    moreover have "min_conc_map\<^sub>\<T> (\<tau>' \<cdot> \<tau>''') n = (min_conc_map\<^sub>\<T> \<tau>' n) ++\<^sub>f (min_conc_map\<^sub>\<T> \<tau>''' n)"
      using Transition.hyps by simp
    \<comment> \<open>We know our induction hypothesis holds for \<open>\<tau>' \<cdot> \<tau>'''\<close>\<close>
    ultimately show "min_conc_map\<^sub>\<T> \<tau> n = (min_conc_map\<^sub>\<T> \<tau>' n) ++\<^sub>f (min_conc_map\<^sub>\<T> (\<tau>''' \<leadsto> ta) n)"
      by (induct ta; simp; metis fmadd_assoc)
    \<comment> \<open>With the help of conc ta comp we can then conclude that the thesis holds\<close>
  qed
qed

text \<open>We prove the \<open>concreteness of minimal concretization mappings\<close>\<close>

lemma conc_min_conc_map\<^sub>\<Sigma>: "concrete\<^sub>\<Sigma>(min_conc_map\<^sub>\<Sigma> \<sigma> n)"
  using fmdom'_notI by (force simp add: concrete\<^sub>\<Sigma>_def)

lemma conc_min_conc_map\<^sub>\<T>: "concrete\<^sub>\<Sigma>(min_conc_map\<^sub>\<T> \<tau> n)"
proof (induct \<tau>)
  \<comment> \<open>We perform an induction over \<open>\<tau>\<close>\<close>
  case Epsilon
  \<comment> \<open>We first assume \<open>\<tau>\<close> to be \<open>\<epsilon>\<close>\<close>
  show ?case 
    using concrete\<^sub>\<Sigma>_def by simp
  \<comment> \<open>Then the case holds trivially\<close>
next
  case (Transition \<tau>' ta)
  \<comment> \<open>We next assume \<open>\<tau>\<close> to be \<open>(\<tau>' \<leadsto> ta)\<close>\<close>
  thus ?case
  proof (induct ta)
  \<comment> \<open>We perform an induction over all trace atoms \<open>ta\<close>\<close>
    case (Event ev e)
    \<comment> \<open>We assume \<open>ta\<close> to be an event\<close>
    thus ?case by simp
    \<comment> \<open>Then the thesis holds trivially\<close>
  next
    case (State \<sigma>)
    \<comment> \<open>We assume \<open>ta\<close> to be a state\<close>
    have "concrete\<^sub>\<Sigma>(min_conc_map\<^sub>\<T> \<tau>' n)"
      using Transition.hyps by simp
    \<comment> \<open>Due to our induction hypothesis, the minimal concretization
       mapping of \<open>\<tau>'\<close> is concrete\<close>
    moreover have "concrete\<^sub>\<Sigma>(min_conc_map\<^sub>\<Sigma> \<sigma> n)"
      using conc_min_conc_map\<^sub>\<Sigma> by simp
    \<comment> \<open>Due to our previous proof, the minimal concretization mapping
       of \<open>\<sigma>\<close> must also be concrete\<close>
    ultimately have "concrete\<^sub>\<Sigma>(min_conc_map\<^sub>\<T> \<tau>' n ++\<^sub>f min_conc_map\<^sub>\<Sigma> \<sigma> n)"
      using concrete\<^sub>\<Sigma>_def by force
    \<comment> \<open>Hence the composited concretization mapping must is also concrete\<close>
    thus ?case by simp
    \<comment> \<open>Thus the case holds\<close>
  qed
qed

text \<open>We prove that \<open>minimal concretization mappings for states can be 
  combined\<close> with each other\<close>

lemma conc_map_combine\<^sub>\<Sigma>: "is_conc_map\<^sub>\<Sigma> \<rho> \<sigma> \<and> is_conc_map\<^sub>\<Sigma> \<rho>' \<sigma> \<longrightarrow> is_conc_map\<^sub>\<Sigma> (\<rho> ++\<^sub>f \<rho>') \<sigma>"
proof (rule impI)
  assume premise: "is_conc_map\<^sub>\<Sigma> \<rho> \<sigma> \<and> is_conc_map\<^sub>\<Sigma> \<rho>' \<sigma>"
  \<comment> \<open>We assume \<open>\<rho>\<close> and \<open>\<rho>'\<close> are valid concretization mappings for \<open>\<sigma>\<close>\<close>
  hence "concrete\<^sub>\<Sigma> \<rho> \<and> concrete\<^sub>\<Sigma> \<rho>'"
    using is_conc_map\<^sub>\<Sigma>_def by simp
  \<comment> \<open>Then both must be concrete\<close>
  hence "concrete\<^sub>\<Sigma> (\<rho> ++\<^sub>f \<rho>')"
    using concrete\<^sub>\<Sigma>_def by force
  \<comment> \<open>Then the combination of both must also be concrete\<close>
  moreover have "(fmdom'(\<rho>) \<union> fmdom'(\<rho>')) \<inter> fmdom'(\<sigma>) = symb\<^sub>\<Sigma>(\<sigma>)" 
    using premise is_conc_map\<^sub>\<Sigma>_def by auto
  \<comment> \<open>Due to our premise, \<open>\<rho>\<close> and \<open>\<rho>'\<close> are not allowed to modify non-symbolic
     variables of \<open>\<sigma>\<close>\<close>
  moreover then have "fmdom'(\<rho> ++\<^sub>f \<rho>') \<inter> fmdom'(\<sigma>) = symb\<^sub>\<Sigma>(\<sigma>)"
    by simp
  \<comment> \<open>Then the combination of both also cannot modify non-symbolic variables of \<open>\<sigma>\<close>\<close>
  ultimately show "is_conc_map\<^sub>\<Sigma> (\<rho> ++\<^sub>f \<rho>') \<sigma>"
    by (simp add: is_conc_map\<^sub>\<Sigma>_def)
  \<comment> \<open>As both conditions have been, proven, the thesis holds\<close>
qed

text \<open>We prove that the \<open>minimal state concretization mapping\<close> is actually a 
  \<open>valid concretization mapping\<close> for the given state\<close>

lemma min_conc_map\<^sub>\<Sigma>: "is_conc_map\<^sub>\<Sigma> (min_conc_map\<^sub>\<Sigma> \<sigma> n) \<sigma>"
proof -
  have "fmdom'(min_conc_map\<^sub>\<Sigma> \<sigma> n) \<inter> fmdom'(\<sigma>) = symb\<^sub>\<Sigma>(\<sigma>) \<and> concrete\<^sub>\<Sigma>(min_conc_map\<^sub>\<Sigma> \<sigma> n)"
  \<comment> \<open>We prove both conjunction expressions\<close>
  proof (rule conjI)
    show "concrete\<^sub>\<Sigma>(min_conc_map\<^sub>\<Sigma> \<sigma> n)"
      using conc_min_conc_map\<^sub>\<Sigma> by simp
    \<comment> \<open>The minimal concretization mapping is concrete due to its construction\<close>
  next
    show "fmdom'(min_conc_map\<^sub>\<Sigma> \<sigma> n) \<inter> fmdom'(\<sigma>) = symb\<^sub>\<Sigma>(\<sigma>)"
      apply (auto simp add: symb\<^sub>\<Sigma>_def)
      apply (metis (mono_tags, lifting) fmdom'_notI fmfilter_fmmap_keys fmlookup_filter)
      using fmlookup_dom'_iff by fastforce
    \<comment> \<open>The minimal concretization mapping also does not modify non-symbolic
       variables of \<open>\<sigma>\<close> due to its construction\<close>
  qed
  thus ?thesis by (simp add: is_conc_map\<^sub>\<Sigma>_def)
  \<comment> \<open>Thus we can conclude that it is a valid concretization mapping\<close>
qed

text \<open>We prove: If \<open>\<rho>\<close> and \<open>\<rho>'\<close> are concrete and no non-symbolic variable of \<open>\<sigma>\<close>
  occurs anywhere in \<open>\<rho>\<close> or \<open>\<rho>'\<close>, then \<open>inserting a minimal concretization\<close> between
  \<open>\<rho>\<close> and \<open>\<rho>'\<close> will lead to a valid concretization mapping for \<open>\<sigma>\<close>\<close>

lemma conc_map_insert\<^sub>\<T>:
  assumes "concrete\<^sub>\<Sigma>(\<rho>) \<and> concrete\<^sub>\<Sigma>(\<rho>') \<and> (fmdom'(\<sigma>) - symb\<^sub>\<Sigma>(\<sigma>)) \<inter> (fmdom'(\<rho>) \<union> fmdom'(\<rho>')) = {}"
  shows "is_conc_map\<^sub>\<Sigma> ((\<rho> ++\<^sub>f (min_conc_map\<^sub>\<Sigma> \<sigma> n)) ++\<^sub>f \<rho>') \<sigma>"
proof -
  have "fmdom'((\<rho> ++\<^sub>f (min_conc_map\<^sub>\<Sigma> \<sigma> n)) ++\<^sub>f \<rho>') \<inter> fmdom'(\<sigma>) = symb\<^sub>\<Sigma>(\<sigma>) \<and> concrete\<^sub>\<Sigma>((\<rho> ++\<^sub>f (min_conc_map\<^sub>\<Sigma> \<sigma> n)) ++\<^sub>f \<rho>')"
  \<comment> \<open>We prove the two properties of a state concretization mapping\<close>
  proof (rule conjI)
    have "concrete\<^sub>\<Sigma>(\<rho>) \<and> concrete\<^sub>\<Sigma>(\<rho>')"
      using assms by simp
    \<comment> \<open>Due to our assumption, \<open>\<rho>\<close> and \<open>\<rho>'\<close> must be concrete\<close>
    moreover have "concrete\<^sub>\<Sigma> (min_conc_map\<^sub>\<Sigma> \<sigma> n)"
      using conc_min_conc_map\<^sub>\<Sigma> by simp
    \<comment> \<open>We also know that our minimal concretization mapping for \<open>\<sigma>\<close> must be concrete\<close>
    ultimately show "concrete\<^sub>\<Sigma>((\<rho> ++\<^sub>f (min_conc_map\<^sub>\<Sigma> \<sigma> n)) ++\<^sub>f \<rho>')"
      by (fastforce simp add: concrete\<^sub>\<Sigma>_def)
    \<comment> \<open>Hence the combination of those concretization mappings must also be concrete\<close>
  next
    have "(fmdom'(\<sigma>) - symb\<^sub>\<Sigma>(\<sigma>)) \<inter> (fmdom'(\<rho>) \<union> fmdom'(\<rho>')) = {}"
      using assms by simp
    \<comment> \<open>We know that no non-symbolic variable of \<open>\<sigma>\<close> occurs anywhere in \<open>\<rho>\<close> and \<open>\<rho>'\<close>\<close>
    moreover have "fmdom'(min_conc_map\<^sub>\<Sigma> \<sigma> n) \<inter> fmdom'(\<sigma>) = symb\<^sub>\<Sigma>(\<sigma>)"
      using is_conc_map\<^sub>\<Sigma>_def min_conc_map\<^sub>\<Sigma> by blast
    \<comment> \<open>We also know that the minimal concretization mapping of \<open>\<sigma>\<close> also refrains
       from mapping non-symbolic variables of \<open>\<sigma>\<close>\<close>
    ultimately show "fmdom'((\<rho> ++\<^sub>f (min_conc_map\<^sub>\<Sigma> \<sigma> n)) ++\<^sub>f \<rho>') \<inter> fmdom'(\<sigma>) = symb\<^sub>\<Sigma>(\<sigma>)"
      using Int_Un_distrib by auto
    \<comment> \<open>Hence the combination of the concretization mappings also only specifies
       symbolic variables or unmapped variables\<close>
  qed
  thus ?thesis by (simp add: is_conc_map\<^sub>\<Sigma>_def)
  \<comment> \<open>Because both properties have been proven, the thesis holds\<close>
qed

text \<open>We prove that the \<open>minimal concretization mapping of a trace\<close> is a
  \<open>concretization mapping for any state of the trace\<close>\<close>

lemma conc_intern: 
  assumes "\<tau> \<diamondop>\<diamondop>\<^sub>\<T> symb\<^sub>\<T> \<tau> \<and> \<tau> = (\<tau>' \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<cdot> \<tau>''"
  shows "is_conc_map\<^sub>\<Sigma> (min_conc_map\<^sub>\<T> \<tau> n) \<sigma>"
proof -
  have "\<exists>\<rho> \<rho>'. min_conc_map\<^sub>\<T> \<tau> n = ((min_conc_map\<^sub>\<T> \<tau>' n) ++\<^sub>f (min_conc_map\<^sub>\<Sigma> \<sigma> n)) ++\<^sub>f (min_conc_map\<^sub>\<T> \<tau>'' n)"
    using assms conc_comp by simp
  \<comment> \<open>Due to the concretization composition, we can simply combine the minimal
     concretization mappings of the internal traces and states to reach a global
     minimal concretization mapping\<close>
  moreover have "concrete\<^sub>\<Sigma>(min_conc_map\<^sub>\<T> \<tau>' n) \<and> concrete\<^sub>\<Sigma>(min_conc_map\<^sub>\<T> \<tau>'' n)"
    using conc_min_conc_map\<^sub>\<T> by simp
  \<comment> \<open>We know the minimal concretization mappings for the internal traces must
     be concrete\<close>
  moreover have "(fmdom'(\<sigma>) - symb\<^sub>\<Sigma>(\<sigma>)) \<inter> (fmdom'(min_conc_map\<^sub>\<T> \<tau>' n) \<union> fmdom'(min_conc_map\<^sub>\<T> \<tau>'' n)) = {}"
  \<comment> \<open>We prove that the minimal concretization mappings of the internal traces
     do not map non-symbolic variables of \<open>\<sigma>\<close>\<close>
  proof -
    have "(fmdom'(\<sigma>) - symb\<^sub>\<Sigma>(\<sigma>)) \<inter> symb\<^sub>\<T> \<tau> = {}"
      using assms non_symb_disjunct\<^sub>\<T>.simps(3) wf\<^sub>1_weaken\<^sub>A by blast
    \<comment> \<open>Because \<open>\<tau>\<close> is wellformed, no non-symbolic variable of \<open>\<sigma>\<close> occurs in the
       symbolic variables of \<open>\<tau>\<close>\<close>
    moreover have "fmdom'(min_conc_map\<^sub>\<T> \<tau>' n) = symb\<^sub>\<T> \<tau>' \<and> fmdom'(min_conc_map\<^sub>\<T> \<tau>'' n) = symb\<^sub>\<T> \<tau>''"
      using only_symb_conc\<^sub>\<T> by simp
    \<comment> \<open>We also know that the minimal concretization mappings of the internal
       traces only specify their own symbolic variables\<close>
    moreover then have "fmdom'(min_conc_map\<^sub>\<T> \<tau>' n) \<subseteq> symb\<^sub>\<T> \<tau> \<and> fmdom'(min_conc_map\<^sub>\<T> \<tau>'' n) \<subseteq> symb\<^sub>\<T> \<tau>"
      using assms symb_union by (metis Un_iff subsetI symb\<^sub>\<T>.simps(3)) 
    \<comment> \<open>Hence we can conclude that the minimal concretization mappings only
       model symbolic variables of the global trace\<close>
    ultimately show ?thesis by auto
    \<comment> \<open>Thus the property holds\<close>
  qed
  ultimately show ?thesis 
    using conc_map_insert\<^sub>\<T> by auto
  \<comment> \<open>We can then use the insert theorem to prove that the minimal concretization
     mapping for the trace is a concretization mapping for the state\<close>
qed

lemma conc_intern_weaken: 
  assumes "\<tau> \<diamondop>\<diamondop>\<^sub>\<T> symb\<^sub>\<T> \<tau> \<and> \<tau> = (\<tau>' \<leadsto> State\<llangle>\<sigma>\<rrangle>)"
  shows "is_conc_map\<^sub>\<Sigma> (min_conc_map\<^sub>\<T> \<tau> n) \<sigma>"
proof -
  have "\<tau> = \<tau> \<cdot> \<epsilon>" by simp
  \<comment> \<open>We know that a trace is equivalent to a trace concatenated with \<open>\<epsilon>\<close>\<close>
  thus ?thesis using assms conc_intern by blast
  \<comment> \<open>Thus a weakened version of the property also holds\<close>
qed

text \<open>We prove that we can \<open>extend minimal concretization mappings\<close> with 
  another state while \<open>preserving the concretization mapping property\<close>\<close>

lemma min_conc_extend\<^sub>\<T>:
  "\<tau>' \<diamondop>\<diamondop>\<^sub>\<T> symb\<^sub>\<Sigma> \<sigma> \<and> is_conc_map\<^sub>\<T> (min_conc_map\<^sub>\<T> \<tau> n) \<tau>' \<longrightarrow> is_conc_map\<^sub>\<T> (min_conc_map\<^sub>\<T> (\<tau> \<leadsto> State\<llangle>\<sigma>\<rrangle>) n) \<tau>'"
proof (induct \<tau>')
  \<comment> \<open>We perform an induction over \<open>\<tau>'\<close>\<close>
  case Epsilon
  \<comment> \<open>We first assume \<open>\<tau>'\<close> to be \<open>\<epsilon>\<close>\<close>
  show ?case by (meson conc_min_conc_map\<^sub>\<T> is_conc_map\<^sub>\<T>.simps(1))
  \<comment> \<open>Then the case holds trivially\<close>
next
  case (Transition \<tau>'' ta)
  \<comment> \<open>We next assume \<open>\<tau>'\<close> to be \<open>(\<tau>'' \<leadsto> ta)\<close>\<close>
  show ?case
  proof (induct ta)
  \<comment> \<open>We perform an induction over \<open>ta\<close>\<close>
    case (Event ev e)
    \<comment> \<open>We first assume \<open>ta\<close> to be an event\<close>
    show ?case using Transition.hyps by simp
    \<comment> \<open>Then the case holds with the induction hypothesis\<close>
  next
    case (State \<sigma>')
    \<comment> \<open>We now assume \<open>ta\<close> to be a state\<close>
    show ?case
    proof (rule impI)
      assume premise: "(\<tau>'' \<leadsto> State\<llangle>\<sigma>'\<rrangle>) \<diamondop>\<diamondop>\<^sub>\<T> symb\<^sub>\<Sigma> \<sigma> \<and> is_conc_map\<^sub>\<T> (min_conc_map\<^sub>\<T> \<tau> n) (\<tau>'' \<leadsto> State\<llangle>\<sigma>'\<rrangle>)"
      \<comment> \<open>We assume \<open>(\<tau>'' \<leadsto> \<sigma>')\<close> is wellformed regarding \<open>\<sigma>\<close> and that the minimal 
         concretization mapping of \<open>\<tau>\<close> is a concretization mapping for \<open>(\<tau>'' \<leadsto> \<sigma>')\<close>\<close>
      hence "symb\<^sub>\<T>(\<tau> \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<inter> fmdom'(\<sigma>') = symb\<^sub>\<Sigma>(\<sigma>')"
        using inf_commute is_conc_map\<^sub>\<Sigma>_def only_symb_conc\<^sub>\<T> by auto
      \<comment> \<open>Due to the wellformedness condition the symbolic variables of 
         \<open>(\<tau> \<leadsto> \<sigma>)\<close> may not consist of non-symbolic variables of \<open>\<sigma>'\<close>\<close>
      moreover then have "fmdom'(min_conc_map\<^sub>\<T> (\<tau> \<leadsto> State\<llangle>\<sigma>\<rrangle>) n) = symb\<^sub>\<T>(\<tau> \<leadsto> State\<llangle>\<sigma>\<rrangle>)"
        using only_symb_conc\<^sub>\<T> by blast
      \<comment> \<open>Hence the domain of the minimal concretization mapping does not
         modify non-symbolic variables of \<open>\<sigma>\<close>\<close>
      moreover then have "concrete\<^sub>\<Sigma> (min_conc_map\<^sub>\<T> (\<tau> \<leadsto> State\<llangle>\<sigma>\<rrangle>) n)"
        using conc_min_conc_map\<^sub>\<T> by meson
      \<comment> \<open>We additionally know that the minimal concretization mapping is concrete\<close>
      ultimately have "is_conc_map\<^sub>\<Sigma> (min_conc_map\<^sub>\<T> (\<tau> \<leadsto> State\<llangle>\<sigma>\<rrangle>) n) \<sigma>'"
        by (simp add: is_conc_map\<^sub>\<Sigma>_def)
      \<comment> \<open>Hence we can conclude that the minimal concretization mapping of
         \<open>(\<tau> \<leadsto> \<sigma>)\<close> is a concretization mapping for \<open>\<sigma>'\<close>\<close>
      moreover have "is_conc_map\<^sub>\<T> (min_conc_map\<^sub>\<T> (\<tau> \<leadsto> State\<llangle>\<sigma>\<rrangle>) n) \<tau>''"
        using premise Transition.hyps by simp
      \<comment> \<open>We use the induction hypothesis to show that the minimal concretization
         mapping of \<open>(\<tau> \<leadsto> \<sigma>)\<close> is also a concretization mapping for \<open>\<tau>''\<close>\<close>
      ultimately show "is_conc_map\<^sub>\<T> (min_conc_map\<^sub>\<T> (\<tau> \<leadsto> State\<llangle>\<sigma>\<rrangle>) n) (\<tau>'' \<leadsto> State\<llangle>\<sigma>'\<rrangle>)"
        by simp
      \<comment> \<open>Thus it is also a concretization mapping for \<open>(\<tau>'' \<leadsto> \<sigma>')\<close>\<close>
    qed
  qed
qed

text \<open>We prove that the \<open>minimal trace concretization mapping\<close> is actually a \<open>valid
  concretization mapping\<close> for the given trace\<close>

lemma min_conc_map\<^sub>\<T>: "\<tau> \<diamondop>\<diamondop>\<^sub>\<T> symb\<^sub>\<T> \<tau> \<longrightarrow> is_conc_map\<^sub>\<T> (min_conc_map\<^sub>\<T> \<tau> n) \<tau>"
proof (induct \<tau>)
  \<comment> \<open>We perform an induction over \<open>\<tau>\<close>\<close>
  case Epsilon
  \<comment> \<open>We first assume \<open>\<tau>\<close> to be \<open>\<epsilon>\<close>\<close>
  show ?case by (simp add: concrete\<^sub>\<Sigma>_def)
  \<comment> \<open>Then the case holds trivially\<close>
next
  case (Transition \<tau>' ta)
  \<comment> \<open>We next assume \<open>\<tau>\<close> to be \<open>(\<tau>' \<leadsto> ta)\<close>\<close>
  show ?case
  proof (induct ta)
  \<comment> \<open>We perform an induction over \<open>ta\<close>\<close>
    case (Event ev e)
    \<comment> \<open>We first assume \<open>ta\<close> to be an event\<close>
    thus ?case using Transition.hyps by simp
    \<comment> \<open>Then the property holds using the induction hypothesis\<close>
  next
    case (State \<sigma>)
    \<comment> \<open>We next assume \<open>ta\<close> to be a state\<close>
    thus ?case
    proof (rule impI)
      assume premise: "(\<tau>' \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<diamondop>\<diamondop>\<^sub>\<T> symb\<^sub>\<T> (\<tau>' \<leadsto> State\<llangle>\<sigma>\<rrangle>)"
      \<comment> \<open>We assume \<open>(\<tau>' \<leadsto> \<sigma>)\<close> is wellformed regarding \<open>(\<tau>' \<leadsto> \<sigma>)\<close>\<close>
      hence red: "\<tau>' \<diamondop>\<diamondop>\<^sub>\<T> symb\<^sub>\<T> (\<tau>' \<leadsto> State\<llangle>\<sigma>\<rrangle>)"
        using wf\<^sub>1_weaken\<^sub>A by simp
      \<comment> \<open>We weaken the wellformedness condition of the premise\<close>
      hence "\<tau>' \<diamondop>\<diamondop>\<^sub>\<T> (symb\<^sub>\<T> (\<tau>' \<cdot> \<langle>\<sigma>\<rangle>))"
        by simp
      \<comment> \<open>We can then rewrite the trace transition with a concatenation\<close>
      hence "\<tau>' \<diamondop>\<diamondop>\<^sub>\<T> (symb\<^sub>\<T> \<tau>')"
        using wf\<^sub>1_weaken\<^sub>B by blast
      \<comment> \<open>We weaken the wellformedness condition even more\<close>
      hence "is_conc_map\<^sub>\<T> (min_conc_map\<^sub>\<T> \<tau>' n) \<tau>'"
        using Transition.hyps by simp
      \<comment> \<open>Hence we can use our induction hypothesis to conclude that the
         minimal concretization mapping of \<open>\<tau>'\<close> is a concretization mapping
         for \<open>\<tau>'\<close>\<close>
      moreover have "\<tau>' \<diamondop>\<diamondop>\<^sub>\<T> (symb\<^sub>\<Sigma> \<sigma>)"
        using red symb_union wf\<^sub>1_weaken\<^sub>B by (metis Un_empty_right symb\<^sub>\<T>.simps(1) symb\<^sub>\<T>.simps(3))
      \<comment> \<open>We can also conclude that no non-symbolic variable of \<open>\<tau>'\<close> occurs
         symbolically in \<open>\<sigma>\<close>\<close>
      ultimately have "is_conc_map\<^sub>\<T> (min_conc_map\<^sub>\<T> (\<tau>' \<leadsto> State\<llangle>\<sigma>\<rrangle>) n) \<tau>'"
        using min_conc_extend\<^sub>\<T> by simp
      \<comment> \<open>Using the conc extend property, the minimal concretization mapping
         of \<open>(\<tau>' \<leadsto> \<sigma>)\<close> must therefore also be a concretization mapping for \<open>\<tau>'\<close>\<close>
      moreover have "is_conc_map\<^sub>\<Sigma> (min_conc_map\<^sub>\<T> (\<tau>' \<leadsto> State\<llangle>\<sigma>\<rrangle>) n) \<sigma>"
        using premise conc_intern_weaken by simp
      \<comment> \<open>Due to our wellformedness and the weakened conc intern property,
         \<open>(\<tau>' \<leadsto> \<sigma>)\<close> must also be a valid concretization mapping for \<open>\<sigma>\<close>\<close>
      ultimately show "is_conc_map\<^sub>\<T> (min_conc_map\<^sub>\<T> (\<tau>' \<leadsto> State\<llangle>\<sigma>\<rrangle>) n) (\<tau>' \<leadsto> State\<llangle>\<sigma>\<rrangle>)"
        by simp
      \<comment> \<open>As both properties hold, the minimal concretization mapping of \<open>(\<tau>' \<leadsto> \<sigma>)\<close>
         must also be a concretization mapping for \<open>(\<tau>' \<leadsto> \<sigma>)\<close>, which concludes the
         induction step\<close>
    qed
  qed
qed

text \<open>We prove that by \<open>concretizing a well-formed state\<close>, the \<open>resulting
  state will be of concrete nature\<close> [\<open>Proposition 2.25\<close>]\<close>

lemma wf_conc_imp\<^sub>\<Sigma>: 
  assumes "wf\<^sub>\<Sigma>(\<sigma>) \<and> is_conc_map\<^sub>\<Sigma> \<rho> \<sigma>" 
  shows "concrete\<^sub>\<Sigma>(\<rho> \<bullet> \<sigma>)" 
proof -
  have "\<forall>v. v \<in> fmdom'(\<rho> \<bullet> \<sigma>) \<longrightarrow> concrete\<^sub>S(the(fmlookup (\<rho> \<bullet> \<sigma>) v))"
  proof (rule allI)
    fix v
    \<comment> \<open>We choose a variable out of the domain of \<open>\<rho>\<bullet>\<sigma>\<close> arbitrary, but fixed\<close>
    show "v \<in> fmdom'(\<rho> \<bullet> \<sigma>) \<longrightarrow> concrete\<^sub>S(the(fmlookup (\<rho> \<bullet> \<sigma>) v))"
    proof (rule impI)
      assume main_premise: "v \<in> fmdom'(\<rho> \<bullet> \<sigma>)"
      show "concrete\<^sub>S(the(fmlookup (\<rho> \<bullet> \<sigma>) v))"
      \<comment> \<open>We still have to prove that the mapped expression of v in \<open>\<rho>\<bullet>\<sigma>\<close> is concrete\<close>
      proof (cases)
        \<comment> \<open>We perform a case distinction over the fact if v is in the domain of \<open>\<sigma>\<close>\<close>
        assume premise: "v \<in> fmdom'(\<sigma>)"
        \<comment> \<open>We first assume v to be in the domain of \<open>\<sigma>\<close>\<close>
        moreover have "{v. \<exists>v' \<in> fmdom'(\<sigma>). v \<in> vars\<^sub>S(the(fmlookup \<sigma> v'))} \<subseteq> symb\<^sub>\<Sigma>(\<sigma>)"
          using assms by (simp add: wf\<^sub>\<Sigma>_def)
        \<comment> \<open>We know that all variables occurring in mapped expressions of \<open>\<sigma>\<close> are symbolic\<close>
        ultimately have "vars\<^sub>S(the(fmlookup \<sigma> v)) \<subseteq> symb\<^sub>\<Sigma>(\<sigma>)"
          by blast
        \<comment> \<open>Hence all variables occurring in the mapped expression of v must also be symbolic\<close>
        moreover have "symb\<^sub>\<Sigma>(\<sigma>) \<subseteq> fmdom'(\<rho>)"
          using assms by (auto simp add: is_conc_map\<^sub>\<Sigma>_def)
        \<comment> \<open>We also know that every symbolic variable must be in the domain of \<open>\<rho>\<close>\<close>
        ultimately have "vars\<^sub>S(the(fmlookup \<sigma> v)) \<subseteq> fmdom'(\<rho>)"
          by simp
        \<comment> \<open>Therefore all variables occurring in the mapped expression of v is also in
           the domain of the concretization mapping \<open>\<rho>\<close>\<close>
        moreover have "concrete\<^sub>\<Sigma>(\<rho>)"
          using assms by (simp add: is_conc_map\<^sub>\<Sigma>_def)
        \<comment> \<open>Due to our assumption, we know that \<open>\<rho>\<close> is concrete\<close>
        ultimately have "the(fmlookup \<sigma> v) = \<^emph> \<or> concrete\<^sub>S (val\<^sub>S (the(fmlookup \<sigma> v)) \<rho>)"
          using concrete_imp\<^sub>S by simp
        \<comment> \<open>Hence we can conclude that the mapped expression of v is either symbolic
           in \<open>\<sigma>\<close> or its expression is concrete after the concretization\<close>
        thus "concrete\<^sub>S(the(fmlookup (\<rho> \<bullet> \<sigma>) v))"
        proof (rule disjE)
          assume "the (fmlookup \<sigma> v) = \<^emph>"
          \<comment> \<open>We first assume that v is symbolic in \<open>\<sigma>\<close>\<close>
          hence "v \<in> symb\<^sub>\<Sigma>(\<sigma>)"
            using premise by (simp add: symb\<^sub>\<Sigma>_def; metis fmdom'_notI option.collapse)
          \<comment> \<open>Hence it must be in the symbolic variables of \<open>\<sigma>\<close>\<close>
          moreover have "fmdom'(\<rho>) \<inter> fmdom'(\<sigma>) = symb\<^sub>\<Sigma>(\<sigma>)"
            using assms by (simp add: is_conc_map\<^sub>\<Sigma>_def)
          \<comment> \<open>According to our premises the concretization mapping must concretize
             all symbolic variables of \<open>\<sigma>\<close>\<close>
          ultimately have "v \<in> fmdom'(\<rho>)"
            by auto
          \<comment> \<open>Hence v must be in the domain of \<open>\<rho>\<close>\<close>
          moreover have "concrete\<^sub>\<Sigma>(\<rho>)"
            using assms by (simp add: is_conc_map\<^sub>\<Sigma>_def)
          \<comment> \<open>We also know that \<open>\<rho>\<close> is concrete\<close>
          ultimately have "concrete\<^sub>S (the(fmlookup \<rho> v)) \<and> v |\<in>| fmdom(\<rho>)"
            by (simp add: concrete\<^sub>\<Sigma>_def fmlookup_dom'_iff fmlookup_dom_iff)
          \<comment> \<open>We therefore know that the mapped expression of v in \<open>\<rho>\<close> is concrete\<close>
          thus "concrete\<^sub>S(the(fmlookup (\<rho> \<bullet> \<sigma>) v))" 
            by simp
          \<comment> \<open>Thus the mapped expression of v in \<open>\<rho>\<bullet>\<sigma>\<close> must also be concrete\<close> 
        next
          assume assm: "concrete\<^sub>S (val\<^sub>S (the(fmlookup \<sigma> v)) \<rho>)"
          \<comment> \<open>Secondly, we assume that the simplified expression is concrete\<close>
          moreover then have "\<not>v \<in> symb\<^sub>\<Sigma>(\<sigma>)"
            by (force simp add: symb\<^sub>\<Sigma>_def)
          \<comment> \<open>Hence v cannot be a symbolic variable in \<open>\<sigma>\<close>\<close>
          moreover have "fmdom'(\<rho>) \<inter> fmdom'(\<sigma>) = symb\<^sub>\<Sigma>(\<sigma>)"
            using assms by (simp add: is_conc_map\<^sub>\<Sigma>_def) 
          \<comment> \<open>We know that the concretization mapping concretizes all symbolic
             variables of \<open>\<sigma>\<close> and no non-symbolic variables\<close>
          ultimately have "\<not>v |\<in>| fmdom(\<rho>)"
            using premise by (metis IntI fmdom'_alt_def fmember.rep_eq)
          \<comment> \<open>Thus v cannot be in the domain of \<open>\<rho>\<close>\<close>
          thus "concrete\<^sub>S(the(fmlookup (\<rho> \<bullet> \<sigma>) v))" 
            using assm premise fmdom'_notI by force 
          \<comment> \<open>Hence the mapped expression of v in \<open>\<rho>\<bullet>\<sigma>\<close> must be concrete\<close>
        qed
      next
        assume "\<not>v \<in> fmdom'(\<sigma>)"
        \<comment> \<open>We secondly assume v to not be in the domain of \<open>\<sigma>\<close>\<close>
        moreover have "fmdom'(\<rho> \<bullet> \<sigma>) = fmdom'(\<sigma>) \<union> fmdom'(\<rho>)"
          by (simp add: fmdom'_alt_def)
        \<comment> \<open>We know that the domains of \<open>\<sigma>\<close> and \<open>\<rho>\<close> are preserved when concretizing\<close>
        ultimately have "v \<in> fmdom'(\<rho>)" 
          using main_premise by blast
        \<comment> \<open>Hence v must be in the domain of \<open>\<rho>\<close>\<close>
        moreover have "concrete\<^sub>\<Sigma>(\<rho>)"
          using assms by (simp add: is_conc_map\<^sub>\<Sigma>_def)
        \<comment> \<open>We know that \<open>\<rho>\<close> is concrete due to our assumptions\<close>
        ultimately have "concrete\<^sub>S (the(fmlookup \<rho> v)) \<and> v |\<in>| fmdom(\<rho>)"
          by (simp add: concrete\<^sub>\<Sigma>_def fmlookup_dom'_iff fmlookup_dom_iff)
        \<comment> \<open>Hence the mapped expression of v in \<open>\<rho>\<close> is concrete\<close>
        thus "concrete\<^sub>S(the(fmlookup (\<rho> \<bullet> \<sigma>) v))" 
          by simp
        \<comment> \<open>Thus the mapped expression of v in \<open>\<rho>\<bullet>\<sigma>\<close> must be concrete\<close>
      qed
    qed
  qed
  thus ?thesis using concrete\<^sub>\<Sigma>_def by blast
  \<comment> \<open>Thus the thesis holds\<close>
qed

text \<open>We prove that by \<open>concretizing a well-formed trace\<close>, the \<open>resulting
  trace will be of concrete nature\<close> [\<open>Proposition 2.25\<close>]\<close>

lemma symb_dom_inc\<^sub>\<T>: "is_conc_map\<^sub>\<T> \<rho> \<tau> \<longrightarrow> symb\<^sub>\<T> \<tau> \<subseteq> fmdom'(\<rho>)"
proof (induct \<tau>)
  \<comment> \<open>We perform an induction over \<open>\<tau>\<close>\<close>
  case Epsilon
  show ?case by simp
  \<comment> \<open>If \<open>\<tau>\<close> is \<open>\<epsilon>\<close>, the case is obvious\<close>
next
  case (Transition \<tau>' ta)
  thus ?case
  proof (induct ta)
    \<comment> \<open>We perform a case distinction over the trace atom \<open>ta\<close>\<close>
    case (Event ev e)
    thus ?case by simp
    \<comment> \<open>If \<open>ta\<close> is an event, the case can be closed with the induction hypothesis\<close>
  next
    case (State \<sigma>)
    thus ?case using is_conc_map\<^sub>\<Sigma>_def by auto
    \<comment> \<open>If \<open>ta\<close> is a state, the case can be closed with the induction hypothesis and
       the premise\<close>
  qed
qed

lemma trans_pr\<^sub>E: "\<tau> \<subseteq>\<^sub>E V \<and> V \<subseteq> V' \<longrightarrow> \<tau> \<subseteq>\<^sub>E V'"
proof (induct \<tau>)
  \<comment> \<open>We perform a case distinction over \<open>\<tau>\<close>\<close>
  case Epsilon
  show ?case by simp
  \<comment> \<open>If \<open>\<tau>\<close> is \<open>\<epsilon>\<close>, the case is trivial\<close>
next
  case (Transition \<tau>' ta)
  thus ?case 
    by (induct ta; auto)
  \<comment> \<open>If \<open>\<tau>\<close> is \<open>(\<tau>' \<leadsto> ta)\<close>, the case can be closed with induction over \<open>ta\<close>\<close>
qed

lemma concrete_pc\<^sub>\<T>:
  "\<pi>' = \<rho> \<sqdot> \<pi> \<and> is_conc_map\<^sub>\<T> \<rho> (\<Down>\<^sub>\<T> \<pi>) \<and> (\<Down>\<^sub>p \<pi>) \<subseteq>\<^sub>p symb\<^sub>\<T> (\<Down>\<^sub>\<T> \<pi>) \<longrightarrow> sconcrete\<^sub>B(\<Down>\<^sub>p \<pi>')"
proof (rule impI)
  assume premise: "\<pi>' = \<rho> \<sqdot> \<pi> \<and> is_conc_map\<^sub>\<T> \<rho> (\<Down>\<^sub>\<T> \<pi>) \<and> (\<Down>\<^sub>p \<pi>) \<subseteq>\<^sub>p symb\<^sub>\<T> (\<Down>\<^sub>\<T> \<pi>)"
  \<comment> \<open>We assume the premise of the formula holds\<close>
  moreover then have "(\<Down>\<^sub>p \<pi>') = sval\<^sub>B (\<Down>\<^sub>p \<pi>) \<rho>"
    by (metis conc_map\<^sub>\<T>.elims pc_projection.simps)
  \<comment> \<open>Hence we can conclude that the path condition of \<open>\<pi>'\<close> is the evaluation
     of the path condition of \<open>\<pi>\<close> via \<open>\<rho>\<close>\<close>
  moreover have "svars\<^sub>B(\<Down>\<^sub>p \<pi>) \<subseteq> symb\<^sub>\<T> (\<Down>\<^sub>\<T> \<pi>)"
    using premise by simp
  \<comment> \<open>Due to the premise, all variables occurring in the path condition of \<open>\<pi>\<close>
     must occur symbolically somewhere in the trace of \<open>\<pi>\<close>\<close>
  moreover then have "svars\<^sub>B(\<Down>\<^sub>p \<pi>) \<subseteq> fmdom'(\<rho>)"
    using premise symb_dom_inc\<^sub>\<T> by auto
  \<comment> \<open>Because \<open>\<rho>\<close> is a concretization mapping, we can then conclude that all
     variables occurring in the path condition of \<open>\<pi>\<close> occur in \<open>\<rho>\<close>\<close>
  moreover have "concrete\<^sub>\<Sigma> \<rho>"
    using premise conc_map_concrete\<^sub>\<T> by auto
  \<comment> \<open>Additionally we know that \<open>\<rho>\<close> must be concrete\<close>
  ultimately show "sconcrete\<^sub>B(\<Down>\<^sub>p \<pi>')"
    using s_concrete_imp\<^sub>B by simp
  \<comment> \<open>Thus we can conclude that the path condition of \<open>\<pi>'\<close> must be concrete\<close>
qed

lemma concrete_symb\<^sub>\<T>:
  "\<tau>' = (trace_conc \<rho> \<tau>) \<and> is_conc_map\<^sub>\<T> \<rho> \<tau> \<and> \<tau> \<subseteq>\<^sub>E (fmdom'(\<rho>)) \<and> wf_states\<^sub>\<T>(\<tau>) \<longrightarrow> concrete\<^sub>\<T>(\<tau>')"
proof (induct \<tau>' arbitrary: \<tau>)
  \<comment> \<open>We perform an induction over the trace \<open>\<tau>'\<close>\<close>
  case Epsilon
  show ?case by simp
  \<comment> \<open>If \<open>\<tau>'\<close> is \<open>\<epsilon>\<close>, the case is trivial\<close>
next
  case (Transition \<tau>'' ta)
  show ?case 
  \<comment> \<open>We now assume \<open>\<tau>'\<close> is \<open>(\<tau>'' \<leadsto> ta)\<close>\<close>
  proof (induct ta)
    \<comment> \<open>We perform an induction over \<open>ta\<close>\<close>
    case (Event ev e)
    \<comment> \<open>We first assume \<open>ta\<close> is an event\<close>
    show ?case 
    proof (rule impI)
      assume premise: "(\<tau>'' \<leadsto> Event\<llangle>ev, e\<rrangle>) = (trace_conc \<rho> \<tau>) \<and> is_conc_map\<^sub>\<T> \<rho> \<tau> \<and> \<tau> \<subseteq>\<^sub>E (fmdom'(\<rho>)) \<and> wf_states\<^sub>\<T>(\<tau>)"
      \<comment> \<open>We assume the premise holds\<close>
      hence "\<exists>\<tau>'''. \<exists>e'. \<tau> = \<tau>''' \<leadsto> Event\<llangle>ev, e'\<rrangle> \<and> trace_conc \<rho> \<tau>''' = \<tau>'' \<and> lval\<^sub>E e' \<rho> = e"
        by (smt (verit) \<T>.distinct(1) \<T>.inject trace_atom.distinct(1) trace_atom.inject(2) trace_conc.elims)
      \<comment> \<open>Hence we know that the last trace atom of \<open>\<tau>\<close> must also be an event, such 
         that the simplification results in the event of \<open>\<tau>'\<close>\<close>
      then obtain \<tau>''' e' where vals: "\<tau> = \<tau>''' \<leadsto> Event\<llangle>ev, e'\<rrangle> \<and> trace_conc \<rho> \<tau>''' = \<tau>'' \<and> lval\<^sub>E e' \<rho> = e"
        by auto
      \<comment> \<open>We then obtain those values\<close>
      moreover have "\<tau> \<subseteq>\<^sub>E (fmdom'(\<rho>)) \<and> concrete\<^sub>\<Sigma>(\<rho>)"
        using premise conc_map_concrete\<^sub>\<T> by auto
      \<comment> \<open>We know that all expressions in events from \<open>\<tau>\<close> are also in the domain of
         \<open>\<rho>\<close> and that \<open>\<rho>\<close> is concrete\<close>
      ultimately have "lconcrete\<^sub>E e"
        using l_concrete_imp\<^sub>E by auto
      \<comment> \<open>We can ultimately conclude that the evaluation of e' must be concrete
         as well\<close>
      thus "concrete\<^sub>\<T>(\<tau>'' \<leadsto> Event\<llangle>ev, e\<rrangle>)"
        using premise vals Transition.hyps by auto
      \<comment> \<open>Using the induction hypothesis, we can then conclude on the concreteness
         of the trace\<close>
    qed
  next
    case (State \<sigma>)
    \<comment> \<open>We now assume ta is a state \<open>\<sigma>\<close>\<close>
    show ?case 
    proof (rule impI)
      assume premise: "(\<tau>'' \<leadsto> State\<llangle>\<sigma>\<rrangle>) = (trace_conc \<rho> \<tau>) \<and> is_conc_map\<^sub>\<T> \<rho> \<tau> \<and> \<tau> \<subseteq>\<^sub>E (fmdom'(\<rho>)) \<and> wf_states\<^sub>\<T>(\<tau>)"
      \<comment> \<open>We assume the premise holds\<close>
      hence "\<exists>\<tau>'''. \<exists>\<sigma>'. \<tau> = \<tau>''' \<leadsto> State\<llangle>\<sigma>'\<rrangle> \<and> trace_conc \<rho> \<tau>''' = \<tau>'' \<and> \<sigma> = \<rho> \<bullet> \<sigma>'"
        by (metis \<T>.distinct(1) \<T>.inject last\<^sub>\<T>.simps(1) trace_atom.distinct(1) trace_conc.elims)
      \<comment> \<open>Hence we know that the last trace atom of \<open>\<tau>\<close> must also be a state \<open>\<sigma>'\<close> such
         that \<open>\<sigma>\<close> is the state concretization of \<open>\<sigma>'\<close> via \<open>\<rho>\<close>\<close>
      then obtain \<tau>''' \<sigma>' where vals: "\<tau> = \<tau>''' \<leadsto> State\<llangle>\<sigma>'\<rrangle> \<and> trace_conc \<rho> \<tau>''' = \<tau>'' \<and> \<sigma> = \<rho> \<bullet> \<sigma>'"
        by auto
      \<comment> \<open>We then obtain those values\<close>
      hence "is_conc_map\<^sub>\<T> \<rho> \<tau>''' \<and> \<tau>''' \<subseteq>\<^sub>E (fmdom'(\<rho>))"
        using premise by auto
      \<comment> \<open>We know that \<open>\<rho>\<close> must be a concretization mapping for \<open>\<tau>'''\<close> as well and
         that all expressions occurring in events of \<open>\<tau>'''\<close> are also in \<open>\<rho>\<close>\<close>
      hence "concrete\<^sub>\<T>(\<tau>'')"
        using Transition.hyps premise vals wf_states\<^sub>\<T>.simps(3) by blast
      \<comment> \<open>We can then conclude that \<open>\<tau>''\<close> is a concrete trace using the induction
         hypothesis\<close>
      moreover have "wf\<^sub>\<Sigma>(\<sigma>')"
        using premise vals by simp
      \<comment> \<open>Due to the premise, we know that \<open>\<sigma>'\<close> must be wellformed\<close>
      moreover then have "concrete\<^sub>\<Sigma>(\<sigma>)"
        using premise vals wf_conc_imp\<^sub>\<Sigma> by simp
      \<comment> \<open>Because \<open>\<sigma>\<close> is the concretization of \<open>\<sigma>'\<close> via \<open>\<rho>\<close>, \<open>\<sigma>\<close> must be concrete\<close>
      ultimately show "concrete\<^sub>\<T>(\<tau>'' \<leadsto> State\<llangle>\<sigma>\<rrangle>)"
        by simp
      \<comment> \<open>Thus both conditions for the trace concreteness have been shown\<close>
    qed
  qed
qed

lemma wf\<^sub>1_pr\<^sub>\<T>:
  "\<tau>' = (trace_conc \<rho> \<tau>) \<and> is_conc_map\<^sub>\<T> \<rho> \<tau> \<and> wf_states\<^sub>\<T> \<tau> \<and> \<tau> \<subseteq>\<^sub>E (fmdom'(\<rho>)) \<longrightarrow> \<tau>' \<diamondop>\<diamondop>\<^sub>\<T> (symb\<^sub>\<T> \<tau>')"
proof (rule impI)
  assume "\<tau>' = (trace_conc \<rho> \<tau>) \<and> is_conc_map\<^sub>\<T> \<rho> \<tau> \<and> wf_states\<^sub>\<T> \<tau> \<and> \<tau> \<subseteq>\<^sub>E (fmdom'(\<rho>))"
  \<comment> \<open>We first assume the premise holds\<close>
  hence "concrete\<^sub>\<T>(\<tau>')"
    using concrete_symb\<^sub>\<T> by simp
  \<comment> \<open>Using our previous theorem, we now know that \<open>\<tau>'\<close> must be concrete\<close>
  hence "symb\<^sub>\<T>(\<tau>') = {}"
    using concrete_symb_imp\<^sub>\<T> by simp
  \<comment> \<open>Thus \<open>\<tau>'\<close> contains no symbolic variables\<close>
  moreover have "\<tau>' \<diamondop>\<diamondop>\<^sub>\<T> {}"
    apply (induct \<tau>')
    using non_symb_disjunct\<^sub>\<T>.elims(1) by auto
  \<comment> \<open>We know that \<open>\<tau>'\<close> is always wellformed regarding the empty set\<close>
  ultimately show "\<tau>' \<diamondop>\<diamondop>\<^sub>\<T> (symb\<^sub>\<T> \<tau>')"
    by simp
  \<comment> \<open>Thus concludes the lemma\<close>
qed

lemma wf\<^sub>2_pr\<^sub>\<T>:
  "\<pi>' = \<rho> \<sqdot> \<pi> \<and> is_conc_map\<^sub>\<T> \<rho> (\<Down>\<^sub>\<T> \<pi>) \<and> (\<Down>\<^sub>p \<pi>) \<subseteq>\<^sub>p symb\<^sub>\<T> (\<Down>\<^sub>\<T> \<pi>) \<longrightarrow> (\<Down>\<^sub>p \<pi>') \<subseteq>\<^sub>p symb\<^sub>\<T> (\<Down>\<^sub>\<T> \<pi>')"
proof (rule impI)
  assume "\<pi>' = \<rho> \<sqdot> \<pi> \<and> is_conc_map\<^sub>\<T> \<rho> (\<Down>\<^sub>\<T> \<pi>) \<and> (\<Down>\<^sub>p \<pi>) \<subseteq>\<^sub>p symb\<^sub>\<T> (\<Down>\<^sub>\<T> \<pi>)"
  \<comment> \<open>We first assume our premise holds\<close>
  hence "sconcrete\<^sub>B (\<Down>\<^sub>p \<pi>')"
    using concrete_pc\<^sub>\<T> by simp
  \<comment> \<open>Using our previous theorem, we can now conclude that the path condition of
     \<open>\<pi>'\<close> must be concrete\<close>
  thus "(\<Down>\<^sub>p \<pi>') \<subseteq>\<^sub>p symb\<^sub>\<T> (\<Down>\<^sub>\<T> \<pi>')"
    using s_concrete_vars_imp\<^sub>B by (metis empty_subsetI wf_pc\<^sub>\<pi>.elims(3))
  \<comment> \<open>Because our path condition has no variables, the thesis holds trivially\<close>
qed

lemma wf\<^sub>3_pr\<^sub>\<T>:
  "\<tau>' = (trace_conc \<rho> \<tau>) \<and> is_conc_map\<^sub>\<T> \<rho> \<tau> \<and> wf_states\<^sub>\<T>(\<tau>) \<and> \<tau> \<subseteq>\<^sub>E (fmdom'(\<rho>)) \<longrightarrow> \<tau>' \<subseteq>\<^sub>E (symb\<^sub>\<T> \<tau>')"
proof (induct \<tau>' arbitrary: \<tau>)
  \<comment> \<open>We perform an induction over \<open>\<tau>'\<close>\<close>
  case Epsilon
  show ?case by simp
  \<comment> \<open>If \<open>\<tau>'\<close> is \<open>\<epsilon>\<close>, the case is obvious\<close>
next
  case (Transition \<tau>'' ta)
  \<comment> \<open>We now assume \<open>\<tau>'\<close> is \<open>(\<tau>'' \<leadsto> ta)\<close>\<close>
  show ?case
  proof (induct ta)
    \<comment> \<open>We perform an induction over \<open>ta\<close>\<close>
    case (Event ev e)
    \<comment> \<open>We first assume \<open>ta\<close> is an event\<close>
    show ?case 
    proof (rule impI)
      assume premise: "(\<tau>'' \<leadsto> Event\<llangle>ev, e\<rrangle>) = (trace_conc \<rho> \<tau>) \<and> is_conc_map\<^sub>\<T> \<rho> \<tau> \<and> wf_states\<^sub>\<T>(\<tau>) \<and> \<tau> \<subseteq>\<^sub>E (fmdom'(\<rho>))"
      \<comment> \<open>We assume the premise holds\<close>
      hence crux: "concrete\<^sub>\<T>(\<tau>'') \<and> lconcrete\<^sub>E e"
        using concrete_symb\<^sub>\<T> concrete\<^sub>\<T>.simps(3) by blast
      \<comment> \<open>Using our previous theorem, we can conclude that \<open>\<tau>''\<close> must be a concrete trace\<close>
      hence "lvars\<^sub>E e \<subseteq> symb\<^sub>\<T> (\<tau>'' \<leadsto> Event\<llangle>ev, e\<rrangle>)"
        using l_concrete_vars_imp\<^sub>E by simp
      \<comment> \<open>Therefore e has no variables and the empty set is a subset of any set\<close>
      moreover have "\<tau>'' \<subseteq>\<^sub>E symb\<^sub>\<T> (\<tau>'' \<leadsto> Event\<llangle>ev, e\<rrangle>)"
        using crux conc_events_imp\<^sub>\<T> by simp
      \<comment> \<open>Hence there are also no variables occurring in expressions of \<open>\<tau>''\<close>\<close>
      ultimately show "(\<tau>'' \<leadsto> Event\<llangle>ev, e\<rrangle>) \<subseteq>\<^sub>E symb\<^sub>\<T> (\<tau>'' \<leadsto> Event\<llangle>ev, e\<rrangle>)"
        by simp
      \<comment> \<open>Thus both properties for the thesis have been proven\<close>
    qed
  next
    case (State \<sigma>)
    \<comment> \<open>We now assume \<open>ta\<close> is a state \<open>\<sigma>\<close>\<close>
    thus ?case 
      using concrete_symb\<^sub>\<T> conc_events_imp\<^sub>\<T> by presburger
    \<comment> \<open>Then the case can be concluded\<close>
  qed
qed

lemma wf_surround_pr\<^sub>\<T>:
  "\<tau>' = (trace_conc \<rho> \<tau>) \<and> is_conc_map\<^sub>\<T> \<rho> \<tau> \<and> wf_surround\<^sub>\<T> \<tau> \<longrightarrow> wf_surround\<^sub>\<T> \<tau>'"
proof (induct \<tau>' arbitrary: \<tau>)
  \<comment> \<open>We perform an induction over \<open>\<tau>'\<close>\<close>
  case Epsilon
  show ?case by simp
  \<comment> \<open>If \<open>\<tau>'\<close> is epsilon, the case is trivial\<close>
next
  case (Transition \<tau>'' ta)
  show ?case
  \<comment> \<open>We then assume \<open>\<tau>'\<close> is \<open>(\<tau>'' \<leadsto> ta)\<close>\<close>
  proof (induct ta)
    \<comment> \<open>We perform an induction over \<open>ta\<close>\<close>
    case (Event ev e)
    \<comment> \<open>We first assume \<open>ta\<close> is an event\<close>
    show ?case using Transition.hyps
      by (smt (verit) \<T>.inject is_conc_map\<^sub>\<T>.simps(2) trace_atom.distinct(1) trace_conc.elims wf_surround\<^sub>\<T>.simps(7))
    \<comment> \<open>Then the case holds via the induction hypothesis\<close>
  next
    case (State \<sigma>)
    \<comment> \<open>We now assume \<open>ta\<close> is a state \<open>\<sigma>\<close>\<close>
    show ?case 
    proof (rule impI)
      assume premise: "(\<tau>'' \<leadsto> State\<llangle>\<sigma>\<rrangle>) = (trace_conc \<rho> \<tau>) \<and> is_conc_map\<^sub>\<T> \<rho> \<tau> \<and> wf_surround\<^sub>\<T> \<tau>"
      \<comment> \<open>We assume the premise holds\<close>
      hence "\<exists>\<tau>'''. \<exists>\<sigma>'. \<tau> = \<tau>''' \<leadsto> State\<llangle>\<sigma>'\<rrangle> \<and> trace_conc \<rho> \<tau>''' = \<tau>'' \<and> \<sigma> = \<rho> \<bullet> \<sigma>'"
        by (metis \<T>.distinct(1) \<T>.inject last\<^sub>\<T>.simps(1) trace_atom.distinct(1) trace_conc.elims)
      \<comment> \<open>Hence the last trace atom of \<open>\<tau>\<close> must be a state that simplifies to \<open>\<sigma>\<close> via \<open>\<rho>\<close>\<close>
      then obtain \<tau>''' \<sigma>' where vals\<^sub>A: "\<tau> = \<tau>''' \<leadsto> State\<llangle>\<sigma>'\<rrangle> \<and> trace_conc \<rho> \<tau>''' = \<tau>'' \<and> \<sigma> = \<rho> \<bullet> \<sigma>'"
        by auto
      \<comment> \<open>We then obtain those values\<close>
      moreover have hypo\<^sub>A: "\<tau>'' = (trace_conc \<rho> \<tau>''') \<and> is_conc_map\<^sub>\<T> \<rho> \<tau>''' \<and> wf_surround\<^sub>\<T> \<tau>''' \<longrightarrow> wf_surround\<^sub>\<T> \<tau>''"
        using Transition.hyps by simp
      \<comment> \<open>We formalize our induction hypothesis\<close>
      ultimately show "wf_surround\<^sub>\<T> (\<tau>'' \<leadsto> State\<llangle>\<sigma>\<rrangle>)"
      proof (induct \<tau>'')
      \<comment> \<open>We now perform an induction over \<open>\<tau>''\<close>\<close>
        case Epsilon
        thus ?case by simp
        \<comment> \<open>If \<open>\<tau>''\<close> is \<open>\<epsilon>\<close>, the case is obvious\<close>
      next
        case (Transition \<tau>'''' ta')
        thus ?case 
        \<comment> \<open>We then assume that \<open>\<tau>''\<close> is \<open>(\<tau>'''' \<leadsto> ta')\<close>\<close>
        proof (induct ta')
        \<comment> \<open>We perform an induction over \<open>ta'\<close>\<close>
          case (State \<sigma>'')
          \<comment> \<open>We first assume \<open>ta'\<close> is a state \<open>\<sigma>''\<close>\<close>
          hence "\<exists>\<tau>'''''. \<exists>\<sigma>'''. \<tau>''' = \<tau>''''' \<leadsto> State\<llangle>\<sigma>'''\<rrangle> \<and> trace_conc \<rho> \<tau>''''' = \<tau>'''' \<and> \<sigma>'' = \<rho> \<bullet> \<sigma>'''"  
            by (smt (verit) \<T>.distinct(1) \<T>.inject last\<^sub>\<T>.simps(1) trace_atom.distinct(1) trace_conc.elims)
          \<comment> \<open>Then the last trace atom of \<open>\<tau>'''\<close> must be a state \<open>\<sigma>'''\<close> that evaluates
             to \<open>\<sigma>''\<close> via \<open>\<rho>\<close>\<close>
          then obtain \<tau>''''' \<sigma>''' where "\<tau>''' = \<tau>''''' \<leadsto> State\<llangle>\<sigma>'''\<rrangle> \<and> trace_conc \<rho> \<tau>''''' = \<tau>'''' \<and> \<sigma>'' = \<rho> \<bullet> \<sigma>'''"
            by auto
          \<comment> \<open>We then obtain those values\<close>
          thus "wf_surround\<^sub>\<T> ((\<tau>'''' \<leadsto> State\<llangle>\<sigma>''\<rrangle>) \<leadsto> State\<llangle>\<sigma>\<rrangle>)"
            using premise vals\<^sub>A hypo\<^sub>A by auto
          \<comment> \<open>And can conclude on our thesis via our first induction hypothesis\<close>
        next
          case (Event ev e)
          \<comment> \<open>We next assume \<open>ta'\<close> is an event\<close>
          hence "trace_conc \<rho> \<tau>''' = \<tau>'''' \<leadsto> Event\<llangle>ev, e\<rrangle>"
            using premise vals\<^sub>A by simp
          \<comment> \<open>We know that \<open>\<tau>'''\<close> concretized is \<open>(\<tau>'''' \<leadsto> ev)\<close>\<close>
          hence "\<exists>\<tau>'''''. \<exists>e'. \<tau>''' = \<tau>''''' \<leadsto> Event\<llangle>ev, e'\<rrangle> \<and> lval\<^sub>E e' \<rho> = e \<and> trace_conc \<rho> \<tau>''''' = \<tau>''''"  
            by (smt (verit) \<T>.distinct(1) \<T>.inject trace_atom.distinct(1) trace_conc.elims trace_atom.inject(2))
          \<comment> \<open>Hence the last trace atom of \<open>\<tau>'''\<close> must be an event with an expression e'
             that evaluates to e via \<open>\<rho>\<close>\<close>
          then obtain \<tau>''''' e' where vals\<^sub>B: "\<tau>''' = \<tau>''''' \<leadsto> Event\<llangle>ev, e'\<rrangle> \<and> lval\<^sub>E e' \<rho> = e \<and> trace_conc \<rho> \<tau>''''' = \<tau>''''"
            by auto
          \<comment> \<open>We then obtain those values\<close>
          thus ?case 
            using premise by (smt (verit) Event.prems(2) Event.prems(3) is_conc_map\<^sub>\<T>.simps(3) trace_conc.elims wf_surround\<^sub>\<T>.simps(2) wf_surround\<^sub>\<T>.simps(4) wf_surround\<^sub>\<T>.simps(5) wf_surround\<^sub>\<T>.simps(7))
          \<comment> \<open>Thus the case holds\<close>
        qed
      qed
    qed
  qed
qed

lemma wf\<^sub>4_pr\<^sub>\<T>:
  "\<tau>' = (trace_conc \<rho> \<tau>) \<and> is_conc_map\<^sub>\<T> \<rho> \<tau> \<and> wf_seq\<^sub>\<T>(\<tau>) \<longrightarrow> wf_seq\<^sub>\<T>(\<tau>')"
proof (induct \<tau>' arbitrary: \<tau>)
  \<comment> \<open>We perform a case distinction over \<open>\<tau>'\<close>\<close>
  case Epsilon
  show ?case by simp
  \<comment> \<open>If \<open>\<tau>'\<close> is \<open>\<epsilon>\<close>, the case is obvious\<close>
next
  case (Transition \<tau>'' ta)
  thus ?case using Transition.hyps wf_surround_pr\<^sub>\<T> 
    by simp
  \<comment> \<open>If \<open>\<tau>'\<close> is \<open>(\<tau>'' \<leadsto> ta)\<close>, the case follows from the previous theorem and
     the induction hypothesis\<close>
qed

lemma wf\<^sub>5_pr\<^sub>\<T>:
  "\<tau>' = (trace_conc \<rho> \<tau>) \<and> is_conc_map\<^sub>\<T> \<rho> \<tau> \<and> \<tau> \<subseteq>\<^sub>E (fmdom'(\<rho>)) \<and> wf_states\<^sub>\<T> \<tau> \<longrightarrow> wf_states\<^sub>\<T> \<tau>'"
proof (induct \<tau>' arbitrary: \<tau>)
  \<comment> \<open>We perform an induction over \<open>\<tau>'\<close>\<close>
  case Epsilon
  show ?case by simp
  \<comment> \<open>If \<open>\<tau>'\<close> is \<open>\<epsilon>\<close>, the case is trivial\<close>
next
  case (Transition \<tau>'' ta)
  show ?case
  \<comment> \<open>We now assume \<open>\<tau>'\<close> is \<open>(\<tau>'' \<leadsto> ta)\<close>\<close>
  proof (rule impI)
    assume "(\<tau>'' \<leadsto> ta) = (trace_conc \<rho> \<tau>) \<and> is_conc_map\<^sub>\<T> \<rho> \<tau> \<and> \<tau> \<subseteq>\<^sub>E (fmdom'(\<rho>)) \<and> wf_states\<^sub>\<T> \<tau>"
    \<comment> \<open>We first assume the premise holds\<close>
    moreover then have "concrete\<^sub>\<T>(\<tau>'' \<leadsto> ta)"
      using concrete_symb\<^sub>\<T> by simp
    \<comment> \<open>Using our previous theorem, we know that \<open>(\<tau>'' \<leadsto> ta)\<close> must be concrete\<close>
    ultimately show "wf_states\<^sub>\<T> (\<tau>'' \<leadsto> ta)"
      using Transition.hyps concrete_wf_imp\<^sub>\<Sigma>
      by (simp add: conc_wf_imp\<^sub>\<T>)
    \<comment> \<open>We then conclude that the trace must be concrete using the induction
       hypothesis\<close>
  qed
qed

lemma wf_conc_imp\<^sub>\<T>: 
  assumes "wf_states\<^sub>\<T>(\<tau>) \<and> \<tau> \<subseteq>\<^sub>E symb\<^sub>\<T>(\<tau>) \<and> is_conc_map\<^sub>\<T> \<rho> \<tau>" 
  shows "concrete\<^sub>\<T>(trace_conc \<rho> \<tau>)" 
  by (meson assms concrete_symb\<^sub>\<T> symb_dom_inc\<^sub>\<T> trans_pr\<^sub>E)

lemma wf_conc_imp\<^sub>\<pi>: 
  assumes "wf\<^sub>\<pi>(\<pi>) \<and> is_conc_map\<^sub>\<T> \<rho> (\<Down>\<^sub>\<T> \<pi>)" 
  shows "concrete\<^sub>\<pi>(\<rho> \<sqdot> \<pi>)" 
proof -
  have incl: "(\<Down>\<^sub>\<T> \<pi>) \<subseteq>\<^sub>E (fmdom'(\<rho>))"
    using assms symb_dom_inc\<^sub>\<T> trans_pr\<^sub>E wf\<^sub>\<pi>_def by meson
  \<comment> \<open>We know that all variables in the trace of \<open>\<tau>\<close> are mapped in \<open>\<rho>\<close>\<close>
  moreover have conc: "(\<Down>\<^sub>\<T> (\<rho> \<sqdot> \<pi>)) = (trace_conc \<rho> (\<Down>\<^sub>\<T> \<pi>))"
    using assms wf\<^sub>\<pi>_def by (metis conc_map\<^sub>\<T>.elims trace_projection.simps)
  \<comment> \<open>We also know that concretizing the trace of \<open>\<pi>\<close> results in the trace of \<open>\<rho> \<sqdot> \<pi>\<close>\<close>
  ultimately have "concrete\<^sub>\<T> (\<Down>\<^sub>\<T> (\<rho> \<sqdot> \<pi>))"
    using assms concrete_symb\<^sub>\<T>  wf\<^sub>\<pi>_def by blast
  \<comment> \<open>Hence \<open>\<rho> \<sqdot> \<pi>\<close> must be concrete due to our earlier theorem\<close>
  moreover have "sconcrete\<^sub>B (\<Down>\<^sub>p (\<rho> \<sqdot> \<pi>))"
    using assms concrete_pc\<^sub>\<T> wf\<^sub>\<pi>_def by blast
  \<comment> \<open>Due to an earlier theorem, the path condition must also be concrete\<close>
  moreover have "wf\<^sub>\<pi> (\<rho> \<sqdot> \<pi>)"
    by (metis incl conc assms wf\<^sub>1_pr\<^sub>\<T> wf\<^sub>2_pr\<^sub>\<T> wf\<^sub>3_pr\<^sub>\<T> wf\<^sub>4_pr\<^sub>\<T> wf\<^sub>5_pr\<^sub>\<T> wf\<^sub>\<pi>_def)
  \<comment> \<open>Using the rest of the theorems, the wellformedness is preserved\<close>
  ultimately show ?thesis by (simp add: concrete\<^sub>\<pi>_def)
  \<comment> \<open>As all 3 properties have been proven, the concretized trace must be concrete\<close>
qed

lemma concretetize\<^sub>\<Sigma>_dom_distinct:
  assumes "concrete\<^sub>\<Sigma> \<rho> \<and> concrete\<^sub>\<Sigma> \<sigma> \<and> fmdom'(\<rho>) \<inter> fmdom'(\<sigma>) = {}"
  shows "\<rho> \<bullet> \<sigma> = \<sigma> ++\<^sub>f \<rho>"
  using assms fmmap_keys_conc by simp

text \<open>The current proof automation for the derivation of minimal concretization
  mappings is insufficient, as the Isabelle simplifier is not able to infer them. 
  We therefore also aim to introduce additional simplification lemmas, thereby
  allowing us to efficiently compute concretization mappings for arbitrary states
  \<open>\<sigma>\<close>. \par 
  Let us assume \<open>\<sigma>\<close> to be an arbitrary concrete state, which is updated by 
  assigning an arbitrary variable \<open>x\<close> the symbolic value. Then the minimal
  concretization mapping of the updated state must concretize only \<open>x\<close>.\<close>
  lemma min_conc_map_of_concrete_upd\<^sub>\<Sigma>: 
    "concrete\<^sub>\<Sigma>(\<sigma>) \<longrightarrow> min_conc_map\<^sub>\<Sigma> ([x \<longmapsto> \<^emph>] \<sigma>) n = [x \<longmapsto> Exp (Num n)] \<circle>"
  proof (rule impI)
    assume premise: "concrete\<^sub>\<Sigma>(\<sigma>)"
    \<comment> \<open>We first assume the premise holds, implying that \<open>\<sigma>\<close> is concrete.\<close>
    have "\<forall>y. fmlookup (min_conc_map\<^sub>\<Sigma> ([x \<longmapsto> \<^emph>] \<sigma>) n) y = fmlookup ([x \<longmapsto> Exp (Num n)] \<circle>) y"
    \<comment> \<open>We next want to prove that the minimal concretization mapping of the updated
       version of \<open>\<sigma>\<close> and the concretization mapping concretizing only \<open>x\<close> match in
       all their lookup values.\<close>
    proof (rule allI)
      fix y
      \<comment> \<open>For this purpose, We assume variable y to be arbitrary, but fixed.\<close>
      show "fmlookup (min_conc_map\<^sub>\<Sigma> ([x \<longmapsto> \<^emph>] \<sigma>) n) y = fmlookup ([x \<longmapsto> Exp (Num n)] \<circle>) y"
      proof (cases)
      \<comment> \<open>We perform a case distinction over the origin of y.\<close>
        assume dom: "y \<in> fmdom'([x \<longmapsto> \<^emph>] \<sigma>)"
        \<comment> \<open>We first assume y is an element of the domain of the updated state.\<close>
        show "fmlookup (min_conc_map\<^sub>\<Sigma> ([x \<longmapsto> \<^emph>] \<sigma>) n) y = fmlookup ([x \<longmapsto> Exp (Num n)] \<circle>) y"
        proof (cases)
        \<comment> \<open>We perform another case distinction over y.\<close>
          assume "y = x"
          \<comment> \<open>We first assume y is the updated variable \<open>x\<close>.\<close>
          thus "fmlookup (min_conc_map\<^sub>\<Sigma> ([x \<longmapsto> \<^emph>] \<sigma>) n) y = fmlookup ([x \<longmapsto> Exp (Num n)] \<circle>) y" by simp
          \<comment> \<open>Because \<open>x\<close> mapped to the symbolic value, the minimal concretization 
             mapping of updated \<open>\<sigma>\<close> must then concretize \<open>x\<close>. Thus both lookup
             values must match.\<close>
        next
          assume "\<not>y = x"
          \<comment> \<open>We next assume that y is not the updated variable \<open>x\<close>.\<close>
          moreover then have "y \<in> fmdom'(\<sigma>)" using dom by auto
          \<comment> \<open>Due to the assumption that y must be located in the domain of the 
             updated version of \<open>\<sigma>\<close>, y must hence be located in the domain of \<open>\<sigma>\<close>.\<close>
          moreover then have "\<exists>e. fmlookup \<sigma> y = Some (Exp e)"
            using premise concrete\<^sub>\<Sigma>_def by (metis fmlookup_dom'_iff option.sel sexp.exhaust sexp.simps(5))
          \<comment> \<open>Considering that \<open>\<sigma>\<close> is concrete, we can deduce that there exists a 
             lookup value of y in \<open>\<sigma>\<close>, which is non-symbolic.\<close>
          ultimately show "fmlookup (min_conc_map\<^sub>\<Sigma> ([x \<longmapsto> \<^emph>] \<sigma>) n) y = fmlookup ([x \<longmapsto> Exp (Num n)] \<circle>) y"
            by fastforce
          \<comment> \<open>Thus, y must have no function value in the minimal concretization
             mapping, thereby closing the case.\<close>
        qed
      next
        assume "\<not>y \<in> fmdom'([x \<longmapsto> \<^emph>] \<sigma>)"
        \<comment> \<open>We next assume that y is not located in the domain of the updated state \<open>\<sigma>\<close>.\<close>
        thus "fmlookup (min_conc_map\<^sub>\<Sigma> ([x \<longmapsto> \<^emph>] \<sigma>) n) y = fmlookup ([x \<longmapsto> Exp (Num n)] \<circle>) y" by auto
        \<comment> \<open>Then y can not have a function value in the minimal concretization mapping,
           which trivially closes this case.\<close>
      qed
    qed
    thus "min_conc_map\<^sub>\<Sigma> ([x \<longmapsto> \<^emph>] \<sigma>) n = [x \<longmapsto> Exp (Num n)] \<circle>"
      using fmap_ext by blast
    \<comment> \<open>We can then deduce that both states are equivalent using the equivalence
       notion of finite maps. This concludes the~lemma.\<close>
  qed

text \<open>If we concretize an already concrete state \<open>\<sigma>\<close> with the empty concretization
  mapping, the original state \<open>\<sigma>\<close> does not change in any way. This is also trivial, 
  considering that concrete states cannot be simplified any~further.\<close>
  lemma state_conc_pr: "concrete\<^sub>\<Sigma>(\<sigma>) \<longrightarrow> \<circle> \<bullet> \<sigma> = \<sigma>"
    proof (rule impI)
      assume premise: "concrete\<^sub>\<Sigma>(\<sigma>)"
      \<comment> \<open>We assume our premise holds, implying that \<open>\<sigma>\<close> must be of concrete nature.\<close>
      have "\<forall>v. fmlookup (\<circle> \<bullet> \<sigma>) v = fmlookup \<sigma> v"
      \<comment> \<open>We next want to prove that \<open>\<sigma>\<close> and the concretized version of \<open>\<sigma>\<close> 
         match in all their lookup values, thereby implying that the concretization 
         did not change the original state~\<open>\<sigma>\<close>.\<close>
      proof (rule allI)
        fix v
        \<comment> \<open>For this purpose, let us assume variable v is arbitrary, but~fixed.\<close>
        show "fmlookup (\<circle> \<bullet> \<sigma>) v = fmlookup \<sigma> v"
        proof (cases)
        \<comment> \<open>We now perform a case distinction over the occurrence of v in the
           domain of \<open>\<sigma>\<close>.\<close>
          assume dom_assumption: "v \<notin> fmdom'(\<sigma>)"
          \<comment> \<open>Let us first assume that v is not located in the domain of \<open>\<sigma>\<close>.\<close>
          thus "fmlookup (\<circle> \<bullet> \<sigma>) v = fmlookup \<sigma> v" by auto
          \<comment> \<open>Then the lookup value of v in \<open>\<sigma>\<close> will be None (i.e. no function
             value exists). As the concretization of \<open>\<sigma>\<close> with the empty state does not
             change the domain of \<open>\<sigma>\<close>, the lookup value of the concretized state will
             also be None. Thus both lookup values match.\<close>
        next
          assume dom_assumption: "\<not>v \<notin> fmdom'(\<sigma>)"
          \<comment> \<open>Let us next assume that v is located in the domain of \<open>\<sigma>\<close>.\<close>
          moreover then obtain e where val\<^sub>e: "fmlookup \<sigma> v = Some e"
            by (auto simp add: fmlookup_dom'_iff)
          \<comment> \<open>Then there must exist an expression \<open>e\<close>, which is the function value
             of v in \<open>\<sigma>\<close>.\<close>
          ultimately obtain a where "e = Exp a"
            using premise concrete\<^sub>\<Sigma>_def by (metis option.sel sexp.exhaust sexp.simps(5))
          \<comment> \<open>Due to the concreteness of \<open>\<sigma>\<close>, \<open>e\<close> cannot be the symbolic value, 
             hence implying that \<open>e\<close> must be an arithmetic expression \<open>a\<close>.\<close>
          moreover then have "concrete\<^sub>S (Exp a)"
            using premise concrete\<^sub>\<Sigma>_def val\<^sub>e dom_assumption by fastforce
          \<comment> \<open>Using the concreteness of \<open>\<sigma>\<close>, we know that \<open>a\<close> must be of concrete 
             nature.\<close>
          moreover then have "val\<^sub>S (Exp a) \<circle> = Exp a" using value_pr\<^sub>S by blast
          \<comment> \<open>Hence the evaluation of \<open>a\<close> under the empty state preserves \<open>a\<close>.\<close>
          ultimately show "fmlookup (\<circle> \<bullet> \<sigma>) v = fmlookup \<sigma> v" using val\<^sub>e by auto
          \<comment> \<open>The concretization of \<open>\<sigma>\<close> with the empty state updates \<open>a\<close> by 
             evaluating \<open>a\<close> under the empty state. As we have proven, the update
             does not change \<open>a\<close>, thus both lookup values must~match.\<close>
        qed
      qed
      thus "(\<circle> \<bullet> \<sigma>) = \<sigma>" using fmap_ext by blast
      \<comment> \<open>This implies that both states are equivalent, thereby concluding the lemma.\<close>
    qed

text \<open>If we concretize an already concrete symbolic trace \<open>\<tau>\<close> with the empty 
  concretization mapping, the original trace \<open>\<tau>\<close> does not change in any way. This is 
  trivial, considering that concrete traces cannot be simplified any~further.\<close>
  lemma trace_conc_pr: "concrete\<^sub>\<T>(\<tau>) \<longrightarrow> trace_conc \<circle> \<tau> = \<tau>"
  proof (induct \<tau>)
  \<comment> \<open>We perform a structural induction over the construction of \<open>\<tau>\<close>.\<close>
    case Epsilon
    show ?case by simp
    \<comment> \<open>If \<open>\<tau>\<close> is the empty trace \<open>\<epsilon>\<close>, the case holds trivially using the definition
       of trace concretizations.\<close>
  next
    case (Transition \<tau>' ta)
    show ?case
    \<comment> \<open>In the induction step, we assume \<open>\<tau>\<close> is \<open>(\<tau>' \<leadsto> ta)\<close>.\<close>
    proof (induct ta)
    \<comment> \<open>We perform a case distinction over the trace atom \<open>ta\<close>.\<close>
      case (Event ev e)
      \<comment> \<open>We first assume that the trace atom \<open>ta\<close> is an event \<open>e\<close>.\<close>
      thus ?case 
      proof (rule impI)
        assume premise: "concrete\<^sub>\<T>(\<tau>' \<leadsto> Event\<llangle>ev, e\<rrangle>)"
        \<comment> \<open>We assume the premise holds, implying that \<open>(\<tau>' \<leadsto> e)\<close> is concrete.\<close>
        hence "concrete\<^sub>\<T>(\<tau>')" using concrete\<^sub>\<T>.elims(2) by auto
        \<comment> \<open>Then the prefix trace \<open>\<tau>'\<close> must also be concrete.\<close>
        hence "trace_conc \<circle> \<tau>' = \<tau>'" using Transition.hyps by simp
        \<comment> \<open>We can then use the induction hypothesis to reason that \<open>\<tau>'\<close> 
           concretized with the empty state preserves \<open>\<tau>'\<close>.\<close>
        thus "trace_conc \<circle> (\<tau>' \<leadsto> Event\<llangle>ev, e\<rrangle>) = (\<tau>' \<leadsto> Event\<llangle>ev, e\<rrangle>)"
          using premise l_value_pr\<^sub>E by auto
        \<comment> \<open>Then the case holds, as concrete event expression lists are also preserved 
           when concretized under the empty state (concreteness preservation theorem).\<close>
      qed
    next
      case (State \<sigma>)
      \<comment> \<open>We next assume that trace atom \<open>ta\<close> is a state \<open>\<sigma>\<close>.\<close>
      thus ?case
      proof (rule impI)
        assume premise: "concrete\<^sub>\<T>(\<tau>' \<leadsto> State\<llangle>\<sigma>\<rrangle>)"
        \<comment> \<open>We assume the premise holds, implying that \<open>(\<tau>' \<leadsto> \<sigma>)\<close> is concrete.\<close>
        hence "concrete\<^sub>\<T>(\<tau>')" using concrete\<^sub>\<T>.elims(2) by auto
        \<comment> \<open>Then the prefix \<open>\<tau>'\<close> must also be concrete.\<close>
        hence "trace_conc \<circle> \<tau>' = \<tau>'" using Transition.hyps by simp
        \<comment> \<open>We can then again use the induction hypothesis to reason that \<open>\<tau>'\<close> 
           concretized with the empty state preserves \<open>\<tau>'\<close>.\<close>
        thus "trace_conc \<circle> (\<tau>' \<leadsto> State\<llangle>\<sigma>\<rrangle>) = (\<tau>' \<leadsto> State\<llangle>\<sigma>\<rrangle>)"
          using premise state_conc_pr by auto
        \<comment> \<open>Then the case holds, as the concrete state \<open>\<sigma>\<close> is also preserved
           when concretized under the empty state, which we have proven earlier.\<close>
      qed
    qed
  qed

text \<open>The second simplification lemma for state concretizations establishes the
  following: If we update a state \<open>\<sigma>\<close> with a variable \<open>x\<close>, which does not occur
  symbolic in \<open>\<sigma>\<close>, the minimal state concretization mapping of the updated version
  of \<open>\<sigma>\<close> must match the minimal state concretization mapping of \<open>\<sigma>\<close>.\<close>
  lemma min_conc_map_of_concrete_no_upd\<^sub>\<Sigma>: 
    "x \<notin> symb\<^sub>\<Sigma>(\<sigma>) \<longrightarrow> min_conc_map\<^sub>\<Sigma> ([x \<longmapsto> Exp e] \<sigma>) n = min_conc_map\<^sub>\<Sigma> \<sigma> n"
  proof (rule impI)
    assume premise: "x \<notin> symb\<^sub>\<Sigma>(\<sigma>)"
    \<comment> \<open>We first assume the premise holds, implying that \<open>x\<close> does not occur
       symbolic in \<open>\<sigma>\<close>.\<close>
    have "\<forall>y. fmlookup (min_conc_map\<^sub>\<Sigma> ([x \<longmapsto> Exp e] \<sigma>) n) y = fmlookup (min_conc_map\<^sub>\<Sigma> \<sigma> n) y"
    \<comment> \<open>We next want to prove that the minimal concretization mapping of the updated
       version of \<open>\<sigma>\<close> and the concretization mapping of \<open>\<sigma>\<close> match in
       all their lookup values.\<close>
    proof (rule allI)
      fix y
      \<comment> \<open>For this purpose, let us assume variable y to be arbitrary, but fixed.\<close>
      show "fmlookup (min_conc_map\<^sub>\<Sigma> ([x \<longmapsto> Exp e] \<sigma>) n) y = fmlookup (min_conc_map\<^sub>\<Sigma> \<sigma> n) y"
      proof (cases)
      \<comment> \<open>We perform a case distinction over the origin of variable y.\<close>
        assume "y \<in> fmdom'([x \<longmapsto> Exp e] \<sigma>)"
        \<comment> \<open>We first assume y is an element of the domain of the updated version of \<open>\<sigma>\<close>.\<close>
        show "fmlookup (min_conc_map\<^sub>\<Sigma> ([x \<longmapsto> Exp e] \<sigma>) n) y = fmlookup (min_conc_map\<^sub>\<Sigma> \<sigma> n) y"
        proof (cases)
        \<comment> \<open>We then perform another case distinction over y.\<close>
          assume "y = x"
          \<comment> \<open>Let us assume that y is the updated variable \<open>x\<close>.\<close>
          thus "fmlookup (min_conc_map\<^sub>\<Sigma> ([x \<longmapsto> Exp e] \<sigma>) n) y = fmlookup (min_conc_map\<^sub>\<Sigma> \<sigma> n) y"
            using premise symb\<^sub>\<Sigma>_def by auto
          \<comment> \<open>Then the case is simple, as y is non-symbolic and therefore not occurring
             in the minimal concretization mapping. Both lookup values therefore do
             not exist, implying that they match.\<close>
        next
          assume "\<not>y = x"
          \<comment> \<open>We next assume that y is not \<open>x\<close>.\<close>
          thus "fmlookup (min_conc_map\<^sub>\<Sigma> ([x \<longmapsto> Exp e] \<sigma>) n) y = fmlookup (min_conc_map\<^sub>\<Sigma> \<sigma> n) y" by simp
          \<comment> \<open>Then both lookup values match, because the minimal concretization
             mapping of \<open>\<sigma>\<close> is independent of the new update.\<close>
        qed
      next
        assume "\<not>y \<in> fmdom'([x \<longmapsto> Exp e] \<sigma>)"
        \<comment> \<open>We next assume that y is not located in the domain of the updated state \<open>\<sigma>\<close>.\<close>
        thus "fmlookup (min_conc_map\<^sub>\<Sigma> ([x \<longmapsto> Exp e] \<sigma>) n) y = fmlookup (min_conc_map\<^sub>\<Sigma> \<sigma> n) y" by auto
        \<comment> \<open>Then y must map onto None in both minimal concretization mappings, 
           implying that they match. This closes the case.\<close>
      qed
    qed
    thus "min_conc_map\<^sub>\<Sigma> ([x \<longmapsto> Exp e] \<sigma>) n = min_conc_map\<^sub>\<Sigma> \<sigma> n"
      using fmap_ext by blast
    \<comment> \<open>We can then deduce that both states are equivalent using the equivalence
       notion of finite maps. This concludes the~lemma.\<close>
  qed

text \<open>We introduce a helper \<open>simplification of the fmmap_keys function\<close>
  supporting proof derivations of the \<open>input statement\<close>\<close>

lemma fmmap_keys_input: "concrete\<^sub>\<Sigma>(\<sigma>) \<longrightarrow> fmmap_keys (\<lambda>v e. val\<^sub>S e ([Y \<longmapsto> Exp (Num n)] \<circle>)) ([x \<longmapsto> Exp (Var Y)] [Y \<longmapsto> \<^emph>] \<sigma>) = [x \<longmapsto> Exp (Num n)] [Y \<longmapsto> \<^emph>] \<sigma>"
proof (rule impI)
  assume premise: "concrete\<^sub>\<Sigma>(\<sigma>)"
  \<comment> \<open>We assume the premise holds, meaning that \<open>\<sigma>\<close> is concrete\<close>
  have "\<forall>v. fmlookup (fmmap_keys (\<lambda>v e. val\<^sub>S e ([Y \<longmapsto> Exp (Num n)] \<circle>)) ([x \<longmapsto> Exp (Var Y)] [Y \<longmapsto> \<^emph>] \<sigma>)) v = fmlookup ([x \<longmapsto> Exp (Num n)] [Y \<longmapsto> \<^emph>] \<sigma>) v" 
  \<comment> \<open>We prove the equivalence by showing that both states align in the lookups
     of their variables\<close>
  proof (rule allI)
    fix v
    \<comment> \<open>We fix a variable v\<close>
    show "fmlookup (fmmap_keys (\<lambda>v e. val\<^sub>S e ([Y \<longmapsto> Exp (Num n)] \<circle>)) ([x \<longmapsto> Exp (Var Y)] [Y \<longmapsto> \<^emph>] \<sigma>)) v = fmlookup ([x \<longmapsto> Exp (Num n)] [Y \<longmapsto> \<^emph>] \<sigma>) v" 
    proof (cases)
      \<comment> \<open>We perform a case distinction about the origin of v\<close>
      assume dom\<^sub>A: "v \<in> fmdom'([x \<longmapsto> Exp (Num n)] [Y \<longmapsto> \<^emph>] \<sigma>)"
      \<comment> \<open>We first assume v originated from the updated state\<close>
      show ?thesis
      proof (cases)
        \<comment> \<open>We perform a case distinction over the value of v\<close>
        assume "v = Y"
        thus ?thesis by auto
        \<comment> \<open>If v is the symbolic variable Y, the case holds trivially\<close>
      next
        assume "\<not>v = Y"
        hence dom\<^sub>B: "v = x \<or> v \<in> fmdom'(\<sigma>)" using dom\<^sub>A by auto
        \<comment> \<open>If v is not Y, it must either be x or originate from the domain
           of state \<open>\<sigma>\<close>\<close>
        show ?thesis
        proof (cases)
          \<comment> \<open>We perform another case distinction over the origin of v\<close>
          assume "v = x"
          thus ?thesis by auto
          \<comment> \<open>If v is x, the case holds trivially again\<close>
        next
          assume "\<not>v = x"
          hence "v \<in> fmdom'(\<sigma>)" using dom\<^sub>B by auto
          \<comment> \<open>If v is not x, it must originate from the domain of \<open>\<sigma>\<close>\<close>
          thus ?thesis using premise concrete\<^sub>\<Sigma>_def value_pr\<^sub>S
            by (auto; metis (no_types, lifting) fmlookup_dom'_iff map_option_eq_Some option.sel)
          \<comment> \<open>Then the case holds using the concreteness premise\<close>
        qed
      qed
    next
      assume "\<not>v \<in> fmdom'([x \<longmapsto> Exp (Num n)] [Y \<longmapsto> \<^emph>] \<sigma>)"
      \<comment> \<open>We secondly assume that v does not originate from the domain of the
         updated state \<open>\<sigma>\<close>\<close>
      thus ?thesis by auto
      \<comment> \<open>Then the case is again obvious\<close>
    qed
  qed
  thus "fmmap_keys (\<lambda>v e. val\<^sub>S e ([Y \<longmapsto> Exp (Num n)] \<circle>)) ([x \<longmapsto> Exp (Var Y)] [Y \<longmapsto> \<^emph>] \<sigma>) = ([x \<longmapsto> Exp (Num n)] [Y \<longmapsto> \<^emph>] \<sigma>)"
    using fmap_ext by blast
  \<comment> \<open>We then use the definition of the equivalence of finite maps to close the
     theorem\<close>
qed

lemma conc_concrete: "concrete\<^sub>\<Sigma>(\<circle>)"
  by (simp add: concrete\<^sub>\<Sigma>_def)

end