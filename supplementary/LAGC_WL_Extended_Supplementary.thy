theory LAGC_WL_Extended_Supplementary
  imports "../WL_Ext/LAGC_WL_Extended" 
begin

declare basic_successors.simps[simp add]
declare successors\<^sub>1.simps[simp add]
declare successors\<^sub>2.simps[simp add]
declare successors.simps[simp add]
declare composition\<^sub>N.simps[simp add]

text \<open>We introduce a [\<open>Variable Mapping\<close>] for \<open>Continuation Markers Multisets\<close> 
  mapping a continuation marker multiset to all \<open>free variables\<close> occurring in it\<close>
fun
  mset_mvars :: "cont_marker multiset \<Rightarrow> var set" where
  "mset_mvars M = {v. \<exists>m \<in> set_mset M. v \<in> mvars m}" 

text \<open>We prove that all found \<open>parameters\<close> in a given trace must be of \<open>concrete
  and arithmetic\<close> nature\<close>

lemma params_conc: "concrete\<^sub>\<T>(sh) \<and> v \<in> params(sh) \<longrightarrow> concrete\<^sub>E v \<and> (\<exists>a. v = A a)"
proof (induct sh)
  \<comment> \<open>We perform an induction over the trace sh\<close>
  case Epsilon
  thus ?case by simp
  \<comment> \<open>If sh is \<epsilon>, the case is trivial\<close>
next
  case (Transition sh' ta)
  thus ?case
  \<comment> \<open>We now assume sh is (sh' \<leadsto> ta)\<close>
  proof (induct ta)
    \<comment> \<open>We perform an induction over the trace atom ta\<close>
    case (Event ev e)
    \<comment> \<open>We first assume ta is an event\<close>
    show ?case 
    proof (rule impI)
      assume premise: "concrete\<^sub>\<T>(sh' \<leadsto> Event\<llangle>ev, e\<rrangle>) \<and> v \<in> params(sh' \<leadsto> Event\<llangle>ev, e\<rrangle>)"
      \<comment> \<open>We assume the premise holds\<close>
      thus "concrete\<^sub>E v \<and> (\<exists>a. v = A a)"
      proof (induct ev)
        \<comment> \<open>We perform a case distinction over the event ev\<close>
        case inpEv
        thus ?case using Transition.hyps by simp
        \<comment> \<open>If ev is the input event, the case is trivial\<close>
      next
        case invEv
        thus ?case
        \<comment> \<open>We now assume ev is the invocation event\<close>
        proof (cases)
          \<comment> \<open>We perform a case distinction over the content of e\<close>
          assume "\<exists>m a. e = [P m, A a]"
          thus ?case using Transition.hyps invEv.prems by force
          \<comment> \<open>If e is of a specific form, then the case holds\<close>
        next
          assume "\<not>(\<exists>m a. e = [P m, A a])"
          thus ?case using Transition.hyps premise
            by (smt (z3) \<T>.distinct(1) \<T>.inject concrete\<^sub>\<T>.simps(3) params.elims trace_atom.inject(2))
          \<comment> \<open>If e is not of that specific form, the case holds as well\<close>
        qed
      next
        case invREv
        thus ?case using Transition.hyps by simp
        \<comment> \<open>If ev is the invocation reaction event, the case is trivial\<close>
      qed
    qed
  next
    case (State \<sigma>)
    \<comment> \<open>We next assume ta is a state\<close>
    thus ?case by simp
    \<comment> \<open>Then the case is trivial\<close>
  qed
qed

text \<open>We prove that each successor configuration of a configuration \<open>ending
  with a state in its trace\<close>, also ends with a state in its trace\<close>

lemma \<delta>\<^sub>s_last\<^sub>\<Sigma>: "(\<tau>, cm) \<in> \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S]) \<longrightarrow> (\<exists>sh'. \<exists>\<sigma>'. \<tau> = sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>)"
proof (induct S arbitrary: \<tau> cm)
  \<comment> \<open>We perform a case distinction over all statements S\<close>
  case SKIP
  thus ?case by fastforce
  \<comment> \<open>If S is the Skip Statement, the case is trivial\<close>
next
  case (Assign x a)
  thus ?case by fastforce
  \<comment> \<open>If S is the Assign Statement, the case is trivial\<close>
next
  case (If b S)
  thus ?case by fastforce
  \<comment> \<open>If S is the If Branch, the case is trivial\<close>
next
  case (While b S)
  thus ?case by fastforce
  \<comment> \<open>If S is the While Loop, the case is trivial\<close>
next
  case (Seq S\<^sub>1 S\<^sub>2)
  thus ?case by fastforce
  \<comment> \<open>If S is the Sequential Statement, the case is trivial\<close>
next
  case (LocPar S\<^sub>1 S\<^sub>2)
  thus ?case by fastforce
  \<comment> \<open>If S is the Local Parallelism Statement, the case is trivial\<close>
next
  case (LocMem D S)
  thus ?case 
  \<comment> \<open>We now assume S is the Variable Declaration\<close>
  proof (induct D)
    \<comment> \<open>We perform a case distinction over the declration D\<close>
    case Nu
    thus ?case by fastforce
    \<comment> \<open>If D is the empty declaration, the case is trivial\<close>
  next
    case (Declaration x D)
    thus ?case by auto
    \<comment> \<open>If D is a declaration, the case is also trivial\<close>
  qed
next
  case (Input x)
  thus ?case by (simp only: basic_successors.simps; fastforce)
  \<comment> \<open>If S is the Input Statement, the case follows from the successor simplifications\<close>
next
  case (Guard g S)
  thus ?case by fastforce
  \<comment> \<open>If S is the Guard Statement, the case is trivial\<close>
next
  case (Call m a)
  thus ?case by fastforce
  \<comment> \<open>If S is the Call Statement, the case is trivial\<close>
qed

lemma \<delta>\<^sub>1_last\<^sub>\<Sigma>: "(\<tau>, cm) \<in> \<delta>\<^sub>1 (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, q) \<longrightarrow> (\<exists>sh'. \<exists>\<sigma>'. \<tau> = sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>)"
proof (rule impI)
  assume "(\<tau>, cm) \<in> \<delta>\<^sub>1 (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, q)"
  \<comment> \<open>We first assume the premise holds\<close>
  moreover then have "(\<tau>, cm) \<in> \<Union>((%cm. (if cm = \<lambda>[\<nabla>] then {} else (%c. (fst(c), (q - {# cm #}) + {# snd(c) #})) ` (\<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, cm)))) ` set_mset q)"
    by simp
  \<comment> \<open>We can then rewrite the premise with the simplifications of the successor function\<close>
  moreover have "\<forall>cm'. cm' \<in> set_mset q \<and> (\<tau>, cm) \<in> (if cm' = \<lambda>[\<nabla>] then {} else (%c. (fst(c), (q - {# cm' #}) + {# snd(c) #})) ` (\<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, cm'))) \<longrightarrow> (\<exists>sh'. \<exists>\<sigma>'. \<tau> = sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>)"
  \<comment> \<open>We prove an intermediate lemma\<close>
  proof (rule allI)
    fix cm'
    \<comment> \<open>We assume the continuation marker cm' arbitrary, but fixed\<close>
    show "cm' \<in> set_mset q \<and> (\<tau>, cm) \<in> (if cm' = \<lambda>[\<nabla>] then {} else (%c. (fst(c), (q - {# cm' #}) + {# snd(c) #})) ` (\<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, cm'))) \<longrightarrow> (\<exists>sh'. \<exists>\<sigma>'. \<tau> = sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>)"
    proof (induct cm')
      \<comment> \<open>We perform a case distinction of the continuation marker cm'\<close>
      case Empty
      thus ?case by simp
      \<comment> \<open>If cm is the empty continuation marker, the case is trivial, as the premise
         of the implication is False\<close>
    next
      case (Lambda S)
      thus ?case
      \<comment> \<open>We now assume cm is not the empty continuation marker\<close>
      proof (rule impI)
        assume "\<lambda>[S] \<in> set_mset q \<and> (\<tau>, cm) \<in> (if \<lambda>[S] = \<lambda>[\<nabla>] then {} else (%c. (fst(c), (q - {# \<lambda>[S] #}) + {# snd(c) #})) ` (\<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S])))"
        \<comment> \<open>We then assume the premise holds\<close>
        hence "\<lambda>[S] \<in> set_mset q \<and> (\<tau>, cm) \<in> (%c. (fst(c), (q - {# \<lambda>[S] #}) + {# snd(c) #})) ` (\<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S]))"
          by (meson cont_marker.distinct(1))
        \<comment> \<open>We can then modify the premise, because cm' is the non-empty
           continuation marker\<close>
        thus "\<exists>sh'. \<exists>\<sigma>'. \<tau> = sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>"
          using \<delta>\<^sub>s_last\<^sub>\<Sigma> by force
        \<comment> \<open>This concludes the intermediate thesis via the theorem about the 
           simple successor function\<close>
      qed
    qed
  qed
  ultimately show "\<exists>sh'. \<exists>\<sigma>'. \<tau> = sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>"
    by blast
  \<comment> \<open>This proves the thesis in question\<close>
qed

lemma \<delta>\<^sub>2_last\<^sub>\<Sigma>: "(\<tau>, cm) \<in> \<delta>\<^sub>2 M (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, q) \<longrightarrow> (\<exists>sh'. \<exists>\<sigma>'. \<tau> = sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>)"
  by auto

lemma \<delta>_last\<^sub>\<Sigma>: "(\<tau>, cm) \<in> \<delta> M (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, q) \<longrightarrow> (\<exists>sh'. \<exists>\<sigma>'. \<tau> = sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>)"
  using \<delta>\<^sub>1_last\<^sub>\<Sigma> \<delta>\<^sub>2_last\<^sub>\<Sigma> by auto

text \<open>We prove that the \<open>domains of the last state in the traces are
   extensive\<close> during program execution\<close>

lemma \<delta>\<^sub>s_dom_ext: "concrete\<^sub>\<T>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<and> vars\<^sub>s(S) \<subseteq> fmdom'(\<sigma>) \<and> (sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>, cm) \<in> \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S]) \<longrightarrow> fmdom'(\<sigma>) \<subseteq> fmdom'(\<sigma>')"
proof (induct S arbitrary: sh' \<sigma>' sh \<sigma> cm)
  \<comment> \<open>We perform an induction over the statement S\<close>
  case SKIP
  show ?case using \<delta>\<^sub>s_Skip by fastforce
  \<comment> \<open>If S is the Skip statement, the case follows from our simplifications\<close>
next
  case (Assign x a)
  show ?case using \<delta>\<^sub>s_Assign by fastforce
  \<comment> \<open>If S is the Assign statement, the case follows from our simplifications\<close>
next
  case (If b S)
  show ?case using \<delta>\<^sub>s_If\<^sub>T \<delta>\<^sub>s_If\<^sub>F consistent_negate\<^sub>B
    by (metis concrete\<^sub>\<T>.simps(2) dual_order.eq_iff last\<^sub>\<T>.simps(1) le_supE prod.inject singletonD vars\<^sub>s.simps(3))
  \<comment> \<open>If S is the If Branch, the case follows from our simplifications\<close>
next
  case (While b S)
  show ?case using \<delta>\<^sub>s_While\<^sub>T \<delta>\<^sub>s_While\<^sub>F consistent_negate\<^sub>B
    by (metis concrete\<^sub>\<T>.simps(2) dual_order.eq_iff last\<^sub>\<T>.simps(1) le_supE prod.inject singletonD vars\<^sub>s.simps(4))
  \<comment> \<open>If S is the While Loop, the case follows from our simplifications\<close>
next
  case (Seq S\<^sub>1 S\<^sub>2)
  show ?case 
  \<comment> \<open>We now assume S is the Sequential statement\<close>
  proof (rule impI)
    assume premise: "concrete\<^sub>\<T>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<and> vars\<^sub>s(S\<^sub>1;;S\<^sub>2) \<subseteq> fmdom'(\<sigma>) \<and> (sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>, cm) \<in> \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1;;S\<^sub>2])"
    \<comment> \<open>We now assume our premise holds\<close>
    hence "(sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>, cm) \<in> (%c. ((fst c), cont_append (snd c) S\<^sub>2)) ` \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1])"
      using \<delta>\<^sub>s_Seq by blast
    \<comment> \<open>We then rewrite the premise with our simplifications\<close>
    moreover then have "\<forall>sh'' \<sigma>'' cm'. (sh'' \<leadsto> State\<llangle>\<sigma>''\<rrangle>, cm') \<in> \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1]) \<longrightarrow> fmdom'(\<sigma>) \<subseteq> fmdom'(\<sigma>'')"
      using premise by (metis (no_types, lifting) Seq.hyps(1) Un_subset_iff vars\<^sub>s.simps(5))
    \<comment> \<open>Using the premise and the induction hypothesis, we can conclude the induction
       step\<close>
    ultimately show "fmdom'(\<sigma>) \<subseteq> fmdom'(\<sigma>')"
      by (smt (verit) fst_conv image_iff prod.collapse)
    \<comment> \<open>This concludes the sequential case\<close>
  qed
next
  case (LocPar S\<^sub>1 S\<^sub>2)
  show ?case 
  \<comment> \<open>We now assume S is the Local Parallelism statement\<close>
  proof (rule impI)
    assume premise: "concrete\<^sub>\<T>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<and> vars\<^sub>s(CO S\<^sub>1 \<parallel> S\<^sub>2 OC) \<subseteq> fmdom'(\<sigma>) \<and> (sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>, cm) \<in> \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[CO S\<^sub>1 \<parallel> S\<^sub>2 OC])"
    \<comment> \<open>We now assume our premise holds\<close>
    hence "(sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>, cm) \<in> ((%c. (fst(c), parallel (snd c) \<lambda>[S\<^sub>2])) ` \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1])) \<union> ((%c. (fst(c), parallel \<lambda>[S\<^sub>1] (snd c))) ` \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>2]))"
      using \<delta>\<^sub>s_LocPar by blast
    \<comment> \<open>We then rewrite the premise with our simplifications\<close>
    moreover then have "\<forall>sh'' \<sigma>'' cm'. (sh'' \<leadsto> State\<llangle>\<sigma>''\<rrangle>, cm') \<in> \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1]) \<longrightarrow> fmdom'(\<sigma>) \<subseteq> fmdom'(\<sigma>'')"
      using premise by (metis (mono_tags, lifting) LocPar.hyps(1) Un_subset_iff vars\<^sub>s.simps(6))
    \<comment> \<open>Using the premise and the induction hypothesis, we can conclude the induction
       step for S1\<close>
    moreover then have "\<forall>sh'' \<sigma>'' cm'. (sh'' \<leadsto> State\<llangle>\<sigma>''\<rrangle>, cm') \<in> \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>2]) \<longrightarrow> fmdom'(\<sigma>) \<subseteq> fmdom'(\<sigma>'')"
      using premise by (metis (mono_tags, lifting) LocPar.hyps(2) Un_subset_iff vars\<^sub>s.simps(6))
    \<comment> \<open>Using the premise and the induction hypothesis, we can conclude the induction
       step for S2\<close>
    ultimately show "fmdom'(\<sigma>) \<subseteq> fmdom'(\<sigma>')"
      by (smt (verit, ccfv_threshold) Pair_inject Un_iff image_iff prod.collapse)
    \<comment> \<open>This concludes the case for the local parallelism\<close>
  qed
next
  case (LocMem D S)
  show ?case 
  \<comment> \<open>We now assume S is the variable declaration\<close>
  proof (induct D)
    \<comment> \<open>We perform an induction over the declaration\<close>
    case Nu
    thus ?case 
    \<comment> \<open>We first assume D is the empty declaration\<close>
    proof (rule impI)
      assume premise: "concrete\<^sub>\<T>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<and> vars\<^sub>s(LocMem \<nu> S) \<subseteq> fmdom'(\<sigma>) \<and> (sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>, cm) \<in> \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[LocMem \<nu> S])"
      \<comment> \<open>We now assume our premise holds\<close>
      hence "(sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>, cm) \<in> \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S])"
        using \<delta>\<^sub>s_Scope\<^sub>A by simp
      \<comment> \<open>We then rewrite the premise with our simplifications\<close>
      moreover have "\<forall>sh'' \<sigma>'' cm'. concrete\<^sub>\<T>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<and> vars\<^sub>s(S) \<subseteq> fmdom'(\<sigma>) \<and> (sh'' \<leadsto> State\<llangle>\<sigma>''\<rrangle>, cm') \<in> \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S]) \<longrightarrow> fmdom'(\<sigma>) \<subseteq> fmdom'(\<sigma>'')"
        using LocMem.hyps by meson
      \<comment> \<open>Using the premise and the induction hypothesis, we can denote the 
         induction step\<close>
      moreover then have "\<forall>sh'' \<sigma>'' cm'. (sh'' \<leadsto> State\<llangle>\<sigma>''\<rrangle>, cm') \<in> \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S]) \<longrightarrow> fmdom'(\<sigma>) \<subseteq> fmdom'(\<sigma>'')"
        using premise by simp
      \<comment> \<open>The induction step can then be concludes with the premise\<close>
      ultimately show "fmdom'(\<sigma>) \<subseteq> fmdom'(\<sigma>')"
        by blast
      \<comment> \<open>This concludes the case for the variable declaration\<close>
    qed
  next
    case (Declaration x D)
    thus ?case using \<delta>\<^sub>s_Scope\<^sub>B 
      by (smt (z3) Pair_inject \<T>.inject fmdom'_fmupd insertCI singletonD subsetI trace_atom.inject(1))
    \<comment> \<open>If D is not the empty declaration, the case follows from our simplifications\<close>
  qed
next
  case (Input x)
  thus ?case 
  \<comment> \<open>We next assume S is the Input statement\<close>
  proof (rule impI)
    assume premise: "concrete\<^sub>\<T>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<and> vars\<^sub>s(Input x) \<subseteq> fmdom'(\<sigma>) \<and> (sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>, cm) \<in> \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[Input x])"
    \<comment> \<open>We now assume our premise holds\<close>
    hence "(sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>, cm) \<in> {(trace_conc ([(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> Exp (Num 0)] \<circle>) ((((sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<leadsto> State\<llangle>[x \<longmapsto> Exp (Var (vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')))] [(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> \<^emph>] \<sigma>\<rrangle>) \<leadsto> Event\<llangle>inpEv, [A (Var (vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')))]\<rrangle>) \<leadsto> State\<llangle>[x \<longmapsto> Exp (Var (vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')))] [(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> \<^emph>] \<sigma>\<rrangle>), \<lambda>[\<nabla>])}"
      using \<delta>\<^sub>s_Input vargen_distinct by blast
    \<comment> \<open>We then rewrite the premise with our simplifications\<close>
    moreover have "fmdom'(\<sigma>) \<subseteq> fmdom'(([(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> Exp (Num 0)] \<circle>) \<bullet> ([x \<longmapsto> Exp (Var (vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')))] [(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> \<^emph>] \<sigma>))"
    \<comment> \<open>We still have to prove that the domain of \<sigma> is a subset of the domain
       of the updated state via the concretization mapping\<close>
    proof -
      have "fmdom'(\<sigma>) \<subseteq> fmdom'([x \<longmapsto> Exp (Var (vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')))] [(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> \<^emph>] \<sigma>)" by auto
      \<comment> \<open>We can conclude that the domain of \<sigma> is a subset of the domain
         of the updated state\<close>
      thus ?thesis using conc_map_dom_ext\<^sub>\<Sigma> by blast
      \<comment> \<open>Then it must also be a subset of the domain of the concretization
         of the updated state using an earlier theorem\<close>
    qed
    ultimately show "fmdom'(\<sigma>) \<subseteq> fmdom'(\<sigma>')" by simp
    \<comment> \<open>Using this information, we can conclude the case for the input statement\<close>
  qed
next
  case (Guard g S)
  thus ?case using \<delta>\<^sub>s_Guard\<^sub>T \<delta>\<^sub>s_Guard\<^sub>F consistent_negate\<^sub>B
    by (metis Set.set_insert Un_subset_iff concrete\<^sub>\<T>.simps(2) dual_order.eq_iff insert_not_empty last\<^sub>\<T>.simps(1) prod.inject singletonD vars\<^sub>s.simps(9))
    \<comment> \<open>If S is the Guarded statement, the case follows from our simplifications\<close>
next
  case (Call m a)
  thus ?case using \<delta>\<^sub>s_Call 
    by (metis Pair_inject \<T>.inject dual_order.eq_iff singletonD trace_atom.inject(1) vars\<^sub>s.simps(10))
    \<comment> \<open>If S is the Call statement, the case follows from our simplifications\<close>
qed

text \<open>We prove the \<open>concreteness of the traces generated by the successor
  function\<close>. This implies that all traces generated by the successor function
  preserve concreteness\<close>

lemma \<delta>\<^sub>s_concrete_pr: "concrete\<^sub>\<T>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<and> vars\<^sub>s(S) \<subseteq> fmdom'(\<sigma>) \<and> (\<forall>v'. ''$BOUND_EXCEEDED::'' @ v' \<notin> fmdom'(\<sigma>))
      \<and> (\<forall>x. sh \<diamondop>\<diamondop>\<^sub>\<T> {(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input''))}) \<longrightarrow> (\<forall>c \<in> (\<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S])). concrete\<^sub>\<T>(fst(c)))"
proof (induct S arbitrary: sh \<sigma>)
  \<comment> \<open>We perform an induction over the statements S\<close>
  case SKIP
  thus ?case by (simp add: \<delta>\<^sub>s_Skip del: basic_successors.simps)
  \<comment> \<open>If S is the Skip statement, the case follows from the simplifications\<close>
next
  case (Assign x a)
  thus ?case by (simp add: \<delta>\<^sub>s_Assign concrete_imp\<^sub>A concrete_upd_pr\<^sub>A del: basic_successors.simps)
  \<comment> \<open>If S is the Assign statement, the case follows from the simplifications and
     the corresponding concreteness preservations theorems\<close>
next
  case (If b S)
  show ?case
  \<comment> \<open>We now assume S is the If branch\<close>
  proof (rule impI)
    assume premise: "concrete\<^sub>\<T>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<and> vars\<^sub>s(IF b THEN S FI) \<subseteq> fmdom'(\<sigma>) \<and> (\<forall>v'. ''$BOUND_EXCEEDED::'' @ v' \<notin> fmdom'(\<sigma>)) \<and> (\<forall>x. sh \<diamondop>\<diamondop>\<^sub>\<T> {(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input''))})"
    \<comment> \<open>We assume the premise holds\<close>
    thus "\<forall>c \<in> (\<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[IF b THEN S FI])). concrete\<^sub>\<T>(fst(c))"
    proof (cases)
      \<comment> \<open>We perform a case distinction over the guard\<close>
      assume "consistent {(val\<^sub>B b \<sigma>)}"
      \<comment> \<open>We first assume the guard is true\<close>
      thus "\<forall>c \<in> (\<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[IF b THEN S FI])). concrete\<^sub>\<T>(fst(c))"
        using premise by (simp add: \<delta>\<^sub>s_If\<^sub>T concrete_imp\<^sub>B del: basic_successors.simps)
      \<comment> \<open>Then the case can be closed with the premise, the simplifications and 
         the corresponding concreteness preservation theorems\<close>
    next
      assume "\<not>consistent {(val\<^sub>B b \<sigma>)}"
      \<comment> \<open>We next assume the guard is not true\<close>
      hence "consistent {(val\<^sub>B (not b) \<sigma>)}"
        using premise consistent_negate\<^sub>A by simp
      \<comment> \<open>The negated boolean condition must then be consistent\<close>
      thus "\<forall>c \<in> (\<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[IF b THEN S FI])). concrete\<^sub>\<T>(fst(c))"
        using premise by (simp add: \<delta>\<^sub>s_If\<^sub>F del: basic_successors.simps) 
      \<comment> \<open>Then the case can be closed with the premise, the simplifications and 
         the corresponding concreteness preservation theorems\<close>
    qed
  qed
next
  case (While b S)
  show ?case 
  \<comment> \<open>We now assume S is the While loop\<close>
  proof (rule impI)
    assume premise: "concrete\<^sub>\<T>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<and> vars\<^sub>s(WHILE b DO S OD) \<subseteq> fmdom'(\<sigma>) \<and> (\<forall>v'. ''$BOUND_EXCEEDED::'' @ v' \<notin> fmdom'(\<sigma>)) \<and> (\<forall>x. sh \<diamondop>\<diamondop>\<^sub>\<T> {(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input''))})"
    \<comment> \<open>We assume the premise holds\<close>
    thus "\<forall>c \<in> (\<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[WHILE b DO S OD])). concrete\<^sub>\<T>(fst(c))"
    proof (cases)
      \<comment> \<open>We perform a case distinction over the guard\<close>
      assume "consistent {(val\<^sub>B b \<sigma>)}"
      \<comment> \<open>We first assume the guard is true\<close>
      thus "\<forall>c \<in> (\<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[WHILE b DO S OD])). concrete\<^sub>\<T>(fst(c))"
        using premise by (simp add: \<delta>\<^sub>s_While\<^sub>T concrete_imp\<^sub>B del: basic_successors.simps)
      \<comment> \<open>Then the case can be closed with the premise, the simplifications and 
         the corresponding concreteness preservation theorems\<close>
    next
      assume "\<not>consistent {(val\<^sub>B b \<sigma>)}"
      \<comment> \<open>We next assume the guard is not true\<close>
      hence "consistent {(val\<^sub>B (not b) \<sigma>)}"
        using premise consistent_negate\<^sub>A by simp
      \<comment> \<open>The negated boolean condition must then be consistent\<close>
      thus "\<forall>c \<in> (\<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[WHILE b DO S OD])). concrete\<^sub>\<T>(fst(c))"
        using premise by (simp add: \<delta>\<^sub>s_While\<^sub>F del: basic_successors.simps) 
      \<comment> \<open>Then the case can be closed with the premise, the simplifications and 
         the corresponding concreteness preservation theorems\<close>
    qed
  qed
next
  case (Seq S\<^sub>1 S\<^sub>2)
  thus ?case by auto
  \<comment> \<open>If S is the Sequential statement, the case follows from the induction 
     hypothesis\<close>
next
  case (LocPar S\<^sub>1 S\<^sub>2)
  thus ?case by auto
  \<comment> \<open>If S is the Local Parallelism statement, the case follows from the
     induction hypothesis\<close>
next
  case (LocMem D S)
  thus ?case 
  \<comment> \<open>We now assume S is the Variable Declaration statement\<close>
  proof (induct D)
    \<comment> \<open>We perform a case distinction over the variable declaration D\<close>
    case Nu
    thus ?case by (simp add: \<delta>\<^sub>s_Scope\<^sub>A del: basic_successors.simps)
    \<comment> \<open>If D is the empty declaration, the case follows from the simplifications\<close>
  next
    case (Declaration x D)
    thus ?case by (simp add: \<delta>\<^sub>s_Scope\<^sub>B concrete\<^sub>\<Sigma>_def concrete_upd_pr\<^sub>A del: basic_successors.simps)
    \<comment> \<open>If D is not the empty declaration, the case follows from the simplifications
       and the corresponding concreteness preservation theorems\<close>
  qed
next
  case (Input x)
  thus ?case 
  \<comment> \<open>We next assume S in the Input statement\<close>
  proof (rule impI)
    assume premise: "concrete\<^sub>\<T>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<and> vars\<^sub>s(INPUT x) \<subseteq> fmdom'(\<sigma>) \<and> (\<forall>v'. ''$BOUND_EXCEEDED::'' @ v' \<notin> fmdom'(\<sigma>)) \<and> (\<forall>x. sh \<diamondop>\<diamondop>\<^sub>\<T> {(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input''))})"
    \<comment> \<open>We assume the premise holds\<close>
    have "\<forall>c. c \<in> (\<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[INPUT x])) \<longrightarrow> concrete\<^sub>\<T>(fst(c))"
    \<comment> \<open>We will next prove that all successor configurations have a concrete trace\<close>
    proof (rule allI)
      fix c
      \<comment> \<open>We assume configuration c is arbitrary, but fixed\<close>
      show "c \<in> (\<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[INPUT x])) \<longrightarrow> concrete\<^sub>\<T>(fst(c))"
      proof (rule impI)
        assume "c \<in> (\<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[INPUT x]))"
        \<comment> \<open>We assume c is a successor configuration of the input statement\<close>
        hence "c = (trace_conc ([(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> Exp (Num 0)] \<circle>) ((((sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<leadsto> State\<llangle>[x \<longmapsto> Exp (Var (vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')))] [(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> \<^emph>] \<sigma>\<rrangle>) \<leadsto> Event\<llangle>inpEv, [A (Var (vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')))]\<rrangle>) \<leadsto> State\<llangle>[x \<longmapsto> Exp (Var (vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')))] [(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> \<^emph>] \<sigma>\<rrangle>), \<lambda>[\<nabla>])"
          using premise vargen_distinct by (simp add: \<delta>\<^sub>s_Input del: basic_successors.simps)
        \<comment> \<open>We can use our simplifications to deduce that c must be a tuple
           containing of a concretized trace and the empty continuation marker\<close>
        moreover have "concrete\<^sub>\<T> (trace_conc ([(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> Exp (Num 0)] \<circle>) ((((sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<leadsto> State\<llangle>[x \<longmapsto> Exp (Var (vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')))] [(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> \<^emph>] \<sigma>\<rrangle>) \<leadsto> Event\<llangle>inpEv, [A (Var (vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')))]\<rrangle>) \<leadsto> State\<llangle>[x \<longmapsto> Exp (Var (vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')))] [(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> \<^emph>] \<sigma>\<rrangle>))"
        \<comment> \<open>It is still left to prove that the trace is of concrete nature after
           the concretization\<close>
        proof -
          have symbolic: "symb\<^sub>\<T> ((((sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<leadsto> State\<llangle>[x \<longmapsto> Exp (Var (vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')))] [(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> \<^emph>] \<sigma>\<rrangle>) \<leadsto> Event\<llangle>inpEv, [A (Var (vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')))]\<rrangle>) \<leadsto> State\<llangle>[x \<longmapsto> Exp (Var (vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')))] [(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> \<^emph>] \<sigma>\<rrangle>) = {(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input''))}"
            using premise symb\<^sub>\<Sigma>_def concrete\<^sub>\<Sigma>_def concrete_symb_imp\<^sub>\<T> vargen_distinct by auto
          \<comment> \<open>We first establish that the only symbolic variable of the given trace is
             the newly generated variable from the vargen function using the premise\<close>
          moreover have "((((sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<leadsto> State\<llangle>[x \<longmapsto> Exp (Var (vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')))] [(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> \<^emph>] \<sigma>\<rrangle>) \<leadsto> Event\<llangle>inpEv, [A (Var (vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')))]\<rrangle>) \<leadsto> State\<llangle>[x \<longmapsto> Exp (Var (vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')))] [(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> \<^emph>] \<sigma>\<rrangle>) \<subseteq>\<^sub>E {(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input''))}"
            using premise conc_events_imp\<^sub>\<T> by simp
          \<comment> \<open>We prove the 1st requirement, stating that all events in a given 
             trace must only have symbolic variables of the trace by using the
             premise and an earlier theorem\<close>
          moreover have "wf_states\<^sub>\<T> ((((sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<leadsto> State\<llangle>[x \<longmapsto> Exp (Var (vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')))] [(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> \<^emph>] \<sigma>\<rrangle>) \<leadsto> Event\<llangle>inpEv, [A (Var (vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')))]\<rrangle>) \<leadsto> State\<llangle>[x \<longmapsto> Exp (Var (vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')))] [(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> \<^emph>] \<sigma>\<rrangle>)"
          \<comment> \<open>We prove the 2nd requirement, stating that all states in the given 
             trace must be of wellformed nature\<close>
          proof -
            have "wf_states\<^sub>\<T> sh"
              using premise conc_wf_imp\<^sub>\<T> by simp
            \<comment> \<open>We know that sh must be wellformed, because it is concrete\<close>
            moreover have hypo: "wf\<^sub>\<Sigma> \<sigma>"
              using premise concrete_wf_imp\<^sub>\<Sigma> by simp
            \<comment> \<open>We additionally know that \<sigma> must be concrete due to the same reason\<close>
            then have "wf\<^sub>\<Sigma> ([x \<longmapsto> Exp (Var (vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')))] [(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> \<^emph>] \<sigma>)"
            \<comment> \<open>It is still necessary to establish that the modified state is also
               of wellformed nature\<close>
            proof -
              have "symb\<^sub>\<Sigma>(\<sigma>) = {}"
                using premise concrete_symb_imp\<^sub>\<Sigma> by simp
              \<comment> \<open>We know that \<sigma> contains no symbolic variables due to the premise\<close>
              moreover then have "symb\<^sub>\<Sigma> ([x \<longmapsto> Exp (Var (vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')))] [(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> \<^emph>] \<sigma>) = {(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input''))}" 
                using premise concrete_symb_imp\<^sub>\<T> symbolic by auto
              \<comment> \<open>We can then conclude that the symbolic variables of the modified
                 trace must be the singleton set containing the newly generated 
                 variable\<close>
              ultimately show ?thesis
                using premise hypo wf\<^sub>\<Sigma>_def by simp
              \<comment> \<open>Using the info that \<sigma> is wellformed, we can now infer that the
                 modified state must also be wellformed\<close>
            qed
            ultimately show ?thesis
              by (metis conc_wf_imp\<^sub>\<T> premise wf_states\<^sub>\<T>.simps(2) wf_states\<^sub>\<T>.simps(3))
            \<comment> \<open>Because all states of the trace are wellformed, the 2nd proposition 
               is also guaranteed to hold\<close>
          qed
          moreover have "is_conc_map\<^sub>\<T> ([(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> Exp (Num 0)] \<circle>) ((((sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<leadsto> State\<llangle>[x \<longmapsto> Exp (Var (vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')))] [(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> \<^emph>] \<sigma>\<rrangle>) \<leadsto> Event\<llangle>inpEv, [A (Var (vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')))]\<rrangle>) \<leadsto> State\<llangle>[x \<longmapsto> Exp (Var (vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')))] [(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> \<^emph>] \<sigma>\<rrangle>)"
          \<comment> \<open>We prove the 3rd proposition, showing that the given concretization
             mapping is a valid concretization mapping for the given trace\<close>
          proof -
            have conc\<^sub>\<sigma>: "is_conc_map\<^sub>\<Sigma> ([(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> Exp (Num 0)] \<circle>) \<sigma>"
            \<comment> \<open>We first prove that the given concretization mapping is a valid
               concretization mapping for the state \<sigma>\<close>
            proof -
              have "concrete\<^sub>\<Sigma> ([(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> Exp (Num 0)] \<circle>)"
                using concrete\<^sub>\<Sigma>_def by simp
              \<comment> \<open>We know that the concretization mapping is concrete\<close>
              moreover have "symb\<^sub>\<Sigma>(\<sigma>) = {}"
                using premise concrete_symb_imp\<^sub>\<T> by (metis Un_empty symb\<^sub>\<T>.simps(3))
              \<comment> \<open>We also know that \<sigma> contains no symbolic variables due to the
                 concreteness property of the premise\<close>
              moreover have "fmdom'(\<sigma>) \<inter> {(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input''))} = {}"
                using premise vargen_dom by simp
              \<comment> \<open>Additionally we know that the domain of \<sigma> intersected with
                 the newly generated variable is the emptyset due to the definition
                 of the variable generation\<close>
              ultimately show ?thesis 
                using is_conc_map\<^sub>\<Sigma>_def by simp
              \<comment> \<open>This proves that the given concretization mapping is a valid
                 concretization mapping for \<sigma>\<close>
            qed
            have conc\<^sub>\<sigma>': "is_conc_map\<^sub>\<Sigma> ([(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> Exp (Num 0)] \<circle>) ([x \<longmapsto> Exp (Var (vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')))] [(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> \<^emph>] \<sigma>)"
            \<comment> \<open>We next prove that the concretization mapping is also a valid
               concretization mapping for the updated state\<close>
            proof -
              have "symb\<^sub>\<T>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) = {}"
                using premise concrete_symb_imp\<^sub>\<T> by blast
              \<comment> \<open>Due to the premise, we can infer that the trace prefix contains
                 no symbolic variables whatsoever\<close>
              hence "symb\<^sub>\<Sigma> ([x \<longmapsto> Exp (Var (vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')))] [(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> \<^emph>] \<sigma>) = {(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input''))}" 
                using symbolic by auto
              \<comment> \<open>Due to that the symbolic variables of the modified state must
                 only include the newly generated variable\<close>
              thus ?thesis
                using is_conc_map\<^sub>\<Sigma>_def conc\<^sub>\<sigma> by auto
              \<comment> \<open>Using the knowledge that the concretization mapping is a valid
                 concretization mapping for \<sigma>, the case can now be closed\<close>
            qed
            have "concrete\<^sub>\<T>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<and> (\<forall>x. sh \<diamondop>\<diamondop>\<^sub>\<T> {(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input''))}) \<longrightarrow> is_conc_map\<^sub>\<T> ([(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> Exp (Num 0)] \<circle>) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>)"
            proof (induct sh)
              \<comment> \<open>We perform an induction over the shining trace sh\<close>
              case Epsilon
              thus ?case using is_conc_map\<^sub>\<Sigma>_def concrete\<^sub>\<Sigma>_def conc\<^sub>\<sigma> by auto
              \<comment> \<open>If sh is \<epsilon>, the case can be closed using the information that
                 the concretization mapping is concrete and the previous theorem\<close>
            next
              case (Transition sh' ta)
              thus ?case
              \<comment> \<open>We next assume sh is (sh' \<leadsto> ta)\<close>
              proof (induct ta)
                \<comment> \<open>We perform an induction over the trace atom ta\<close>
                case (Event ev e)
                \<comment> \<open>We first assume ta is an event\<close>
                thus ?case by auto
                \<comment> \<open>The case can then be trivially closed using the induction
                   hypothesis\<close>
              next
                case (State \<sigma>')
                show ?case
                \<comment> \<open>We next assume ta is a state \<sigma>'\<close>
                proof (rule impI)
                  assume assumption: "concrete\<^sub>\<T>((sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>) \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<and> (\<forall>x. (sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>) \<diamondop>\<diamondop>\<^sub>\<T> {(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input''))})"
                  \<comment> \<open>We assume the premise holds, hence the trace is concrete\<close>
                  hence "is_conc_map\<^sub>\<T> ([(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> Exp (Num 0)] \<circle>) sh'"
                    using Transition.hyps by simp
                  \<comment> \<open>Using the induction hypothesis, we know that the concretization
                     mapping is a valid concretization mapping for sh'\<close>
                  moreover have "is_conc_map\<^sub>\<Sigma> ([(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> Exp (Num 0)] \<circle>) \<sigma>'"
                  \<comment> \<open>We still need to show that the concretization mapping is a 
                     valid concretization mapping for \<sigma>' as well\<close>
                  proof -
                    have "concrete\<^sub>\<Sigma> ([(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> Exp (Num 0)] \<circle>)"
                      using concrete\<^sub>\<Sigma>_def by simp
                    \<comment> \<open>We know that the concretization mapping is concrete\<close>
                    moreover have "fmdom'(\<sigma>') \<inter> fmdom'([(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> Exp (Num 0)] \<circle>) = symb\<^sub>\<Sigma>(\<sigma>')"
                      using assumption concrete_symb_imp\<^sub>\<Sigma> by auto
                    \<comment> \<open>The domains of \<sigma>' and the concretization mapping are also
                       disjunct due to the premise\<close>
                    ultimately show ?thesis 
                      using is_conc_map\<^sub>\<Sigma>_def by auto
                    \<comment> \<open>Using these two propositions, we can conclude that the
                       cocnretization mapping is a valid concretization mapping for \<sigma>'\<close>
                  qed
                  ultimately show "is_conc_map\<^sub>\<T> ([(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> Exp (Num 0)] \<circle>) ((sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>) \<leadsto> State\<llangle>\<sigma>\<rrangle>)" 
                    using conc\<^sub>\<sigma> is_conc_map\<^sub>\<T>.simps(3) by blast
                  \<comment> \<open>Using that information, we can finally conclude that the
                     concretization mapping is a valid concretization mapping for
                     the whole prefix trace sh\<close>
                qed
              qed
            qed
            hence conc\<^sub>s\<^sub>h: "is_conc_map\<^sub>\<T> ([(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> Exp (Num 0)] \<circle>) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>)"
              using premise by simp      
            \<comment> \<open>Last, but not least, we show that the given concretization
               mapping is a valid concretization mapping for the prefix trace sh
               using the previous theorem and the premise\<close>
            show ?thesis using conc\<^sub>\<sigma>' conc\<^sub>s\<^sub>h by auto
            \<comment> \<open>Given both proofs, it has been shown that the concretization
               mapping is a valid concretization mapping for the whole trace\<close>
          qed
          ultimately show "concrete\<^sub>\<T>(trace_conc ([(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> Exp (Num 0)] \<circle>) ((((sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<leadsto> State\<llangle>[x \<longmapsto> Exp (Var (vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')))] [(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> \<^emph>] \<sigma>\<rrangle>) \<leadsto> Event\<llangle>inpEv, [A (Var (vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')))]\<rrangle>) \<leadsto> State\<llangle>[x \<longmapsto> Exp (Var (vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')))] [(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> \<^emph>] \<sigma>\<rrangle>))"
            using wf_conc_imp\<^sub>\<T> by presburger
          \<comment> \<open>As all 3 propositions hold, we can guarantee the concreteness of the
             given trace with an earlier theorem\<close>
        qed
        ultimately show "concrete\<^sub>\<T>(fst(c))" by simp
        \<comment> \<open>Hence the trace of c must be of concrete nature\<close>
      qed
    qed
    thus "\<forall>c \<in> (\<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[INPUT x])). concrete\<^sub>\<T>(fst(c))" by blast
    \<comment> \<open>Thus the case holds\<close>
  qed
next
  case (Guard g S)
  show ?case 
  \<comment> \<open>We next assume S is the guard statement\<close>
  proof (rule impI)
    assume premise: "concrete\<^sub>\<T>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<and> vars\<^sub>s(:: g;; S END) \<subseteq> fmdom'(\<sigma>) \<and> (\<forall>v'. ''$BOUND_EXCEEDED::'' @ v' \<notin> fmdom'(\<sigma>)) \<and> (\<forall>x. sh \<diamondop>\<diamondop>\<^sub>\<T> {(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input''))})"
    \<comment> \<open>We assume the premise holds\<close>
    thus "\<forall>c \<in> (\<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[:: g;; S END])). concrete\<^sub>\<T>(fst(c))"
    proof (cases)
      \<comment> \<open>We perform a case distinction over the consistency of the guard g\<close>
      assume "consistent {(val\<^sub>B g \<sigma>)}"
      \<comment> \<open>We first assume the guard is true\<close>
      thus "\<forall>c \<in> (\<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[:: g;; S END])). concrete\<^sub>\<T>(fst(c))"
        using premise by (simp add: \<delta>\<^sub>s_Guard\<^sub>T concrete_imp\<^sub>B del: basic_successors.simps)
      \<comment> \<open>Then the case can be closed with the premise, the simplifications and 
         the corresponding concreteness preservation theorems\<close>
    next
      assume "\<not>consistent {(val\<^sub>B g \<sigma>)}"
      \<comment> \<open>We next assume the guard is not true\<close>
      hence "consistent {(val\<^sub>B (not g) \<sigma>)}"
        using premise consistent_negate\<^sub>A by simp
      \<comment> \<open>The negated boolean condition must then be consistent\<close>
      thus "\<forall>c \<in> (\<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[:: g;; S END])). concrete\<^sub>\<T>(fst(c))"
        using premise by (simp add: \<delta>\<^sub>s_Guard\<^sub>F del: basic_successors.simps) 
      \<comment> \<open>Then the case can be closed with the premise, the simplifications and 
         the corresponding concreteness preservation theorems\<close>
    qed
  qed
next
  case (Call m a)
  thus ?case by (simp add: \<delta>\<^sub>s_Call concrete_imp\<^sub>A del: basic_successors.simps)
  \<comment> \<open>If S is the Call statement, the case follows from the simplifications
     and the corresponding concreteness preservation theorems\<close>
qed

lemma \<delta>\<^sub>1_concrete_pr: "concrete\<^sub>\<T>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<and> mset_mvars(q) \<subseteq> fmdom'(\<sigma>) \<and> (\<forall>v'. ''$BOUND_EXCEEDED::'' @ v' \<notin> fmdom'(\<sigma>))
      \<and> (\<forall>x. sh \<diamondop>\<diamondop>\<^sub>\<T> {(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input''))}) \<longrightarrow> (\<forall>c \<in> (\<delta>\<^sub>1 (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, q)). concrete\<^sub>\<T>(fst(c)))"
proof (rule impI)
  assume premise: "concrete\<^sub>\<T>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<and> mset_mvars(q) \<subseteq> fmdom'(\<sigma>) \<and> (\<forall>v'. ''$BOUND_EXCEEDED::'' @ v' \<notin> fmdom'(\<sigma>)) \<and> (\<forall>x. sh \<diamondop>\<diamondop>\<^sub>\<T> {(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input''))})"
  \<comment> \<open>We assume our premise holds\<close>
  have "\<forall>c. c \<in> (\<delta>\<^sub>1 (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, q)) \<longrightarrow> concrete\<^sub>\<T>(fst(c))"
  \<comment> \<open>We then prove an intermediate theorem\<close>
  proof (rule allI)
    fix c
    \<comment> \<open>We assume configuration c is arbitrary, but fixed\<close>
    show "c \<in> (\<delta>\<^sub>1 (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, q)) \<longrightarrow> concrete\<^sub>\<T>(fst(c))"
    proof (rule impI)
      assume "c \<in> (\<delta>\<^sub>1 (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, q))"
      \<comment> \<open>We assume the premise holds\<close>
      moreover then have "c \<in> \<Union>((%cm. (if cm = \<lambda>[\<nabla>] then {} else (%c. (fst(c), (q - {# cm #}) + {# snd(c) #})) ` (\<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, cm)))) ` set_mset q)"
        by simp
      \<comment> \<open>We can then rewrite the premise with the simplifications of the successor function\<close>
      moreover have "\<forall>cm'. cm' \<in> set_mset q \<and> c \<in> (if cm' = \<lambda>[\<nabla>] then {} else (%c. (fst(c), (q - {# cm' #}) + {# snd(c) #})) ` (\<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, cm'))) \<and> mvars(cm') \<subseteq> fmdom'(\<sigma>) \<and> (\<forall>v'. ''$BOUND_EXCEEDED::'' @ v' \<notin> fmdom'(\<sigma>)) \<and> (\<forall>x. sh \<diamondop>\<diamondop>\<^sub>\<T> {(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input''))}) \<longrightarrow> concrete\<^sub>\<T>(fst(c))"
       \<comment> \<open>We prove another intermediate lemma\<close>
      proof (rule allI)
        fix cm'
        \<comment> \<open>We assume the continuation marker cm' arbitrary, but fixed\<close>
        show "cm' \<in> set_mset q \<and> c \<in> (if cm' = \<lambda>[\<nabla>] then {} else (%c. (fst(c), (q - {# cm' #}) + {# snd(c) #})) ` (\<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, cm'))) \<and> mvars(cm') \<subseteq> fmdom'(\<sigma>) \<and> (\<forall>v'. ''$BOUND_EXCEEDED::'' @ v' \<notin> fmdom'(\<sigma>)) \<and> (\<forall>x. sh \<diamondop>\<diamondop>\<^sub>\<T> {(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input''))}) \<longrightarrow> concrete\<^sub>\<T>(fst(c))"
        proof (induct cm')
          \<comment> \<open>We perform a case distinction of the continuation marker cm'\<close>
          case Empty
          thus ?case by simp
          \<comment> \<open>If cm is the empty continuation marker, the case is trivial, as the premise
             of the implication is False\<close>
        next
          case (Lambda S')
          thus ?case
          \<comment> \<open>We now assume cm is not the empty continuation marker\<close>
          proof (rule impI)
            assume "\<lambda>[S'] \<in> set_mset q \<and> c \<in> (if \<lambda>[S'] = \<lambda>[\<nabla>] then {} else (%c. (fst(c), (q - {# \<lambda>[S'] #}) + {# snd(c) #})) ` (\<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S']))) \<and> mvars(\<lambda>[S']) \<subseteq> fmdom'(\<sigma>) \<and> (\<forall>v'. ''$BOUND_EXCEEDED::'' @ v' \<notin> fmdom'(\<sigma>)) \<and> (\<forall>x. sh \<diamondop>\<diamondop>\<^sub>\<T> {(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input''))})"
            \<comment> \<open>We then assume the premise holds\<close>
            moreover then have "vars\<^sub>s(S') \<subseteq> fmdom'(\<sigma>) \<and> \<lambda>[S'] \<in> set_mset q \<and> c \<in> (%c. (fst(c), (q - {# \<lambda>[S'] #}) + {# snd(c) #})) ` (\<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S']))"
              by (simp; meson cont_marker.distinct(1))
            \<comment> \<open>We can then modify the premise, because cm' is the non-empty
               continuation marker\<close>
            ultimately show "concrete\<^sub>\<T>(fst(c))"
              using premise \<delta>\<^sub>s_concrete_pr by auto
            \<comment> \<open>Using our earlier theorem and our premise, we can now infer the
               concreteness of the trace of configuration c\<close>
          qed
        qed
      qed
      ultimately show "concrete\<^sub>\<T>(fst(c))" 
        using premise by fastforce
      \<comment> \<open>We can then conclude that the trace of the configuration must be concrete\<close>
    qed
  qed
  thus "\<forall>c \<in> (\<delta>\<^sub>1 (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, q)). concrete\<^sub>\<T>(fst(c))" by auto
  \<comment> \<open>Thus we can deduce the thesis\<close>
qed

lemma \<delta>\<^sub>2_concrete_pr: "concrete\<^sub>\<T>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<and> mset_mvars(q) \<subseteq> fmdom'(\<sigma>) \<longrightarrow> (\<forall>c \<in> (\<delta>\<^sub>2 M (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, q)). concrete\<^sub>\<T>(fst(c)))"
proof (rule impI)
  assume premise: "concrete\<^sub>\<T>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<and> mset_mvars(q) \<subseteq> fmdom'(\<sigma>)"
  \<comment> \<open>We assume our premise holds\<close>
  have "\<forall>c. c \<in> (\<delta>\<^sub>2 M (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, q)) \<longrightarrow> concrete\<^sub>\<T>(fst(c))"
  \<comment> \<open>We then prove an intermediate theorem\<close>
  proof (rule allI)
    fix c
    \<comment> \<open>We assume configuration c is arbitrary, but fixed\<close>
    show "c \<in> (\<delta>\<^sub>2 M (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, q)) \<longrightarrow> concrete\<^sub>\<T>(fst(c))"
    proof (rule impI)
      assume "c \<in> (\<delta>\<^sub>2 M (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, q))"
      \<comment> \<open>We assume the premise holds\<close>
      hence "\<exists>(m, v) \<in> Set.filter (%(m, v). wellformed (sh \<cdot> (gen_event invREv \<sigma> [P (\<Up>\<^sub>n m), v]))) (M \<times> params(sh)). c = (((sh \<cdot> (gen_event invREv \<sigma> [P (\<Up>\<^sub>n m), v]))  \<leadsto> State\<llangle>[(vargen \<sigma> 0 100 (''$'' @ (\<Up>\<^sub>n m) @ ''::Param'')) \<longmapsto> Exp (proj\<^sub>A v)] \<sigma>\<rrangle>), q + {# \<lambda>[(\<Up>\<^sub>s m) [(\<Up>\<^sub>v m) \<leftarrow>\<^sub>s (vargen \<sigma> 0 100 (''$'' @ (\<Up>\<^sub>n m) @ ''::Param''))] ] #})"
        by auto
      \<comment> \<open>We then rewrite the premise using the definition of the successor function\<close>
      then obtain m v where vals: "c = (((sh \<cdot> (gen_event invREv \<sigma> [P (\<Up>\<^sub>n m), v]))  \<leadsto> State\<llangle>[(vargen \<sigma> 0 100 (''$'' @ (\<Up>\<^sub>n m) @ ''::Param'')) \<longmapsto> Exp (proj\<^sub>A v)] \<sigma>\<rrangle>), q + {# \<lambda>[(\<Up>\<^sub>s m) [(\<Up>\<^sub>v m) \<leftarrow>\<^sub>s (vargen \<sigma> 0 100 (''$'' @ (\<Up>\<^sub>n m) @ ''::Param''))] ] #}) \<and> (m, v) \<in> (M \<times> params(sh))"
        by auto
      \<comment> \<open>We then obtain those values\<close>
      hence conc\<^sub>v: "concrete\<^sub>E v \<and> (\<exists>a. v = A a)"
        using premise params_conc by auto
      \<comment> \<open>Using an earlier theorem, we can conclude that v must be of concrete nature\<close>
      have crux: "fst(c) = (sh \<cdot> (gen_event invREv \<sigma> [P (\<Up>\<^sub>n m), v])) \<leadsto> State\<llangle>[(vargen \<sigma> 0 100 (''$'' @ (\<Up>\<^sub>n m) @ ''::Param'')) \<longmapsto> Exp (proj\<^sub>A v)] \<sigma>\<rrangle>"
        using vals by auto
      \<comment> \<open>As only the trace is of interest, we focus on the trace on the first element
         of the configuration\<close>
      thus "concrete\<^sub>\<T>(fst(c))"
      \<comment> \<open>We still need to prove that this trace is concrete\<close>
      proof -
        have "concrete\<^sub>\<T>(sh) \<and> concrete\<^sub>\<Sigma>(\<sigma>)" 
          using premise by simp
        \<comment> \<open>We know that sh and \<sigma> are concrete respectively due to the premise\<close>
        moreover then have "concrete\<^sub>\<T>(gen_event invREv \<sigma> [P (\<Up>\<^sub>n m), v])"
          using conc\<^sub>v concrete_pr\<^sub>E by simp
        \<comment> \<open>Given that v is concrete, we can also conclude that the inserted
           event must also be concrete\<close>
        moreover have "concrete\<^sub>\<Sigma>([(vargen \<sigma> 0 100 (''$'' @ (\<Up>\<^sub>n m) @ ''::Param'')) \<longmapsto> Exp (proj\<^sub>A v)] \<sigma>)"
          using premise concrete_upd_pr\<^sub>A conc\<^sub>v by auto
        \<comment> \<open>Using several already proven theorems, the updated state must hence
           also be concrete\<close>
        ultimately show ?thesis 
          using crux by simp
        \<comment> \<open>Combing this information, we conclude that the whole trace must be
           of concrete nature\<close>
      qed
    qed
  qed
  thus "\<forall>c \<in> (\<delta>\<^sub>2 M (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, q)). concrete\<^sub>\<T>(fst(c))" by blast
  \<comment> \<open>Thus we can deduce the thesis\<close>
qed

lemma \<delta>_concrete_pr: "concrete\<^sub>\<T>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<and> mset_mvars(q) \<subseteq> fmdom'(\<sigma>) \<and> (\<forall>v'. ''$BOUND_EXCEEDED::'' @ v' \<notin> fmdom'(\<sigma>))
      \<and> (\<forall>x. sh \<diamondop>\<diamondop>\<^sub>\<T> {(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input''))}) \<longrightarrow> (\<forall>c \<in> (\<delta> M (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, q)). concrete\<^sub>\<T>(fst(c)))"
proof (rule impI)
  assume premise: "concrete\<^sub>\<T>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<and> mset_mvars(q) \<subseteq> fmdom'(\<sigma>) \<and> (\<forall>v'. ''$BOUND_EXCEEDED::'' @ v' \<notin> fmdom'(\<sigma>)) \<and> (\<forall>x. sh \<diamondop>\<diamondop>\<^sub>\<T> {(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input''))})"
  \<comment> \<open>We assume the premise holds\<close>
  have "\<forall>c. c \<in> (\<delta> M (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, q)) \<longrightarrow> concrete\<^sub>\<T>(fst(c))"
  \<comment> \<open>We prove an intermediate theorem\<close>
  proof (rule allI)
    fix c
    \<comment> \<open>We assume configuration c is arbitrary, but fixed\<close>
    show "c \<in> (\<delta> M (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, q)) \<longrightarrow> concrete\<^sub>\<T>(fst(c))"
    proof (rule impI)
      assume "c \<in> (\<delta> M (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, q))"
      \<comment> \<open>We assume c is a successor configuration of \<delta>\<close>
      hence "c \<in> \<delta>\<^sub>1 (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, q) \<or> c \<in> \<delta>\<^sub>2 M (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, q)"
        by simp
      \<comment> \<open>Hence it must either be a successor configuration of \<delta>1 or \<delta>2\<close>
      thus "concrete\<^sub>\<T>(fst(c))"
      proof (rule disjE)
        \<comment> \<open>We perform a case distinction over both cases\<close>
        assume "c \<in> \<delta>\<^sub>1 (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, q)"
        thus "concrete\<^sub>\<T>(fst(c))" 
          using premise \<delta>\<^sub>1_concrete_pr by blast
        \<comment> \<open>If c is a successor configuration of \<delta>1, the case follows from the
           premise and an earlier theorem\<close>
      next
        assume "c \<in> \<delta>\<^sub>2 M (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, q)"
        thus "concrete\<^sub>\<T>(fst(c))" 
          using premise \<delta>\<^sub>2_concrete_pr by blast
        \<comment> \<open>If c is a successor configuration of \<delta>2, the case follows from the
           premise and an earlier theorem as well\<close>
      qed
    qed
  qed
  thus "\<forall>c \<in> (\<delta> M (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, q)). concrete\<^sub>\<T>(fst(c))" by blast
  \<comment> \<open>We can then rewrite the intermediate theorem to deduce the thesis\<close>
qed

text \<open>We prove that each composition function output with a configuration \<open>ending
  with a state in its trace\<close> as its input, also ends with a state in its trace\<close>

lemma \<Delta>\<^sub>N_end: "(\<tau>, q') \<in> \<Delta>\<^sub>N n M (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, q) \<longrightarrow> (\<exists>sh'. \<exists>\<sigma>'. \<tau> = sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>)"
proof (induct n arbitrary: sh \<sigma> q)
  \<comment> \<open>We perform an induction over n\<close>
  case 0 
  thus ?case by simp
  \<comment> \<open>If n is 0, the case is trivial\<close>
next
  case (Suc n)
  show ?case
  \<comment> \<open>We now assume n is not 0\<close>
  proof (cases)
    \<comment> \<open>We perform a case distinction over the guard\<close>
    assume "\<delta> M (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, q) = {}"
    thus ?case by simp
    \<comment> \<open>If we terminate, the case is trivial\<close>
  next
    assume premise: "\<not>\<delta> M (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, q) = {}"
    show ?case
    \<comment> \<open>We now assume the guard evaluates to false\<close>
    proof (rule impI)
      assume "(\<tau>, q') \<in> \<Delta>\<^sub>N (Suc n) M (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, q)"
      \<comment> \<open>We assume the premise holds\<close>
      hence "(\<tau>, q') \<in> \<Union>((%c. \<Delta>\<^sub>N n M c) ` \<delta> M (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, q))"
        using premise by (metis composition\<^sub>N.simps(2))
      \<comment> \<open>We then rewrite the premise with the definition of the stepwise
         composition function\<close>
      hence "\<exists>sh' \<sigma>' q''. (sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>, q'') \<in> \<delta> M (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, q) \<and> (\<tau>, q') \<in> \<Delta>\<^sub>N n M (sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>, q'')"
        using \<delta>_last\<^sub>\<Sigma> by fast
      \<comment> \<open>Because of an earlier theorem, we can conclude that all traces generated
         by the \<delta> function end with a state, also the one in question\<close>
      thus "\<exists>sh'. \<exists>\<sigma>'. \<tau> = sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>"
        using Suc.hyps by blast
      \<comment> \<open>Then we can conclude the thesis with the induction hypothesis\<close>
    qed
  qed
qed

lemma Tr\<^sub>N_end: "\<tau> \<in> Tr\<^sub>N (Program M \<lbrace> S \<rbrace>) \<sigma> n \<longrightarrow> (\<exists>sh'. \<exists>\<sigma>'. \<tau> = sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>)"
  using \<Delta>\<^sub>N_end by auto


end