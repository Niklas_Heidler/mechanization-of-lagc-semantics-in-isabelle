theory LAGC_WL_Supplementary
  imports "../WL/LAGC_WL"
begin

declare successors.simps[simp add]
declare composition\<^sub>N.simps[simp add]

text \<open>Let us assume \<open>\<sigma>\<close> to be an arbitrary concrete state and \<open>S\<close> to be an 
  arbitrary statement consisting only of variables from the domain of \<open>\<sigma>\<close>. We
  can now prove that all continuation traces generated from \<open>S\<close> called in state \<open>\<sigma>\<close> 
  are of concrete~nature.\<close>
  context notes [simp] = symb\<^sub>\<Sigma>_def concrete\<^sub>\<Sigma>_def wf\<^sub>\<pi>_def concrete\<^sub>\<pi>_def begin
  lemma val_concrete:
    shows "concrete\<^sub>\<Sigma>(\<sigma>) \<and> vars(S) \<subseteq> fmdom'(\<sigma>) \<longrightarrow> (\<forall>cont \<in> (val S \<sigma>). concrete\<^sub>\<pi>(\<down>\<^sub>p cont \<triangleright> \<down>\<^sub>\<tau> cont))"
  proof (induct S)
  \<comment> \<open>We prove the theorem via structural induction over~statement~S.\<close>
    case SKIP
    show ?case using concrete_wf_imp\<^sub>\<Sigma> by auto
    \<comment> \<open>If S is the Skip statement, the concreteness of the conditioned symbolic
       trace trivially holds, as the only occurring state \<open>\<sigma>\<close> is guaranteed to be
       concrete. Note that we additionally have to use the \<open>concrete-wf-imp\<^sub>\<Sigma>\<close>
       lemma to prove the wellformedness of~the~states.\<close>
  next
    case (Assign x a)
    show ?case using concrete_wf_imp\<^sub>\<Sigma> concrete_imp\<^sub>A by auto
    \<comment> \<open>If S is an assignment, we can use the previously established \<open>concrete-imp\<^sub>A\<close> 
       theorem to prove that the contained arithmetic expression becomes concrete
       after one evaluation step under \<open>\<sigma>\<close>, thus preserving the concreteness
       of the~trace.\<close>
  next
    case (If b S)
    show ?case using concrete_wf_imp\<^sub>\<Sigma> concrete_imp\<^sub>B concrete_vars_imp\<^sub>B by auto
    \<comment> \<open>If S is the conditional statement, we utilize the \<open>concrete-imp\<^sub>B\<close>
       theorem to prove that the Boolean guard turns concrete after being evaluated
       under \<open>\<sigma>\<close>. Using \<open>concrete-vars-imp\<^sub>B\<close>, we can then show that the updated guard 
       won't contain any variables, thus trivially proving the second 
       wellformedness~condition.\<close>
  next
    case (While b S)
    show ?case using concrete_wf_imp\<^sub>\<Sigma> concrete_imp\<^sub>B concrete_vars_imp\<^sub>B by auto
    \<comment> \<open>The proof for the While loop is analogous to the proof of the conditional 
       statement.\<close>
  next
    case (Seq S\<^sub>1 S\<^sub>2)
    thus ?case by auto
    \<comment> \<open>If S is the sequential statement, the case can be closed via the 
       induction~hypothesis.\<close>
  qed
  end

text \<open>We prove the \<open>finiteness of the successor function\<close>. Hence, we only have
  finite successors for any configuration\<close>

lemma \<delta>_finite: "finite (\<delta> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S]))"
  using \<delta>_Seq by (induct S; auto)

text \<open>We prove the \<open>determinism of the successor function\<close>. This implies that
  there is at most one successor configuration for any configuration in WL\<close>

lemma \<delta>_determinism: "card (\<delta> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S])) \<le> 1"
proof (induct S)
  \<comment> \<open>We perform an induction over all statements\<close>
  case SKIP
  thus ?case using \<delta>_Skip by simp
  \<comment> \<open>In the case of SKIP, we use \<delta>_Skip to conclude on the determinism\<close>
next
  case (Assign x a)
  thus ?case using \<delta>_Assign by simp
  \<comment> \<open>In the case of Assign, we use \<delta>_Assign to conclude on the determinism\<close>
next
  case (If b S)
  thus ?case
  proof (cases)
    \<comment> \<open>We perform a case distinction based on if the Boolean guard can be
       fully evaluated\<close>
    assume "consistent {(val\<^sub>B b \<sigma>)} \<or> consistent {(val\<^sub>B (Not b) \<sigma>)}"
    thus ?case using \<delta>_If\<^sub>T \<delta>_If\<^sub>F by (metis eq_iff is_singletonI is_singleton_altdef)
    \<comment> \<open>If it can be fully evaluated, we conclude on the determinism of the
       If statement using \<delta>_If\<close>
  next
    assume "\<not>(consistent {(val\<^sub>B b \<sigma>)} \<or> consistent {(val\<^sub>B (Not b) \<sigma>)})"
    thus ?case using \<delta>_If\<^sub>E by (metis card.empty zero_le_one)
    \<comment> \<open>If it is not fully evaluated, the resulting set is empty, hence the
       determinism is also guaranteed\<close>
  qed
next
  case (While b S)
  thus ?case 
  proof (cases)
    \<comment> \<open>We perform a case distinction based on if the Boolean guard can be
       fully evaluated\<close>
    assume "consistent {(val\<^sub>B b \<sigma>)} \<or> consistent {(val\<^sub>B (Not b) \<sigma>)}"
    thus ?case using \<delta>_While\<^sub>T \<delta>_While\<^sub>F by (metis eq_iff is_singletonI is_singleton_altdef)
    \<comment> \<open>If it can be fully evaluated, we conclude on the determinism of the
       While loop using \<delta>_While\<close>
  next
    assume "\<not>(consistent {(val\<^sub>B b \<sigma>)} \<or> consistent {(val\<^sub>B (Not b) \<sigma>)})"
    thus ?case using \<delta>_While\<^sub>E by (metis card.empty zero_le_one)
    \<comment> \<open>If it is not fully evaluated, the resulting set is empty, hence the
       determinism is also guaranteed\<close>
  qed
next
  case (Seq S\<^sub>1 S\<^sub>2)
  thus ?case using \<delta>_Seq \<delta>_finite Seq.hyps 
    by (metis (no_types, lifting) card_image_le dual_order.trans)
  \<comment> \<open>We know S1 evaluates deterministically. We can also conclude from
     the finiteness of the \<delta> successors that the image of the successors
     of S1 must also be less equal 1. Thus \<delta>_Seq proves the determinism\<close>
qed

text \<open>We prove the \<open>concreteness of the traces generated by the successor
  function\<close>. This implies that all traces generated by the successor function
  preserve concreteness\<close>

lemma \<delta>_concrete_pr: "concrete\<^sub>\<T>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<and> vars(S) \<subseteq> fmdom'(\<sigma>) 
      \<longrightarrow> (\<forall>c \<in> (\<delta> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S])). concrete\<^sub>\<T>(fst(c)))"
proof (induct S)
  \<comment> \<open>We perform a structural induction over all statements\<close>
  case SKIP
  thus ?case by simp
  \<comment> \<open>If it is a SKIP statement, the case is trivial\<close>
next
  case (Assign x a)
  thus ?case using concrete_imp\<^sub>A concrete_upd_pr\<^sub>A by auto
  \<comment> \<open>If it is an Assign statement, the case can be concluded by realizing
     that the update of the state stays concrete due a evaluating to a concrete
     arithmetic expression\<close>
next
  case (If b S)
  thus ?case using concrete_imp\<^sub>B by auto
  \<comment> \<open>If it is a If statement, the guard evaluates to a concrete value, thus
     the path condition is fully evaluated, closing this case\<close>
next
  case (While b S)
  thus ?case using concrete_imp\<^sub>B by auto
  \<comment> \<open>If it is a While loop, the guard evaluates to a concrete value, thus
     the path condition is fully evaluated, closing this case\<close>
next
  case (Seq S\<^sub>1 S\<^sub>2)
  thus ?case by auto
  \<comment> \<open>The sequential case immediately follows from the induction hypothesis\<close>
qed

text \<open>We prove that each successor configuration of a configuration \<open>ending
  with a state in its trace\<close>, also ends with a state in its trace\<close>

lemma \<delta>\<^sub>\<Sigma>: "(\<tau>, cm) \<in> \<delta> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S]) \<longrightarrow> (\<exists>sh'. \<exists>\<sigma>'. \<tau> = sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>)"
  by (induct S arbitrary: \<tau> cm; fastforce)

text \<open>We prove that the \<open>variables in the continuation marker of a configuration
  are reductive\<close> during program execution\<close>

lemma var_red: "(\<tau>, cm) \<in> \<delta> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S]) \<longrightarrow> mvars(cm) \<subseteq> mvars(\<lambda>[S])"
proof (induct S arbitrary: \<tau> cm)
  \<comment> \<open>We perform an induction over the WL statements\<close>
  case SKIP
  show ?case by auto
  \<comment> \<open>In the case of the SKIP command, the proof is trivial\<close>
next
  case (Assign x a)
  show ?case by auto
  \<comment> \<open>In the case of the Assign command, the proof is trivial\<close>
next
  case (If b S)
  show ?case by auto
  \<comment> \<open>In the case of the If Branch, the proof is trivial\<close>
next
  case (While b S)
  show ?case by auto
  \<comment> \<open>In the case of the While loop, the proof is trivial\<close>
next
  case (Seq S\<^sub>1 S\<^sub>2)
  thus ?case
  proof (induct cm)
  \<comment> \<open>In the case of the Sequential command, we perform an induction over
     the continuation marker cm\<close>
    case Empty
    show ?case by simp
    \<comment> \<open>If the continuation marker is empty, the case is trivial\<close>
  next
    case (Lambda S)
    show ?case
    \<comment> \<open>We now assume there is still a statement left to execute\<close>
    proof (rule impI)
      assume "(\<tau>, \<lambda>[S]) \<in> \<delta> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1;;S\<^sub>2])"
      \<comment> \<open>We assume the premise holds\<close>
      hence "(\<tau>, \<lambda>[S]) \<in> (%c. ((fst c), cont_append (snd c) S\<^sub>2)) ` \<delta> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1])"
        using \<delta>_Seq by simp
      \<comment> \<open>We then use the \<delta>_Seq simplification\<close>
      hence "(\<exists>S\<^sub>1'. \<exists>\<tau>'. S = S\<^sub>1';;S\<^sub>2 \<and> (\<tau>', \<lambda>[S\<^sub>1']) \<in> \<delta> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1])) \<or> S = S\<^sub>2"
        by (smt (verit) prod.collapse cont_append.elims cont_marker.inject imageE snd_conv)
      \<comment> \<open>This mean that S1 either directly terminates or that it does not terminate
         and a statement S1' is still left to execute before S2\<close>
      thus "mvars(\<lambda>[S]) \<subseteq> mvars(\<lambda>[S\<^sub>1;;S\<^sub>2])"
      proof (rule disjE)
        \<comment> \<open>We perform a case distinction over those two cases\<close>
        assume "S = S\<^sub>2"
        \<comment> \<open>We first assume S1 managed to terminate in one step\<close>
        thus "mvars(\<lambda>[S]) \<subseteq> mvars(\<lambda>[S\<^sub>1;;S\<^sub>2])" by simp
        \<comment> \<open>Then the thesis is obvious\<close>
      next
        assume "\<exists>S\<^sub>1'. \<exists>\<tau>'. S = S\<^sub>1';;S\<^sub>2 \<and> (\<tau>', \<lambda>[S\<^sub>1']) \<in> \<delta> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1])"
        \<comment> \<open>We next assume S1 did not manage to terminate in one step\<close>
        thus "mvars(\<lambda>[S]) \<subseteq> mvars(\<lambda>[S\<^sub>1;;S\<^sub>2])" 
          using Seq.hyps by (metis mvars.simps(2) subset_refl sup_mono vars.simps(5))
        \<comment> \<open>Then the thesis holds via the induction hypothesis\<close>
      qed
    qed
  qed
qed

text \<open>We prove that the \<open>domains of the last state in the traces are
   extensive\<close> during program execution\<close>

lemma dom_ext: "(sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>, cm) \<in> \<delta> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S]) \<longrightarrow> fmdom'(\<sigma>) \<subseteq> fmdom'(\<sigma>')"
proof (induct S arbitrary: sh' \<sigma>' sh \<sigma> cm)
  \<comment> \<open>We perform an induction over the WL statements\<close>
  case SKIP
  show ?case by auto
  \<comment> \<open>In the case of the SKIP command, the proof is trivial\<close>
next
  case (Assign x a)
  show ?case by auto
  \<comment> \<open>In the case of the Assign command, the proof is trivial\<close>
next
  case (If b S)
  show ?case by auto
  \<comment> \<open>In the case of the If Branch, the proof is trivial\<close>
next
  case (While b S)
  show ?case by auto
  \<comment> \<open>In the case of the While loop, the proof is trivial\<close>
next
  case (Seq S\<^sub>1 S\<^sub>2)
  show ?case
  \<comment> \<open>We now assume we have a sequential statement\<close>
  proof (rule impI)
    assume "(sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>, cm) \<in> \<delta> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1;;S\<^sub>2])"
    \<comment> \<open>We assume the premise holds\<close>
    hence "(sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>, cm) \<in> (%c. ((fst c), cont_append (snd c) S\<^sub>2)) ` \<delta> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1])"
      using \<delta>_Seq by simp
    \<comment> \<open>We then rewrite using the \<delta>_Seq lemma\<close>
    thus "fmdom'(\<sigma>) \<subseteq> fmdom'(\<sigma>')"
      using Seq.hyps by (smt (verit) fst_conv image_iff prod.collapse)
    \<comment> \<open>Which concludes the thesis using the induction hypothesis\<close>
  qed
qed

text \<open>We prove that the \<open>configurations always change\<close> when executing the
  successors function. Hence the successors are always different from the
  current configuration\<close>

lemma \<delta>_no_stutter:
  "\<forall>c \<in> \<delta> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S]). c \<noteq> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S])"
proof (induct S arbitrary: sh \<sigma>)
  \<comment> \<open>We perform an induction on statements S\<close>
  case SKIP
  show ?case using \<delta>_Skip by auto
  \<comment> \<open>In the case of SKIP, the proof is trivial\<close>
next
  case (Assign x a)
  show ?case using \<delta>_Assign by auto
  \<comment> \<open>In the case of Assign, the proof is trivial\<close>
next
  case (If b S)
  show ?case using If.hyps by auto
  \<comment> \<open>In the case of If, the proof is trivial via the induction hypothesis\<close>
next
  case (While b S)
  show ?case using While.hyps by auto
  \<comment> \<open>In the case of While, the proof is trivial via the induction hypothesis\<close>
next
  case (Seq S\<^sub>1 S\<^sub>2)
  show ?case using Seq.hyps \<delta>_Seq 
    by (smt (verit) cont_append.elims cont_marker.inject fst_eqD image_iff prod_eqI snd_eqD stmt.inject(4))
  \<comment> \<open>In the case of Seq, we use the \<delta>_Seq simplification and the induction
     hypothesis to close the proof\<close>
qed

text \<open>We prove the \<open>finiteness of bounded LAGC WL Traces\<close>\<close>

lemma \<Delta>\<^sub>N_finite: 
  shows "finite (\<Delta>\<^sub>N n (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, cm))"
proof (induct n arbitrary: sh \<sigma> cm)
  \<comment> \<open>We perform a case distinction over n\<close>
  case 0
  show ?case by simp
  \<comment> \<open>If n is 0, the case is trivial\<close>
next
  case (Suc n)
  show ?case
  \<comment> \<open>We assume n is not 0\<close>
  proof (induct cm)
    \<comment> \<open>We perform a case distinction over the continuation marker\<close>
    case Empty
    \<comment> \<open>We first assume it is the empty continuation marker\<close>
    show ?case by simp
    \<comment> \<open>Then the case is trivial\<close>
  next
    case (Lambda S)
    \<comment> \<open>We next assume the continuation marker is not empty\<close>
    have "finite (\<delta>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S]))"
      using \<delta>_finite by simp
    \<comment> \<open>We know the output of the \<delta> function is finite\<close>
    moreover then have "finite (\<Union>((%c. \<Delta>\<^sub>N n c) ` \<delta>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S])))"
      using Suc.hyps \<delta>\<^sub>\<Sigma> by (smt (verit) finite_UN_I image_iff successors.simps(1))
    \<comment> \<open>Moreover the image application will preserve the finiteness\<close>
    ultimately show "finite (\<Delta>\<^sub>N (Suc n) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S]))"
      by simp
    \<comment> \<open>Thus concludes the thesis\<close>
  qed
qed

lemma Tr\<^sub>N_finite: 
  shows "finite (Tr\<^sub>N S \<sigma> n)"
  using \<Delta>\<^sub>N_finite by (simp add: Traces\<^sub>N_def)

text \<open>We prove the \<open>determinism of the bounded LAGC WL semantics\<close>, showing that 
  there is at most one trace that can be generated using the trace composition\<close>

lemma \<Delta>\<^sub>N_determinism:
  shows "card (\<Delta>\<^sub>N n (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, cm)) \<le> 1" 
proof (induct n arbitrary: cm sh \<sigma>)
  \<comment> \<open>We perform an induction over natural numbers\<close>
  case 0
  \<comment> \<open>We first assume n is 0\<close>
  thus ?case by simp
  \<comment> \<open>Then the output is (sh \<leadsto> \<sigma>), hence the case is trivial\<close>
next
  case (Suc n)
  \<comment> \<open>We now assume n is not 0\<close>
  show "card (\<Delta>\<^sub>N (Suc n) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, cm)) \<le> 1"
  proof (induct cm)
  \<comment> \<open>We perform a case distinction on the continuation marker\<close>
    case Empty
    show ?case by simp
    \<comment> \<open>If it is the empty continuation marker, the case is also trivial\<close>
  next
    case (Lambda S)
    thus ?case
    \<comment> \<open>We now assume cm is a continuation marker of the form \<lambda>[S]\<close>
    proof (cases)
      \<comment> \<open>We perform a case distinction on the cardinality of the
         configurations of (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S])\<close>
      assume "card (\<delta>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S])) = 0"
      \<comment> \<open>We first assume the cardinality to be 0\<close>
      hence "\<delta>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S]) = {}"
        using \<delta>_finite by simp
      \<comment> \<open>Because the successors must be finite, we have no successor configuration\<close>
      hence "card (\<Delta>\<^sub>N (Suc n) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S])) = 0"
        using composition\<^sub>N.simps(3) by (metis Union_empty card.infinite card_0_eq image_is_empty)
      \<comment> \<open>Thus the cardinality of the application of composition function 
         must also be 0\<close>
      thus "card (\<Delta>\<^sub>N (Suc n) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S])) \<le> 1"
        by simp
      \<comment> \<open>Hence we can conclude that the cardinality is lower 1\<close>
    next
      assume "\<not>card (\<delta>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S])) = 0"
      \<comment> \<open>We now assume the cardinality of the successors is not 0\<close>
      hence "card (\<delta>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S])) = 1"
        using \<delta>_determinism by (metis One_nat_def bot_nat_0.extremum_uniqueI le_SucE)
      \<comment> \<open>Because of the \<delta> determinism, the cardinality must hence be 1\<close>
      hence "\<exists>sh'. \<exists>\<sigma>'. \<exists>cm'. \<delta>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S]) = {(sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>, cm')}"
        using \<delta>\<^sub>\<Sigma> by (metis card.empty card_1_singletonE card_mono finite.emptyI insertE not_one_le_zero subrelI)
      \<comment> \<open>Hence we can conclude that there must exist a new trace and continuation
         marker, which is the output of the composition function\<close>
      then obtain sh' cm' \<sigma>' where vals: "\<delta>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S]) = {(sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>, cm')}"
        by auto
      \<comment> \<open>We then obtain those values\<close>
      moreover have "card (\<Delta>\<^sub>N n (sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>, cm')) \<le> 1"
        using Suc.hyps by simp
      \<comment> \<open>Due to the induction hypothesis, the cardinality of the successor
         configuration on composition must be lower equal 1\<close>
      ultimately show "card (\<Delta>\<^sub>N (Suc n) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S])) \<le> 1"
        by simp
      \<comment> \<open>Thus the cardinality of the application of composition function 
         must also be lower equal 1\<close>
    qed
  qed
qed

lemma Tr\<^sub>N_determinism:
  shows "card (Tr\<^sub>N S \<sigma> n) \<le> 1"
  using Traces\<^sub>N_def \<Delta>\<^sub>N_determinism \<Delta>\<^sub>N_finite
  by (metis finite_if_finite_subsets_card_bdd le_trans surj_card_le)

text \<open>We prove the \<open>concreteness of traces of the bounded LAGC WL semantics\<close>,
  showing that there is at most one trace that can be generated using the 
  trace composition\<close>

lemma \<Delta>\<^sub>N_concrete: 
  shows "concrete\<^sub>\<T>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<and> mvars(cm) \<subseteq> fmdom'(\<sigma>) \<longrightarrow> (\<forall>c \<in> (\<Delta>\<^sub>N n (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, cm)). concrete\<^sub>\<T>(fst(c)))"
proof (induct n arbitrary: sh \<sigma> cm)
  \<comment> \<open>We perform an induction over all natural numbers n\<close>
  case 0
  show ?case by simp
  \<comment> \<open>If n is 0, the case is trivial\<close>
next
  case (Suc n)
  show ?case
  \<comment> \<open>We now assume n is not 0\<close>
  proof (induct cm)
    \<comment> \<open>We perform a case distinction over the continuation marker cm\<close>
    case Empty
    thus ?case by simp
    \<comment> \<open>If it is the empty continuation marker, the case is obvious\<close>
  next
    case (Lambda S)
    thus ?case
    \<comment> \<open>We now assume it is not the empty continuation marker\<close>
    proof (rule impI)
      assume premise: "concrete\<^sub>\<T>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<and> mvars(\<lambda>[S]) \<subseteq> fmdom'(\<sigma>)"
      \<comment> \<open>We assume the premise holds\<close>
      have "card (\<delta> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S])) \<le> 1"
        using \<delta>_determinism by simp
      \<comment> \<open>Due to the determinism of WL, the cardinality of the successor  
         configurations must be lower than 1\<close>
      thus "\<forall>c \<in> (\<Delta>\<^sub>N (Suc n) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S])). concrete\<^sub>\<T>(fst(c))"
      proof (cases)
        \<comment> \<open>We perform a case distinction on the cardinality of the successor
           configurations\<close>
        assume "card (\<delta> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S])) = 0"
        \<comment> \<open>We first assume the cardinality is 0\<close>
        hence "\<delta>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S]) = {}"
          using \<delta>_finite by simp
        \<comment> \<open>Because there can only be a finite amount of successor configurations,
           there can be no successor configuration\<close>
        moreover then have "card (\<Delta>\<^sub>N (Suc n) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S])) = 0"
          by (metis composition\<^sub>N.simps(3) Union_empty card.infinite card_0_eq image_is_empty)
        \<comment> \<open>Therefore the cardinality of the result must also be 0\<close>
        moreover have "\<forall>c \<in> \<delta> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S]). concrete\<^sub>\<T>(fst(c))"
          using premise \<delta>_concrete_pr by auto
        \<comment> \<open>Due to the \<delta>_concrete_pr lemma, we know that the concreteness is
           preserved when using the \<delta> operation\<close>
        ultimately show "\<forall>c \<in> (\<Delta>\<^sub>N (Suc n) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S])). concrete\<^sub>\<T>(fst(c))"
          using \<Delta>\<^sub>N_finite by (metis card_eq_0_iff)
        \<comment> \<open>Because the composition is finite, all results must hence be concrete
           as well, concluding the case\<close>
      next
        assume "\<not>card (\<delta> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S])) = 0"
        \<comment> \<open>We now assume the cardinality of the successor configurations is not 0\<close>
        hence "card (\<delta> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S])) = 1"
          using \<delta>_determinism by (metis One_nat_def bot_nat_0.extremum_uniqueI le_SucE)
        \<comment> \<open>Because of the determinism, the cardinality must hence be 1\<close>
        hence "\<exists>sh'. \<exists>\<sigma>'. \<exists>cm'. \<delta>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S]) = {(sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>, cm')}"
          using \<delta>\<^sub>\<Sigma> by (metis card.empty card_1_singletonE card_mono finite.emptyI insertE not_one_le_zero subrelI)
        \<comment> \<open>Then there can only be 1 successor configuration where the trace ends
           with a state \<sigma>'\<close>
        then obtain sh' cm' \<sigma>' where vals: "\<delta>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S]) = {(sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>, cm')}"
          by auto
        \<comment> \<open>We then obtain those values\<close>
        moreover then have "mvars(cm') \<subseteq> fmdom'(\<sigma>)"
          using premise var_red by (metis dual_order.trans insertCI)
        \<comment> \<open>Because of the reductiveness of the variables in the continuation marker,
           we know that all variables in the continuation marker of the successor
           configuration must have been in the domain of \<sigma>\<close>
        moreover then have "mvars(cm') \<subseteq> fmdom'(\<sigma>')"
          using dom_ext vals by (metis in_mono insertCI subsetI)
        \<comment> \<open>Because the domain is extensive during program execution, the variables
           must have also been in the domain of \<sigma>'\<close>
        moreover have "concrete\<^sub>\<T>(sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>)"
          using premise \<delta>_concrete_pr by (metis fst_eqD mvars.simps(2) singletonI vals)
        \<comment> \<open>We also know that the trace of the successor configuration must be 
           concrete due to the \<delta>_concrete_pr lemma\<close>
        ultimately have "\<forall>c \<in> (\<Delta>\<^sub>N n (sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>, cm')). concrete\<^sub>\<T>(fst(c))"
          using Suc.hyps by meson
        \<comment> \<open>With all this information, we can then use the induction hypothesis to
           infer that the trace results of applying the analysis on the successor 
           configuration must be concrete\<close>
        thus "\<forall>c \<in> (\<Delta>\<^sub>N (Suc n) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S])). concrete\<^sub>\<T>(fst(c))"
          using premise vals by auto
        \<comment> \<open>Thus concludes the last case\<close>
      qed
    qed
  qed
qed

lemma Tr\<^sub>N_concrete: 
  shows "concrete\<^sub>\<Sigma>(\<sigma>) \<and> vars(S) \<subseteq> fmdom'(\<sigma>) \<longrightarrow> (\<forall>\<tau> \<in> Tr\<^sub>N S \<sigma> n. concrete\<^sub>\<T>(\<tau>))"
  using Traces\<^sub>N_def \<Delta>\<^sub>N_concrete
  by (metis (no_types, lifting) concrete\<^sub>\<T>.simps(1) concrete\<^sub>\<T>.simps(2) image_iff mvars.simps(2))

text \<open>We prove that each composition function output with a configuration \<open>ending
  with a state in its trace\<close> as its input, also ends with a state in its trace\<close>

lemma \<Delta>\<^sub>N_end: "(\<tau>, cm') \<in> \<Delta>\<^sub>N n (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, cm) \<longrightarrow> (\<exists>sh'. \<exists>\<sigma>'. \<tau> = sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>)"
proof (induct n arbitrary: sh \<sigma> cm)
  \<comment> \<open>We perform an induction over n\<close>
  case 0 
  thus ?case by simp
  \<comment> \<open>If n is 0, the case is trivial\<close>
next
  case (Suc n)
  show ?case
  \<comment> \<open>We now assume n is not 0\<close>
  proof (induct cm)
    \<comment> \<open>We perform an induction over the continuation marker\<close>
    case Empty
    thus ?case by simp
    \<comment> \<open>If cm is the empty continuation marker, the case is trivial\<close>
  next
    case (Lambda S)
    thus ?case
    \<comment> \<open>We now assume cm is not the empty continuation marker\<close>
    proof (rule impI)
      assume "(\<tau>, cm') \<in> \<Delta>\<^sub>N (Suc n) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S])"
      \<comment> \<open>We first assume the premise holds\<close>
      hence "(\<tau>, cm') \<in> \<Union>((%c. \<Delta>\<^sub>N n c) ` \<delta>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S]))"
        by simp
      \<comment> \<open>We then rewrite the premise with the definition of the stepwise
         composition function\<close>
      hence "\<exists>sh' \<sigma>' cm''. (sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>, cm'') \<in> \<delta>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S]) \<and> (\<tau>, cm') \<in> \<Delta>\<^sub>N n (sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>, cm'')"
        using \<delta>\<^sub>\<Sigma> by fast
      \<comment> \<open>Because of an earlier theorem, we can conclude that all traces generated
         by the \<delta> function end with a state, also the one in question\<close>
      thus "\<exists>sh'. \<exists>\<sigma>'. \<tau> = sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>" 
        using Suc.hyps by blast
      \<comment> \<open>Then we can conclude the thesis with the induction hypothesis\<close>
    qed
  qed
qed

lemma Tr\<^sub>N_end: "\<tau> \<in> Tr\<^sub>N S \<sigma> n \<longrightarrow> (\<exists>sh'. \<exists>\<sigma>'. \<tau> = sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>)"
  using Traces\<^sub>N_def \<Delta>\<^sub>N_end by auto

text \<open>We prove that once a computation has \<open>reached an empty set of configurations\<close>,
  \<open>no new configuration\<close> can be generated anymore (this occurs when a guard cannot 
  be fully evaluated because of symbolic values)\<close>

lemma \<Delta>\<^sub>N_Suc_empty: 
  shows "\<Delta>\<^sub>N n (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S]) = {} \<longrightarrow> \<Delta>\<^sub>N (Suc n) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S]) = {}"
proof (induct n arbitrary: sh \<sigma> S)
  \<comment> \<open>We perform an induction over the natural numbers n\<close>
  case 0
  show ?case by simp
  \<comment> \<open>If n is 0, the case is trivial\<close>
next
  case (Suc n)
  thus ?case 
  \<comment> \<open>We now assume n is not 0\<close>
  proof (induct S)
    \<comment> \<open>We now perform a case distinction on all statements S\<close>
    case SKIP
    thus ?case using \<Delta>\<^sub>N_Skip \<Delta>\<^sub>N_End by metis
    \<comment> \<open>If S is SKIP, the case can be closed with their corresponding lemmas\<close>
  next
    case (Assign x a)
    thus ?case using \<Delta>\<^sub>N_Assign \<Delta>\<^sub>N_End by metis
    \<comment> \<open>If S is Assign, the case can be closed with their corresponding lemmas\<close>
  next
    case (If b S)
    thus ?case using \<Delta>\<^sub>N_If\<^sub>T \<Delta>\<^sub>N_If\<^sub>F \<Delta>\<^sub>N_If\<^sub>E \<Delta>\<^sub>N_End by (metis Suc.hyps)
    \<comment> \<open>If S is an If Branch, the case can be closed with their corresponding 
       lemmas and the induction hypothesis\<close>
  next
    case (While b S)
    thus ?case using \<Delta>\<^sub>N_While\<^sub>T \<Delta>\<^sub>N_While\<^sub>F \<Delta>\<^sub>N_While\<^sub>E \<Delta>\<^sub>N_End by (metis Suc.hyps)
    \<comment> \<open>If S is a While Loop, the case can be closed with their corresponding 
       lemmas and the induction hypothesis\<close>
  next
    case (Seq S\<^sub>1 S\<^sub>2)
    show ?case
    \<comment> \<open>We now assume S is a sequential statement\<close>
    proof (rule impI)
      assume "\<Delta>\<^sub>N (Suc n) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1;;S\<^sub>2]) = {}"
      \<comment> \<open>We assume the premise holds, meaning that S1;;S2 terminates in
         (n+1) steps with the empty configuration set\<close>
      hence "\<Union>((%c. \<Delta>\<^sub>N n c) ` \<delta> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1;;S\<^sub>2])) = {}"
        using \<Delta>\<^sub>N_Seq by simp
      \<comment> \<open>We know that this is equivalent to getting all successor configurations
         of S1;;S2 and then executing those n steps\<close>
      moreover have "\<forall>c. c \<in> \<delta> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1;;S\<^sub>2]) \<and> \<Delta>\<^sub>N n c = {} \<longrightarrow> \<Delta>\<^sub>N (Suc n) c = {}"
      \<comment> \<open>We still have to proof that all successor configurations, which
         terminate in n steps with the empty configuration set, must also
         terminate in (n+1) steps with the empty configuration set\<close>
      proof (rule allI)
        fix c
        \<comment> \<open>Let c be an arbitrary, but fixed successor configuration\<close>
        show "c \<in> \<delta> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1;;S\<^sub>2]) \<and> \<Delta>\<^sub>N n c = {} \<longrightarrow> \<Delta>\<^sub>N (Suc n) c = {}"
        proof (rule impI)
          assume premise: "c \<in> \<delta> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1;;S\<^sub>2]) \<and> \<Delta>\<^sub>N n c = {}"
          \<comment> \<open>We now assume the premise holds, meaning c is a successor configration
             and terminates in n steps with the empty configuration set\<close>
          hence "\<exists>sh'. \<exists>\<sigma>'. fst(c) = sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>"
            using \<delta>\<^sub>\<Sigma> by (metis prod.collapse)
          \<comment> \<open>We know that the trace of the configuration must end with a state
             due to an earlier proof\<close>
          then obtain sh' \<sigma>' where "fst(c) = sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>"
            by auto
          \<comment> \<open>We then obtain those values\<close>
          moreover have "\<exists>S'. snd(c) = \<lambda>[S']"
            using premise by (smt (verit) composition\<^sub>N.elims empty_iff singletonI sndI)
          \<comment> \<open>We also know that there is a rest statement left, because a sequential
             statement cannot terminate in 1 step\<close>
          moreover then obtain S' where "snd(c) = \<lambda>[S']"
            by auto
          \<comment> \<open>We then obtain this statement\<close>
          ultimately have "c = (sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>, \<lambda>[S'])"
            using premise by auto
          \<comment> \<open>We can therefore write c extensionally\<close>
          moreover then have "\<Delta>\<^sub>N n (sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>, \<lambda>[S']) = {}"
            using premise by auto
          \<comment> \<open>Due to our premise, we know that this configuration reaches
             the empty configuration set in n steps\<close>
          moreover then have "\<Delta>\<^sub>N (Suc n) (sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>, \<lambda>[S']) = {}"
            using Suc.hyps by simp
          \<comment> \<open>We can then conclude that it also reaches the empty configuration
             set in (n+1) steps via the induction hypothesis\<close>
          ultimately show "\<Delta>\<^sub>N (Suc n) c = {}"
            by simp
          \<comment> \<open>Which concludes the theorem\<close>
        qed
      qed
      ultimately show "\<Delta>\<^sub>N (Suc (Suc n)) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1;;S\<^sub>2]) = {}"
        using \<Delta>\<^sub>N_Seq by simp
      \<comment> \<open>We can then rewrite the theorem with an earlier thesis in order
         to proof the current thesis\<close>
    qed
  qed
qed

lemma \<Delta>\<^sub>N_empty_fixpoint:
  shows "\<Delta>\<^sub>N n (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S]) = {} \<and> k \<ge> n \<longrightarrow> \<Delta>\<^sub>N k (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S]) = {}"
proof (induct k arbitrary: n sh \<sigma> S)
  \<comment> \<open>We perform an induction over k\<close>
  case 0
  thus ?case by blast
  \<comment> \<open>If k is 0, the case can be trivially closed with the assumptions\<close>
next
  case (Suc k)
  show ?case
  \<comment> \<open>We now assume k is not 0\<close>
  proof (rule impI)
    assume premise: "\<Delta>\<^sub>N n (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S]) = {} \<and> (Suc k) \<ge> n"
    \<comment> \<open>We assume the premise holds\<close>
    thus "\<Delta>\<^sub>N (Suc k) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S]) = {}"
    proof (cases)
      \<comment> \<open>We perform a case distinction over the value of (k+1)\<close>
      assume "(Suc k) = n"
      \<comment> \<open>We first assume (k+1) is n\<close>
      thus "\<Delta>\<^sub>N (Suc k) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S]) = {}" 
        using premise by simp
      \<comment> \<open>Then the case can be trivially closed with the assumptions\<close>
    next
      assume "\<not>(Suc k) = n"
      \<comment> \<open>We now assume (k+1) is not n\<close>
      hence "(Suc k) > n"
        using premise le_neq_implies_less by blast
      \<comment> \<open>Due to the premise (k+1) must hence be greater than n\<close>
      thus "\<Delta>\<^sub>N (Suc k) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S]) = {}" 
        using premise Suc.hyps \<Delta>\<^sub>N_Suc_empty le_Suc_eq by metis
      \<comment> \<open>Then we can use our earlier theorem and the induction hypothesis
         in order to close the induction step\<close>
    qed
  qed
qed 

text \<open>We provide proofs that the \<open>stepwise composition reaches a fixpoint\<close> iff
  a terminal configuration is reached (or no configuration is reached anymore)\<close>

lemma \<Delta>\<^sub>N_Suc_fixpoint:
  shows "(\<forall>c \<in> (\<Delta>\<^sub>N n (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S])). snd(c) = \<lambda>[\<nabla>]) \<longrightarrow> \<Delta>\<^sub>N n (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S]) = \<Delta>\<^sub>N (Suc n) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S])"
proof (induct n arbitrary: sh \<sigma> S)
  \<comment> \<open>We perform an induction over n\<close>
  case 0
  show ?case by simp
  \<comment> \<open>If n is 0, the case is trivial\<close>
next
  case (Suc n)
  show ?case 
  \<comment> \<open>We now assume n is not 0\<close>
  proof (rule impI)
    assume premise: "\<forall>c \<in> (\<Delta>\<^sub>N (Suc n) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S])). snd(c) = \<lambda>[\<nabla>]"
    \<comment> \<open>We first assume the premise holds\<close>
    thus "\<Delta>\<^sub>N (Suc n) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S]) = \<Delta>\<^sub>N (Suc (Suc n)) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S])"
    proof (cases)
      \<comment> \<open>We perform a case dinstinction over the cardinality of the successor
         configigurations of S in 1 step\<close>
      assume "card (\<Delta>\<^sub>N (Suc 0) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S])) = 0"
      \<comment> \<open>We first assume the cardinality is 0\<close>
      hence "\<Delta>\<^sub>N (Suc 0) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S]) = {}"
        using \<Delta>\<^sub>N_finite card_0_eq by blast
      \<comment> \<open>Then the set must be empty\<close>
      hence "\<Delta>\<^sub>N (Suc n) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S]) = {} \<and> \<Delta>\<^sub>N (Suc (Suc n)) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S]) = {}"
        using \<Delta>\<^sub>N_empty_fixpoint by auto
      \<comment> \<open>Using an earlier theorem, the configuration sets reached after (n+1)
         and (n+2) steps must be empty as well\<close>
      thus ?thesis by blast
      \<comment> \<open>Which trivially concludes on the theorem\<close>
    next
      assume "\<not>card (\<Delta>\<^sub>N (Suc 0) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S])) = 0"
      \<comment> \<open>We then assume the cardinality is not 0\<close>
      then have "card (\<Delta>\<^sub>N (Suc 0) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S])) = 1"
        using \<Delta>\<^sub>N_determinism by (metis One_nat_def bot_nat_0.extremum_uniqueI le_SucE)
      \<comment> \<open>Due to the determinism of the stepwise composition, the cardinality
         of the successor set after a 1 step execution must hence be 1\<close>
      hence "\<exists>c'. \<Delta>\<^sub>N (Suc 0) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S]) = {c'}"
        by (meson card_1_singletonE)
      \<comment> \<open>Hence, there must exist such a configuration c\<close>
      then obtain c' where vals\<^sub>A: "\<Delta>\<^sub>N (Suc 0) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S]) = {c'}"
        by auto
      \<comment> \<open>We then obtain that value\<close>
      moreover then have "\<exists>\<tau>' cm'. c' = (\<tau>', cm')"
        by simp
      \<comment> \<open>We know a configuration consists of a trace and a continuation marker\<close>
      then obtain \<tau>' cm' where vals\<^sub>B: "c' = (\<tau>', cm')"
        by blast
      \<comment> \<open>We then obtain those values\<close>
      moreover then have "\<Union>((%c. \<Delta>\<^sub>N 0 c) ` \<delta>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S])) = {(\<tau>', cm')}"
        using vals\<^sub>A by simp
      \<comment> \<open>We then rewrite the earlier theorem using the simplifications
         of the stepwise composition function and our obtained values\<close>
      moreover then have "\<exists>sh' \<sigma>'. \<tau>' = sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>"
        using \<delta>\<^sub>\<Sigma> by (smt (verit) SUP_bot_conv(2) UN_I composition\<^sub>N.simps(1) empty_iff insert_iff singletonI)
      \<comment> \<open>Because of an earlier theorem, the obtained trace must end with a state
         as the last trace atom\<close>
      moreover then obtain sh' \<sigma>' where vals\<^sub>C: "\<tau>' = sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>"
        by auto
      \<comment> \<open>We then obtain that state\<close>
      ultimately show ?thesis 
      proof (induct cm')
        \<comment> \<open>We perform a case distinction over the continuation marker\<close>
        case Empty
        \<comment> \<open>We first assume the continuation marker is empty\<close>
        thus ?case using \<Delta>\<^sub>N_Bounded
          by (simp; smt (verit) SUP_cong UN_I composition\<^sub>N.elims composition\<^sub>N.simps(1) composition\<^sub>N.simps(2) composition\<^sub>N.simps(3) singletonD singletonI)
        \<comment> \<open>Then the case holds using the bounded theorem\<close>
      next
        case (Lambda S')
        \<comment> \<open>We now assume the continuation marker is not empty\<close>
        have "\<Delta>\<^sub>N (Suc 0) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S]) = {(sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>, \<lambda>[S'])}"
          using premise vals\<^sub>A vals\<^sub>B vals\<^sub>C Lambda.prems(2) by blast
        \<comment> \<open>We then rewrite our earlier theorem using that information\<close>
        moreover then have "\<forall>c \<in> (\<Delta>\<^sub>N n (sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>, \<lambda>[S'])). snd c = \<lambda>[\<nabla>]"
          using premise composition\<^sub>N.simps by auto
        \<comment> \<open>We know that all configurations reaches after n steps from the successor
           configuration must be terminal configurations using the premise\<close>
        moreover then have "\<Delta>\<^sub>N n (sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>, \<lambda>[S']) = \<Delta>\<^sub>N (Suc n) (sh' \<leadsto> State\<llangle>\<sigma>'\<rrangle>, \<lambda>[S'])"
          using Suc.hyps by simp
        \<comment> \<open>Due to the induction hypothesis, we conclude that executing the 
           successor configuration n times is the same as executing it (n+1) times\<close>
        ultimately show ?case
          by simp
        \<comment> \<open>This concludes our thesis using our earlier theorem\<close>
      qed
    qed
  qed
qed

lemma \<Delta>\<^sub>N_fixpoint:
  shows "(\<forall>c \<in> (\<Delta>\<^sub>N n (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S])). snd(c) = \<lambda>[\<nabla>]) \<and> k \<ge> n \<longrightarrow> \<Delta>\<^sub>N n (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S]) = \<Delta>\<^sub>N k (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S])"
proof (induct k arbitrary: n sh \<sigma>)
  \<comment> \<open>We perform an induction over the natural number k\<close>
  case 0
  thus ?case by simp
  \<comment> \<open>If k is 0, the case is trivial\<close>
next
  case (Suc k)
  show ?case
  \<comment> \<open>We now assume k is not 0\<close>
  proof (rule impI)
    assume premise: "(\<forall>c \<in> (\<Delta>\<^sub>N n (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S])). snd(c) = \<lambda>[\<nabla>]) \<and> (Suc k) \<ge> n"
    \<comment> \<open>We assume the premise holds, meaning that we reach a terminal configuration
       after n steps and (k+1) \<ge> n\<close>
    thus "\<Delta>\<^sub>N n (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S]) = \<Delta>\<^sub>N (Suc k) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S])"
    proof (cases)
      \<comment> \<open>We perform a case distinction over the value of k\<close>
      assume "(Suc k) = n"
      \<comment> \<open>We first assume (k+1) = n\<close>
      thus "\<Delta>\<^sub>N n (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S]) = \<Delta>\<^sub>N (Suc k) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S])"
        by simp
      \<comment> \<open>This concludes on our case trivially due to the assumptions\<close>
    next
      assume "\<not>(Suc k) = n"
      \<comment> \<open>We now assume (k+1) is not n\<close>
      hence "(Suc k) > n"
        using premise le_neq_implies_less by blast
      \<comment> \<open>Due to our premise (k+1) must be greater than n\<close>
      hence "\<Delta>\<^sub>N n (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S]) = \<Delta>\<^sub>N k (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S])"
        using Suc.hyps premise le_SucE by blast
      \<comment> \<open>Using our induction hypothesis, we know that executing S n steps
         is the same as executing S k steps\<close>
      thus "\<Delta>\<^sub>N n (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S]) = \<Delta>\<^sub>N (Suc k) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S])"
        using \<Delta>\<^sub>N_Suc_fixpoint premise by blast
      \<comment> \<open>With the earlier theorem, we can then conclude the induction step by
         deducing that executing S n steps is the same as executing S (k+1) steps\<close>
    qed
  qed
qed

end    