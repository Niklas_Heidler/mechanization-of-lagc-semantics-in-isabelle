chapter \<open>Basics\<close>
theory LAGC_Base
  imports Main "HOL-Library.Finite_Map"
begin

text \<open>In this chapter, we formalize the basics of the LAGC semantics as
  described in section~2 of the original paper. This section describes the 
  reusable interface for all concrete instantiations of the LAGC semantics,
  providing the basis for the subsequent chapters.\<close>

section \<open>Expressions\<close>

subsection \<open>Primitives\<close>

text \<open>We model program variables and method names as strings, ensuring that
  the domains for variables and method names are both infinite. This design
  choice also ensures easy usability and readability of variables in 
  concrete~programs.\<close>
  type_synonym var = string
  type_synonym method_name = string


subsection \<open>Syntax\<close>

text \<open>We now formalize the syntax of expressions for a generic programming language. 
  The formalization is highly modular in order to guarantee that the syntax can be 
  easily adapted to accommodate new syntactical concepts when~needed. \par
  However, the formalization of the syntax in the original paper is not easily 
  transferable to Isabelle due to a lack of type constraints. Without loss of 
  generality we thus propose a strictly typed grammar that matches the original
  grammar in its core concept. The grammar includes the common connectives and
  enforces expressions to be of either arithmetic or Boolean~nature. \par
  We first introduce the standard arithmetic, Boolean and relational operators, 
  which can be used in syntactical derivations of our~expressions.\<close>
  datatype op\<^sub>a = 
      add \<comment> \<open>Addition\<close>
    | sub \<comment> \<open>Subtraction\<close>
    | mul \<comment> \<open>Multiplication\<close>

  datatype op\<^sub>b = 
      conj \<comment> \<open>Logical And\<close>
    | disj \<comment> \<open>Logical Or\<close>

  datatype op\<^sub>r = 
      leq \<comment> \<open>Less Equal\<close>
    | geq \<comment> \<open>Greater Equal\<close>
    | eq \<comment> \<open>Equal\<close>

text \<open>We now provide a formal definition of the grammar, separating between
  the following types of expressions: \begin{description}
  \item[Arithmetic Expressions] Arithmetic expressions consist of arithmetic 
  numerals, variables and binary arithmetic operations using arithmetic 
  operators and arithmetic operands.
  \item[Boolean Expressions] Boolean expressions consist of Boolean truth values, 
  the not operation, binary Boolean operations using Boolean operators and Boolean 
  operands, as well as binary relational operations using relational operators 
  and arithmetic~operands.
  \item[Expressions] Expressions can either be arithmetic expressions, 
  Boolean expressions or method names. The addition of method names to the 
  expression syntax deviates from the original paper. The reason for this
  will be explained at a later point.
  \item[Starred Expressions] Starred Expressions can only be arithmetic expressions 
  or the symbolic value. Note that we do not allow the derivation of Boolean starred 
  expressions, thereby differing from the original paper. This later ensures that 
  all variables have an arithmetic type, thus establishing a simpler model 
  without loss~of~generality.
  \end{description} Note that the syntax can be easily adapted to accommodate new 
  expression concepts.\<close>
  datatype aexp = 
      Numeral int \<comment> \<open>Arithmetic numeral\<close>
    | Variable var \<comment> \<open>Variables\<close>
    | Op\<^sub>A aexp op\<^sub>a aexp \<comment> \<open>Binary arithmetic operations\<close>

  datatype bexp = 
      Boolean bool \<comment> \<open>Boolean truth values\<close>
    | Not bexp \<comment> \<open>Not Operation\<close>
    | Op\<^sub>B bexp op\<^sub>b bexp \<comment> \<open>Binary Boolean operations\<close>
    | Op\<^sub>R aexp op\<^sub>r aexp \<comment> \<open>Binary Relational operations\<close>

  datatype exp = 
      Aexp aexp \<comment> \<open>Arithmetic Expressions\<close>
    | Bexp bexp \<comment> \<open>Boolean Expressions\<close>
    | Param method_name \<comment> \<open>Method Names\<close>

  datatype sexp = 
      Expression aexp \<comment> \<open>Arithmetic Expressions\<close>
    | Star \<comment> \<open>Symbolic Value\<close>

text \<open>We also add a minimal concrete syntax for expressions, thereby improving 
  the readability of expressions~in~programs.\<close>
  notation Numeral ("Num _" [1000] 63)
  notation Variable ("Var _" [1000] 63)
  notation Op\<^sub>A ("_ \<^sub>A_ _" [1000, 1000, 1000] 62)

  notation Boolean ("Bool _" [1000] 63)
  notation Not ("not _" [1000] 63)
  notation Op\<^sub>B ("_ \<^sub>B_ _" [1000, 1000, 1000] 62)
  notation Op\<^sub>R ("_ \<^sub>R_ _" [1000, 1000, 1000] 62)

  notation Aexp ("A _" [1000] 63)
  notation Bexp ("B _" [1000] 63)
  notation Param ("P _" [1000] 63)

  notation Expression ("Exp _" [1000] 63)
  notation Star ("\<^emph>")

text \<open>We additionally define helper functions, which project expressions
  onto their encased arithmetic/Boolean expression. Note that both
  projection functions are partial, hence they should only be called if the
  encased type of expression is known~beforehand.\<close>
  abbreviation
    proj\<^sub>A :: "exp \<Rightarrow> aexp" where
    "proj\<^sub>A e \<equiv> (case e of A a \<Rightarrow> a)"

  abbreviation
    proj\<^sub>B :: "exp \<Rightarrow> bexp" where
    "proj\<^sub>B e \<equiv> (case e of B b \<Rightarrow> b)"

text \<open>Using our previously defined grammar, we can now derive syntactically correct 
  expressions. We demonstrate this by providing examples for arithmetic and Boolean 
  expressions with our minimal concrete~syntax.\<close>
  definition 
    aExp_ex :: aexp where
    "aExp_ex \<equiv> ((Var ''x'') \<^sub>Amul (Var ''y'')) \<^sub>Asub (Var ''x'')" \<comment> \<open>\<open>(x * y) - x\<close>\<close>

  definition 
    bExp_ex :: bexp where
    "bExp_ex \<equiv> ((Var ''x'') \<^sub>Req (Num 2)) \<^sub>Bdisj (Bool False)"  \<comment> \<open>\<open>x = 2 \<or> False\<close>\<close>



subsection \<open>Variable Mappings\<close>

text \<open>We now formally introduce variable functions mapping a specific type of expression
  onto a set of their enclosed free variables. We also provide a variable mapping
  for lists of expressions and sets of Boolean expressions, which will be utilized as
  handy abbreviations at a later point. The definitions of these functions 
  are~straightforward.\<close>
  primrec
    vars\<^sub>A :: "aexp \<Rightarrow> var set" where
    "vars\<^sub>A (Num n) = {}" |
    "vars\<^sub>A (Var x) = {x}" |
    "vars\<^sub>A (a\<^sub>1 \<^sub>Aop a\<^sub>2) = vars\<^sub>A(a\<^sub>1) \<union> vars\<^sub>A(a\<^sub>2)" 

  primrec 
    vars\<^sub>B :: "bexp \<Rightarrow> var set" where
    "vars\<^sub>B (Bool b) = {}" |
    "vars\<^sub>B (not b) = vars\<^sub>B(b)" |
    "vars\<^sub>B (b\<^sub>1 \<^sub>Bop b\<^sub>2) = vars\<^sub>B(b\<^sub>1) \<union> vars\<^sub>B(b\<^sub>2)" |
    "vars\<^sub>B (a\<^sub>1 \<^sub>Rop a\<^sub>2) = vars\<^sub>A(a\<^sub>1) \<union> vars\<^sub>A(a\<^sub>2)" 

  primrec 
    vars\<^sub>E :: "exp \<Rightarrow> var set" where
    "vars\<^sub>E (A a) = vars\<^sub>A(a)" |
    "vars\<^sub>E (B b) = vars\<^sub>B(b)" |
    "vars\<^sub>E (P m) = {}"

  primrec 
    vars\<^sub>S :: "sexp \<Rightarrow> var set" where
    "vars\<^sub>S (Exp a) = vars\<^sub>A(a)" |
    "vars\<^sub>S \<^emph> = {}"

  primrec 
    lvars\<^sub>E :: "exp list \<Rightarrow> var set" where
    "lvars\<^sub>E [] = {}" |
    "lvars\<^sub>E (exp # rest) = vars\<^sub>E(exp) \<union> lvars\<^sub>E(rest)"

  fun 
    svars\<^sub>B :: "bexp set \<Rightarrow> var set" where
    "svars\<^sub>B S = \<Union>(vars\<^sub>B ` S)"

text \<open>In contrast to the original paper we additionally define variable occurrence
  functions mapping an arithmetic/Boolean expression onto a list of their
  enclosed free variables. Note that a variable may occur multiple times inside a 
  returned list, depending on the number of occurrences in the~expression.\<close>
  primrec
    occ\<^sub>A :: "aexp \<Rightarrow> var list" where
    "occ\<^sub>A (Num n) = []" |
    "occ\<^sub>A (Var x) = [x]" |
    "occ\<^sub>A (a\<^sub>1 \<^sub>Aop a\<^sub>2) = occ\<^sub>A(a\<^sub>1) @ occ\<^sub>A(a\<^sub>2)" 

  primrec 
    occ\<^sub>B :: "bexp \<Rightarrow> var list" where
    "occ\<^sub>B (Bool b) = []" |
    "occ\<^sub>B (not b) = occ\<^sub>B(b)" |
    "occ\<^sub>B (b\<^sub>1 \<^sub>Bop b\<^sub>2) = occ\<^sub>B(b\<^sub>1) @ occ\<^sub>B(b\<^sub>2)" |
    "occ\<^sub>B (a\<^sub>1 \<^sub>Rop a\<^sub>2) = occ\<^sub>A(a\<^sub>1) @ occ\<^sub>A(a\<^sub>2)" 

text \<open>Although the variable occurrence functions are similar to the previous variable
  mappings, they differ in their result type. Whilst the variable occurrence
  functions return a finite list, the previous variable mappings return
  a (theoretically) infinite set. Thus, the variable occurrence functions ensure
  that the variables of an arithmetic/Boolean expression can be iterated~over. \par\<close>

text \<open>We can now provide examples for the use of variable mappings and variable 
  occurrence functions in our proof~system.\<close>
  lemma "vars\<^sub>A(aExp_ex) = {''x'', ''y''}" 
    by (auto simp add: aExp_ex_def)

  lemma "vars\<^sub>B(bExp_ex) = {''x''}"
    by (auto simp add: bExp_ex_def)

  lemma "occ\<^sub>A(aExp_ex) = [''x'', ''y'', ''x'']" 
    by (simp add: aExp_ex_def)

  lemma "occ\<^sub>B(bExp_ex) = [''x'']"
    by (simp add: bExp_ex_def)



subsection \<open>Variable Substitutions\<close>

text \<open>We now introduce recursive variable substitution functions, substituting every 
  occurrence of a variable in a specific type of expression with another variable. 
  We again denote abbreviations for lists of expressions and sets of Boolean 
  expressions. The definitions of the substitution functions are~straightforward.\<close>
  primrec
    substitute\<^sub>A :: "aexp \<Rightarrow> var \<Rightarrow> var \<Rightarrow> aexp" where
    "substitute\<^sub>A (Num n) x y = Num n" |
    "substitute\<^sub>A (Var v) x y = (if x = v then Var y else Var v)" | 
    "substitute\<^sub>A (a\<^sub>1 \<^sub>Aop a\<^sub>2) x y = (substitute\<^sub>A a\<^sub>1 x y) \<^sub>Aop (substitute\<^sub>A a\<^sub>2 x y)"

  primrec
    substitute\<^sub>B :: "bexp \<Rightarrow> var \<Rightarrow> var \<Rightarrow> bexp" where
    "substitute\<^sub>B (Bool b) x y = Bool b" |
    "substitute\<^sub>B (not b) x y = not (substitute\<^sub>B b x y)" |
    "substitute\<^sub>B (b\<^sub>1 \<^sub>Bop b\<^sub>2) x y = (substitute\<^sub>B b\<^sub>1 x y) \<^sub>Bop (substitute\<^sub>B b\<^sub>2 x y)" |
    "substitute\<^sub>B (a\<^sub>1 \<^sub>Rop a\<^sub>2) x y = (substitute\<^sub>A a\<^sub>1 x y) \<^sub>Rop (substitute\<^sub>A a\<^sub>2 x y)"

  primrec
    substitute\<^sub>E :: "exp \<Rightarrow> var \<Rightarrow> var \<Rightarrow> exp" where
    "substitute\<^sub>E (A a) x y = A (substitute\<^sub>A a x y)" |
    "substitute\<^sub>E (B b) x y = B (substitute\<^sub>B b x y)" |
    "substitute\<^sub>E (P m) x y = P m"

  primrec
    substitute\<^sub>S :: "sexp \<Rightarrow> var \<Rightarrow> var \<Rightarrow> sexp" where
    "substitute\<^sub>S (Exp a) x y = Exp (substitute\<^sub>A a x y)" |
    "substitute\<^sub>S \<^emph> x y = \<^emph>"

  primrec
    lsubstitute\<^sub>E :: "exp list \<Rightarrow> var \<Rightarrow> var \<Rightarrow> exp list" where
    "lsubstitute\<^sub>E [] x y = []" |
    "lsubstitute\<^sub>E (exp # rest) x y = (substitute\<^sub>E exp x y) # (lsubstitute\<^sub>E rest x y)"

  fun
    ssubstitute\<^sub>B :: "bexp set \<Rightarrow> var \<Rightarrow> var \<Rightarrow> bexp set" where
    "ssubstitute\<^sub>B S x y = (%b. substitute\<^sub>B b x y) ` S"

text \<open>In the following we again provide several examples for the variable 
  substitution~function.\<close>
  lemma "substitute\<^sub>A aExp_ex ''x'' ''z'' = ((Var ''z'') \<^sub>Amul (Var ''y'')) \<^sub>Asub (Var ''z'')" 
    by (simp add: aExp_ex_def)

  lemma "substitute\<^sub>B bExp_ex ''x'' ''y'' = ((Var ''y'') \<^sub>Req (Num 2)) \<^sub>Bdisj (Bool False)"
    by (simp add: bExp_ex_def)


subsection \<open>Concreteness\<close>

text \<open>We call a specific type of expression concrete iff it contains no
  variables or symbolic values and has already been simplified as much as possible. 
  Similar to the variable mappings, we again provide concreteness
  notions for lists of expressions and sets of Boolean expressions, which will be  
  useful abbreviations at a later point. Although the notion of a concrete
  expression does not exist in the original paper, we propose such a notion 
  in order to later be able to circumvent the implicit simplifications of 
  expressions~in~states.\<close>
  abbreviation
    concrete\<^sub>A :: "aexp \<Rightarrow> bool" where
    "concrete\<^sub>A a \<equiv> (case a of (Num n) \<Rightarrow> True | _ \<Rightarrow> False)" 

  abbreviation
    concrete\<^sub>B :: "bexp \<Rightarrow> bool" where
    "concrete\<^sub>B b \<equiv> (case b of (Bool b) \<Rightarrow> True | _ \<Rightarrow> False)"

  abbreviation
    concrete\<^sub>E :: "exp \<Rightarrow> bool" where
    "concrete\<^sub>E e \<equiv> (case e of (A a) \<Rightarrow> concrete\<^sub>A(a) | (B b) \<Rightarrow> concrete\<^sub>B(b) | (P m) \<Rightarrow> True)" 

  abbreviation
    concrete\<^sub>S :: "sexp \<Rightarrow> bool" where
    "concrete\<^sub>S s \<equiv> (case s of (Exp a) \<Rightarrow> concrete\<^sub>A(a) | \<^emph> \<Rightarrow> False)"

  primrec
    lconcrete\<^sub>E :: "exp list \<Rightarrow> bool" where
    "lconcrete\<^sub>E [] = True" |
    "lconcrete\<^sub>E (exp # rest) = (concrete\<^sub>E(exp) \<and> lconcrete\<^sub>E(rest))"

  abbreviation
    sconcrete\<^sub>B :: "bexp set \<Rightarrow> bool" where
    "sconcrete\<^sub>B S \<equiv> (\<forall>b \<in> S. concrete\<^sub>B(b))"

text \<open>We can now establish a connection between the notion of concrete
  expressions and the variable mappings. By making use of structural induction,
  we show that a concrete expression is always variable-free. This property is
  obvious, as it trivially holds due to the definition of the concreteness notion.\<close>
  lemma concrete_vars_imp\<^sub>A: "concrete\<^sub>A(a) \<longrightarrow> vars\<^sub>A(a) = {}" 
    by (induct a; simp)

  lemma concrete_vars_imp\<^sub>B: "concrete\<^sub>B(b) \<longrightarrow> vars\<^sub>B(b) = {}" 
    by (induct b; simp)

  lemma concrete_vars_imp\<^sub>E: "concrete\<^sub>E(e) \<longrightarrow> vars\<^sub>E(e) = {}"
    using concrete_vars_imp\<^sub>A concrete_vars_imp\<^sub>B by (induct e; simp)

  lemma concrete_vars_imp\<^sub>S: "concrete\<^sub>S(s) \<longrightarrow> vars\<^sub>S(s) = {}"
    using concrete_vars_imp\<^sub>A by (induct s; simp)

  lemma l_concrete_vars_imp\<^sub>E: "lconcrete\<^sub>E(l) \<longrightarrow> lvars\<^sub>E(l) = {}"
    using concrete_vars_imp\<^sub>E by (induct l; simp)

  lemma s_concrete_vars_imp\<^sub>B: "sconcrete\<^sub>B(S) \<longrightarrow> svars\<^sub>B(S) = {}"
    using concrete_vars_imp\<^sub>B by simp



section \<open>States\<close>

subsection \<open>Definition of States\<close>

text \<open>Similar to the original paper, we define a symbolic state as a 
  partial mapping from the set of program variables to the set of starred 
  expressions. However, this approach also allows symbolic states
  to be total functions, thereby resulting in symbolic states possibly having 
  an infinite domain. Due to this fact, Isabelle will not be able to generate
  code when trying to compute the domain of a state, which clearly violates
  our objective of contributing an efficient code generation. \par
  In order to solve this problem, we enforce that the domain of a symbolic state
  must be of finite nature. The \emph{Finite Map} theory of the HOL-Library builds 
  on top of the partial function theory and already implements this exact concept. 
  Whilst a finite map guarantees the domain of the function to be finite, 
  it otherwise behaves very similar to a normal explicit partial function. Using
  this approach, we can guarantee the computability of~state~domains. \par
  We denote the set of all symbolic states as \<open>\<Sigma>\<close>.\<close>
  type_synonym \<Sigma> = "(var, sexp) fmap"

text \<open>We now introduce several notational abbreviations in order to ease the 
  handling of finite maps. We abbreviate the state that does not
   define any program variables as \<open>\<circle>\<close>. Additionally we introduce 
  \<open>fm\<close> as an abbreviation for the translation from a tuple list to a corresponding
  finite map. Last but not least, we also denote an easier readable abbreviation for 
  the finite~map~update.\<close>
  notation fmempty ("\<circle>")
  notation fmap_of_list ("fm")
  notation fmupd ("[_ \<longmapsto> _] _" 70)

text \<open>Using the newly defined concepts and notations, we can now provide
  several examples for symbolic~states. Note that we always denote a state
  as a tuple list that is translated to a corresponding finite map using~$fm$.\<close>
  definition 
    \<sigma>\<^sub>1 :: \<Sigma> where
    "\<sigma>\<^sub>1 \<equiv> fm[(''x'', Exp ((Var ''y'') \<^sub>Amul (Num 4))), (''y'', \<^emph>)]"

  definition 
    \<sigma>\<^sub>2 :: \<Sigma> where
    "\<sigma>\<^sub>2 \<equiv> fm[(''x'', Exp (Num 8)), (''y'', Exp (Num 2))]"


subsection \<open>Symbolic Variables\<close>

text \<open>A symbolic variable of a state is a variable that maps to the symbolic value. 
  The symbolic variable function maps a given state to the set of all 
  its symbolic variables. The definition of the function is straightforward. 
  Note that the nature of a symbolic state enforces the set of symbolic
  variables to~be~finite.\<close>
  definition 
    symb\<^sub>\<Sigma> :: "\<Sigma> \<Rightarrow> var set" where
    "symb\<^sub>\<Sigma> \<sigma> \<equiv> {X \<in> fmdom'(\<sigma>). fmlookup \<sigma> X = Some \<^emph>}"

text \<open>We can now provide various examples for the use of the symbolic 
  variable~function.\<close>
  lemma "symb\<^sub>\<Sigma> \<sigma>\<^sub>1 = {''y''}" 
    by (auto simp add: \<sigma>\<^sub>1_def symb\<^sub>\<Sigma>_def)

  lemma "symb\<^sub>\<Sigma> \<sigma>\<^sub>2 = {}" 
    by (auto simp add: \<sigma>\<^sub>2_def symb\<^sub>\<Sigma>_def)


subsection \<open>Wellformedness\<close>

text \<open>We consider a state wellformed iff all variables occurring
  in mapped expressions of a state are symbolic variables. The definition
  of this predicate is straightforward and smoothly aligns with the definition in 
  the original paper.\<close>
  definition 
    wf\<^sub>\<Sigma> :: "\<Sigma> \<Rightarrow> bool" where
    "wf\<^sub>\<Sigma> \<sigma> \<equiv> {v. \<exists>v' \<in> fmdom'(\<sigma>). v \<in> vars\<^sub>S(the(fmlookup \<sigma> v'))} \<subseteq> symb\<^sub>\<Sigma>(\<sigma>)"

text \<open>As can be easily observed, \<open>\<sigma>\<^sub>1\<close> and \<open>\<sigma>\<^sub>2\<close> are both wellformed states, which
  can also be infered~in~Isabelle.\<close>
  lemma "wf\<^sub>\<Sigma> \<sigma>\<^sub>1 \<and> wf\<^sub>\<Sigma> \<sigma>\<^sub>2" 
    by (simp add: \<sigma>\<^sub>1_def \<sigma>\<^sub>2_def symb\<^sub>\<Sigma>_def wf\<^sub>\<Sigma>_def)


subsection \<open>Concreteness\<close>

text \<open>We now propose a notion of concrete states which greatly differs
  from the original paper. A state is considered concrete iff it is wellformed 
  and contains no symbolic variables. However, the original paper additionally 
  assumes an implicit state simplification, which cannot be reasonably 
  modeled~in~Isabelle. \par
  As a solution to this problem, we will explicitly require concrete states to be 
  fully simplified. We have already introduced concrete expressions as fully 
  simplified non-symbolic variable-free expressions. Consequently, the concreteness 
  notion of the original paper is equivalent to confirming that all domain 
  variables map to concrete expressions. This design choice ensures a feasible
  formalization of concreteness, whilst keeping the property~simple.\<close>
  definition
    concrete\<^sub>\<Sigma> :: "\<Sigma> \<Rightarrow> bool" where
    "concrete\<^sub>\<Sigma> \<sigma> \<equiv> \<forall>v \<in> fmdom'(\<sigma>). concrete\<^sub>S(the(fmlookup \<sigma> v))"

text \<open>We can now establish that the concreteness property of a 
  state is preserved if it is updated with a concrete arithmetic expression. This
  ensures a speedup of automatic proofs as it is faster to show the concreteness 
  of a singular arithmetic expression instead of reproofing the concreteness of 
  the whole state after each update. Thus we will prefer the derived lemmas
  instead of the original concreteness definition when trying to prove that the
  concreteness of a state has been preserved.\<close>
  lemma concrete_upd_pr\<^sub>S: "concrete\<^sub>\<Sigma>(\<sigma>) \<and> concrete\<^sub>S(e) \<longrightarrow> concrete\<^sub>\<Sigma>([x \<longmapsto> e] \<sigma>)"
    by (simp add: concrete\<^sub>\<Sigma>_def)

  lemma concrete_upd_pr\<^sub>A: "concrete\<^sub>\<Sigma> \<sigma> \<and> concrete\<^sub>A a \<longrightarrow> concrete\<^sub>\<Sigma>([x \<longmapsto> Exp a] \<sigma>)" 
    using concrete_upd_pr\<^sub>S by simp

text \<open>In the following lemmas we can now establish a connection between
  wellformed states and concrete states. We trivially prove that a concrete
  state has no symbolic variables. Additionally, we deduce that concreteness
  implies wellformedness, but not vice versa.\<close>
  lemma concrete_symb_imp\<^sub>\<Sigma>: "concrete\<^sub>\<Sigma>(\<sigma>) \<longrightarrow> symb\<^sub>\<Sigma>(\<sigma>) = {}"
    by (auto simp add: symb\<^sub>\<Sigma>_def concrete\<^sub>\<Sigma>_def) 

  lemma concrete_wf_imp\<^sub>\<Sigma>: "concrete\<^sub>\<Sigma>(\<sigma>) \<longrightarrow> wf\<^sub>\<Sigma>(\<sigma>)"
    using concrete_vars_imp\<^sub>S by (simp add: wf\<^sub>\<Sigma>_def concrete\<^sub>\<Sigma>_def)

  lemma "\<exists>\<sigma>. \<not>(wf\<^sub>\<Sigma>(\<sigma>) \<longrightarrow> concrete\<^sub>\<Sigma>(\<sigma>))"
    apply (rule_tac x = "[''x0'' \<longmapsto> \<^emph>] \<circle>" in exI)
    by (simp add: wf\<^sub>\<Sigma>_def concrete\<^sub>\<Sigma>_def)

text \<open>We can now take another look at our example states. \<open>\<sigma>\<^sub>1\<close> is not concrete as 
  x1 and x2 do not map to concrete expressions. However, \<open>\<sigma>\<^sub>2\<close> is concrete, because
  8 and 2 are both arithmetic numerals. We prove this reasoning with a corresponding
  Isabelle lemma.\<close>
  lemma "\<not>concrete\<^sub>\<Sigma> \<sigma>\<^sub>1 \<and> concrete\<^sub>\<Sigma> \<sigma>\<^sub>2" 
    by (simp add: \<sigma>\<^sub>1_def \<sigma>\<^sub>2_def concrete\<^sub>\<Sigma>_def)


subsection \<open>Variable Generation\<close>

text \<open>Let a fresh variable denote a variable that does not occur in the domain 
  of a given state. We now setup a variable generation that deterministically
  returns a fresh variable for any given state. The design choice of enforcing
  determinism ensures an easier handling of future definitions and proofs without 
  loss of generality. It also guarantees an automatic generation of executable 
  code for the variable generator, which would have not have been easily feasible 
  without imposing~determinism. \par
  Our general idea on how to choose fresh variables is repeatedly attaching the
  character $c$ in front of a given variable until we eventually find a variable name, 
  which is not an element of the given state domain. This approach is easy and
  fast, but can possibly result in very long variable names. However, this is not
  a big problem, as redefining a variable more than a few times is not common
  in standard~programs. \par
  We first provide a recursive helper function that attaches the character $c$ 
  n-times in front of a given variable, before finally returning~it.\<close>
  primrec
    vargen\<^sub>N :: "nat \<Rightarrow> var \<Rightarrow> var" where
    "vargen\<^sub>N 0 v = v" |
    "vargen\<^sub>N (Suc n) v = vargen\<^sub>N n (CHR ''c'' # v)"

text \<open>Using the prepared helper function, we can now define the variable generation 
  selecting a fresh variable for any given state. The generator is given
  a state \<open>\<sigma>\<close>, a variable length n, a maximum bound b and a variable v. It then
  checks the freshness of variable v prepended with n characters. If the variable
  is fresh, we can return it, otherwise we recursively call our variable generation
  with an increased variable length and a reduced bound. If our bound becomes 0, we
  return a standardized variable implying that something~went~wrong. \par
  The notion of a maximal bound ensures that the variable generation terminates
  in finite time. However, this design choice also violates the desired 
  deterministic property as well as the freshness of the chosen variable, 
  considering that the returned variable is standardized when the bound is exceeded. 
  Hence we have to accept that the variable generation is only deterministic as long 
  as the corresponding bound is not exceeded during a program execution. This is 
  not a problem however, as long as a sufficiently high bound~is~provided.\<close>
  primrec
    vargen :: "\<Sigma> \<Rightarrow> nat \<Rightarrow> nat \<Rightarrow> var \<Rightarrow> var" where
    "vargen \<sigma> n 0 v = ''$BOUND_EXCEEDED::'' @ v" |
    "vargen \<sigma> n (Suc b) v = (if vargen\<^sub>N n v \<notin> fmdom'(\<sigma>) then vargen\<^sub>N n v else (vargen \<sigma> (Suc n) b v))"

text \<open>We introduce the convention that the variable input with which the
  variable generator is called, must have the standardized form \<open>$<var><sf>\<close> 
  where var matches the original variable and sf matches an arbitrary suffix. 
  This later guarantees easier readability of freshly generated~variables.\<close>


subsection \<open>Initial States\<close>

text \<open>An initial state of a program is a state that maps all program variables
  onto the arithmetic numeral 0. We are now interested in the automatic construction 
  of such initial~states. \par 
  We first define a helper function that receives a list of program variables
  and constructs a corresponding list of variable and starred expression tuples,
  assigning each variable the initial numeral 0. In order for a traversion of all
  program variables to be feasible, the input of the function must be a list instead 
  of a set. This motivates the previously defined occurrence~functions.\<close>
  fun
    init\<^sub>\<Sigma> :: "var list \<Rightarrow> (var * sexp) list" where
    "init\<^sub>\<Sigma> [] = []" |
    "init\<^sub>\<Sigma> (v # rest) = (v, Exp (Num 0)) # init\<^sub>\<Sigma> rest"

text \<open>It is now possible to construct an initial state from a given list
  of variables. In order to achieve this, we start by eliminating all duplicate 
  variables in the given list. We then use the init function to construct a tuple
  list assigning each variable the initial value, before finally transforming
  the list into a finite map. Note that the computation of the variable list used
  as an input depends on the overlying programming~language.\<close>
  fun
    get_initial\<^sub>\<Sigma> :: "var list \<Rightarrow> \<Sigma>" where
    "get_initial\<^sub>\<Sigma> vars = fm (init\<^sub>\<Sigma> (remdups vars))" 


section \<open>Evaluation\<close>

subsection \<open>Operator Interpretations\<close>

text \<open>In order to provide formal semantics for expressions, it is of  
  crucial importance to give faithful interpretations of the syntactic 
  operator~symbols.\<close>
  primrec 
    valop\<^sub>a :: "op\<^sub>a \<Rightarrow> int \<Rightarrow> int \<Rightarrow> int" where
    "valop\<^sub>a add x y = x + y" |
    "valop\<^sub>a sub x y = x - y" |
    "valop\<^sub>a mul x y = x * y"

  primrec 
    valop\<^sub>b :: "op\<^sub>b \<Rightarrow> bool \<Rightarrow> bool \<Rightarrow> bool" where
    "valop\<^sub>b conj x y = (x \<and> y)" |
    "valop\<^sub>b disj x y = (x \<or> y)" 

  primrec 
    valop\<^sub>r :: "op\<^sub>r \<Rightarrow> int \<Rightarrow> int \<Rightarrow> bool" where
    "valop\<^sub>r leq x y = (x \<le> y)" |
    "valop\<^sub>r geq x y = (x \<ge> y)" |
    "valop\<^sub>r eq x y = (x = y)" 


subsection \<open>Evaluation Functions\<close>

text \<open>Before formalizing the evaluation of expressions, we will first have to
  introduce several necessary helper functions. The first function maps an
  arithmetic expression onto its encased numeral while the second function 
  maps a Boolean expression onto its encased truth value. The mappings are
  undefined iff the arithmetic/Boolean expression is not of concrete nature,
  indicating that these are partial functions. This also implies that the
  functions should only be called if the arithmetic/Boolean expression is known
  to~be~concrete.\<close>
  abbreviation 
    getNum :: "aexp \<Rightarrow> int" where
    "getNum a \<equiv> (case a of (Num n) \<Rightarrow> n)"

  abbreviation 
    getBool :: "bexp \<Rightarrow> bool" where
    "getBool b \<equiv> (case b of (Bool v) \<Rightarrow> v)"

text \<open>We can now formalize the evaluation of expressions in a very similar fashion to
  the original paper. Whilst the given paper only needed one singular evaluation
  function, we need to define an evaluation function for each expression type due
  to our additional type constraints. In contrast to the paper, ill-typed expressions
  can not be derived via our grammar, hence ensuring more compact and easier
  readable semantics as no type checks need to take place. \par
  The evaluation of a specific type of expression in a given state generally
  works~as~follows: \begin{description}
  \item[Primitives] Numerals, truth values and method names are already
  considered concrete, hence they are not simplified~any~further.
  \item[Variables] Variables are simplified by mapping them to their designated 
    arithmetic expression in the given state. If the state maps the variable to
    the symbolic value, the variable will not be simplified. As a variable can 
    only map to arithmetic expressions or the symbolic value, no further type 
    checks need to take place. Note that we, similar to the paper, assume all 
    occurring variables to occur in the state domain, otherwise the evaluation 
    function will be~undefined. 
  \item[Operations] Operations are simplified by evaluating them iff all 
    subexpressions can be simplified to a concrete expression. Otherwise both 
    subexpressions will be simplified as much as possible, whilst syntactically 
    preserving the~operation.
  \end{description} We also provide an evaluation function for lists of expressions 
  and sets of Boolean expressions, as these will later serve as useful~abbreviations.\<close>
  primrec 
    val\<^sub>A :: "aexp \<Rightarrow> \<Sigma> \<Rightarrow> aexp" where
    "val\<^sub>A (Num n) \<sigma> = Num n" |
    "val\<^sub>A (Var x) \<sigma> = (case fmlookup \<sigma> x of Some \<^emph> \<Rightarrow> Var x | Some (Exp e) \<Rightarrow> e)" |
    "val\<^sub>A (Op\<^sub>A a\<^sub>1 op a\<^sub>2) \<sigma> = (if concrete\<^sub>A (val\<^sub>A a\<^sub>1 \<sigma>) \<and> concrete\<^sub>A (val\<^sub>A a\<^sub>2 \<sigma>) 
          then Num (valop\<^sub>a op (getNum (val\<^sub>A a\<^sub>1 \<sigma>)) (getNum (val\<^sub>A a\<^sub>2 \<sigma>)))
          else Op\<^sub>A (val\<^sub>A a\<^sub>1 \<sigma>) op (val\<^sub>A a\<^sub>2 \<sigma>))"

  primrec 
    val\<^sub>B :: "bexp \<Rightarrow> \<Sigma> \<Rightarrow> bexp" where
    "val\<^sub>B (Bool b) \<sigma> = Bool b" |
    "val\<^sub>B (Not b) \<sigma> = (if concrete\<^sub>B (val\<^sub>B b \<sigma>) then Bool (\<not>(getBool (val\<^sub>B b \<sigma>))) 
          else (Not (val\<^sub>B b \<sigma>)))" |
    "val\<^sub>B (Op\<^sub>B b\<^sub>1 op b\<^sub>2) \<sigma> = (if concrete\<^sub>B (val\<^sub>B b\<^sub>1 \<sigma>) \<and> concrete\<^sub>B (val\<^sub>B b\<^sub>2 \<sigma>) 
          then Bool (valop\<^sub>b op (getBool (val\<^sub>B b\<^sub>1 \<sigma>)) (getBool (val\<^sub>B b\<^sub>2 \<sigma>)))
          else Op\<^sub>B (val\<^sub>B b\<^sub>1 \<sigma>) op (val\<^sub>B b\<^sub>2 \<sigma>))" |
    "val\<^sub>B (Op\<^sub>R a\<^sub>1 op a\<^sub>2) \<sigma> = (if concrete\<^sub>A (val\<^sub>A a\<^sub>1 \<sigma>) \<and> concrete\<^sub>A (val\<^sub>A a\<^sub>2 \<sigma>) 
          then Bool (valop\<^sub>r op (getNum (val\<^sub>A a\<^sub>1 \<sigma>)) (getNum (val\<^sub>A a\<^sub>2 \<sigma>)))
          else Op\<^sub>R (val\<^sub>A a\<^sub>1 \<sigma>) op (val\<^sub>A a\<^sub>2 \<sigma>))"

  primrec 
    val\<^sub>E :: "exp \<Rightarrow> \<Sigma> \<Rightarrow> exp" where
    "val\<^sub>E (A a) \<sigma> = A (val\<^sub>A a \<sigma>)" |
    "val\<^sub>E (B b) \<sigma> = B (val\<^sub>B b \<sigma>)" |
    "val\<^sub>E (P m) \<sigma> = P m"

  primrec 
    val\<^sub>S :: "sexp \<Rightarrow> \<Sigma> \<Rightarrow> sexp" where
    "val\<^sub>S (Exp a) \<sigma> = Exp (val\<^sub>A a \<sigma>)" |
    "val\<^sub>S \<^emph> \<sigma> = \<^emph>" 

  primrec 
    lval\<^sub>E :: "exp list \<Rightarrow> \<Sigma> \<Rightarrow> exp list" where
    "lval\<^sub>E [] \<sigma> = []" |
    "lval\<^sub>E (exp # rest) \<sigma> = (val\<^sub>E exp \<sigma>) # (lval\<^sub>E rest \<sigma>)"

  fun
    sval\<^sub>B :: "bexp set \<Rightarrow> \<Sigma> \<Rightarrow> bexp set" where
    "sval\<^sub>B S \<sigma> = (%e. (val\<^sub>B e \<sigma>)) ` S"

text \<open>The following proofs establish that a concrete expression preserves
  its concreteness when evaluated under an arbitrary state. This is obvious,
  considering that a concrete expression cannot be simplified any further.
  The proofs are trivial by making use of structural induction over
  the construction~of~expressions.\<close>
  lemma concrete_pr\<^sub>A: "concrete\<^sub>A(a) \<longrightarrow> concrete\<^sub>A(val\<^sub>A a \<sigma>)" 
    by (induct a, auto)

  lemma concrete_pr\<^sub>B: "concrete\<^sub>B(b) \<longrightarrow> concrete\<^sub>B(val\<^sub>B b \<sigma>)" 
    by (induct b, auto)

  lemma concrete_pr\<^sub>E: "concrete\<^sub>E(e) \<longrightarrow> concrete\<^sub>E(val\<^sub>E e \<sigma>)"
    using concrete_pr\<^sub>A concrete_pr\<^sub>B by (induct e, auto)

  lemma concrete_pr\<^sub>S: "concrete\<^sub>S(s) \<longrightarrow> concrete\<^sub>S(val\<^sub>S s \<sigma>)"
    using concrete_pr\<^sub>A by (induct s, auto)

  lemma l_concrete_pr\<^sub>E: "lconcrete\<^sub>E(l) \<longrightarrow> lconcrete\<^sub>E(lval\<^sub>E l \<sigma>)"
    using concrete_pr\<^sub>E by (induct l, auto)

  lemma s_concrete_pr\<^sub>B: "sconcrete\<^sub>B(S) \<longrightarrow> sconcrete\<^sub>B(sval\<^sub>B S \<sigma>)"
    using concrete_pr\<^sub>B by simp

text \<open>We now infer that the value of a concrete expression does not change 
  after an evaluation under an arbitrary state. Similar to the above proof,
  we use structural induction to reason that a concrete expression cannot
  be simplified~any~further.\<close>
  lemma value_pr\<^sub>A: "concrete\<^sub>A(a) \<longrightarrow> val\<^sub>A a \<sigma> = a"
    by (induct a; auto)

  lemma value_pr\<^sub>B: "concrete\<^sub>B(b) \<longrightarrow> val\<^sub>B b \<sigma> = b"
    by (induct b; auto)

  lemma value_pr\<^sub>E: "concrete\<^sub>E(e) \<longrightarrow> val\<^sub>E e \<sigma> = e"
    using value_pr\<^sub>A value_pr\<^sub>B by (induct e; auto)

  lemma value_pr\<^sub>S: "concrete\<^sub>S(s) \<longrightarrow> val\<^sub>S s \<sigma> = s"
    using value_pr\<^sub>A by (induct s; auto)

  lemma l_value_pr\<^sub>E: "lconcrete\<^sub>E(l) \<longrightarrow> lval\<^sub>E l \<sigma> = l"
    using value_pr\<^sub>E by (induct l; auto)

  lemma s_value_pr\<^sub>B: "sconcrete\<^sub>B(S) \<longrightarrow> sval\<^sub>B S \<sigma> = S"
    using value_pr\<^sub>B by auto

text \<open>In the following proofs we now establish that a variable-free expression 
  always becomes concrete after an evaluation under an arbitrary state. This is
  obvious, as the evaluation of variable-free expressions is guaranteed to be 
  independent of any given state. We again prove the property using 
  structural~induction. Note that we have to make an exception for the symbolic
  value, as it is not considered~concrete.\<close>
  lemma vars_concrete_imp\<^sub>A: "vars\<^sub>A(a) = {} \<longrightarrow> concrete\<^sub>A(val\<^sub>A a \<sigma>)"
    by (induct a, auto)

  lemma vars_concrete_imp\<^sub>B: "vars\<^sub>B(b) = {} \<longrightarrow> concrete\<^sub>B(val\<^sub>B b \<sigma>)"
    using vars_concrete_imp\<^sub>A by (induct b, auto) 

  lemma vars_concrete_imp\<^sub>E: "vars\<^sub>E(e) = {} \<longrightarrow> concrete\<^sub>E(val\<^sub>E e \<sigma>)"
    using vars_concrete_imp\<^sub>A vars_concrete_imp\<^sub>B by (induct e, auto) 

  lemma vars_concrete_imp\<^sub>S: "vars\<^sub>S(s) = {} \<longrightarrow> s = \<^emph> \<or> concrete\<^sub>S(val\<^sub>S s \<sigma>)"
    using vars_concrete_imp\<^sub>A by (induct s, auto)

  lemma l_vars_concrete_imp\<^sub>E: "lvars\<^sub>E(l) = {} \<longrightarrow> lconcrete\<^sub>E(lval\<^sub>E l \<sigma>)"
    using vars_concrete_imp\<^sub>E by (induct l, auto) 

  lemma s_vars_concrete_imp\<^sub>B: "svars\<^sub>B(S) = {} \<longrightarrow> sconcrete\<^sub>B(sval\<^sub>B S \<sigma>)"
    using vars_concrete_imp\<^sub>B by simp 

text \<open>Last but not least, we establish that any expression turns concrete 
  after an evaluation under a concrete state, provided that all variables occurring 
  in the expression are located in the domain of the state. The property holds, since
  all variables occurring in the expression can be replaced with concrete numerals
  due to the concreteness of the state. The proofs are conducted by 
  making use of structural induction. Note that we again have to make an exception 
  for the symbolic~value.\<close>
  lemma concrete_imp\<^sub>A: "concrete\<^sub>\<Sigma>(\<sigma>) \<and> vars\<^sub>A(a) \<subseteq> fmdom'(\<sigma>) \<longrightarrow> concrete\<^sub>A(val\<^sub>A a \<sigma>)"
  proof (induct a)
    case (Numeral n)
    show ?case by auto 
    \<comment> \<open>The evaluation of numerals is independent of the state, thus trivially
       concrete.\<close>
  next
    case (Variable x)
    show "concrete\<^sub>\<Sigma>(\<sigma>) \<and> vars\<^sub>A(Var x) \<subseteq> fmdom'(\<sigma>) \<longrightarrow> concrete\<^sub>A(val\<^sub>A (Var x) \<sigma>)"
    \<comment> \<open>The evaluation of variables depends on the state, thus we have to analyze
       this case further.\<close>
    proof (rule impI)
      assume premise: "concrete\<^sub>\<Sigma>(\<sigma>) \<and> vars\<^sub>A(Var x) \<subseteq> fmdom'(\<sigma>)"
      \<comment> \<open>We assume \<open>\<sigma>\<close> is concrete and x is located in the domain of \<open>\<sigma>\<close>.\<close>
      hence "concrete\<^sub>S(the(fmlookup \<sigma> x))"
        by (simp add: concrete\<^sub>\<Sigma>_def) 
      \<comment> \<open>Hence the value of x in \<open>\<sigma>\<close> must also be of concrete nature.\<close>
      hence "\<exists>a. the(fmlookup \<sigma> x) = Exp a \<and> concrete\<^sub>A a" 
        by (metis sexp.exhaust sexp.simps(4) sexp.simps(5)) 
      \<comment> \<open>This means that the value of x in \<open>\<sigma>\<close> must be a concrete arithmetic expression a.\<close>
      hence "\<exists>n. the(fmlookup \<sigma> x) = Exp (Num n)"
        by (metis aexp.exhaust aexp.simps(11) aexp.simps(12))
      \<comment> \<open>The concreteness of $a$ in turn implies that the value of x in \<open>\<sigma>\<close> must be a 
         numeral n.\<close>
      with premise show "concrete\<^sub>A(val\<^sub>A (Var x) \<sigma>)" 
        using fmdom'_notI by force
      \<comment> \<open>Knowing that x maps to a numeral, we infer that the evaluation of x must
         be concrete.\<close>
    qed
  next
    case (Op\<^sub>A a\<^sub>1 op a\<^sub>2)
    show ?case 
      using Op\<^sub>A.hyps(1) Op\<^sub>A.hyps(2) by simp 
    \<comment> \<open>The induction step can be trivially closed via the induction hypothesis.\<close>
  qed

  lemma concrete_imp\<^sub>B: "concrete\<^sub>\<Sigma>(\<sigma>) \<and> vars\<^sub>B(b) \<subseteq> fmdom'(\<sigma>) \<longrightarrow> concrete\<^sub>B(val\<^sub>B b \<sigma>)"
    using concrete_imp\<^sub>A by (induct b, auto)

  lemma concrete_imp\<^sub>E: "concrete\<^sub>\<Sigma>(\<sigma>) \<and> vars\<^sub>E(e) \<subseteq> fmdom'(\<sigma>) \<longrightarrow> concrete\<^sub>E(val\<^sub>E e \<sigma>)"
    using concrete_imp\<^sub>A concrete_imp\<^sub>B by (induct e, auto)

  lemma concrete_imp\<^sub>S: "concrete\<^sub>\<Sigma>(\<sigma>) \<and> vars\<^sub>S(s) \<subseteq> fmdom'(\<sigma>) \<longrightarrow> s = \<^emph> \<or> concrete\<^sub>S(val\<^sub>S s \<sigma>)"
    using concrete_imp\<^sub>A by (induct s, auto)

  lemma l_concrete_imp\<^sub>E: "concrete\<^sub>\<Sigma>(\<sigma>) \<and> lvars\<^sub>E(l) \<subseteq> fmdom'(\<sigma>) \<longrightarrow> lconcrete\<^sub>E(lval\<^sub>E l \<sigma>)"
    using concrete_imp\<^sub>E by (induct l, auto)

  lemma s_concrete_imp\<^sub>B: "concrete\<^sub>\<Sigma>(\<sigma>) \<and> svars\<^sub>B(S) \<subseteq> fmdom'(\<sigma>) \<longrightarrow> sconcrete\<^sub>B(sval\<^sub>B S \<sigma>)"
    using concrete_imp\<^sub>B by (simp add: UN_subset_iff)

text \<open>The evaluation of expressions can be demonstrated using the following
  practical examples.\<close>
  lemma "val\<^sub>A aExp_ex \<sigma>\<^sub>2 = Num 8"
    by (simp add: aExp_ex_def \<sigma>\<^sub>2_def)

  lemma "val\<^sub>B bExp_ex \<sigma>\<^sub>1 = (((Var ''y'') \<^sub>Amul (Num 4)) \<^sub>Req (Num 2)) \<^sub>Bdisj (Bool False)"
    by (simp add: bExp_ex_def \<sigma>\<^sub>1_def)


subsection \<open>State Simplifications\<close>

text \<open>In the original paper, states are always implicitly assumed to be simplified
  as much as possible. However, such a formalization is not reasonably feasible in 
  Isabelle. We therefore decide to additionally propose a state simplification function 
  that explicitly simplifies all expressions in the image of a given state.  This
  function can be used to further simplify non-simplified states. It can also be 
  utilized to simplify a state if the simplification status of the given 
  state~is~unknown. \par
  Note that the already predefined fmmap-keys function of the finite~map theory 
  can be used to update all key-value tuples of the state given as the second argument 
  with the function provided as the first~argument.\<close>
  definition
    sim\<^sub>\<Sigma> :: "\<Sigma> \<Rightarrow> \<Sigma>" where
    "sim\<^sub>\<Sigma> \<sigma> \<equiv> fmmap_keys (\<lambda>v e. val\<^sub>S e \<sigma>) \<sigma>"

text \<open>We can now show that the execution of the state simplification function
  preserves the domain of the state, implying that no variables are added 
  or~removed.\<close>
  lemma sim\<^sub>\<Sigma>_dom_pr: "fmdom'(\<sigma>) = fmdom'(sim\<^sub>\<Sigma> \<sigma>)"
    by (simp add: sim\<^sub>\<Sigma>_def fmdom'_alt_def)

text \<open>Using the previous property, we can now establish that a concrete
  state preserves its concreteness after performing a state~simplification.
  This holds, because the concreteness of starred expressions are preserved under
  arbitrary state evaluations, which we have previously~shown.\<close>
  lemma concrete_sim_pr: "concrete\<^sub>\<Sigma>(\<sigma>) \<longrightarrow> concrete\<^sub>\<Sigma>(sim\<^sub>\<Sigma> \<sigma>)" 
    apply (simp add: concrete\<^sub>\<Sigma>_def sim\<^sub>\<Sigma>_def)
    using concrete_pr\<^sub>S sim\<^sub>\<Sigma>_dom_pr sim\<^sub>\<Sigma>_def by (metis fmdom'_notI option.map_sel)

text \<open>We close this section by proving that a wellformed state without symbolic 
  variables is guaranteed to turn concrete after a state simplification. This holds,
  because any expression in a wellformed state without symbolic variables must be
  variable-free, and therefore can always be fully~simplified.\<close>
  lemma sim_concrete_imp: 
    assumes "symb\<^sub>\<Sigma>(\<sigma>) = {} \<and> wf\<^sub>\<Sigma>(\<sigma>)" 
    shows "concrete\<^sub>\<Sigma>(sim\<^sub>\<Sigma> \<sigma>)"
  proof -
    have "\<forall>v \<in> fmdom'(\<sigma>). vars\<^sub>S(the(fmlookup \<sigma> v)) = {}" 
      using assms by (auto simp add: wf\<^sub>\<Sigma>_def)
    \<comment> \<open>Due to the wellformedness of the state and the empty set of symbolic variables, 
       no variable of any kind can occur in mapped expressions of the state.\<close>
    thus "concrete\<^sub>\<Sigma>(sim\<^sub>\<Sigma> \<sigma>)" 
      using assms vars_concrete_imp\<^sub>S apply (simp add: symb\<^sub>\<Sigma>_def concrete\<^sub>\<Sigma>_def sim\<^sub>\<Sigma>_def) 
      by (metis (no_types, lifting) None_eq_map_option_iff fmlookup_dom'_iff fmlookup_fmmap_keys is_none_code option.collapse the_map_option)
    \<comment> \<open>Hence our state must be concrete after a singular state simplification.\<close>
  qed



section \<open>Traces\<close>

subsection \<open>Symbolic Traces\<close>

text \<open>We are now interested in introducing a notion of symbolic traces, similar
  to the one provided in the original paper. We begin by proposing a new type
  consisting of all possible events that can occur during program executions. An
  input event represents the receival of a temporary unknown expression from another 
  system. A method invocation event symbolizes the invocation of another method, whilst
  a method invocation reaction event matches the corresponding reaction of~the~callee. 
  \par
  Instead of providing a type for events, it is also possible to denote a constructor 
  for each kind of event in the trace syntax. However, this would cause problems
  when adding or removing specific events, as all inductions over traces would
  need to be adapted. We have therefore decided to outsource the events
  into an own event type, thereby increasing the modularity of the model. Note that
  this implies that the event type can be freely modified without influencing
  program-independent~properties.\<close>
  datatype event_marker = 
      inpEv \<comment> \<open>Input Event\<close>
    | invEv \<comment> \<open>Method Invocation Event\<close>
    | invREv \<comment> \<open>Method Invocation Reaction Event\<close>

text \<open>Singular elements of a trace are called trace atoms. A trace atom can either 
  be a state or an event with a list of expressions as~its~arguments.\<close>
  datatype trace_atom = 
      State \<Sigma> \<comment> \<open>State\<close>
    | Event event_marker "exp list" \<comment> \<open>Event\<close>

text \<open>We can now consider a symbolic trace as a sequence of trace atoms starting
  with the empty trace. Note that this definition forces all of our traces to be
  finite, meaning that infinite traces can not be formalized in this model. This
  problem can be circumvented by defining the set of traces as the set of all 
  partial functions mapping from natural numbers to trace atoms. We propose this as
  an idea for further developments of~this~model.\<close>
  datatype \<T> = 
      Epsilon \<comment> \<open>Empty Trace\<close>
    | Transition \<T> trace_atom \<comment> \<open>Trace Transition\<close>

text \<open>We also add a minimal concrete syntax for traces, thereby improving 
  the readability of trace atoms and traces~in~programs.\<close>
  notation State ("State \<llangle>_\<rrangle>" 61)
  notation Event ("Event \<llangle>_,_\<rrangle>" 61)
  notation Epsilon ("\<epsilon>") 
  notation Transition (infix "\<leadsto>" 60)

text \<open>We call a trace consisting of a singular state a singleton trace. Similar
  to the paper, we denote the singleton trace of state \<open>\<sigma>\<close> as \<open>\<langle>\<sigma>\<rangle>\<close>. This
  abbreviation will be used as a condensed notation when formalizing~traces.\<close>
  abbreviation 
    singleton_trace :: "\<Sigma> \<Rightarrow> \<T>" ("\<langle>_\<rangle>") where
    "\<langle>\<sigma>\<rangle> \<equiv> \<epsilon> \<leadsto> State\<llangle>\<sigma>\<rrangle>" 

text \<open>In the following we provide multiple examples for traces that can be
  syntactically derived in the given trace~syntax.\<close>
  definition
    \<tau>\<^sub>1 :: \<T> where
    "\<tau>\<^sub>1 \<equiv> (\<langle>\<sigma>\<^sub>1\<rangle> \<leadsto> Event\<llangle>inpEv, []\<rrangle>) \<leadsto> State\<llangle>\<sigma>\<^sub>1\<rrangle>"

  definition
    \<tau>\<^sub>2 :: \<T> where
    "\<tau>\<^sub>2 \<equiv> (\<langle>\<sigma>\<^sub>1\<rangle> \<leadsto> Event\<llangle>invREv, [P ''foo'', A (Num 2)]\<rrangle>) \<leadsto> State\<llangle>\<sigma>\<^sub>2\<rrangle>"

  definition
    \<tau>\<^sub>3 :: \<T> where
    "\<tau>\<^sub>3 \<equiv> \<langle>\<sigma>\<^sub>2\<rangle> \<leadsto> State\<llangle>\<circle>\<rrangle>"


subsection \<open>Conditioned Symbolic Traces\<close>

text \<open>Resembling the definition of the original paper, we introduce a path condition
  as a set of Boolean~expressions.\<close>
  type_synonym path_condition = "bexp set"

text \<open>A path condition is consistent iff the path condition is concrete and does 
  not contain~false.\<close>
  definition 
    consistent :: "path_condition \<Rightarrow> bool" where
    "consistent pc \<equiv> sconcrete\<^sub>B(pc) \<and> (\<forall>b \<in> pc. getBool(b))"

text \<open>We define a conditioned symbolic trace as a combination of a path condition 
  and a symbolic trace. Similar to the paper, we use \<open>\<triangleright>\<close> as an infix~symbol.\<close>
  datatype \<CC>\<T> = 
    Trace path_condition \<T> (infix "\<triangleright>" 59)

text \<open>We can now denote projections mapping a conditioned symbolic trace onto its 
  specific components. These functions will serve as handy abbreviations at a
  later~point.\<close>
  fun
    pc_projection :: "\<CC>\<T> \<Rightarrow> path_condition" ("\<Down>\<^sub>p") where
    "\<Down>\<^sub>p (pc \<triangleright> \<tau>) = pc"

  fun
    trace_projection :: "\<CC>\<T> \<Rightarrow> \<T>" ("\<Down>\<^sub>\<T>") where
    "\<Down>\<^sub>\<T> (pc \<triangleright> \<tau>) = \<tau>"

text \<open>We prove that a consistent path condition must always be concrete, which 
  can be trivially inferred using the consistency~definition.\<close>
  lemma consistent_concrete_imp: "consistent(pc) \<longrightarrow> sconcrete\<^sub>B(pc)"
    by (simp add: consistent_def)

text \<open>Similar to the properties of the evaluation functions, we establish
  that the consistency of a path condition is preserved when evaluated under
  an arbitrary~state.\<close>
  lemma consistent_pr: "consistent pc \<longrightarrow> consistent (sval\<^sub>B pc \<sigma>)"
    using consistent_def s_value_pr\<^sub>B by auto

text \<open>We furthermore derive that the consistency of a path condition constructed 
  from an evaluated property \textit{b} also implies the concreteness of the path 
  condition constructed from the evaluated negated property \textit{not b}. This is 
  trivial, considering that a consistent path condition is guaranteed to be concrete 
  and the negation of a concrete Boolean expression is always a concrete 
  Boolean~expression.\<close>
  lemma conc_pc_pr\<^sub>B: "consistent {val\<^sub>B b \<sigma>} \<longrightarrow> sconcrete\<^sub>B {val\<^sub>B (not b) \<sigma>}"
    by (simp add: consistent_def)

  lemma conc_pc_pr\<^sub>B\<^sub>N: "consistent {val\<^sub>B (not b) \<sigma>} \<longrightarrow> sconcrete\<^sub>B {val\<^sub>B b \<sigma>}"
    by (simp add: consistent_def)

text \<open>We can now take a look at several examples for conditioned symbolic traces in
  our~model.\<close>
  definition
    \<pi>\<^sub>1 :: \<CC>\<T> where
    "\<pi>\<^sub>1 \<equiv> {} \<triangleright> \<tau>\<^sub>1"

  definition
    \<pi>\<^sub>2 :: \<CC>\<T> where
    "\<pi>\<^sub>2 \<equiv> {Bool True} \<triangleright> \<tau>\<^sub>2"

  definition
    \<pi>\<^sub>3 :: \<CC>\<T> where
    "\<pi>\<^sub>3 \<equiv> {Bool False} \<triangleright> \<tau>\<^sub>3"


subsection \<open>Trace Modifications\<close>

text \<open>We provide a straightforward, formal definition of trace concatenation.\<close>
  primrec 
    concat :: "\<T> \<Rightarrow> \<T> \<Rightarrow> \<T>" (infix "\<cdot>" 60) where
    "\<tau> \<cdot> \<epsilon> = \<tau>" |
    "\<tau> \<cdot> (\<tau>' \<leadsto> t) = (\<tau> \<cdot> \<tau>') \<leadsto> t"

text \<open>We next introduce recursive partial functions that map a symbolic trace onto 
  its first and last occurring state. In order for the function mapping a trace onto 
  its first state to be defined, we demand the given trace to have a state as its 
  initial trace atom. In contrast, the function mapping a trace onto its last element 
  requires the trace to end with a state. This behaviour is not a problem however, as 
  we will later ensure that any wellformed trace starts and ends with~a~state.\<close>
  fun 
    first\<^sub>\<T> :: "\<T> \<Rightarrow> \<Sigma>" where
    "first\<^sub>\<T> \<langle>\<sigma>\<rangle> = \<sigma>" |
    "first\<^sub>\<T> (\<tau> \<leadsto> t) = first\<^sub>\<T>(\<tau>)" |
    "first\<^sub>\<T> \<epsilon> = undefined"

  fun 
    last\<^sub>\<T> :: "\<T> \<Rightarrow> \<Sigma>" where
    "last\<^sub>\<T> (\<tau> \<leadsto> State\<llangle>\<sigma>\<rrangle>) = \<sigma>" |
    "last\<^sub>\<T> _ = undefined"

text \<open>We next give a definition for the semantic chop of two symbolic traces, 
  while slightly deviating from the definition in the original paper.
  In order to perform a semantic chop on two traces, it is vital that both traces align,
  meaning that the last state of the first trace must match with the first state of 
  the second trace. The semantic chop then concatenates both traces, whilst removing 
  one of the duplicates. In contrast to the original paper however, we will not 
  explicitly ensure that both traces align with each other. Instead, the alignment 
  will be established in the formalization of the overlying LAGC semantics. The 
  outsourcing of this property makes sure that Isabelle can generate code for the 
  semantic chop~operation. \par
  Note that we require the first trace to end with a state in order for this 
  function to be defined, thus making this a partial~function.\<close>
  fun 
    semantic_chop\<^sub>\<T> :: "\<T> \<Rightarrow> \<T> \<Rightarrow> \<T>" (infix "\<^emph>\<^emph>" 61) where
    "(\<tau> \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<^emph>\<^emph> \<tau>' = \<tau> \<cdot> \<tau>'" |
    "_ \<^emph>\<^emph> \<tau>' = undefined"

text \<open>We can now heighten the previous definition to the semantic chop between 
  two conditioned symbolic traces. In this scenario, the semantic chop denotes
  the union of both path conditions and the semantic chop of both encased
  symbolic~traces.\<close>
  fun 
    semantic_chop\<^sub>\<pi> :: "\<CC>\<T> \<Rightarrow> \<CC>\<T> \<Rightarrow> \<CC>\<T>" (infix "\<^emph>\<^emph>\<^sub>\<pi>" 61) where
    "(pc\<^sub>1 \<triangleright> \<tau>) \<^emph>\<^emph>\<^sub>\<pi> (pc\<^sub>2 \<triangleright> \<tau>') = (pc\<^sub>1 \<union> pc\<^sub>2) \<triangleright> (\<tau> \<^emph>\<^emph> \<tau>')"

text \<open>In the following we provide an example for the semantic chop~operation.\<close>
  lemma "\<pi>\<^sub>1 \<^emph>\<^emph>\<^sub>\<pi> \<pi>\<^sub>3 = {Bool False} \<triangleright> ((\<langle>\<sigma>\<^sub>1\<rangle> \<leadsto> Event\<llangle>inpEv, []\<rrangle>) \<leadsto> State\<llangle>\<sigma>\<^sub>2\<rrangle>) \<leadsto> State\<llangle>\<circle>\<rrangle>"
    by (auto simp add: \<tau>\<^sub>1_def \<tau>\<^sub>3_def \<pi>\<^sub>1_def \<pi>\<^sub>3_def)


subsection \<open>Event Insertions\<close>

text \<open>The wellformedness notion for traces will require each event occurring in a
  trace to be surrounded by identical states. In fashion to the original paper, 
  we can now introduce a function that automatically surrounds a given
  event with a particular state. This function can later be used to 
  abbreviate the insertion of specific events into given traces whilst 
  preserving~wellformedness. Note that the inserted event is simplified before
  the actual~insertion.\<close>
  definition [simp]:
    "gen_event ev \<sigma> e \<equiv> (\<langle>\<sigma>\<rangle> \<leadsto> Event\<llangle>ev, lval\<^sub>E e \<sigma>\<rrangle>) \<leadsto> State\<llangle>\<sigma>\<rrangle>"

text \<open>We establish that in any symbolic trace generated by the previous definition,
  the first and last state match.\<close>
  lemma gen_event_match\<^sub>\<Sigma>: "first\<^sub>\<T> (gen_event ev \<sigma> e) = last\<^sub>\<T> (gen_event ev \<sigma> e)"
    by simp


subsection \<open>Wellformedness\<close>

text \<open>Before we begin with the formalization of the wellformedness predicate,
  we first denote an inductive function that maps a trace onto a set of all its 
  symbolic variables, i.e. all variables that occur symbolic in at least one state 
  of the trace. The definition of the function is straightforward. We unfold the
  trace from the back, whilst recursively collecting its symbolic~variables.\<close>
fun
  symb\<^sub>\<T> :: "\<T> \<Rightarrow> var set" where
  "symb\<^sub>\<T> \<epsilon> = {}" |
  "symb\<^sub>\<T> (\<tau> \<leadsto> Event\<llangle>ev, e\<rrangle>) = symb\<^sub>\<T>(\<tau>)" |
  "symb\<^sub>\<T> (\<tau> \<leadsto> State\<llangle>\<sigma>\<rrangle>) = symb\<^sub>\<Sigma>(\<sigma>) \<union> symb\<^sub>\<T>(\<tau>)" 

text \<open>Using the previous function definition, we can now establish a wellformedness
  notion for conditioned symbolic traces. Similar to the paper, we demand the 
  following five properties~to~hold: \begin{description} \par

  \item[Condition 1] All variables that occur non-symbolic in any state of the given 
  trace, are not allowed to occur symbolic anywhere in the same~trace. \par
  In order to formalize this property, it is conceivable to define a function 
  that collects all states of a trace and then quantifies over all these states, 
  thereby closely following the formalization in the paper. However, we instead 
  decide to provide an inductive definition of the property, as this ensures that we 
  can later provide simple inductive proofs without another layer of~complexity. \par
  \item[Condition 2] All variables occurring in the path condition of the conditioned
  symbolic trace must be symbolic variables of the~trace. \par
  The definition of this property is trivial, matching the definition
  in the~paper. \par
  \item[Condition 3] All variables occurring in expressions of trace events must be 
  symbolic variables of the~trace. \par
  Similar to the first condition, we again do not quantify over all events, 
  but provide an inductive property instead, thus also ensuring that our 
  formalizations are consistent in its design~choices. \par
  \item[Condition 4] All events occurring in a trace must be surrounded by 
  identical~states. \par
  The formalization of this property strongly deviates from the paper, as we
  again choose to give an inductive formalization instead of quantifying over all
  events in order to uphold consistency. \par
  We split the formalization of this property into two separate functions. The first
  function uses trace pattern matching to check that any two states, which occur next
  to trace events, match. The second function encases the first function, whilst
  additionally verifying that the trace does not start or end with~an~event. \par
  \item[Condition 5] All states of the trace must be wellformed. \par
  The formalization of this property is also straightforward by making use
  of~recursion. \par
  \end{description} We can now call a conditioned symbolic trace wellformed iff 
  all five wellformedness conditions are~satisfied.\<close>
  fun \<comment> \<open>Condition (1)\<close>
    non_symb_disjunct\<^sub>\<T> :: "\<T> \<Rightarrow> var set \<Rightarrow> bool" (infix "\<diamondop>\<diamondop>\<^sub>\<T>" 64) where
    "\<epsilon> \<diamondop>\<diamondop>\<^sub>\<T> V = True" |
    "(\<tau> \<leadsto> Event\<llangle>ev, e\<rrangle>) \<diamondop>\<diamondop>\<^sub>\<T> V = \<tau> \<diamondop>\<diamondop>\<^sub>\<T> V" |
    "(\<tau> \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<diamondop>\<diamondop>\<^sub>\<T> V = ((fmdom'(\<sigma>) - symb\<^sub>\<Sigma>(\<sigma>)) \<inter> V = {} \<and> \<tau> \<diamondop>\<diamondop>\<^sub>\<T> V)"

  fun \<comment> \<open>Condition (2)\<close>
    wf_pc\<^sub>\<pi> :: "path_condition \<Rightarrow> var set \<Rightarrow> bool" (infix "\<subseteq>\<^sub>p" 64) where
    "pc \<subseteq>\<^sub>p V = (svars\<^sub>B(pc) \<subseteq> V)"

  fun \<comment> \<open>Condition (3)\<close>
    wf_events\<^sub>\<T> :: "\<T> \<Rightarrow> var set \<Rightarrow> bool" (infix "\<subseteq>\<^sub>E" 64) where 
    "\<epsilon> \<subseteq>\<^sub>E V = True" |
    "(\<tau> \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<subseteq>\<^sub>E V = \<tau> \<subseteq>\<^sub>E V" |
    "(\<tau> \<leadsto> Event\<llangle>ev, e\<rrangle>) \<subseteq>\<^sub>E V = (lvars\<^sub>E(e) \<subseteq> V \<and> \<tau> \<subseteq>\<^sub>E V)"

  fun \<comment> \<open>Condition (4a)\<close>
    wf_surround\<^sub>\<T> :: "\<T> \<Rightarrow> bool" where
    "wf_surround\<^sub>\<T> \<epsilon> = True" |
    "wf_surround\<^sub>\<T> (((\<tau> \<leadsto> State\<llangle>\<sigma>'\<rrangle>) \<leadsto> Event\<llangle>ev, e\<rrangle>) \<leadsto> State\<llangle>\<sigma>\<rrangle>) = (\<sigma> = \<sigma>' \<and> wf_surround\<^sub>\<T>(\<tau> \<leadsto> State\<llangle>\<sigma>'\<rrangle>))" |
    "wf_surround\<^sub>\<T> (\<tau> \<leadsto> ta) = wf_surround\<^sub>\<T>(\<tau>)"

  fun \<comment> \<open>Condition (4b)\<close>
    wf_seq\<^sub>\<T> :: "\<T> \<Rightarrow> bool" where
    "wf_seq\<^sub>\<T> \<tau> = (wf_surround\<^sub>\<T>(\<tau>) \<and> (\<exists>\<sigma> \<sigma>'. first\<^sub>\<T>(\<tau>) = \<sigma> \<and> last\<^sub>\<T>(\<tau>) = \<sigma>'))"

  fun \<comment> \<open>Condition (5)\<close>
    wf_states\<^sub>\<T> :: "\<T> \<Rightarrow> bool" where
    "wf_states\<^sub>\<T> \<epsilon> = True" |
    "wf_states\<^sub>\<T> (\<tau> \<leadsto> Event\<llangle>ev, e\<rrangle>) = wf_states\<^sub>\<T>(\<tau>)" |
    "wf_states\<^sub>\<T> (\<tau> \<leadsto> State\<llangle>\<sigma>\<rrangle>) = (wf\<^sub>\<Sigma>(\<sigma>) \<and> wf_states\<^sub>\<T>(\<tau>))"

  definition \<comment> \<open>Wellformedness\<close>
    wf\<^sub>\<pi> :: "\<CC>\<T> \<Rightarrow> bool" where
    "wf\<^sub>\<pi> \<pi> \<equiv> (\<Down>\<^sub>\<T> \<pi> \<diamondop>\<diamondop>\<^sub>\<T> symb\<^sub>\<T> (\<Down>\<^sub>\<T> \<pi>) \<and> 
              \<Down>\<^sub>p \<pi> \<subseteq>\<^sub>p symb\<^sub>\<T> (\<Down>\<^sub>\<T> \<pi>) \<and> 
              \<Down>\<^sub>\<T> \<pi> \<subseteq>\<^sub>E symb\<^sub>\<T> (\<Down>\<^sub>\<T> \<pi>) \<and> 
              wf_seq\<^sub>\<T> (\<Down>\<^sub>\<T> \<pi>) \<and> 
              wf_states\<^sub>\<T> (\<Down>\<^sub>\<T> \<pi>))"

text \<open>We establish that the symbolic variables of two arbitrary traces
  match the symbolic variables of the concatenation of both traces, showing that
  the symbolic function for traces is homomorphic wrt. union~and~concatenation.\<close>
  lemma symb_union: "symb\<^sub>\<T> \<tau> \<union> symb\<^sub>\<T> \<tau>' = symb\<^sub>\<T>(\<tau> \<cdot> \<tau>')"
  proof (induct \<tau>')
  \<comment> \<open>We prove the theorem via induction over trace \<open>\<tau>'\<close>.\<close>
    case Epsilon
    thus ?case by simp
    \<comment> \<open>If \<open>\<tau>'\<close> is \<open>\<epsilon>\<close>, the case is trivial, because \<open>symb\<^sub>\<T>(\<epsilon>) = {}\<close>.\<close>
  next
    case (Transition \<tau>'' ta)
    thus ?case
      using Transition.hyps by (induct ta; auto)
    \<comment> \<open>If \<open>\<tau>'\<close> is \<open>(\<tau>'' \<leadsto> ta)\<close>, we use our induction hypothesis and a case
       distinction regarding the nature of trace atom \<open>ta\<close> to 
       automatically close the case.\<close>
  qed

text \<open>We take another look at our example traces. As can be reasoned, 
  \<open>\<tau>\<^sub>2\<close> is not wellformed, as it already violates the first wellformedness
  condition. $y$ occurs non-symbolic in \<open>\<sigma>\<^sub>2\<close>, but symbolic in \<open>\<sigma>\<^sub>1\<close>, which is a state 
  of the same trace. However, \<open>\<tau>\<^sub>1\<close> and \<open>\<tau>\<^sub>3\<close> are wellformed, as all five conditions 
  are satisfied. This conclusion can also be inferred using the Isabelle~system.\<close>
  lemma "wf\<^sub>\<pi> \<pi>\<^sub>1 \<and> \<not>wf\<^sub>\<pi> \<pi>\<^sub>2 \<and> wf\<^sub>\<pi> \<pi>\<^sub>3"
    by (auto simp add: \<sigma>\<^sub>1_def \<sigma>\<^sub>2_def \<tau>\<^sub>1_def \<tau>\<^sub>2_def \<tau>\<^sub>3_def \<pi>\<^sub>1_def \<pi>\<^sub>2_def \<pi>\<^sub>3_def symb\<^sub>\<Sigma>_def wf\<^sub>\<Sigma>_def wf\<^sub>\<pi>_def)


subsection \<open>Concreteness\<close>

text \<open>We will now provide a definition for the concreteness of traces.
  Note that we are going to deviate from the original paper by clearly separating 
  the concreteness notion of symbolic traces from the concreteness notion of 
  conditioned symbolic~traces. \par
  We consider a symbolic trace concrete iff all states and event expressions
  occurring in the trace are concrete. In contrast to the original paper, we do not 
  require a concrete symbolic trace to satisfy any wellformedness condition. This design 
  choice strongly simplifies the original property of the paper, thus later ensuring 
  a significant reduction of the size of trace concreteness proofs. We propose an
  extension with wellformedness guarantees as a possible idea to further 
  develop~this~model.\<close>
  fun
    concrete\<^sub>\<T> :: "\<T> \<Rightarrow> bool" where
    "concrete\<^sub>\<T> \<epsilon> = True" |
    "concrete\<^sub>\<T> (\<tau> \<leadsto> State\<llangle>\<sigma>\<rrangle>) = (concrete\<^sub>\<Sigma> \<sigma> \<and> concrete\<^sub>\<T> \<tau>)" |
    "concrete\<^sub>\<T> (\<tau> \<leadsto> Event\<llangle>ev, e\<rrangle>) = (lconcrete\<^sub>E e \<and> concrete\<^sub>\<T> \<tau>)" 

text \<open>Similar to the original paper, we consider a conditioned symbolic trace as 
  concrete iff it is wellformed, and consists of a concrete path condition, as well as 
  a concrete symbolic trace. Note that this strongly differs from our concreteness
  notion of symbolic traces, as we require all wellformedness conditions to
  be~satisfied.\<close>
  definition
    concrete\<^sub>\<pi> :: "\<CC>\<T> \<Rightarrow> bool" where
    "concrete\<^sub>\<pi> \<pi> = (wf\<^sub>\<pi> \<pi> \<and> sconcrete\<^sub>B (\<Down>\<^sub>p \<pi>) \<and> concrete\<^sub>\<T> (\<Down>\<^sub>\<T> \<pi>))" 

text \<open>We establish that a concrete symbolic trace only consists of wellformed
  states. This proof is trivial, since all states occurring in concrete traces
  must be of concrete nature. Concrete states in turn are always wellformed, as we
  have already proven in an earlier~lemma.\<close>
  lemma conc_wf_imp\<^sub>\<T>: "concrete\<^sub>\<T> \<tau> \<longrightarrow> wf_states\<^sub>\<T> \<tau>"
  proof (induct \<tau>)
  \<comment> \<open>We conduct a structural induction over the trace \<open>\<tau>\<close>.\<close>
    case Epsilon
    thus ?case by simp
    \<comment> \<open>If \<open>\<tau>\<close> is \<open>\<epsilon>\<close>, the case can be trivially closed, as it consists of no states.\<close>
  next
    case (Transition \<tau>' ta)
    thus ?case using concrete_wf_imp\<^sub>\<Sigma> by (induct ta; simp)
    \<comment> \<open>If \<open>\<tau>\<close> is \<open>(\<tau>' \<leadsto> ta)\<close>, the case can be closed via a case distinction
       over trace atom \<open>ta\<close> and the knowledge that every concrete state is
       also~wellformed.\<close>
  qed

text \<open>Every concrete conditioned symbolic trace is guaranteed to be wellformed.
  This directly follows from the definition of conditioned symbolic~traces.\<close>
  lemma conc_wf_imp\<^sub>\<pi>: "concrete\<^sub>\<pi>(\<tau>) \<longrightarrow> wf\<^sub>\<pi>(\<tau>)"
    by (simp add: concrete\<^sub>\<pi>_def)

text \<open>We prove the concreteness of the minimal event trace generated from 
  arbitrary events, arbitrary concrete symbolic states and an empty expression
  list. The concreteness holds, because all path conditions and event expressions
  of arbitrary concrete traces are guaranteed to be~variable-free.\<close>
  lemma "concrete\<^sub>\<Sigma> \<sigma> \<longrightarrow> concrete\<^sub>\<pi> ({} \<triangleright> (gen_event ev \<sigma> []))"
    using concrete_wf_imp\<^sub>\<Sigma> by (auto simp add: consistent_def wf\<^sub>\<pi>_def concrete\<^sub>\<pi>_def)

text \<open>A concrete trace only contains concrete states, which in turn are not 
  allowed to map variables to symbolic values. Hence, we can use Isabelle to
  derive that all concrete traces contain no symbolic~variables.\<close>
  lemma concrete_symb_imp\<^sub>\<T>: "concrete\<^sub>\<T>(\<tau>) \<longrightarrow> symb\<^sub>\<T>(\<tau>) = {}"
  proof (induct \<tau>)
  \<comment> \<open>We conduct a structural induction over \<open>\<tau>\<close>.\<close>
    case Epsilon
    show ?case by simp
    \<comment> \<open>If \<open>\<tau>\<close> is \<open>\<epsilon>\<close>, the case is trivial, because \<open>symb\<^sub>\<T>(\<epsilon>) = {}\<close>.\<close>
  next
    case (Transition \<tau>' ta)
    show ?case
      using Transition.hyps concrete_symb_imp\<^sub>\<Sigma> by (induct ta; simp)
      \<comment> \<open>If \<open>\<tau>\<close> is \<open>(\<tau>' \<leadsto> ta)\<close>, we can close the case using the induction
         hypothesis and the information that every concrete state contains
         no symbolic variables.\<close>
  qed

text \<open>We can now take another look at our earlier examples of conditioned
  symbolic traces and analyze their concreteness. \<open>\<pi>\<^sub>1\<close> is not concrete as 
  state \<open>\<sigma>\<^sub>1\<close> is already not concrete. Given that \<open>\<pi>\<^sub>2\<close> is not even wellformed, 
  its concreteness is trivially violated. However, \<open>\<pi>\<^sub>3\<close> satisfies all concreteness 
  conditions and is therefore a concrete conditioned symbolic trace. These conclusions
  can be derived in Isabelle using the following~lemma.\<close>
  lemma "\<not>concrete\<^sub>\<pi> \<pi>\<^sub>1 \<and> \<not>concrete\<^sub>\<pi> \<pi>\<^sub>2 \<and> concrete\<^sub>\<pi> \<pi>\<^sub>3"
    by (simp add: \<sigma>\<^sub>1_def \<sigma>\<^sub>2_def \<tau>\<^sub>1_def \<tau>\<^sub>2_def \<tau>\<^sub>3_def \<pi>\<^sub>1_def \<pi>\<^sub>2_def \<pi>\<^sub>3_def symb\<^sub>\<Sigma>_def wf\<^sub>\<Sigma>_def concrete\<^sub>\<Sigma>_def wf\<^sub>\<pi>_def concrete\<^sub>\<pi>_def)



section \<open>Concretization Mappings\<close>

subsection \<open>Concretization of States\<close>

text \<open>States with symbolic variables are eventually concretized,
  meaning that all previously symbolic variables are reassigned to concrete 
  values. This motivates a notion of concretization mappings. A concretization 
  mapping \<open>\<rho>\<close> for a state \<open>\<sigma>\<close> is supposed to map all symbolic variables of \<open>\<sigma>\<close> to 
  arithmetic numerals, thereby concretizing them. Note that \<open>\<rho>\<close> may additionally 
  define variables, which are not in the domain~of~\<open>\<sigma>\<close>. \par
  We can therefore call \<open>\<rho>\<close> a concretization mapping for \<open>\<sigma>\<close> if the domains of
  \<open>\<rho>\<close> and \<open>\<sigma>\<close> match exactly in the symbolic variables of \<open>\<sigma>\<close>. Additionally we
  require \<open>\<rho>\<close> to be concrete, such that all variables of \<open>\<rho>\<close> map to arithmetic
  numerals. \par
  In the original paper, concretization mappings are not explicitly considered
  as states. Their concreteness is ensured by choosing their image set to be atomic 
  values. However, this idea does not synergize well with the actual concretization 
  process in Isabelle, as these concretization mappings cannot be easily combined with 
  states due to their differing~datatypes. \par
  In order to solve this problem, we instead choose to explicitly model 
  concretization mappings as states and enforce their concreteness via the state 
  concreteness notion. Both modeling choices match in their core ideas. However, our 
  design choice ensures that states and concretization mappings originate from the 
  same datatype, thus simplifying the actual concretization process.\<close>
  definition
    is_conc_map\<^sub>\<Sigma> :: "\<Sigma> \<Rightarrow> \<Sigma> \<Rightarrow> bool" where
    "is_conc_map\<^sub>\<Sigma> \<rho> \<sigma> \<equiv> fmdom'(\<rho>) \<inter> fmdom'(\<sigma>) = symb\<^sub>\<Sigma>(\<sigma>) \<and> concrete\<^sub>\<Sigma>(\<rho>)"

text \<open>We can now define a function that models the actual state concretization
  process. For this purpose, we aim to formalize that a concretization mapping 
  \<open>\<rho>\<close> concretizes all symbolic variables of the original state \<open>\<sigma>\<close>, whilst optionally
  introducing new~variables. \par
  Our formalization closely resembles the definition in the paper. The predefined
  function \<open>++\<^sub>f\<close> of the finite map theory guarantees that the resulting state
  matches the concretization mapping \<open>\<rho>\<close>, provided as the second argument, in all 
  its defined variables and the simplified state \<open>\<sigma>\<close>, provided as the first argument, 
  in all other (i.e. non-symbolic) variables. Note that \<open>\<sigma>\<close> is additionally evaluated 
  under \<open>\<rho>\<close> in order to simplify the resulting state, thereby ensuring the 
  concreteness of states after corresponding state~concretizations.\<close>
  fun
    conc_map\<^sub>\<Sigma> :: "\<Sigma> \<Rightarrow> \<Sigma> \<Rightarrow> \<Sigma>" (infix "\<bullet>" 61) where
    "\<rho> \<bullet> \<sigma> = fmmap_keys (\<lambda>v e. val\<^sub>S e \<rho>) \<sigma> ++\<^sub>f \<rho>"

text \<open>In order to analyze how the empty state connects to state concretizations, we
  first establish that the empty state is a concretization mapping of itself. This
  is trivial, considering that the empty state~is~concrete.\<close>
  lemma empty_conc_map\<^sub>\<Sigma>: "is_conc_map\<^sub>\<Sigma> \<circle> \<circle>"
    by (simp add: symb\<^sub>\<Sigma>_def concrete\<^sub>\<Sigma>_def is_conc_map\<^sub>\<Sigma>_def)

text \<open>We now prove that the empty state is a concretization mapping for every 
  concrete state. This holds, because no concrete state contains any symbolic 
  variables. Thus the concretization mapping conditions are trivially~satisfied.\<close>
  lemma empty_conc_map_imp\<^sub>\<Sigma>: "concrete\<^sub>\<Sigma>(\<sigma>) \<longrightarrow> is_conc_map\<^sub>\<Sigma> \<circle> \<sigma>"
    using concrete_symb_imp\<^sub>\<Sigma> by (simp add: concrete\<^sub>\<Sigma>_def is_conc_map\<^sub>\<Sigma>_def)

text \<open>If we concretize the empty state, the resulting state always matches with the 
  applied concretization mapping. This property is also trivially provable using the
  definition of state~concretizations.\<close>  
  lemma empty_conc_map_pr\<^sub>\<Sigma>: "\<rho> \<bullet> \<circle> = \<rho>"
    by (auto; metis fmadd_empty(1) fmrestrict_set_fmmap_keys fmrestrict_set_null)

text \<open>Finally, we establish that any concretization mapping is guaranteed to be
  concrete, which obviously holds due to the definition of the 
  corresponding~predicate.\<close>
  lemma conc_map_concrete\<^sub>\<Sigma>: "is_conc_map\<^sub>\<Sigma> \<rho> \<sigma> \<longrightarrow> concrete\<^sub>\<Sigma>(\<rho>)"
    by (simp add: is_conc_map\<^sub>\<Sigma>_def)

text \<open>We demonstrate the state concretization with an example. The previously
  defined state \<open>\<sigma>\<^sub>1\<close> can be concretized with any concrete state that includes
  y, but not x, in its domain. If y is mapped onto the arithmetic numeral 2, 
  we result in state \<open>\<sigma>\<^sub>2\<close>. This intuitive conclusion can also be inferred by 
  Isabelle using the following~lemma.\<close>
  lemma "fm[(''y'', Exp (Num 2))] \<bullet> \<sigma>\<^sub>1 = \<sigma>\<^sub>2"
    by (simp add: \<sigma>\<^sub>1_def \<sigma>\<^sub>2_def fmap_ext fmupd.rep_eq map_upd_def)


subsection \<open>Minimal State Concretization\<close>

text \<open>In contrast to the original paper, we additionally propose a minimal state 
  concretization that will later prove useful when automatically constructing
  deterministic state concretization mappings in our proof system. A minimal
  state concretization mapping \<open>\<rho>\<close> for a given state \<open>\<sigma>\<close> is characterized by 
  ensuring that \<open>dom(\<rho>) = symb(\<sigma>)\<close>, implying that the concretization mapping
  is not allowed to introduce new variables. Also note that all variables of the
  minimal state concretization are mapped onto a singular, deterministic
  arithmetic~numeral. \par
  The deterministic minimal state concretization for a state \<open>\<sigma>\<close> is constructed as 
  follows: At the start, all key-value tuples of \<open>\<sigma>\<close> are modified, such that all 
  variables switch their symbolic nature. This implies that all previously symbolic
  variables afterwards map to a deterministic arithmetic numeral $n$, while all
  previously non-symbolic variables become symbolic. In the second step all 
  symbolic variables are filtered out of the modified state, resulting
  in a concrete state, which aligns with the intuition of our desired minimal 
  state~concretization.\<close>
  fun
    min_conc_map\<^sub>\<Sigma> :: "\<Sigma> \<Rightarrow> int \<Rightarrow> \<Sigma>" where
    "min_conc_map\<^sub>\<Sigma> \<sigma> n = fmfilter (%v. fmlookup \<sigma> v = Some \<^emph>) 
                       (fmmap_keys (\<lambda>v e. (if e = \<^emph> then Exp (Num n) else \<^emph>)) \<sigma>)"

text \<open>It can now be inferred that the domain of any minimal concretization mapping
  for \<open>\<sigma>\<close> contains exactly the symbolic variables of \<open>\<sigma>\<close>. This directly follows
  from the construction algorithm of minimal state concretizations as described~above.\<close>
  lemma only_symb_conc\<^sub>\<Sigma>: "fmdom'(min_conc_map\<^sub>\<Sigma> \<sigma> n) = symb\<^sub>\<Sigma>(\<sigma>)"
    apply (auto simp add: symb\<^sub>\<Sigma>_def)
    apply (metis (mono_tags, lifting) fmdom'_notI fmfilter_fmmap_keys fmlookup_filter fmlookup_dom'_iff)
    apply (metis (mono_tags, lifting) fmdom'_notI fmfilter_fmmap_keys fmlookup_filter)
    using fmlookup_dom'_iff by fastforce

text \<open>The minimal state concretization of a concrete state is always the empty
  state. This is obvious, considering that a concrete state is not allowed to
  contain any symbolic variables, implying that no concretization can take~place.\<close>
  lemma min_conc_map_of_concrete\<^sub>\<Sigma>: "concrete\<^sub>\<Sigma>(\<sigma>) \<longrightarrow> min_conc_map\<^sub>\<Sigma> \<sigma> n = \<circle>"
    using only_symb_conc\<^sub>\<Sigma> concrete_symb_imp\<^sub>\<Sigma> 
    by (metis fmrestrict_set_dom fmrestrict_set_null)

text \<open>We can now take a look at the minimal concretization mapping for our
  previously defined state \<open>\<sigma>\<^sub>1\<close>. As only y is symbolic in \<open>\<sigma>\<^sub>1\<close>, its minimal
  concretization mapping will only specify y. We demonstrate this fact using
  the following~lemma.\<close>
  lemma "min_conc_map\<^sub>\<Sigma> \<sigma>\<^sub>1 0 = fm[(''y'', Exp (Num 0))]"
    by (simp add: \<sigma>\<^sub>1_def fmap_ext)


subsection \<open>Concretization of Traces\<close>

text \<open>We now aim to strengthen the notion of state concretization mappings to 
  concretization mappings for symbolic traces and conditioned symbolic~traces. \par
  Similar to the original paper, we call a state \<open>\<rho>\<close> a valid concretization mapping 
  for a given symbolic trace \<open>\<tau>\<close> iff \<open>\<rho>\<close> is a concretization mapping for all states 
  occurring in \<open>\<tau>\<close>. The definition of the predicate is straightforward by making
  use of recursion. Note that we enforce the concreteness of \<open>\<rho>\<close>, even if
  the corresponding trace is the empty~trace.\<close>
  fun
    is_conc_map\<^sub>\<T> :: "\<Sigma> \<Rightarrow> \<T> \<Rightarrow> bool" where
    "is_conc_map\<^sub>\<T> \<rho> \<epsilon> = concrete\<^sub>\<Sigma>(\<rho>)" |
    "is_conc_map\<^sub>\<T> \<rho> (\<tau> \<leadsto> Event\<llangle>ev, e\<rrangle>) = is_conc_map\<^sub>\<T> \<rho> \<tau>" |
    "is_conc_map\<^sub>\<T> \<rho> (\<tau> \<leadsto> State\<llangle>\<sigma>\<rrangle>) = (is_conc_map\<^sub>\<Sigma> \<rho> \<sigma> \<and> is_conc_map\<^sub>\<T> \<rho> \<tau>)"

text \<open>We can now formalize that a concretization mapping \<open>\<rho>\<close> concretizes a
  symbolic trace \<open>\<tau>\<close> by concretizing all states of \<open>\<tau>\<close> with \<open>\<rho>\<close> and 
  simplifying all event expressions occurring in events of \<open>\<tau>\<close> with \<open>\<rho>\<close>.
  Note that we additionally require \<open>\<tau>\<close> to be wellformed in order to ensure 
  that the application of this function results in a concrete symbolic~trace.\<close>
  fun
    trace_conc :: "\<Sigma> \<Rightarrow> \<T> \<Rightarrow> \<T>" where
    "trace_conc \<rho> \<epsilon> = \<epsilon>" |
    "trace_conc \<rho> (\<tau> \<leadsto> State\<llangle>\<sigma>\<rrangle>) = (trace_conc \<rho> \<tau>) \<leadsto> State\<llangle>(\<rho> \<bullet> \<sigma>)\<rrangle>" |
    "trace_conc \<rho> (\<tau> \<leadsto> Event\<llangle>ev, e\<rrangle>) = (trace_conc \<rho> \<tau>) \<leadsto> Event\<llangle>ev, (lval\<^sub>E e \<rho>)\<rrangle>"

text \<open>A concretization mapping \<open>\<rho>\<close> concretizes a conditioned symbolic
  trace \<open>\<pi>\<close> by concretizing the symbolic trace of \<open>\<pi>\<close> with \<open>\<rho>\<close> and
  simplifying the path condition of \<open>\<pi>\<close> with \<open>\<rho>\<close>. Note that we again require 
  \<open>\<pi>\<close> to be wellformed, if we want to guarantee the concreteness of the resulting 
  conditioned symbolic~trace.\<close>
  fun
    conc_map\<^sub>\<T> :: "\<Sigma> \<Rightarrow> \<CC>\<T> \<Rightarrow> \<CC>\<T>" (infix "\<sqdot>" 61) where
    "\<rho> \<sqdot> (pc \<triangleright> \<tau>) = (sval\<^sub>B pc \<rho>) \<triangleright> (trace_conc \<rho> \<tau>)"

text \<open>We next establish that a valid trace concretization mapping is always concrete.
  This property trivially holds and can be inferred by Isabelle using the 
  definition of the corresponding~predicate.\<close>
  lemma conc_map_concrete\<^sub>\<T>: "is_conc_map\<^sub>\<T> \<rho> \<tau> \<longrightarrow> concrete\<^sub>\<Sigma>(\<rho>)"
    apply (induct \<tau>)
    using is_conc_map\<^sub>\<T>.elims(1) by auto

text \<open>The following lemma provides an example for a trace concretization. 
  As we have previously reasoned, applying a specific concretization mapping on \<open>\<sigma>\<^sub>1\<close>
  results in \<open>\<sigma>\<^sub>2\<close>. Hence, we can also concretize the conditioned symbolic trace \<open>\<pi>\<^sub>1\<close> 
  with the same concretization mapping in order to result in a conditioned symbolic 
  trace, in which the state \<open>\<sigma>\<^sub>1\<close> is replaced~with~\<open>\<sigma>\<^sub>2\<close>.\<close>
  lemma "fm[(''y'', Exp (Num 2))] \<sqdot> \<pi>\<^sub>1 = {} \<triangleright> (\<langle>\<sigma>\<^sub>2\<rangle> \<leadsto> Event\<llangle>inpEv, []\<rrangle>) \<leadsto> State\<llangle>\<sigma>\<^sub>2\<rrangle>"
    by (auto simp add: \<sigma>\<^sub>1_def \<sigma>\<^sub>2_def \<tau>\<^sub>1_def \<pi>\<^sub>1_def fmap_ext fmupd.rep_eq map_upd_def)


subsection \<open>Minimal Trace Concretization\<close>

text \<open>Given that we have already defined minimal state concretizations, we are now 
  interested in heightening this definition to traces. For this purpose, we 
  define a minimal trace concretization as a minimal concretization mapping for 
  all of the states occurring in the trace. The choice of this concretization mapping 
  will again be deterministic, thus ensuring that the Isabelle compiler can generate 
  efficient code for their~construction. \par
  The construction of the minimal trace concretization is simple. We unfold
  the trace from the back, whilst recursively combining the minimal concretization 
  mappings of all traversed states. Note that the constructed concretization
  mapping is therefore only a valid trace concretization mapping if all states 
  occurring in the trace agree on their symbolic variables, which is guaranteed by 
  the wellformedness~notion.\<close>
  fun
    min_conc_map\<^sub>\<T> :: "\<T> \<Rightarrow> int \<Rightarrow> \<Sigma>" where
    "min_conc_map\<^sub>\<T> \<epsilon> n = \<circle>" |
    "min_conc_map\<^sub>\<T> (\<tau> \<leadsto> State\<llangle>\<sigma>\<rrangle>) n = min_conc_map\<^sub>\<T> \<tau> n ++\<^sub>f min_conc_map\<^sub>\<Sigma> \<sigma> n" |
    "min_conc_map\<^sub>\<T> (\<tau> \<leadsto> Event\<llangle>ev, e\<rrangle>) n = min_conc_map\<^sub>\<T> \<tau> n"

text \<open>The domain of any minimal concretization mapping for \<open>\<tau>\<close> contains exactly the 
  symbolic variables of \<open>\<tau>\<close>. This directly follows from the construction algorithm 
  of minimal trace concretizations as described~above.\<close>
  lemma only_symb_conc\<^sub>\<T>: "fmdom'(min_conc_map\<^sub>\<T> \<tau> n) = symb\<^sub>\<T>(\<tau>)"
  proof (induct \<tau>)
  \<comment> \<open>We perform a structural induction over the construction of \<open>\<tau>\<close>.\<close>
    case Epsilon
    thus ?case by simp
    \<comment> \<open>If \<open>\<tau>\<close> is \<open>\<epsilon>\<close>, the case is trivial.\<close>
  next
    case (Transition \<tau>' ta)
    \<comment> \<open>In the induction step, we assume \<open>\<tau>\<close> is \<open>(\<tau>' \<leadsto> ta)\<close>.\<close>
    thus ?case
    proof (induct ta)
    \<comment> \<open>We perform a case distinction over trace atom \<open>ta\<close>.\<close>
      case (Event ev e)
      thus ?case by simp
      \<comment> \<open>We assume \<open>ta\<close> is an event \<open>e\<close>. Then the case trivially holds in combination
         with the induction hypothesis, because the domain of the minimal trace 
         concretization mapping is independent of the events ocurring in the trace.\<close>
    next
      case (State \<sigma>)
      thus ?case using only_symb_conc\<^sub>\<Sigma> by auto
      \<comment> \<open>We assume \<open>ta\<close> is a state \<open>\<sigma>\<close>. Due to an earlier proof, we know that the 
         domain of the minimal state concretization mapping of \<open>\<sigma>\<close> contains exactly 
         the symbolic variables of \<open>\<sigma>\<close>. Combined with the induction hypothesis, this
         closes the case.\<close>
    qed
  qed

text \<open>The minimal trace concretization of a concrete trace is always the empty
  state. This is obvious, considering that a concrete trace is not allowed to
  contain any symbolic variables, implying that no concretization can take~place.\<close>
lemma min_conc_map_of_concrete\<^sub>\<T>: "concrete\<^sub>\<T>(\<tau>) \<longrightarrow> min_conc_map\<^sub>\<T> \<tau> n = \<circle>"
  using only_symb_conc\<^sub>\<T> concrete_symb_imp\<^sub>\<T> 
  by (metis fmrestrict_set_dom fmrestrict_set_null)

text \<open>We can now take another look at our earlier example. Considering that \<open>\<tau>\<^sub>1\<close> 
  only contains the state \<open>\<sigma>\<^sub>1\<close>, its minimal trace concretization mapping will match 
  the minimal state concretization mapping of \<open>\<sigma>\<^sub>1\<close> from~earlier.\<close>
  lemma "min_conc_map\<^sub>\<T> \<tau>\<^sub>1 0 = fm[(''y'', Exp (Num 0))]"
    by (simp add: \<sigma>\<^sub>1_def \<tau>\<^sub>1_def fmap_ext)


subsection \<open>Proof Automation\<close>

text \<open>During a state concretization, the fmmap-keys function of the finite map
  theory is used to simplify all expressions in the image of a given state 
  with a provided concretization mapping. However, under several circumstances
  the Isabelle simplifier is not able to automatically compute this function, 
  hence obstructing the proof automation for state concretizations. In order to
  circumvent this problem, we propose additional supporting theorems for the 
  simplifier, which can be used to resolve these problematic~situations. \par
  We first establish that the simplification of the empty state with an arbitrary
  state preserves the empty state. This property is trivial, considering that
  there exists no key-value pair in the empty~state.\<close>
  lemma fmmap_keys_empty: "fmmap_keys (\<lambda>v e. val\<^sub>S e \<rho>) \<circle> = \<circle>"
    by (metis fmrestrict_set_fmmap_keys fmrestrict_set_null)

text \<open>We next prove that any concrete state \<open>\<sigma>\<close> simplified with another concrete 
  state \<open>\<rho>\<close> preserves \<open>\<sigma>\<close>. Considering that \<open>\<sigma>\<close> is already concrete, no update 
  can take place. This implies that \<open>\<rho>\<close> is not used during the simplification process,
  thus trivially proving the~lemma.\<close>
  lemma fmmap_keys_conc: "concrete\<^sub>\<Sigma>(\<sigma>) \<longrightarrow> fmmap_keys (\<lambda>v e. val\<^sub>S e \<rho>) \<sigma> = \<sigma>"
  proof (rule impI)
    assume premise: "concrete\<^sub>\<Sigma>(\<sigma>)"
    \<comment> \<open>We assume that \<open>\<sigma>\<close> is of concrete nature.\<close>
    hence "\<forall>v \<in> fmdom'(\<sigma>). val\<^sub>S(the(fmlookup \<sigma> v)) \<rho> = the(fmlookup \<sigma> v)"
      by (simp add: concrete\<^sub>\<Sigma>_def value_pr\<^sub>S)
    \<comment> \<open>This in turn implies that all expressions in the image of \<open>\<sigma>\<close> will be 
       preserved when simplified, which can be inferred by using our earlier expression 
       preservation~theorem~\<open>value_pr\<^sub>S\<close>.\<close>
    hence "\<forall>v. fmlookup (fmmap_keys (\<lambda>v e. val\<^sub>S e \<rho>) \<sigma>) v = fmlookup \<sigma> v"
      by (smt (z3) fmdom'_notD fmlookup_dom'_iff fmlookup_fmmap_keys map_option_eq_Some option.sel)
    \<comment> \<open>We can now use the SMT solver in order to establish that the simplified state 
       and the original state match in all their key-value~tuples.\<close>
    thus "fmmap_keys (\<lambda>v e. val\<^sub>S e \<rho>) \<sigma> = \<sigma>" 
      using fmap_ext by blast
    \<comment> \<open>Finally, we apply the equivalence definition of finite maps to deduce their 
       equality, which is what needed to be proven in the first~place.\<close>
  qed

text \<open>Let us assume that \<open>\<sigma>'\<close> is the result of simplifying \<open>\<sigma>\<close> with a state \<open>\<rho>\<close>. 
  Let us furthermore assume that we update \<open>\<sigma>\<close>, such that an arbitrary variable \<open>y\<close>
  is now mapped onto an arbitrary expression \<open>e\<close>. If we simplify this updated state 
  using \<open>\<rho>\<close>, we will then result in state \<open>\<sigma>'\<close>, in which \<open>y\<close> is updated with the 
  under \<open>\<rho>\<close> evaluated version of \<open>e\<close>. \par
  This property directly follows from the definition of the fmmap-keys function. 
  Note that this lemma is crucial in our proof system, as it ensures that 
  we can later remove the fmmap-keys function step by step out of the corresponding 
  proof~obligations.\<close>
  lemma fmmap_keys_dom_upd:
    assumes "\<sigma>' = fmmap_keys (\<lambda>v e. val\<^sub>S e \<rho>) \<sigma>"
    shows "fmmap_keys (\<lambda>v e. val\<^sub>S e \<rho>) ([y \<longmapsto> e] \<sigma>) = [y \<longmapsto> val\<^sub>S e \<rho>] \<sigma>'"
  proof -
    have "\<forall>v. fmlookup (fmmap_keys (\<lambda>v e. val\<^sub>S e \<rho>) ([y \<longmapsto> e] \<sigma>)) v = fmlookup ([y \<longmapsto> val\<^sub>S e \<rho>] \<sigma>') v"
      using assms by fastforce    
    \<comment> \<open>Due to the assumptions, we know that simplifying \<open>\<sigma>\<close> with \<open>\<rho>\<close> results in \<open>\<sigma>'\<close>.
       The definition of the fmmap-keys function furthermore guarantees that the
       lookup value of the updated variable y matches \<open>e\<close> simplified with \<open>\<rho>\<close>. This
       establishes that both states of the equation align in all their lookup~values.\<close>
    thus ?thesis 
      using fmap_ext by blast
    \<comment> \<open>We can then deduce that both states are equivalent using the equivalence
       notion of finite maps. This concludes the~lemma.\<close>
  qed

end