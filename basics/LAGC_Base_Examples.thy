theory LAGC_Base_Examples
  imports LAGC_Base
begin


subsection \<open>Expressions\<close>

text \<open>Symbolic Value \<open>\<^emph>\<close>\<close>
definition 
  symbolic_value :: sexp where
  "symbolic_value \<equiv> \<^emph>"

text \<open>\<open>x\<^sub>0\<close>\<close>
definition 
  var\<^sub>x\<^sub>0 :: exp where
  "var\<^sub>x\<^sub>0 \<equiv> A (Var ''x0'')"

text \<open>\<open>99\<close>\<close>
definition 
  val\<^sub>9\<^sub>9 :: exp where
  "val\<^sub>9\<^sub>9 \<equiv> A (Num 99)"

text \<open>\<open>(x\<^sub>1 * x\<^sub>2) - x\<^sub>1\<close>\<close>
definition 
  complex\<^sub>1 :: aexp where
  "complex\<^sub>1 \<equiv> ((Var ''x1'') \<^sub>Amul (Var ''x2'')) \<^sub>Asub (Var ''x1'')"

text \<open>\<open>99 \<le> 5\<close>\<close>
definition 
  complex\<^sub>2 :: bexp where
  "complex\<^sub>2 \<equiv> (Num 99) \<^sub>Rleq (Num 5)"

text \<open>\<open>x\<^sub>1 = (w\<^sub>0 + 2) \<or> False\<close>\<close>
definition 
  complex\<^sub>3 :: bexp where
  "complex\<^sub>3 \<equiv> ((Var ''x1'') \<^sub>Req ((Var ''w0'') \<^sub>Aadd (Num 2))) \<^sub>Bdisj (Bool False)"

context notes [simp] = insert_commute begin

lemma "vars\<^sub>S(symbolic_value) = {}"
  by (simp add: symbolic_value_def)

lemma "vars\<^sub>E(var\<^sub>x\<^sub>0) = {''x0''}"
  by (simp add: var\<^sub>x\<^sub>0_def)

lemma "vars\<^sub>E(val\<^sub>9\<^sub>9) = {}"
  by (simp add: val\<^sub>9\<^sub>9_def)

lemma "vars\<^sub>A(complex\<^sub>1) = {''x1'', ''x2''}"
  by (simp add: complex\<^sub>1_def)

lemma "vars\<^sub>B(complex\<^sub>2) = {}"
  by (simp add: complex\<^sub>2_def)

lemma "vars\<^sub>B(complex\<^sub>3) = {''x1'', ''w0''}"
  by (simp add: complex\<^sub>3_def)

lemma "lvars\<^sub>E([A complex\<^sub>1, B complex\<^sub>2, B complex\<^sub>3]) = {''x1'', ''x2'', ''w0''}"
  by (simp add: complex\<^sub>1_def complex\<^sub>2_def complex\<^sub>3_def)

lemma "svars\<^sub>B({(Bool True), complex\<^sub>3}) = {''x1'', ''w0''}"
  by (simp add: complex\<^sub>3_def)

end

lemma "occ\<^sub>A complex\<^sub>1 = [''x1'', ''x2'', ''x1'']"
  by (simp add: complex\<^sub>1_def)

lemma "occ\<^sub>B complex\<^sub>2 = []"
  by (simp add: complex\<^sub>2_def)

lemma "occ\<^sub>B complex\<^sub>3 = [''x1'', ''w0'']"
  by (simp add: complex\<^sub>3_def)

lemma "\<not>concrete\<^sub>S symbolic_value"
  by (simp add: symbolic_value_def)

lemma "\<not>concrete\<^sub>E var\<^sub>x\<^sub>0 \<and> concrete\<^sub>E val\<^sub>9\<^sub>9"
  by (simp add: var\<^sub>x\<^sub>0_def val\<^sub>9\<^sub>9_def)

lemma "\<not>concrete\<^sub>A complex\<^sub>1 \<and> \<not>concrete\<^sub>B complex\<^sub>2 \<and> \<not>concrete\<^sub>B complex\<^sub>3"
  by (simp add: complex\<^sub>1_def complex\<^sub>2_def complex\<^sub>3_def)

lemma "\<not>lconcrete\<^sub>E([A complex\<^sub>1, B complex\<^sub>2, B complex\<^sub>3])"
  by (simp add: complex\<^sub>1_def complex\<^sub>2_def complex\<^sub>3_def)

lemma "sconcrete\<^sub>B({(Bool True), (Bool False)})"
  by simp

lemma "substitute\<^sub>A complex\<^sub>1 ''x1'' ''z1'' = ((Var ''z1'') \<^sub>Amul (Var ''x2'')) \<^sub>Asub (Var ''z1'')"
  by (simp add: complex\<^sub>1_def)

lemma "substitute\<^sub>B complex\<^sub>2 ''x0'' ''z1'' = complex\<^sub>2"
  by (simp add: complex\<^sub>2_def)

lemma "substitute\<^sub>B complex\<^sub>3 ''w0'' ''x1'' = ((Var ''x1'') \<^sub>Req ((Var ''x1'') \<^sub>Aadd (Num 2))) \<^sub>Bdisj (Bool False)"
  by (simp add: complex\<^sub>3_def)



subsection \<open>States\<close>

text \<open>\<open>\<sigma>\<^sub>0 = [x\<^sub>0 \<mapsto> Y\<^sub>0 + w\<^sub>0][Y\<^sub>0 \<mapsto> \<^emph>][w\<^sub>0 \<mapsto> 42][x\<^sub>1 \<mapsto> Y\<^sub>1]\<close> [\<open>Example 2.4\<close>]\<close>
definition 
  \<sigma>\<^sub>0 :: \<Sigma> where
  "\<sigma>\<^sub>0 \<equiv> fm[
         (''x0'', Exp ((Var ''Y0'') \<^sub>Aadd (Var ''w0''))), 
         (''Y0'', \<^emph>), 
         (''w0'', Exp (Num 42)), 
         (''x1'', Exp (Var ''Y1''))
          ]"

text \<open>\<open>\<sigma>\<^sub>1 = [x\<^sub>0 \<mapsto> Y\<^sub>0 + 42][Y\<^sub>0 \<mapsto> \<^emph>][w\<^sub>0 \<mapsto> 42][x\<^sub>1 \<mapsto> Y\<^sub>1][Y\<^sub>1 \<mapsto> \<^emph>]\<close> [\<open>Example 2.6\<close>]\<close>
definition 
  \<sigma>\<^sub>1 :: \<Sigma> where
  "\<sigma>\<^sub>1 \<equiv> fm[
         (''x0'', Exp ((Var ''Y0'') \<^sub>Aadd (Num 42))), 
         (''Y0'', \<^emph>), 
         (''w0'', Exp (Num 42)), 
         (''x1'', Exp (Var ''Y1'')), 
         (''Y1'', \<^emph>)
          ]"

text \<open>\<open>\<sigma>\<^sub>2 = [x\<^sub>0 \<mapsto> 45][Y\<^sub>0 \<mapsto> 3][w\<^sub>0 \<mapsto> 42][x\<^sub>1 \<mapsto> 2][Y\<^sub>1 \<mapsto> 2]\<close> [\<open>Example 2.6\<close>]\<close>
definition 
  \<sigma>\<^sub>2 :: \<Sigma> where
  "\<sigma>\<^sub>2 \<equiv> fm[
         (''x0'', Exp (Num 45)), 
         (''Y0'', Exp (Num 3)), 
         (''w0'', Exp (Num 42)), 
         (''x1'', Exp (Num 2)), 
         (''Y1'', Exp (Num 2))
          ]"

text \<open>\<open>\<sigma>\<^sub>3 = [x\<^sub>0 \<mapsto> 45 + 3][Y\<^sub>0 \<mapsto> 3][w\<^sub>0 \<mapsto> 42][x\<^sub>1 \<mapsto> 2][Y\<^sub>1 \<mapsto> 2]\<close>\<close>
definition 
  \<sigma>\<^sub>3 :: \<Sigma> where
  "\<sigma>\<^sub>3 \<equiv> fm[
         (''x0'', Exp ((Num 45) \<^sub>Aadd (Num 3))), 
         (''Y0'', Exp (Num 3)), 
         (''w0'', Exp (Num 42)), 
         (''x1'', Exp (Num 2)), 
         (''Y1'', Exp (Num 2))
          ]"

text \<open>\<open>\<sigma>\<^sub>4 = [x\<^sub>1 \<mapsto> Y\<^sub>1]\<close>\<close>
definition 
  \<sigma>\<^sub>4 :: \<Sigma> where
  "\<sigma>\<^sub>4 \<equiv> fm[
         (''x1'', Exp (Var ''Y1''))
          ]"

text \<open>\<open>\<sigma>\<^sub>5 = [x\<^sub>0 \<mapsto> x\<^sub>1 + 42][x\<^sub>1 \<mapsto> 3]\<close>\<close>
definition 
  \<sigma>\<^sub>5 :: \<Sigma> where
  "\<sigma>\<^sub>5 \<equiv> fm[
         (''x0'', Exp ((Var ''Y0'') \<^sub>Aadd (Num 42))), 
         (''x1'', Exp (Num 3)),
         (''Y1'', \<^emph>) 
          ]"

lemma "\<sigma>\<^sub>1 = [''x0'' \<longmapsto> Exp ((Var ''Y0'') \<^sub>Aadd (Num 42))] [''Y1'' \<longmapsto> \<^emph>] \<sigma>\<^sub>0"
  by (simp add: \<sigma>\<^sub>0_def \<sigma>\<^sub>1_def fmupd_reorder_neq)

context notes [simp] = symb\<^sub>\<Sigma>_def wf\<^sub>\<Sigma>_def concrete\<^sub>\<Sigma>_def begin

lemma "symb\<^sub>\<Sigma>(\<sigma>\<^sub>0) = {''Y0''}"
  by (simp add: \<sigma>\<^sub>0_def; blast)

lemma "symb\<^sub>\<Sigma>(\<sigma>\<^sub>1) = {''Y0'', ''Y1''}"
  by (simp add: \<sigma>\<^sub>1_def; blast)

lemma "symb\<^sub>\<Sigma>(\<sigma>\<^sub>2) = {}"
  by (simp add: \<sigma>\<^sub>2_def)

lemma "symb\<^sub>\<Sigma>(\<sigma>\<^sub>3) = {}"
  by (simp add: \<sigma>\<^sub>3_def)

lemma "symb\<^sub>\<Sigma>(\<sigma>\<^sub>4) = {}"
  by (simp add: \<sigma>\<^sub>4_def)

lemma "symb\<^sub>\<Sigma>(\<sigma>\<^sub>5) = {''Y1''}"
  by (simp add: \<sigma>\<^sub>5_def, blast)

lemma "\<not>wf\<^sub>\<Sigma>(\<sigma>\<^sub>0) \<and> wf\<^sub>\<Sigma>(\<sigma>\<^sub>1) \<and> wf\<^sub>\<Sigma>(\<sigma>\<^sub>2) \<and> wf\<^sub>\<Sigma>(\<sigma>\<^sub>3) \<and> \<not>wf\<^sub>\<Sigma>(\<sigma>\<^sub>4) \<and> \<not>wf\<^sub>\<Sigma>(\<sigma>\<^sub>5)"
  by (simp add: \<sigma>\<^sub>0_def \<sigma>\<^sub>1_def \<sigma>\<^sub>2_def \<sigma>\<^sub>3_def \<sigma>\<^sub>4_def \<sigma>\<^sub>5_def; blast)

lemma "\<not>concrete\<^sub>\<Sigma>(\<sigma>\<^sub>0) \<and> \<not>concrete\<^sub>\<Sigma>(\<sigma>\<^sub>1) \<and> concrete\<^sub>\<Sigma>(\<sigma>\<^sub>2) \<and> \<not>concrete\<^sub>\<Sigma>(\<sigma>\<^sub>3) \<and> \<not>concrete\<^sub>\<Sigma>(\<sigma>\<^sub>4) \<and> \<not>concrete\<^sub>\<Sigma>(\<sigma>\<^sub>5)"
  by (simp add: \<sigma>\<^sub>0_def \<sigma>\<^sub>1_def \<sigma>\<^sub>2_def \<sigma>\<^sub>3_def \<sigma>\<^sub>4_def \<sigma>\<^sub>5_def)

end



subsection \<open>Evaluation\<close>

text  \<open>\<open>x\<^sub>0 + (Y\<^sub>0 + Y\<^sub>1)\<close> [\<open>Example 2.8\<close>]\<close>
definition 
  prog\<^sub>1 :: exp where
  "prog\<^sub>1 \<equiv> A ((Var ''x0'') \<^sub>Aadd ((Var ''Y0'') \<^sub>Aadd (Var ''Y1'')))"

text  \<open>\<open>(5 \<le> 15) \<and> ((5 + Y\<^sub>1) == 7)\<close>\<close>
definition 
  prog\<^sub>2 :: exp where
  "prog\<^sub>2 \<equiv> B (((Num 5) \<^sub>Rleq (Num 15)) \<^sub>Bconj (((Num 5) \<^sub>Aadd (Var ''Y1'')) \<^sub>Req (Num 7)))"

lemma "val\<^sub>E val\<^sub>9\<^sub>9 \<sigma>\<^sub>0 = A (Num 99)"
  by (simp add: val\<^sub>9\<^sub>9_def)

lemma "val\<^sub>E prog\<^sub>1 \<sigma>\<^sub>1 = A (((Var ''Y0'') \<^sub>Aadd (Num 42)) \<^sub>Aadd ((Var ''Y0'') \<^sub>Aadd (Var ''Y1'')))"
  by (simp add: \<sigma>\<^sub>1_def prog\<^sub>1_def)

lemma "val\<^sub>E prog\<^sub>1 \<sigma>\<^sub>2 = A (Num 50)"
  by (simp add: \<sigma>\<^sub>2_def prog\<^sub>1_def)

lemma "val\<^sub>E prog\<^sub>2 \<sigma>\<^sub>1 = B ((Bool True) \<^sub>Bconj (((Num 5) \<^sub>Aadd (Var ''Y1'')) \<^sub>Req (Num 7)))"
  by (simp add: \<sigma>\<^sub>1_def prog\<^sub>2_def)

lemma "val\<^sub>E prog\<^sub>2 \<sigma>\<^sub>2 = B (Bool True)"
  by (simp add: \<sigma>\<^sub>2_def prog\<^sub>2_def)

lemma "lval\<^sub>E [
              A (Var ''Y1''), 
              A ((Var ''x1'') \<^sub>Amul (Var ''Y1'')),
              P ''foo''
            ] \<sigma>\<^sub>1 
          = [
              A (Var ''Y1''), 
              A ((Var ''Y1'') \<^sub>Amul (Var ''Y1'')),  
              P ''foo''
            ]"
  by (simp add: \<sigma>\<^sub>1_def)

lemma "sval\<^sub>B {
              (Var ''w0'') \<^sub>Req (Num 42), 
              Bool True, 
              (Bool True) \<^sub>Bconj ((Var ''Y0'') \<^sub>Rleq (Num 12))
            } \<sigma>\<^sub>1 
          = {
              (Bool True) \<^sub>Bconj ((Var ''Y0'') \<^sub>Rleq (Num 12)),
              Bool True
            }"
  by (simp add: \<sigma>\<^sub>1_def insert_commute)

lemma "\<sigma>\<^sub>2 = sim\<^sub>\<Sigma> ([''Y0'' \<longmapsto> Exp (Num 3)] [''Y1'' \<longmapsto> Exp (Num 2)] \<sigma>\<^sub>1)"
  by (simp add: \<sigma>\<^sub>1_def \<sigma>\<^sub>2_def sim\<^sub>\<Sigma>_def fmap_ext fmupd.rep_eq map_upd_def)



subsection \<open>Traces\<close>

text \<open>\<open>{True, x\<^sub>0 = 12}\<close>\<close>
definition 
  pc\<^sub>1 :: path_condition where
  "pc\<^sub>1 \<equiv> {Bool True, (Var ''x0'') \<^sub>Req (Num 12)}"

text \<open>\<open>{True, False}\<close>\<close>
definition 
  pc\<^sub>2 :: path_condition where
  "pc\<^sub>2 \<equiv> {Bool True, Bool False}"

text \<open>\<open>{True}\<close>\<close>
definition 
  pc\<^sub>3 :: path_condition where
  "pc\<^sub>3 \<equiv> {Bool True}"

text \<open>\<open>{}\<close>\<close>
definition 
  pc\<^sub>4 :: path_condition where
  "pc\<^sub>4 \<equiv> {}"

context notes [simp] = consistent_def begin

lemma "\<not>consistent(pc\<^sub>1)"
  by (simp add: pc\<^sub>1_def)

lemma "\<not>consistent(pc\<^sub>2)"
  by (simp add: pc\<^sub>2_def)

lemma "consistent(pc\<^sub>3)"
  by (simp add: pc\<^sub>3_def)

lemma "consistent(pc\<^sub>4)"
  by (simp add: pc\<^sub>4_def)

end

text \<open>\<open>\<sigma>\<^sub>1 \<leadsto> \<sigma>\<^sub>1[x0 \<mapsto> 17]\<close> [\<open>Example 2.12\<close>]\<close>
definition
  \<tau>\<^sub>0 :: \<T> where
  "\<tau>\<^sub>0 \<equiv> \<langle>\<sigma>\<^sub>1\<rangle> 
        \<leadsto> State\<llangle>[ ''x0'' \<longmapsto> Exp (Num 17)] \<sigma>\<^sub>1\<rrangle>"

text \<open>\<open>\<sigma>\<^sub>2 \<leadsto> \<sigma>\<^sub>2[x0 \<mapsto> 17]\<close> [\<open>Example 2.18\<close>]\<close>
definition
  \<tau>\<^sub>1 :: \<T> where
  "\<tau>\<^sub>1 \<equiv> (\<langle>\<sigma>\<^sub>2\<rangle> 
        \<leadsto> State\<llangle>[ ''x0'' \<longmapsto> Exp (Num 17)] \<sigma>\<^sub>2\<rrangle>)"

text \<open>\<open>\<epsilon>\<close>\<close>
definition 
  \<tau>\<^sub>2 :: \<T> where
  "\<tau>\<^sub>2 \<equiv> \<epsilon>"

text \<open>\<open>\<sigma>\<^sub>2\<close>\<close>
definition 
  \<tau>\<^sub>3 :: \<T> where
  "\<tau>\<^sub>3 \<equiv> \<langle>\<sigma>\<^sub>2\<rangle>"

text \<open>\<open>\<sigma>\<^sub>1 \<leadsto> inpEv[] \<leadsto> \<sigma>\<^sub>1 \<leadsto> \<sigma>\<^sub>2\<close>\<close>
definition 
  \<tau>\<^sub>4 :: \<T> where
  "\<tau>\<^sub>4 \<equiv> ((\<langle>\<sigma>\<^sub>1\<rangle> 
        \<leadsto> Event \<llangle>inpEv,[]\<rrangle>) 
        \<leadsto> State\<llangle>\<sigma>\<^sub>1\<rrangle>)
        \<leadsto> State\<llangle>\<sigma>\<^sub>2\<rrangle>"

text \<open>\<open>\<sigma>\<^sub>2 \<leadsto> \<sigma>\<^sub>2 \<leadsto> inpEv[Y2] \<leadsto> \<sigma>\<^sub>2\<close>\<close>
definition 
  \<tau>\<^sub>5 :: \<T> where
  "\<tau>\<^sub>5 \<equiv> ((\<langle>\<sigma>\<^sub>2\<rangle> 
        \<leadsto> State\<llangle>\<sigma>\<^sub>2\<rrangle>)
        \<leadsto> Event\<llangle>inpEv,[A (Var ''Y2'')]\<rrangle>) 
        \<leadsto> State\<llangle>\<sigma>\<^sub>2\<rrangle>"

text \<open>\<open>\<sigma>\<^sub>1 \<leadsto> \<sigma>\<^sub>1 \<leadsto> inpEv[] \<leadsto> \<sigma>\<^sub>4\<close>\<close>
definition 
  \<tau>\<^sub>6 :: \<T> where
  "\<tau>\<^sub>6 \<equiv> ((\<langle>\<sigma>\<^sub>1\<rangle> 
        \<leadsto> State\<llangle>\<sigma>\<^sub>1\<rrangle>)
        \<leadsto> Event\<llangle>inpEv,[]\<rrangle>) 
        \<leadsto> State\<llangle>\<sigma>\<^sub>4\<rrangle>"

text \<open>\<open>\<sigma>\<^sub>1 \<leadsto> \<sigma>\<^sub>4\<close>\<close>
definition 
  \<tau>\<^sub>7 :: \<T> where
  "\<tau>\<^sub>7 \<equiv> \<langle>\<sigma>\<^sub>1\<rangle> 
        \<leadsto> State\<llangle>\<sigma>\<^sub>4\<rrangle>"

text \<open>\<open>{\<not>Y0 \<le> Y1} \<triangleright> \<sigma>\<^sub>1 \<leadsto> \<sigma>\<^sub>1[x0 \<mapsto> 17]\<close> [\<open>Example 2.12\<close>]\<close>
definition 
  cst\<^sub>0 :: \<CC>\<T> where
  "cst\<^sub>0 \<equiv> {not ((Var ''Y0'') \<^sub>Rleq (Var ''Y1''))} \<triangleright> \<tau>\<^sub>0" 

text \<open>\<open>{True} \<triangleright> \<sigma>\<^sub>2 \<leadsto> \<sigma>\<^sub>2[x0 \<mapsto> 17]\<close>\<close>
definition 
  cst\<^sub>1 :: \<CC>\<T> where
  "cst\<^sub>1 \<equiv> {Bool True} \<triangleright> \<tau>\<^sub>1" 

text \<open>\<open>{True, False} \<triangleright> \<epsilon>\<close>\<close>
definition 
  cst\<^sub>2 :: \<CC>\<T> where
  "cst\<^sub>2 \<equiv> {Bool True, Bool False} \<triangleright> \<tau>\<^sub>2" 

text \<open>\<open>{True} \<triangleright> \<sigma>\<^sub>2\<close>\<close>
definition 
  cst\<^sub>3 :: \<CC>\<T> where
  "cst\<^sub>3 \<equiv> {Bool True} \<triangleright> \<tau>\<^sub>3"

text \<open>\<open>{} \<triangleright> \<sigma>\<^sub>1 \<leadsto> ev[] \<leadsto> \<sigma>\<^sub>1 \<leadsto> \<sigma>\<^sub>2\<close>\<close>
definition 
  cst\<^sub>4 :: \<CC>\<T> where
  "cst\<^sub>4 \<equiv> {} \<triangleright> \<tau>\<^sub>4" 

text \<open>\<open>{False} \<triangleright> \<sigma>\<^sub>2 \<leadsto> \<sigma>\<^sub>2 \<leadsto> ev[Y2] \<leadsto> \<sigma>\<^sub>2\<close>\<close>
definition 
  cst\<^sub>5 :: \<CC>\<T> where
  "cst\<^sub>5 \<equiv> {Bool False} \<triangleright> \<tau>\<^sub>5" 

text \<open>\<open>{} \<triangleright> \<sigma>\<^sub>1 \<leadsto> \<sigma>\<^sub>1 \<leadsto> ev[] \<leadsto> \<sigma>\<^sub>4\<close>\<close>
definition 
  cst\<^sub>6 :: \<CC>\<T> where
  "cst\<^sub>6 \<equiv> {} \<triangleright> \<tau>\<^sub>6" 

text \<open>\<open>{} \<triangleright> \<sigma>\<^sub>1 \<leadsto> \<sigma>\<^sub>4\<close>\<close>
definition 
  cst\<^sub>7 :: \<CC>\<T> where
  "cst\<^sub>7 \<equiv> {} \<triangleright> \<tau>\<^sub>7" 

text \<open>\<open>\<sigma>\<^sub>1 \<leadsto> inpEv[] \<leadsto> \<sigma>\<^sub>1 \<leadsto> \<sigma>\<^sub>2 \<leadsto> \<sigma>\<^sub>2 \<leadsto> \<sigma>\<^sub>2 \<leadsto> inpEv[Y2] \<leadsto> \<sigma>\<^sub>2\<close>\<close>
lemma "\<tau>\<^sub>4 \<cdot> \<tau>\<^sub>5 = ((((((\<langle>\<sigma>\<^sub>1\<rangle> 
                \<leadsto> Event\<llangle>inpEv,[]\<rrangle>) 
                \<leadsto> State\<llangle>\<sigma>\<^sub>1\<rrangle>) 
                \<leadsto> State\<llangle>\<sigma>\<^sub>2\<rrangle>)
                \<leadsto> State\<llangle>\<sigma>\<^sub>2\<rrangle>)
                \<leadsto> State\<llangle>\<sigma>\<^sub>2\<rrangle>) 
                \<leadsto> Event\<llangle>inpEv,[A (Var ''Y2'')]\<rrangle>) 
                \<leadsto> State\<llangle>\<sigma>\<^sub>2\<rrangle>"
  by (simp add: \<tau>\<^sub>4_def \<tau>\<^sub>5_def)

text \<open>\<open>\<sigma>\<^sub>2\<close>\<close>
lemma "\<tau>\<^sub>2 \<cdot> \<tau>\<^sub>3 = \<langle>\<sigma>\<^sub>2\<rangle>"
  by (simp add: \<tau>\<^sub>2_def \<tau>\<^sub>3_def)

text \<open>\<open>\<sigma>\<^sub>1 \<leadsto> \<sigma>\<^sub>4\<close>\<close>
lemma "(\<tau>\<^sub>2 \<cdot> \<tau>\<^sub>2) \<cdot> \<tau>\<^sub>7 = \<langle>\<sigma>\<^sub>1\<rangle> \<leadsto> State\<llangle>\<sigma>\<^sub>4\<rrangle>"
  by (simp add: \<tau>\<^sub>2_def \<tau>\<^sub>7_def)

lemma "first\<^sub>\<T> \<tau>\<^sub>0 = \<sigma>\<^sub>1"
  by (simp add: \<tau>\<^sub>0_def)

lemma "first\<^sub>\<T> \<tau>\<^sub>1 = \<sigma>\<^sub>2"
  by (simp add: \<tau>\<^sub>1_def)

lemma "first\<^sub>\<T> \<tau>\<^sub>2 = undefined"
  by (simp add: \<tau>\<^sub>2_def)

lemma "first\<^sub>\<T> \<tau>\<^sub>3 = \<sigma>\<^sub>2"
  by (simp add: \<tau>\<^sub>3_def)

lemma "first\<^sub>\<T> \<tau>\<^sub>4 = \<sigma>\<^sub>1"
  by (simp add: \<tau>\<^sub>4_def)

lemma "first\<^sub>\<T> \<tau>\<^sub>5 = \<sigma>\<^sub>2"
  by (simp add: \<tau>\<^sub>5_def)

lemma "first\<^sub>\<T> \<tau>\<^sub>6 = \<sigma>\<^sub>1"
  by (simp add: \<tau>\<^sub>6_def)

lemma "first\<^sub>\<T> \<tau>\<^sub>7 = \<sigma>\<^sub>1"
  by (simp add: \<tau>\<^sub>7_def)

lemma "last\<^sub>\<T> \<tau>\<^sub>0 = [''x0'' \<longmapsto> Exp (Num 17)] \<sigma>\<^sub>1"
  by (simp add: \<tau>\<^sub>0_def)

lemma "last\<^sub>\<T> \<tau>\<^sub>1 = [''x0'' \<longmapsto> Exp (Num 17)] \<sigma>\<^sub>2"
  by (simp add: \<tau>\<^sub>1_def)

lemma "last\<^sub>\<T> \<tau>\<^sub>2 = undefined"
  by (simp add: \<tau>\<^sub>2_def)

lemma "last\<^sub>\<T> \<tau>\<^sub>3 = \<sigma>\<^sub>2"
  by (simp add: \<tau>\<^sub>3_def)

lemma "last\<^sub>\<T> \<tau>\<^sub>4 = \<sigma>\<^sub>2"
  by (simp add: \<tau>\<^sub>4_def)

lemma "last\<^sub>\<T> \<tau>\<^sub>5 = \<sigma>\<^sub>2"
  by (simp add: \<tau>\<^sub>5_def)

lemma "last\<^sub>\<T> \<tau>\<^sub>6 = \<sigma>\<^sub>4"
  by (simp add: \<tau>\<^sub>6_def)

lemma "last\<^sub>\<T> \<tau>\<^sub>7 = \<sigma>\<^sub>4"
  by (simp add: \<tau>\<^sub>7_def)

text \<open>\<open>\<sigma>\<^sub>1 \<leadsto> inpEv[] \<leadsto> \<sigma>\<^sub>1 \<leadsto> \<sigma>\<^sub>2 \<leadsto> \<sigma>\<^sub>2 \<leadsto> inpEv[Y2] \<leadsto> \<sigma>\<^sub>2\<close>\<close>
lemma "cst\<^sub>4 \<^emph>\<^emph>\<^sub>\<pi> cst\<^sub>5 = 
                {Bool False} \<triangleright> 
                ((((((\<langle>\<sigma>\<^sub>1\<rangle> 
                \<leadsto> Event \<llangle>inpEv,[]\<rrangle>) 
                \<leadsto> State\<llangle>\<sigma>\<^sub>1\<rrangle>)
                \<leadsto> State\<llangle>\<sigma>\<^sub>2\<rrangle>)
                \<leadsto> State\<llangle>\<sigma>\<^sub>2\<rrangle>)
                \<leadsto> Event\<llangle>inpEv,[A (Var ''Y2'')]\<rrangle>) 
                \<leadsto> State\<llangle>\<sigma>\<^sub>2\<rrangle>)"
  by (simp add: \<tau>\<^sub>4_def \<tau>\<^sub>5_def cst\<^sub>4_def cst\<^sub>5_def)

text \<open>\<open>\<sigma>\<^sub>1 \<leadsto> inpEv[Y0] \<leadsto> \<sigma>\<^sub>1 \<leadsto> \<sigma>\<^sub>1[x0 \<mapsto> 17]\<close> [\<open>Example 2.14\<close>]\<close>
lemma "(\<epsilon> \<cdot> (gen_event inpEv \<sigma>\<^sub>1 [A (Var ''Y0'')])) \<leadsto> State\<llangle>[''x0'' \<longmapsto> Exp (Num 17)] \<sigma>\<^sub>1\<rrangle>
      = ((\<langle>\<sigma>\<^sub>1\<rangle> 
      \<leadsto> Event\<llangle>inpEv, [A (Var ''Y0'')]\<rrangle>)
      \<leadsto> State\<llangle>\<sigma>\<^sub>1\<rrangle>)
      \<leadsto> State\<llangle>[''x0'' \<longmapsto> Exp (Num 17)] \<sigma>\<^sub>1\<rrangle>"
  by (simp add: \<sigma>\<^sub>1_def)

text \<open>\<open>\<sigma>\<^sub>1 \<leadsto> inpEv[42] \<leadsto> \<sigma>\<^sub>1 \<leadsto> \<sigma>\<^sub>1[x0 \<mapsto> 17]\<close>\<close>
lemma "(gen_event inpEv \<sigma>\<^sub>2 [A (Var ''w0'')]) \<leadsto> State\<llangle>[''z0'' \<longmapsto> Exp (Num 17)] \<sigma>\<^sub>2\<rrangle> 
       = ((\<langle>\<sigma>\<^sub>2\<rangle> 
       \<leadsto> Event\<llangle>inpEv, [A (Num 42)]\<rrangle>)
       \<leadsto> State\<llangle>\<sigma>\<^sub>2\<rrangle>)
       \<leadsto> State\<llangle>[''z0'' \<longmapsto> Exp (Num 17)] \<sigma>\<^sub>2\<rrangle>" 
  by (simp add: \<sigma>\<^sub>2_def)

lemma "(gen_event ev \<sigma> e\<^sub>1) \<^emph>\<^emph> (gen_event ev \<sigma> e\<^sub>2)
      = (((\<langle>\<sigma>\<rangle> 
        \<leadsto> Event\<llangle>ev, lval\<^sub>E e\<^sub>1 \<sigma>\<rrangle>) 
        \<leadsto> State\<llangle>\<sigma>\<rrangle>) 
        \<leadsto> Event\<llangle>ev, lval\<^sub>E e\<^sub>2 \<sigma>\<rrangle>) 
        \<leadsto> State\<llangle>\<sigma>\<rrangle>"
  by simp

context notes [simp] = symb\<^sub>\<Sigma>_def wf\<^sub>\<Sigma>_def begin

lemma "\<tau>\<^sub>0 \<diamondop>\<diamondop>\<^sub>\<T> symb\<^sub>\<T> \<tau>\<^sub>0"
  by (auto simp add: \<tau>\<^sub>0_def \<sigma>\<^sub>1_def \<sigma>\<^sub>2_def)

lemma "\<tau>\<^sub>1 \<diamondop>\<diamondop>\<^sub>\<T> symb\<^sub>\<T> \<tau>\<^sub>1"
  by (simp add: \<tau>\<^sub>1_def \<sigma>\<^sub>1_def \<sigma>\<^sub>2_def)

lemma "\<tau>\<^sub>2 \<diamondop>\<diamondop>\<^sub>\<T> symb\<^sub>\<T> \<tau>\<^sub>2"
  by (simp add: \<tau>\<^sub>2_def)

lemma "\<tau>\<^sub>3 \<diamondop>\<diamondop>\<^sub>\<T> symb\<^sub>\<T> \<tau>\<^sub>3"
  by (simp add: \<tau>\<^sub>3_def \<sigma>\<^sub>2_def)

lemma "\<not>\<tau>\<^sub>4 \<diamondop>\<diamondop>\<^sub>\<T> symb\<^sub>\<T> \<tau>\<^sub>4"
  by (simp add: \<tau>\<^sub>4_def \<sigma>\<^sub>1_def \<sigma>\<^sub>2_def)
  
lemma "\<tau>\<^sub>5 \<diamondop>\<diamondop>\<^sub>\<T> (symb\<^sub>\<T> \<tau>\<^sub>5)"
  by (simp add: \<tau>\<^sub>5_def \<sigma>\<^sub>2_def)

lemma "\<tau>\<^sub>6 \<diamondop>\<diamondop>\<^sub>\<T> (symb\<^sub>\<T> \<tau>\<^sub>6)"
  by (auto simp add: \<tau>\<^sub>6_def \<sigma>\<^sub>1_def \<sigma>\<^sub>4_def)

lemma "\<tau>\<^sub>7 \<diamondop>\<diamondop>\<^sub>\<T> (symb\<^sub>\<T> \<tau>\<^sub>7)"
  by (auto simp add: \<tau>\<^sub>7_def \<sigma>\<^sub>1_def \<sigma>\<^sub>4_def)

lemma "{} \<subseteq>\<^sub>p (symb\<^sub>\<T> \<tau>\<^sub>0)"
  by simp

lemma "{(Num 5) \<^sub>Req (Var ''Y0'')} \<subseteq>\<^sub>p (symb\<^sub>\<T> \<tau>\<^sub>0)"
  by (simp add: \<tau>\<^sub>0_def \<sigma>\<^sub>1_def)

lemma "\<not>{(Num 5) \<^sub>Req (Var ''x0'')} \<subseteq>\<^sub>p (symb\<^sub>\<T> \<tau>\<^sub>0)"
  by (simp add: \<tau>\<^sub>0_def \<sigma>\<^sub>1_def)

lemma "\<tau>\<^sub>0 \<subseteq>\<^sub>E (symb\<^sub>\<T> \<tau>\<^sub>0)"
  by (simp add: \<tau>\<^sub>0_def)

lemma "\<tau>\<^sub>1 \<subseteq>\<^sub>E (symb\<^sub>\<T> \<tau>\<^sub>1)"
  by (simp add: \<tau>\<^sub>1_def)

lemma "\<tau>\<^sub>2 \<subseteq>\<^sub>E (symb\<^sub>\<T> \<tau>\<^sub>2)"
  by (simp add: \<tau>\<^sub>2_def)

lemma "\<tau>\<^sub>3 \<subseteq>\<^sub>E (symb\<^sub>\<T> \<tau>\<^sub>3)"
  by (simp add: \<tau>\<^sub>3_def)

lemma "\<tau>\<^sub>4 \<subseteq>\<^sub>E (symb\<^sub>\<T> \<tau>\<^sub>4)"
  by (simp add: \<tau>\<^sub>4_def)

lemma "\<not>\<tau>\<^sub>5 \<subseteq>\<^sub>E (symb\<^sub>\<T> \<tau>\<^sub>5)"
  by (simp add: \<tau>\<^sub>5_def \<sigma>\<^sub>2_def)

lemma "\<tau>\<^sub>6 \<subseteq>\<^sub>E (symb\<^sub>\<T> \<tau>\<^sub>6)"
  by (simp add: \<tau>\<^sub>6_def)

lemma "\<tau>\<^sub>7 \<subseteq>\<^sub>E (symb\<^sub>\<T> \<tau>\<^sub>7)"
  by (simp add: \<tau>\<^sub>7_def)

lemma "wf_seq\<^sub>\<T> \<tau>\<^sub>0"
  by (simp add: \<tau>\<^sub>0_def)

lemma "wf_seq\<^sub>\<T> \<tau>\<^sub>1"
  by (simp add: \<tau>\<^sub>1_def)

lemma "wf_seq\<^sub>\<T> \<tau>\<^sub>2"
  by (simp add: \<tau>\<^sub>2_def)
  
lemma "wf_seq\<^sub>\<T> \<tau>\<^sub>3"
  by (simp add: \<tau>\<^sub>3_def)

lemma "wf_seq\<^sub>\<T> \<tau>\<^sub>4"
  by (simp add: \<tau>\<^sub>4_def)

lemma "wf_seq\<^sub>\<T> \<tau>\<^sub>5"
  by (simp add: \<tau>\<^sub>5_def)

lemma "\<not>wf_seq\<^sub>\<T> \<tau>\<^sub>6"
proof -
  have "\<exists>x. fmlookup \<sigma>\<^sub>1 x \<noteq> fmlookup \<sigma>\<^sub>4 x" by (auto simp add: \<sigma>\<^sub>1_def \<sigma>\<^sub>4_def)
  thus ?thesis by (auto simp add: \<tau>\<^sub>6_def) 
qed

lemma "wf_seq\<^sub>\<T> \<tau>\<^sub>7"
  by (simp add: \<tau>\<^sub>7_def)

lemma "wf_states\<^sub>\<T> \<tau>\<^sub>0"
  by (auto simp add: \<sigma>\<^sub>1_def \<tau>\<^sub>0_def)

lemma "wf_states\<^sub>\<T> \<tau>\<^sub>1"
  by (simp add: \<sigma>\<^sub>2_def \<tau>\<^sub>1_def)

lemma "wf_states\<^sub>\<T> \<tau>\<^sub>2"
  by (simp add: \<tau>\<^sub>2_def)
  
lemma "wf_states\<^sub>\<T> \<tau>\<^sub>3"
  by (simp add: \<sigma>\<^sub>2_def \<tau>\<^sub>3_def)

lemma "wf_states\<^sub>\<T> \<tau>\<^sub>4"
  by (auto simp add: \<sigma>\<^sub>1_def \<sigma>\<^sub>2_def \<tau>\<^sub>4_def)

lemma "wf_states\<^sub>\<T> \<tau>\<^sub>5"
  by (simp add: \<sigma>\<^sub>2_def \<tau>\<^sub>5_def)

lemma "\<not>wf_states\<^sub>\<T> \<tau>\<^sub>6"
  by (simp add: \<sigma>\<^sub>4_def \<tau>\<^sub>6_def)

lemma "\<not>wf_states\<^sub>\<T> \<tau>\<^sub>7"
  by (simp add: \<sigma>\<^sub>4_def \<tau>\<^sub>7_def)

context notes [simp] = wf\<^sub>\<pi>_def begin

lemma "wf\<^sub>\<pi> cst\<^sub>0" 
  by (auto simp add: cst\<^sub>0_def \<tau>\<^sub>0_def \<sigma>\<^sub>1_def)

lemma "wf\<^sub>\<pi> cst\<^sub>1" 
  by (simp add: cst\<^sub>1_def \<tau>\<^sub>1_def \<sigma>\<^sub>2_def)

lemma "wf\<^sub>\<pi> cst\<^sub>2"
  by (simp add: cst\<^sub>2_def \<tau>\<^sub>2_def)

lemma "wf\<^sub>\<pi> cst\<^sub>3" 
  by (simp add: cst\<^sub>3_def \<tau>\<^sub>3_def \<sigma>\<^sub>2_def)

lemma "\<not>wf\<^sub>\<pi> cst\<^sub>4" \<comment> \<open>(1) violated\<close>
  by (simp add: cst\<^sub>4_def \<tau>\<^sub>4_def \<sigma>\<^sub>1_def \<sigma>\<^sub>2_def)

lemma "\<not>wf\<^sub>\<pi> cst\<^sub>5" \<comment> \<open>(3) violated\<close>
  by (auto simp add: cst\<^sub>5_def \<tau>\<^sub>5_def \<sigma>\<^sub>2_def)
  
lemma "\<not>wf\<^sub>\<pi> cst\<^sub>6" \<comment> \<open>(4, 5) violated\<close>
proof -
  have "\<exists>x. fmlookup \<sigma>\<^sub>1 x \<noteq> fmlookup \<sigma>\<^sub>4 x" by (auto simp add: \<sigma>\<^sub>1_def \<sigma>\<^sub>4_def)
  thus ?thesis by (force simp add: cst\<^sub>6_def \<tau>\<^sub>6_def)
qed

lemma "\<not>wf\<^sub>\<pi> cst\<^sub>7" \<comment> \<open>(5) violated\<close>
  by (auto simp add: cst\<^sub>7_def \<tau>\<^sub>7_def \<sigma>\<^sub>4_def)

context notes [simp] = concrete\<^sub>\<pi>_def begin

lemma "\<not>concrete\<^sub>\<pi> cst\<^sub>0" \<comment> \<open>(2, 3) violated\<close>
  by (simp add: cst\<^sub>0_def \<tau>\<^sub>0_def)

lemma "concrete\<^sub>\<pi> cst\<^sub>1" 
  by (simp add: cst\<^sub>1_def \<tau>\<^sub>1_def \<sigma>\<^sub>2_def concrete\<^sub>\<Sigma>_def)

lemma "concrete\<^sub>\<pi> cst\<^sub>2" 
  by (simp add: cst\<^sub>2_def \<tau>\<^sub>2_def)

lemma "concrete\<^sub>\<pi> cst\<^sub>3" 
  by (simp add: cst\<^sub>3_def \<tau>\<^sub>3_def \<sigma>\<^sub>2_def concrete\<^sub>\<Sigma>_def)

lemma "\<not>concrete\<^sub>\<pi> cst\<^sub>4" \<comment> \<open>(1, 3) violated\<close>
  by (simp add: cst\<^sub>4_def \<tau>\<^sub>4_def \<sigma>\<^sub>1_def \<sigma>\<^sub>2_def)

lemma "\<not>concrete\<^sub>\<pi> cst\<^sub>5" \<comment> \<open>(1, 3) violated\<close>
  by (simp add: cst\<^sub>5_def \<tau>\<^sub>5_def)

lemma "\<not>concrete\<^sub>\<pi> cst\<^sub>6" \<comment> \<open>(1, 3) violated\<close>
  by (simp add: cst\<^sub>6_def \<tau>\<^sub>6_def \<sigma>\<^sub>1_def concrete\<^sub>\<Sigma>_def)

lemma "\<not>concrete\<^sub>\<pi> cst\<^sub>7" \<comment> \<open>(1, 3) violated\<close>
  by (simp add: cst\<^sub>7_def \<tau>\<^sub>7_def \<sigma>\<^sub>1_def concrete\<^sub>\<Sigma>_def)

end
end
end



subsection \<open>Concretization Mappings\<close>

text \<open>\<open>\<sigma>\<^sub>6 = [X \<mapsto> \<^emph>][z \<mapsto> 3]\<close> [\<open>Example 2.20\<close>]\<close>
definition
  \<sigma>\<^sub>6 :: \<Sigma> where
  "\<sigma>\<^sub>6 \<equiv> fm[
         (''X'', \<^emph>), 
         (''z'', Exp (Num 3))
          ]"

context notes [simp] = symb\<^sub>\<Sigma>_def concrete\<^sub>\<Sigma>_def is_conc_map\<^sub>\<Sigma>_def begin

text \<open>\<open>\<rho> = [Y0 \<mapsto> 3][Y1 \<mapsto> 2]\<close>\<close>
lemma "is_conc_map\<^sub>\<Sigma> (fm[(''Y0'', Exp (Num 3)), (''Y1'', Exp (Num 2))]) \<sigma>\<^sub>0"
  by (simp add: \<sigma>\<^sub>0_def; blast)

text \<open>\<open>\<rho> = [Y0 \<mapsto> 3][Y1 \<mapsto> 2]\<close> [\<open>Example 2.20\<close>]\<close>
lemma "is_conc_map\<^sub>\<Sigma> (fm[(''Y0'', Exp (Num 3)), (''Y1'', Exp (Num 2))]) \<sigma>\<^sub>1"
  by (simp add: \<sigma>\<^sub>1_def; blast)

text \<open>\<open>\<rho> = [Y0 \<mapsto> 3][Y1 \<mapsto> 2]\<close>\<close>
lemma "\<not>is_conc_map\<^sub>\<Sigma> (fm[(''Y0'', Exp (Num 3)), (''Y1'', Exp (Num 2))]) \<sigma>\<^sub>2"
  by (simp add: \<sigma>\<^sub>2_def)

text \<open>\<open>\<rho> = [Y0 \<mapsto> 3][Y1 \<mapsto> 2]\<close>\<close>
lemma "is_conc_map\<^sub>\<Sigma> (fm[(''Y0'', Exp (Num 3)), (''Y1'', Exp (Num 2))]) \<sigma>\<^sub>5"
  by (simp add: \<sigma>\<^sub>5_def; blast)

text \<open>\<open>\<rho> = [Y \<mapsto> 0]\<close>\<close>
lemma "\<not>is_conc_map\<^sub>\<Sigma> (fm[(''Y'', Exp (Num 0))]) \<sigma>\<^sub>6"
  by (simp add: \<sigma>\<^sub>6_def)

text \<open>\<open>\<rho> = [X \<mapsto> 2]\<close>\<close>
lemma "is_conc_map\<^sub>\<Sigma> (fm[(''X'', Exp (Num 2))]) \<sigma>\<^sub>6"
  by (simp add: \<sigma>\<^sub>6_def)

text \<open>\<open>\<rho> = [X \<mapsto> 2][Y \<mapsto> 0]\<close> [\<open>Example 2.20\<close>]\<close>
lemma "is_conc_map\<^sub>\<Sigma> (fm[(''X'', Exp (Num 2)), (''Y'', Exp (Num 0))]) \<sigma>\<^sub>6"
  by (simp add: \<sigma>\<^sub>6_def)

text \<open>\<open>\<rho> = [Y0 \<mapsto> 3][Y1 \<mapsto> 2]\<close>\<close>
lemma "fm[(''Y0'', Exp (Num 3)), (''Y1'', Exp (Num 2))] \<bullet> \<circle> = fm[(''Y0'', Exp (Num 3)), (''Y1'', Exp (Num 2))]"
  by (simp add: fmmap_keys_empty)

text \<open>\<open>\<rho> = [Y0 \<mapsto> 3][Y1 \<mapsto> 2]\<close> [\<open>Example 2.22\<close>]\<close>
lemma "fm[(''Y0'', Exp (Num 3)), (''Y1'', Exp (Num 2))] \<bullet> \<sigma>\<^sub>1 = \<sigma>\<^sub>2"
  by (simp add: \<sigma>\<^sub>1_def \<sigma>\<^sub>2_def fmap_ext fmupd.rep_eq map_upd_def)

lemma "min_conc_map\<^sub>\<Sigma> \<circle> 0 = \<circle>"
  by (simp add: fmap_ext)

lemma "min_conc_map\<^sub>\<Sigma> \<sigma>\<^sub>1 0 = fm[(''Y0'', Exp (Num 0)),(''Y1'', Exp (Num 0))]"
  by (simp add: \<sigma>\<^sub>1_def fmap_ext)

lemma "min_conc_map\<^sub>\<Sigma> \<sigma>\<^sub>6 15 = fm[(''X'', Exp (Num 15))]"
  by (simp add: \<sigma>\<^sub>6_def fmap_ext)

text \<open>\<open>\<rho> = [Y0 \<mapsto> 3][Y1 \<mapsto> 2]\<close>\<close>
lemma "is_conc_map\<^sub>\<T> (fm[(''Y0'', Exp (Num 3)), (''Y1'', Exp (Num 2))]) \<tau>\<^sub>0"
  by (auto simp add: \<tau>\<^sub>0_def \<sigma>\<^sub>1_def)

text \<open>\<open>\<rho> = [Y0 \<mapsto> 3][Y1 \<mapsto> 2]\<close> [\<open>Example 2.26\<close>]\<close>
lemma "trace_conc (fm[(''Y0'', Exp (Num 3)), (''Y1'', Exp (Num 2))]) \<tau>\<^sub>0 = \<tau>\<^sub>1"
  apply (simp add: \<tau>\<^sub>0_def \<tau>\<^sub>1_def \<sigma>\<^sub>1_def \<sigma>\<^sub>2_def)
  by (simp add: fmap_ext fmupd.rep_eq map_upd_def)

text \<open>\<open>\<rho> = [Y0 \<mapsto> 3][Y1 \<mapsto> 2]\<close> [\<open>Example 2.26\<close>]\<close>
lemma "fm[(''Y0'', Exp (Num 3)), (''Y1'', Exp (Num 2))] \<sqdot> cst\<^sub>0 = cst\<^sub>1"
  apply (auto simp add: cst\<^sub>0_def cst\<^sub>1_def \<tau>\<^sub>0_def \<tau>\<^sub>1_def \<sigma>\<^sub>1_def \<sigma>\<^sub>2_def)
  by (auto simp add: fmap_ext fmupd.rep_eq map_upd_def)

lemma "min_conc_map\<^sub>\<T> \<langle>\<circle>\<rangle> 0 = \<circle>"
  by (simp add: fmap_ext)

lemma "min_conc_map\<^sub>\<T> \<tau>\<^sub>0 0 = fm[(''Y0'', Exp (Num 0)), (''Y1'', Exp (Num 0))]"
  by (simp add: \<sigma>\<^sub>1_def \<tau>\<^sub>0_def fmap_ext)

lemma "min_conc_map\<^sub>\<T> \<tau>\<^sub>1 0 = \<circle>"
  by (simp add: \<sigma>\<^sub>2_def \<tau>\<^sub>1_def fmap_ext)

end

end