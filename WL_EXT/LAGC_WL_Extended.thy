chapter \<open>LAGC Semantics for $WL_{EXT}$\<close>
theory LAGC_WL_Extended
  imports "../basics/LAGC_Base" "../supplementary/LAGC_Base_Supplementary" "HOL-Library.Multiset" 
begin

text \<open>In this chapter, we formalize the LAGC semantics for the extended 
  While Language $WL_{EXT}$ as described in section~5 of the original paper.
  For this purpose, we faithfully expand on the LAGC semantics for \<open>WL\<close> in 
  order to accommodate new programming language concepts (e.g. concurrency, scopes).
  We show that this adaption can take place without endangering the automatic
  generation of corresponding code in Isabelle. We furthermore enhance our
  proof automation system in order to preserve our ability to systematically
  derive global traces for any specific program. However, note that our
  derivation system will get slightly more complex due to the introduction
  of trace concretizations, which will be applied in every step of the
  trace composition.\<close>

section \<open>Extended While Language ($WL_{EXT}$)\<close>

subsection \<open>Syntax\<close>

text \<open>We begin by introducing the syntax of the extended While Language $WL_{EXT}$,
  which is an expansion of the standard While Language \<open>WL\<close>. \par 
  However, before defining the statements of $WL_{EXT}$, we first introduce a new 
  datatype that models the declaration of fresh variables upon opening new scopes.
  These variables can then only be used inside the newly opened scope of the 
  corresponding program, implying that they are only alive when the scope is open. We 
  decide to model these variable declarations as finite sequences of variables, which 
  are separated by semicolons. Note that \<open>\<nu>\<close> represents the empty variable declaration, 
  referring to the case in which no variable is declared upon opening a new~scope.\<close>
  datatype varDecl =
        Nu ("\<nu>")  \<comment> \<open>Empty Declaration\<close>
      | Declaration var varDecl (infix ";" 58)  \<comment> \<open>Variable Declaration\<close>

text \<open>Statements of $WL_{EXT}$ expand the statements of \<open>WL\<close> with the following 
      new concepts:
      \begin{description}
  \item[Local Parallelism] This command executes two statements concurrently,
      until both statements have been fully~executed.
  \item[Local Memory] The extended While Language also supports a hierarchical block 
      structure. When opening a new scope, an arbitrary amount of fresh variables
      can be declared, which are then usable inside the body of the~scope.
  \item[Input] This command receives an unknown value, which is then stored 
      inside a provided variable. This later motivates the usage of 
      symbolic~variables.
  \item[Guarded Statement] This command hides a statement behind a Boolean
      guard. When the guard evaluates to true, the statement body is executed.
      Otherwise, the command blocks, halting the execution of the~statement.
  \item[Call Statement] This command calls a new method associated with
     the provided method name, passing it an arithmetic expression as an actual 
     parameter. Note that this implies that all method arguments must be of arithmetic 
     nature. Additionally, only one singular parameter can be passed as an argument. 
     Both of these design choices simplify our model without any loss of generality.
     The call statement will furthermore introduce implicit concurrency, meaning that
     callee and caller can both be scheduled after the~call. 
  \end{description} 
  Note that our model does not include a formalization of atomic statements, as
  this statement would later break the termination argument of the valuation 
  function. Although it is possible to circumvent this predicament (e.g. by using the
  \<open>partial_function\<close> theory), the corresponding formalization would add another 
  layer of complexity to our model, thereby later complicating our proof 
  automation. We therefore propose the addition of an atomic statement as an idea to 
  further extend this~model.\<close>
  datatype stmt = 
        SKIP  \<comment> \<open>No-Op\<close>
      | Assign var aexp \<comment> \<open>Assignment of a variable\<close>
      | If bexp stmt  \<comment> \<open>Conditional Branch\<close>
      | While bexp stmt \<comment> \<open>While Loop\<close>
      | Seq stmt stmt \<comment> \<open>Sequential Statement\<close>

      \<comment> \<open>Extension of the standard While Language:\<close>

      | LocPar stmt stmt  \<comment> \<open>Local Parallelism\<close>
      | LocMem varDecl stmt \<comment> \<open>Local Memory\<close>
      | Input var \<comment> \<open>Input\<close>  
      | Guard bexp stmt \<comment> \<open>Guard Statement\<close>
      | Call method_name aexp \<comment> \<open>Call Statement\<close>

text \<open>We can now build on top of the previously defined statements in order to 
  introduce a notion of methods. A method consists of a method  name (i.e. in 
  order to identify a method), a formal parameter, as well as a corresponding 
  method~body.\<close>
  datatype method = 
        Method method_name var stmt 

text \<open>A program consists of a finite list of methods, as well as a main statement 
  body, which is executed upon starting the program. Contrary to the original paper, 
  we enforce the list of methods to be finite, such that we can later traverse them 
  in finite time. Note that this design choice does not reduce the expressivity of 
  the programming~language.\<close>
  datatype program =
        Program "method list" stmt 

text \<open>We also add a minimal concrete syntax for our programming language, thereby 
  greatly improving the readability of~programs.\<close>
  notation Assign (infix ":=" 61)
  notation If ("(IF _/ THEN _/ FI)" [1000, 0] 61)
  notation While ("(WHILE _/ DO _/ OD)" [1000, 0] 61)
  notation Seq (infix ";;" 60)
  notation LocPar ("(CO _/ \<parallel> _/ OC)" [0, 0] 61)
  notation LocMem ("\<lbrace>_/ _/\<rbrace>" [1000, 0] 61)
  notation Input ("(INPUT _/)" [1000] 61) 
  notation Guard ("(:: _/ ;; _/ END)" [1000, 0] 61)
  notation Call ("(CALL _/ _/)" [1000, 1000] 61)
  notation Method ("Method _ _ \<lbrace>_\<rbrace>" [0, 0, 0] 62)
  notation Program ("Program _ \<lbrace>_\<rbrace>" [0, 0] 62)

text \<open>In order to ease the handling of methods, we propose additional
  straightforward projections which map a method onto its encased~components.\<close>
  fun
    method_name_proj :: "method \<Rightarrow> method_name" ("\<Up>\<^sub>n") where
    "\<Up>\<^sub>n (Method m x \<lbrace> S \<rbrace>) = m"

  fun
    method_var_proj :: "method \<Rightarrow> var" ("\<Up>\<^sub>v") where
    "\<Up>\<^sub>v (Method m x \<lbrace> S \<rbrace>) = x"

  fun
    method_stmt_proj :: "method \<Rightarrow> stmt" ("\<Up>\<^sub>s") where
    "\<Up>\<^sub>s (Method m x \<lbrace> S \<rbrace>) = S"

text \<open>Using our previously defined grammar, it is now possible to derive 
  syntactically correct $WL_{EXT}$ programs. We demonstrate this by presenting short 
  examples for programs utilizing our minimal concrete syntax. The first program 
  non-deterministically assigns a variable of an inner scope the value 1 or 2, the 
  second program demonstrates the usage of method calls, and the third program
  presents a computation on an input variable.\<close>
  definition
    WL_ex\<^sub>1 :: program where
    "WL_ex\<^sub>1 \<equiv> Program [] \<lbrace> \<lbrace> (''x'';\<nu>) CO ''x'' := Num 1 \<parallel> ''x'' := Num 2 OC \<rbrace> \<rbrace>"

  definition
    WL_ex\<^sub>2 :: program where
    "WL_ex\<^sub>2 \<equiv> Program [
                     (Method ''foo'' ''x'' \<lbrace> ''x'' := Num 2 \<rbrace>)
                    ] \<lbrace> (''x'' := Num 0;; CALL ''foo'' (Var ''x''));; ''x'' := Num 1 \<rbrace>"

  definition
    WL_ex\<^sub>3 :: program where
    "WL_ex\<^sub>3 \<equiv> Program [] \<lbrace> INPUT ''x'';; ''x'' := ((Var ''x'') \<^sub>Aadd (Num 1)) \<rbrace>"


subsection \<open>Variable Mappings\<close>

text \<open>We now introduce variable mappings for WL programs mapping specific
  programs to a set of their enclosed free variables. Note that all variables 
  introduced by variable declarations, as well as all formal parameters, are not 
  considered free, hence causing us to explicitly exclude them. The definition of 
  the corresponding recursive functions are~straightforward.\<close>
  fun
    vars\<^sub>d :: "varDecl \<Rightarrow> var set" where
    "vars\<^sub>d \<nu> = {}" |
    "vars\<^sub>d (x;d) = {x} \<union> vars\<^sub>d(d)" 

  fun
    vars\<^sub>s :: "stmt \<Rightarrow> var set" where
    "vars\<^sub>s SKIP = {}" |
    "vars\<^sub>s (Assign x a) = {x} \<union> vars\<^sub>A(a)" |
    "vars\<^sub>s (If b S) = vars\<^sub>B(b) \<union> vars\<^sub>s(S)" |
    "vars\<^sub>s (While b S) = vars\<^sub>B(b) \<union> vars\<^sub>s(S)" |
    "vars\<^sub>s (Seq S\<^sub>1 S\<^sub>2) = vars\<^sub>s(S\<^sub>1) \<union> vars\<^sub>s(S\<^sub>2)" |
    "vars\<^sub>s (LocPar S\<^sub>1 S\<^sub>2) = vars\<^sub>s(S\<^sub>1) \<union> vars\<^sub>s(S\<^sub>2)" |
    "vars\<^sub>s (LocMem D S) = vars\<^sub>s(S) - vars\<^sub>d(D)" |
    "vars\<^sub>s (Input x) = {x}" |
    "vars\<^sub>s (Guard g S) = vars\<^sub>B(g) \<union> vars\<^sub>s(S)" |
    "vars\<^sub>s (Call m a) = vars\<^sub>A(a)"

  fun
    vars\<^sub>m :: "method \<Rightarrow> var set" where
    "vars\<^sub>m (Method m x \<lbrace> S \<rbrace>) = vars\<^sub>s(S) - {x}"

  fun
    lvars\<^sub>m :: "method list \<Rightarrow> var set" where
    "lvars\<^sub>m [] = {}" |
    "lvars\<^sub>m (m # rest) = vars\<^sub>m(m) \<union> lvars\<^sub>m(rest)"

  fun
    vars\<^sub>p :: "program \<Rightarrow> var set" where
    "vars\<^sub>p (Program M \<lbrace> S \<rbrace>) = lvars\<^sub>m(M) \<union> vars\<^sub>s(S)"

text \<open>Similar to \<open>WL\<close>, we again provide variable occurrence functions,
  mapping a program onto a list of all free variables occurring in it. This 
  again ensures that all free program variables can be systematically traversed,
  thereby later allowing us to establish a notion of initial program states.\<close>
  fun
    occ\<^sub>d :: "varDecl \<Rightarrow> var list" where
    "occ\<^sub>d \<nu> = []" |
    "occ\<^sub>d (x;d) = x # occ\<^sub>d(d)"

  fun
    occ\<^sub>s :: "stmt \<Rightarrow> var list" where
    "occ\<^sub>s SKIP = []" |
    "occ\<^sub>s (Assign x a) = x # occ\<^sub>A(a)" |
    "occ\<^sub>s (If b S) = occ\<^sub>B(b) @ occ\<^sub>s(S)" |
    "occ\<^sub>s (While b S) = occ\<^sub>B(b) @ occ\<^sub>s(S)" |
    "occ\<^sub>s (Seq S\<^sub>1 S\<^sub>2) = occ\<^sub>s(S\<^sub>1) @ occ\<^sub>s(S\<^sub>2)" |
    "occ\<^sub>s (LocPar S\<^sub>1 S\<^sub>2) = occ\<^sub>s(S\<^sub>1) @ occ\<^sub>s(S\<^sub>2)" |
    "occ\<^sub>s (LocMem D S) = filter (\<lambda>x. \<not>List.member (occ\<^sub>d D) x) (occ\<^sub>s S)" |
    "occ\<^sub>s (Input x) = [x]" |
    "occ\<^sub>s (Guard g S) = occ\<^sub>B(g) @ occ\<^sub>s(S)" |
    "occ\<^sub>s (Call m a) = occ\<^sub>A(a)"

  fun
    occ\<^sub>m :: "method \<Rightarrow> var list" where
    "occ\<^sub>m (Method m x \<lbrace> S \<rbrace>) = removeAll x (occ\<^sub>s S)"

  fun
    locc\<^sub>m :: "method list \<Rightarrow> var list" where
    "locc\<^sub>m [] = []" |
    "locc\<^sub>m (m # rest) = occ\<^sub>m(m) @ locc\<^sub>m(rest)"

  fun
    occ\<^sub>p :: "program \<Rightarrow> var list" where
    "occ\<^sub>p (Program M \<lbrace> S \<rbrace>) = locc\<^sub>m(M) @ occ\<^sub>s(S)"

text \<open>We can now take another look at our earlier program examples and analyze
  the result of applying a variable mapping and variable occurrence~function.\<close>
  lemma "vars\<^sub>p WL_ex\<^sub>1 = {}"
    by (auto simp add: WL_ex\<^sub>1_def)

  lemma "vars\<^sub>p WL_ex\<^sub>2 = {''x''}"
    by (auto simp add: WL_ex\<^sub>2_def)

  lemma "occ\<^sub>p WL_ex\<^sub>1 = []"
    by (auto simp add: WL_ex\<^sub>1_def member_rec(1))

  lemma "occ\<^sub>p WL_ex\<^sub>2 = [''x'', ''x'', ''x'']"
    by (auto simp add: WL_ex\<^sub>2_def)


subsection \<open>Variable Substitutions\<close>

text \<open>In order to later handle variable conflicts, it becomes necessary to replace
  conflicting variables with fresh variables, thus motivating a notion of variable 
  substitutions. We therefore introduce recursive variable substitution functions, 
  which substitute every occurrence of a variable in a program with another variable. 
  Note that we will from now on use the abbreviation \<open>c [v \<leftarrow>\<^sub>d z]\<close> when referring to 
  the substitution of \<open>v\<close> with \<open>z\<close> in~command~\<open>c\<close>.\<close>
  primrec
    substitute\<^sub>d :: "varDecl \<Rightarrow> var \<Rightarrow> var \<Rightarrow> varDecl" ("_ [_ \<leftarrow>\<^sub>d _]" 70) where
    "\<nu> [v \<leftarrow>\<^sub>d z] = \<nu>" |
    "(x;d) [v \<leftarrow>\<^sub>d z] = (if x = v then z; (d [v \<leftarrow>\<^sub>d z]) else x; (d [v \<leftarrow>\<^sub>d z]))"

  primrec
    substitute\<^sub>s :: "stmt \<Rightarrow> var \<Rightarrow> var \<Rightarrow> stmt" ("_ [_ \<leftarrow>\<^sub>s _]" 70) where
    "SKIP [v \<leftarrow>\<^sub>s z] = SKIP" |
    "(Assign x a) [v \<leftarrow>\<^sub>s z] = (if x = v then (z := substitute\<^sub>A a v z) else (x := substitute\<^sub>A a v z))" |
    "(If b S) [v \<leftarrow>\<^sub>s z] = IF (substitute\<^sub>B b v z) THEN (S [v \<leftarrow>\<^sub>s z]) FI" |
    "(While b S) [v \<leftarrow>\<^sub>s z] = WHILE (substitute\<^sub>B b v z) DO (S [v \<leftarrow>\<^sub>s z]) OD" |
    "(Seq S\<^sub>1 S\<^sub>2) [v \<leftarrow>\<^sub>s z] = (S\<^sub>1 [v \<leftarrow>\<^sub>s z]);; (S\<^sub>2 [v \<leftarrow>\<^sub>s z])" |
    "(LocPar S\<^sub>1 S\<^sub>2) [v \<leftarrow>\<^sub>s z] = CO (S\<^sub>1 [v \<leftarrow>\<^sub>s z]) \<parallel> (S\<^sub>2 [v \<leftarrow>\<^sub>s z]) OC" |
    "(LocMem D S) [v \<leftarrow>\<^sub>s z] = \<lbrace> (substitute\<^sub>d D v z) (S [v \<leftarrow>\<^sub>s z]) \<rbrace>" |
    "(Input x) [v \<leftarrow>\<^sub>s z] = (if x = v then INPUT z else INPUT x)" |
    "(Guard g S) [v \<leftarrow>\<^sub>s z] = :: (substitute\<^sub>B g v z);; (S [v \<leftarrow>\<^sub>s z]) END" |
    "(Call m a) [v \<leftarrow>\<^sub>s z] = CALL m (substitute\<^sub>A a v z)" 

  primrec
    substitute\<^sub>m :: "method \<Rightarrow> var \<Rightarrow> var \<Rightarrow> method" ("_ [_ \<leftarrow>\<^sub>m _]" 70) where
    "(Method m x \<lbrace> S \<rbrace>) [v \<leftarrow>\<^sub>m z] = (if x = v then (Method m z \<lbrace> S [v \<leftarrow>\<^sub>s z] \<rbrace>) else (Method m x \<lbrace> S [v \<leftarrow>\<^sub>s z] \<rbrace>))"

  primrec
    lsubstitute\<^sub>m :: "method list \<Rightarrow> var \<Rightarrow> var \<Rightarrow> method list" where
    "lsubstitute\<^sub>m [] v z = []" |
    "lsubstitute\<^sub>m (m # rest) v z = (m [v \<leftarrow>\<^sub>m z]) # (lsubstitute\<^sub>m rest v z)"

  primrec
    substitute\<^sub>p :: "program \<Rightarrow> var \<Rightarrow> var \<Rightarrow> program" ("_ [_ \<leftarrow>\<^sub>p _]" 70) where
    "(Program M \<lbrace> S \<rbrace>) [v \<leftarrow>\<^sub>p z] = (Program (lsubstitute\<^sub>m M v z) \<lbrace> S [v \<leftarrow>\<^sub>s z] \<rbrace>)"

text \<open>We can now demonstrate the variable substitution functions by utilizing one 
  of our earlier defined example~programs.\<close>
  lemma "WL_ex\<^sub>1 [''x'' \<leftarrow>\<^sub>p ''y''] = Program [] \<lbrace> \<lbrace> (''y'';\<nu>) CO ''y'' := Num 1 \<parallel> ''y'' := Num 2 OC \<rbrace> \<rbrace>"
    by (simp add: WL_ex\<^sub>1_def)


subsection \<open>Initial States\<close>

text \<open>Utilizing our variable occurrence functions, we now introduce initial 
  program states in the same manner as we did for the standard While Language \<open>WL\<close>.\<close>
  fun
    initial :: "program \<Rightarrow> \<Sigma>" ("\<sigma>\<^sub>I") where
    "initial prog = get_initial\<^sub>\<Sigma> (occ\<^sub>p prog)"

text \<open>Using Isabelle, we can now infer that every initial state, constructed using
  the algorithm above, must be of concrete nature. This trivially holds due to
  the definition of the \<open>get_initial\<^sub>\<Sigma>\<close> function, which assigns every variable occurring
  in the provided program the concrete arithmetic~expression~0.\<close>
  lemma initial_concrete: "concrete\<^sub>\<Sigma> (get_initial\<^sub>\<Sigma> l)"
    by (induct l; simp add: concrete\<^sub>\<Sigma>_def)

  lemma \<sigma>\<^sub>I_concrete: "concrete\<^sub>\<Sigma> (\<sigma>\<^sub>I S)"
    using initial_concrete by simp

text \<open>We can now provide an example for the construction of an initial program state 
  using one of our earlier~programs.\<close>
  lemma "\<sigma>\<^sub>I WL_ex\<^sub>2 = fm[(''x'', Exp (Num 0))]"
    by (simp add: WL_ex\<^sub>2_def fmupd_reorder_neq)



section \<open>Continuations\<close>

subsection \<open>Continuation Markers\<close>

text \<open>The notion of continuation markers of the standard While Language \<open>WL\<close> 
  does not need to be changed in order to handle $WL_{EXT}$.\<close>
  datatype cont_marker = 
      Lambda stmt ("\<lambda>[_]")  \<comment> \<open>Non-Empty Continuation Marker\<close>
      | Empty ("\<lambda>[\<nabla>]")  \<comment> \<open>Empty Continuation Marker\<close>

  fun
    mvars :: "cont_marker \<Rightarrow> var set" where
    "mvars \<lambda>[\<nabla>] = {}" |
    "mvars \<lambda>[S] = vars\<^sub>s(S)"


subsection \<open>Continuation Traces\<close>

text \<open>Analogous to \<open>WL\<close>, we again define a continuation trace as a 
  conditioned symbolic trace with an additional appended continuation~marker.\<close>
  datatype cont_trace = 
      Cont \<CC>\<T> cont_marker (infix "\<^item>" 55)

text \<open>In order to ease the handling of continuation traces, we propose additional
  projections which map a continuation trace onto its encased~components.\<close>
  fun
    proj_pc :: "cont_trace \<Rightarrow> path_condition" ("\<down>\<^sub>p") where
    "\<down>\<^sub>p (pc \<triangleright> \<tau> \<^item> cm) = pc" 

  fun
    proj_\<tau> :: "cont_trace \<Rightarrow> \<T>" ("\<down>\<^sub>\<tau>") where
    "\<down>\<^sub>\<tau> (pc \<triangleright> \<tau> \<^item> cm) = \<tau>" 

  fun
    proj_cont :: "cont_trace \<Rightarrow> cont_marker" ("\<down>\<^sub>\<lambda>") where
    "\<down>\<^sub>\<lambda> (cont \<^item> cm) = cm" 



section \<open>Local Evaluation\<close>

text \<open>In order to faithfully handle the new language concepts of $WL_{EXT}$, adapting
  the local evaluation of statements is crucial. However, note that the 
  local semantics of the original \<open>WL\<close> commands do not have to be changed, as their
  local semantics are identical to the standard While Language. This directly implies 
  that we only have to add support for the newly added programming 
  language~concepts. \par
  For this purpose, we first provide a helper function, which modifies a given 
  continuation marker by sequentially appending another statement onto the command 
  inside the continuation marker. If the continuation marker is empty, we simply 
  insert the provided~statement.\<close>
  fun
    cont_append :: "cont_marker \<Rightarrow> stmt \<Rightarrow> cont_marker" where
    "cont_append \<lambda>[S\<^sub>1'] S\<^sub>2 = \<lambda>[S\<^sub>1';;S\<^sub>2]" |
    "cont_append \<lambda>[\<nabla>] S\<^sub>2 = \<lambda>[S\<^sub>2]"

text \<open>We also define another helper function, which will later assist us when
  trying to reconstruct the local parallelism construct from two commands. The 
  function receives two continuation markers as arguments. If both continuation 
  markers are non-empty, we return a continuation marker containing the local 
  parallelism construct made up of both argument continuation marker contents. 
  If at least one continuation marker is empty, we simply return the other
  continuation~marker.\<close>
  fun
    parallel :: "cont_marker \<Rightarrow> cont_marker \<Rightarrow> cont_marker" where
    "parallel \<lambda>[S\<^sub>1] \<lambda>[S\<^sub>2] = \<lambda>[CO S\<^sub>1 \<parallel> S\<^sub>2 OC]" |
    "parallel \<lambda>[\<nabla>] \<lambda>[S] = \<lambda>[S]" |
    "parallel \<lambda>[S] \<lambda>[\<nabla>] = \<lambda>[S]" |
    "parallel \<lambda>[\<nabla>] \<lambda>[\<nabla>] = \<lambda>[\<nabla>]"

text \<open>We now have sufficient means to establish the valuation function, which
  maps statements in a given state onto a corresponding set of possible
  continuation traces. Note that the continuation traces for the standard \<open>WL\<close> 
  commands do not change, thus we only have to add function values for the
  newly added concepts. For this purpose, we adhere to the following~core~ideas: 
  \begin{description}
  \item[Local Parallelism] The rule for evaluating the local parallelism construct 
    \<open>CO S\<^sub>1;;S\<^sub>2 OC\<close> called in \<open>\<sigma>\<close> is simple. Considering that the choice of 
    evaluating \<open>S\<^sub>1\<close> or \<open>S\<^sub>2\<close> for one step is non-deterministic, two potential scenarios 
    have to be taken into consideration. If \<open>S\<^sub>1\<close> is evaluated to \<open>S\<^sub>1'\<close>, the subsequent 
    continuation marker should have the form \<open>\<lambda>[CO S\<^sub>1';;S\<^sub>2 OC]\<close>. If \<open>S\<^sub>2\<close> is evaluated 
    to \<open>S\<^sub>2'\<close>, the subsequent continuation marker should have the form 
    \<open>\<lambda>[CO S\<^sub>1;;S\<^sub>2' OC]\<close>. However, if the respective evaluated statement terminates in 
    one step, the local parallelism construct is abolished, whilst the other command 
    still remains to be evaluated. \par
    Considering that we have to adhere to Isabelle syntax, our formalization gets 
    slightly more complex than the definition of the original paper. We first compute 
    \<open>(val S\<^sub>1 \<sigma>)\<close> and \<open>(val S\<^sub>2 \<sigma>)\<close> in order to figure out all continuation 
    traces generated by \<open>S\<^sub>1\<close> and \<open>S\<^sub>2\<close> called in \<open>\<sigma>\<close>. We can then apply the 
    predefined operator \<open>`\<close> in order to compute the image of both sets under 
    a function, which reconstructs the local parallelism construct with the other 
    command in all their continuation markers. For this purpose, we utilize one of our 
    earlier defined helper functions. Finally, we merge both sets of continuation 
    traces, thus faithfully capturing the semantics of local~parallelism. \par
    Note that we again circumvent quantifications over infinite types (e.g. traces)
    in order to ensure that Isabelle can automatically generate efficient code for
    this~function.
  \item[Local Memory] The local memory command corresponds to two distinct rules,
    covering both empty and non-empty variable declarations. If no variable is 
    declared upon opening the scope in state \<open>\<sigma>\<close>, the continuation traces generated 
    from the local memory command match the continuation traces generated from the 
    scope body. If a variable \<open>x\<close> is declared when opening the scope in state \<open>\<sigma>\<close>, 
    one single continuation trace can be generated. The path condition of this 
    continuation trace is empty, considering that there are no constraints for
    variable declarations in our programming language. Its symbolic trace transits 
    from \<open>\<sigma>\<close> into an updated version of \<open>\<sigma>\<close>, in which a freshly generated variable 
    \<open>x'\<close> maps to initial value 0. This is done to ensure that every declared variable
    is automatically initialized with 0. The continuation marker also needs to be 
    adapted, such that all occurrences of \<open>x\<close> in the scope body are substituted by 
    \<open>x'\<close>. Note that we rename the declared variable in order to avoid variable 
    conflicts, as declarations in different scopes could theoretically introduce the 
    same variable name. The fresh variable is then only usable inside the opened
    scope, thus cleanly aligning with the desired scope~semantics. \par
    Note that the formalization of this valuation rule has been a major obstacle
    during the modeling procedure. In the original paper, the constraint \<open>x' \<notin> dom(\<sigma>)\<close>
    is used to ensure that \<open>x'\<close> is fresh, implying that \<open>x'\<close> is arbitrarily chosen
    out of the variables not occurring in the domain of \<open>\<sigma>\<close>. However, this causes the 
    choice of \<open>x'\<close> to be non-deterministic, thereby indicating an infinite
    set of possible continuation traces. This endangers our code generation 
    objective. In order to circumvent this predicament, we have therefore decided 
    to model the generation of fresh variables in a deterministic manner using a 
    self-defined variable generation function, thereby greatly deviating
    from the definition of the original paper. This design choice also simplifies
    the proof automation, as only one continuation trace needs to be~considered. \par
    We additionally propose the name convention \<open>$x::Scope\<close> for new scope
    variables, which ensures that we can easier associate the variables
    occurring in traces with their corresponding~scopes.  
  \item[Input] Evaluating the input command in state \<open>\<sigma>\<close> generates exactly one
    continuation trace. Its path condition and continuation marker are empty,
    implying that an input statement always terminates in one singular 
    evaluation step. However, the construction of its symbolic trace is slightly more 
    complicated. The variable that contains the input value (i.e. the unknown value) 
    will be updated, such that it maps onto a freshly generated variable \<open>x'\<close>. This 
    freshly generated variable must in turn be initialized with the symbolic
    value \<open>\<^emph>\<close>, thereby modeling the lack of knowledge about the input. Rerouting the
    input variable via \<open>x'\<close> to \<open>\<^emph>\<close> (instead of directly mapping the input variable
    to \<open>\<^emph>\<close>) ensures the possibility of further symbolic computations on the received
    input. We furthermore insert an event capturing the introduction 
    of the new variable \<open>x'\<close>, thus putting forth a possible interaction point for 
    the trace composition. \par
    Note that we again slightly deviate from the original paper by utilizing the 
    self-defined variable generation function, so as to deterministically generate a 
    fresh symbolic~variable. 
  \item[Guarded Statement] The guarded statement called in state \<open>\<sigma>\<close> generates exactly 
    one continuation trace. This continuation trace can only be taken iff 
    the Boolean guard evaluates to true, indicated by its path condition. While its 
    symbolic trace contains only the original state \<open>\<sigma>\<close>, its continuation marker 
    encases the statement S, suggesting that the statement body is still left to be 
    evaluated. Note that this implies a scheduling point right after the evaluation 
    of the Boolean guard expression. Guard and statement are therefore never evaluated 
    in the same evaluation step. Considering that there is no second possible
    continuation trace, this statement can only be evaluated iff the guard holds,
    implying that it blocks~otherwise. \par
    The semantics of this statement greatly deviate from the original paper, as the
    original paper additionally suggests a second continuation trace that preserves
    the guarded statement in its continuation marker iff the guard evaluates to false.
    However, this kind of model would later cause the transitive closure of our
    trace composition to unfavourably diverge when dealing with blocked guarded 
    statements, as they could just be continuously evaluated without making any 
    progress (i.e. stutter). This is a result of a missing fairness notion in our 
    semantics. In order to circumvent this problem, we therefore completely exclude 
    this continuation trace, thus completely eliminating possible stuttering. \par
    Note that the guarded statement introduces deadlocks in our semantics, as it
    could possibly block continuously. Due to our deviations, the method of handling 
    these deadlocks has drastically changed. In our formalization, reaching a deadlock 
    terminates the program, as there cannot exist a possible continuation trace with a 
    consistent path condition. However, the definition of this function in the original 
    paper would indicate infinite stuttering, thus always implying the construction
    of an infinite trace in the case of a~deadlock. 
  \item[Call Statement] Evaluating the call statement in state \<open>\<sigma>\<close> results in
    one singular continuation trace, which consists of an empty path condition, as 
    well as an empty continuation marker. This again indicates that a call statement
    terminates after exactly one evaluation step. The symbolic trace includes
    a method invocation event, which contains the method name of the callee and
    an arithmetic expression as its arguments. The method name is later used
    to identify the callee, whilst the arithmetic expression models the actual
    parameter. \par
    Note that the formalization of this continuation trace was slightly tricky,
    as the original paper just assumes that method names can be passed alongside
    the argument via the event. However, considering that our formalization
    enforces strict adherence to type constraints, this cannot pass. Two 
    alternative solutions to this predicament come into mind: Firstly, it would
    be an option to model a method name using fresh variable names. However, this
    would later conflict with our concreteness notion, as method names should
    not be simplified. The second option involves modeling a method name 
    as a new type of expression, thereby greatly deviating from the expression 
    syntax of the original paper. We select this second alternative, as this design 
    choice cleanly decouples method names from other kinds of expressions, hence 
    avoiding possible~conflicts.  
  \end{description} Due to its construction, each application of the valuation 
  function results in only finitely many continuation~traces, thereby simplifying
  our proof automation and code~generation.\<close>
  fun 
    val\<^sub>s :: "stmt \<Rightarrow> \<Sigma> \<Rightarrow> cont_trace set" where
    "val\<^sub>s SKIP \<sigma> = { {} \<triangleright> \<langle>\<sigma>\<rangle> \<^item> \<lambda>[\<nabla>] }" |
    "val\<^sub>s (x := a) \<sigma> = { {} \<triangleright> \<langle>\<sigma>\<rangle> \<leadsto> State\<llangle>[x \<longmapsto> Exp (val\<^sub>A a \<sigma>)] \<sigma>\<rrangle> \<^item> \<lambda>[\<nabla>] }" |
    "val\<^sub>s (IF b THEN S FI) \<sigma> = {
        {val\<^sub>B b \<sigma>} \<triangleright> \<langle>\<sigma>\<rangle> \<^item> \<lambda>[S],
        {val\<^sub>B (Not b) \<sigma>} \<triangleright> \<langle>\<sigma>\<rangle> \<^item> \<lambda>[\<nabla>]
      }" |
    "val\<^sub>s (WHILE b DO S OD) \<sigma> = {
        {val\<^sub>B b \<sigma>} \<triangleright> \<langle>\<sigma>\<rangle> \<^item> \<lambda>[S;;WHILE b DO S OD],
        {val\<^sub>B (Not b) \<sigma>} \<triangleright> \<langle>\<sigma>\<rangle> \<^item> \<lambda>[\<nabla>]
      }" |
    "val\<^sub>s (S\<^sub>1;;S\<^sub>2) \<sigma> = (%c. (\<down>\<^sub>p c) \<triangleright> (\<down>\<^sub>\<tau> c) \<^item> cont_append (\<down>\<^sub>\<lambda> c) S\<^sub>2) ` (val\<^sub>s S\<^sub>1 \<sigma>)" |
    "val\<^sub>s (CO S\<^sub>1 \<parallel> S\<^sub>2 OC) \<sigma> = 
          (%c. (\<down>\<^sub>p c) \<triangleright> (\<down>\<^sub>\<tau> c) \<^item> parallel (\<down>\<^sub>\<lambda> c) \<lambda>[S\<^sub>2]) ` (val\<^sub>s S\<^sub>1 \<sigma>) 
          \<union> (%c. (\<down>\<^sub>p c) \<triangleright> (\<down>\<^sub>\<tau> c) \<^item> parallel \<lambda>[S\<^sub>1] (\<down>\<^sub>\<lambda> c)) ` (val\<^sub>s S\<^sub>2 \<sigma>)" |
    "val\<^sub>s (\<lbrace> \<nu> S \<rbrace>) \<sigma> = val\<^sub>s S \<sigma>" |
    "val\<^sub>s (\<lbrace> (x;d) S \<rbrace>) \<sigma> = { 
        {} \<triangleright> \<langle>\<sigma>\<rangle> \<leadsto> State\<llangle>[(vargen \<sigma> 0 100 (''$'' @ x @ ''::Scope'')) \<longmapsto> Exp (Num 0)] \<sigma>\<rrangle> 
                   \<^item> \<lambda>[\<lbrace> d S \<rbrace> [x \<leftarrow>\<^sub>s (vargen \<sigma> 0 100 (''$'' @ x @ ''::Scope''))] ] }" |
    "val\<^sub>s (INPUT x) \<sigma> = { 
        {} \<triangleright> (\<langle>\<sigma>\<rangle> \<cdot> (gen_event inpEv ([x \<longmapsto> Exp (Var (vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')))] 
                   [(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> \<^emph>] \<sigma>) 
                   [A (Var (vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')))])) \<^item> \<lambda>[\<nabla>] 
      }" |
    "val\<^sub>s (:: g;; S END) \<sigma> = {
        {val\<^sub>B g \<sigma>} \<triangleright> \<langle>\<sigma>\<rangle> \<^item> \<lambda>[S]
      }" |
    "val\<^sub>s (CALL m a) \<sigma> = { {} \<triangleright> (gen_event invEv \<sigma> [P m, A a]) \<^item> \<lambda>[\<nabla>] }"

text \<open>As we have extended the local evaluation of our semantics, we can now analyze 
  several examples of valuation function applications including our new 
  language~concepts.\<close>
  lemma "val\<^sub>s (CO ''x'' := Num 1 \<parallel> ''x'' := Num 2;; SKIP OC) \<sigma>\<^sub>1 = { 
                  {} \<triangleright> \<langle>\<sigma>\<^sub>1\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 1)] \<sigma>\<^sub>1\<rrangle> \<^item> \<lambda>[''x'' := Num 2;; SKIP], 
                  {} \<triangleright> \<langle>\<sigma>\<^sub>1\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 2)] \<sigma>\<^sub>1\<rrangle> \<^item> \<lambda>[CO ''x'' := Num 1 \<parallel> SKIP OC]
              }" 
    by (simp add: \<sigma>\<^sub>1_def insert_commute)

  lemma "val\<^sub>s (\<lbrace> (''x'';\<nu>) ''x'' := Num 5 \<rbrace>) \<sigma>\<^sub>1 = { 
                  {} \<triangleright> \<langle>\<sigma>\<^sub>1\<rangle> \<leadsto> State\<llangle>[''$x::Scope'' \<longmapsto> Exp (Num 0)] \<sigma>\<^sub>1\<rrangle> \<^item> \<lambda>[\<lbrace>\<nu> ''$x::Scope'' := Num 5 \<rbrace>] 
              }" 
    by (simp add: \<sigma>\<^sub>1_def eval_nat_numeral)

  lemma "val\<^sub>s (CALL ''foo'' (Var ''x'')) \<sigma>\<^sub>2 = { 
                  {} \<triangleright> (\<langle>\<sigma>\<^sub>2\<rangle> \<leadsto> Event\<llangle>invEv, [P ''foo'', A (Num 8)]\<rrangle>) \<leadsto> State\<llangle>\<sigma>\<^sub>2\<rrangle> \<^item> \<lambda>[\<nabla>]
              }"
    by (simp add: \<sigma>\<^sub>2_def eval_nat_numeral)



section \<open>Trace Composition\<close>

subsection \<open>Configurations\<close>

text \<open>In order to setup the trace composition for $WL_{EXT}$, a notion of program
  configurations becomes indispensable. In \<open>WL\<close> we have introduced program
  configurations as tuples of symbolic traces and continuation markers. However,
  note that $WL_{EXT}$ expands the standard While Language with the call command. 
  The semantics of the call command enforce the caller and callee process to 
  execute in a concurrent manner, specifically depending on the underlying scheduling 
  algorithm. Considering that each process is associated with exactly one continuation 
  marker, the previous notion of program configurations (i.e. configurations only 
  containing one single continuation marker) is simply not strong~enough. \par
  We first introduce \<open>basic\<close> program configurations as tuples consisting of a 
  symbolic trace and one single continuation marker. Note that this notion directly 
  corresponds to the notion of program configurations for \<open>WL\<close>. We then
  introduce program configurations for $WL_{EXT}$ as tuples consisting of a symbolic 
  trace and a continuation marker multiset. This multiset will later contain the 
  continuation markers of all concurrently executing~processes. \par
  Note that the distinction between basic and non-basic program configurations 
  is not made in the original paper. However, both notions will later turn out
  to be useful for the purpose of establishing a high level of modularity in our 
  trace~composition.\<close>
  type_synonym basic_config = "\<T> * cont_marker" 
  type_synonym config = "\<T> * (cont_marker multiset)" 


subsection \<open>$\<open>\<delta>\<close>_1$-function\<close>

text \<open>Our next objective is to faithfully adjust the successor function (\<open>\<delta>\<close>-function),
  which maps each configuration onto the set of all possible successor configurations
  (i.e. all configurations reachable in one evaluation step). Considering that
  $WL_{EXT}$ is more complex that \<open>WL\<close> (i.e. due to its higher expressivity), we 
  decide to split up the \<open>\<delta>\<close>-function into two separate mappings, which correspond 
  to the two inductive rules given in the original~paper. \par
  We first aim to formalize the composition rule, which selects and removes
  one continuation marker from the corresponding multiset (i.e. schedules the
  associated process), evaluates it to next scheduling point, and then inserts the 
  updated continuation marker back into the multiset. Also note that the 
  composed trace needs to be concretized after every evaluation step in order to 
  turn the local trace into a global~trace. \par 
  In order to ensure the modularity of our formalization, we choose to first introduce 
  a basic successor function~($\<open>\<delta>\<close>_s$-function), which maps a basic configuration onto 
  all reachable basic successor configurations. This design choice later reduces the 
  complexity of the non-basic successor function ($\<open>\<delta>\<close>_1$-function), whilst also 
  guaranteeing a higher~readability. \par
  The basic successor function is defined as follows: Let us assume we have a 
  trace (\<open>sh \<leadsto> \<sigma>\<close>), whilst statement \<open>S\<close> is still left to be evaluated. 
  \begin{description}
  \item[Step 1] We first collect all continuation traces \<open>\<Pi>\<close> generated from \<open>S\<close> in \<open>\<sigma>\<close>, 
  which have a consistent path condition. Note that we beforehand, similar to the 
  original paper, simplify each path condition under the minimal concretization mapping 
  of the corresponding continuation trace \<open>\<pi> \<in> \<Pi>\<close>, as the path condition could contain 
  symbolic variables (e.g. due to the input~statement). 
  \item[Step 2] The symbolic traces of all consistent continuation traces \<open>\<pi> \<in> \<Pi>\<close> 
  are then concretized, thus ensuring the concreteness of all composed traces.
  We can then translate the concretized symbolic traces and continuation markers into 
  corresponding concrete basic successor~configurations. 
  \end{description} 
  Note that our formalization again greatly deviates from the original paper, as we 
  do not model the composition using an inductive rule, but with a deterministic 
  function. Remember that this is done in order to circumvent quantifications over 
  infinitely many~traces. \par 
  In the original paper, there are no constraints set up for the
  applied concretization mapping, implying that any valid trace concretization
  mapping could be used during the trace composition. Considering that there are 
  infinitely many concretization mappings, this would entail that the set of all basic 
  successor configurations is infinite. Hence, this kind of model would strongly 
  interfere with our objective of providing an efficient code generation. We therefore 
  decide to deviate from the paper by requiring the corresponding trace concretization 
  mapping to be minimal. We furthermore also enforce that each concretization mapping 
  concretizes all symbolic variables with 0. Although this guarantees the finiteness 
  of all basic successor configurations, it also greatly restricts the reachable 
  successor configurations in our model. We therefore propose a more faithful 
  representation of the trace concretization as an idea for an extension of this~work.\<close>
  fun
    basic_successors :: "basic_config \<Rightarrow> basic_config set" ("\<delta>\<^sub>s")  where
    "\<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S]) = 
        (%c. (trace_conc (min_conc_map\<^sub>\<T> (sh \<cdot> (\<down>\<^sub>\<tau> c)) 0) (sh \<cdot> (\<down>\<^sub>\<tau> c)), \<down>\<^sub>\<lambda> c))  
        ` {cont \<in> (val\<^sub>s S \<sigma>). consistent(sval\<^sub>B (\<down>\<^sub>p cont) (min_conc_map\<^sub>\<T> (\<down>\<^sub>\<tau> cont) 0))}" |
    "\<delta>\<^sub>s _ = undefined"

text \<open>By utilizing the basic successor function, we can now define the normal
  successor function ($\<open>\<delta>\<close>_1$-function) as follows: Let us assume we have a
  trace (\<open>sh \<leadsto> \<sigma>\<close>), while the statements of the continuation markers in multiset 
  \<open>q\<close> still need to be evaluated. 
  \begin{description}
  \item[Step 1] We begin by translating the multiset \<open>q\<close> into a normal set \<open>M\<close>, such 
  that we can use the element-wise operator \<open>`\<close> in order to apply a function on every 
  continuation marker contained in \<open>q\<close>. 
  \item[Step 2a] If the continuation marker \<open>cm \<in> M\<close> is empty, it returns the empty set. 
  Knowing that the process has already terminated, no successor configurations can be 
  generated. 
  \item[Step 2b] If the continuation marker \<open>cm \<in> M\<close> still contains a statement, we 
  utilize the basic successor function in order to map \<open>cm\<close> onto the set of all 
  reachable basic successor configurations \<open>C\<close>. We then translate all these basic 
  successor configurations \<open>cm' \<in> C\<close> into non-basic successor configurations by 
  removing \<open>cm\<close> from \<open>q\<close>, and adding \<open>cm'\<close> to \<open>q\<close>. 
  \end{description} Note that this makes up the set of all successor configurations,
  which can be constructed by the continuation markers in \<open>q\<close>. This matches exactly
  what the $\<open>\<delta>\<close>_1$-function is supposed~to~compute.\<close>
  fun
    successors\<^sub>1 :: "config \<Rightarrow> config set" ("\<delta>\<^sub>1")  where
    "\<delta>\<^sub>1 (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, q) = 
        \<Union>((%cm. (if cm = \<lambda>[\<nabla>] then {} else (%c. (fst(c), (q - {# cm #}) + {# snd(c) #})) 
            ` (\<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, cm)))) 
          ` set_mset q)" |
    "\<delta>\<^sub>1 _ = undefined"


subsection \<open>$\<open>\<delta>\<close>_2$-function\<close>

text \<open>The $\<open>\<delta>\<close>_1$-function allows the evaluation of call statements, which insert
  correlating invocation events into the symbolic trace during the composition
  procedure. However, we have not yet modeled the corresponding reaction of the called
  methods (i.e. the process creation). This motivates the definition of a separate 
  deterministic function ($\<open>\<delta>\<close>_2$-function), which maps a given program configuration 
  onto all successor configurations containing a newly created process. Note that
  we will only allow a reaction to occur iff a corresponding method invocation 
  took place~beforehand. \par
  Before we can begin with the actual formalization of this function, we first 
  introduce a helper function, which counts the occurrences of a specific trace atom 
  in a provided symbolic trace. The definition of this function is straightforward
  due to the use of~recursion.\<close>
  fun
    counter :: "\<T> \<Rightarrow> trace_atom \<Rightarrow> nat" ("#\<^sub>\<T>" 65) where
    "(#\<^sub>\<T>) \<epsilon> ta = 0" |
    "(#\<^sub>\<T>) (\<tau> \<leadsto> Event\<llangle>ev, e\<rrangle>) ta = (if ta = Event\<llangle>ev, e\<rrangle> then 1 + (#\<^sub>\<T>) \<tau> ta else (#\<^sub>\<T>) \<tau> ta)" |
    "(#\<^sub>\<T>) (\<tau> \<leadsto> State\<llangle>\<sigma>\<rrangle>) ta = (if ta = State\<llangle>\<sigma>\<rrangle> then 1 + (#\<^sub>\<T>) \<tau> ta else (#\<^sub>\<T>) \<tau> ta)" 

text \<open>We can now setup a separate wellformedness condition on traces,
  which will later ensure that processes can only be created iff they have been 
  invocated at an earlier point of the program. Whilst an invocation event 
  represents that a method is called, an invocation reaction event models the 
  corresponding reaction of the callee (i.e. the process creation). We call a 
  trace during the composition procedure wellformed iff there is an injective
  function mapping every invocation reaction event ocurring in the trace onto 
  a matching (preceding) invocation event. Note that we can easily formalize this 
  predicate by recursively traversing the provided trace, whilst checking that every 
  occurrence of an invocation reaction~event has been preceded by a incovation event
  that has not yet been~reacted~to.\<close>
  fun
    wellformed :: "\<T> \<Rightarrow> bool" where
    "wellformed \<epsilon> = True" |
    "wellformed (\<tau> \<leadsto> Event\<llangle>invREv, e\<rrangle>) = (wellformed(\<tau>) \<and> (#\<^sub>\<T>) \<tau> (Event\<llangle>invEv, e\<rrangle>) > (#\<^sub>\<T>) \<tau> (Event\<llangle>invREv, e\<rrangle>))" |
    "wellformed (\<tau> \<leadsto> t) = wellformed(\<tau>)"

text \<open>We also provide another helper function, which projects a symbolic trace \<open>\<tau>\<close>
  onto all method arguments that were passed in invocation events occurring
  in \<open>\<tau>\<close>. Every invocation reaction event occurring directly after \<open>last(\<tau>)\<close> 
  will only be allowed to receive an argument from a method call that 
  has already been executed at an earlier point. Hence, this definition restricts 
  the infinite set of possible actual parameters received in invocation reaction 
  events onto a finite set. Note that this will turn out to be a major advantage 
  when trying to setup the code generation for the~$\<open>\<delta>\<close>_2$-function.\<close>
  fun
    params :: "\<T> \<Rightarrow> exp set" where
    "params \<epsilon> = {}" |
    "params (\<tau> \<leadsto> Event\<llangle>invEv, [P m, A a]\<rrangle>) = params(\<tau>) \<union> {A a}" |
    "params (\<tau> \<leadsto> t) = params(\<tau>)"

text \<open>Using the helper functions above, we can finally provide a definition
  for the $\<open>\<delta>\<close>_2$-function. We formalize the function as follows: Let us assume
  we have a trace (\<open>sh \<leadsto> \<sigma>\<close>), while the statements of the continuation markers 
  in multiset \<open>q\<close> still need to be evaluated.
  \begin{description}
  \item[Step 1] We begin by computing all tuples of methods and arithmetic 
    method arguments \<open>M \<times> params(sh)\<close>, which could be used as parameters for invocation 
    reaction events directly after state~\<open>\<sigma>\<close>. Note that we use the filter-method of 
    the \<open>Set-theory\<close> in order to filter out all tuples, which would violate the 
    previously established notion of wellformedness. This results in the set of all 
    allowed tuples \<open>T\<close>. \par
    Note that it would be more intuitive to directly filter out the wellformed
    tuples out of the \<open>M \<times> aexp\<close> tuple set, as the result would be the same. 
    However, this model would interfere with our code generation, considering that
    the type of arithmetic expressions is infinitely big. Hence, we decide to 
    circumvent this by utilizing the previously defined helper function \<open>params\<close>,
    thus ensuring that we only have to consider finitely many (concrete) 
    arithmetic~arguments.
  \item[Step 2] We can now use the element-wise operator \<open>`\<close> in order to map
    each of the previously established tuples \<open>(m, v) \<in> T\<close> onto a corresponding 
    successor configuration. For this purpose, \<open>sh\<close> is expanded by appending
    the correlating invocation reaction event, which consists of \<open>m's\<close> method name
    and argument \<open>v\<close> as parameters. The trace afterwards transits into an updated
    version of \<open>\<sigma>\<close>, in which a freshly generated variable \<open>x'\<close> maps onto \<open>v\<close>. Note
    that \<open>x'\<close> represents the disambiguated call parameter (i.e. formal parameter) 
    of the~callee. \par
    We then merge \<open>q\<close> with the continuation marker consisting of \<open>m's\<close> method 
    body in order to result in the multiset of the desired successor configuration. 
    This represents the creation of a new process. Note that we additionally have 
    to substitute every occurrence of the formal parameter \<open>x\<close> in \<open>m\<close> with the 
    disambiguated call parameter \<open>x'\<close>, so as to avoid possible variable~conflicts. 
  \end{description} In contrast to the paper, we again use our self-defined 
  deterministic variable generation function in order to generate the fresh
  disambiguated call parameter \<open>x'\<close>. This ensures the existence of only finitely 
  many successor configurations, hence not endangering our code~generation. \par
  Note that the formalization of this function is based on the draft of the original
  paper from June 2021, thus we are slightly deviating from its final version. We
  therefore propose a faithful adaption of this function as an idea for further
  work on this~model.\<close>
  fun
    successors\<^sub>2 :: "method set \<Rightarrow> config \<Rightarrow> config set" ("\<delta>\<^sub>2")  where
    "\<delta>\<^sub>2 M (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, q) = 
        (%(m, v). (((sh \<cdot> (gen_event invREv \<sigma> [P (\<Up>\<^sub>n m), v])) 
                      \<leadsto> State\<llangle>[(vargen \<sigma> 0 100 (''$'' @ (\<Up>\<^sub>n m) @ ''::Param'')) \<longmapsto> Exp (proj\<^sub>A v)] \<sigma>\<rrangle>), 
                  q + {# \<lambda>[(\<Up>\<^sub>s m) [(\<Up>\<^sub>v m) \<leftarrow>\<^sub>s (vargen \<sigma> 0 100 (''$'' @ (\<Up>\<^sub>n m) @ ''::Param''))] ] #})) 
        ` Set.filter (%(m, v). wellformed (sh \<cdot> (gen_event invREv \<sigma> [P (\<Up>\<^sub>n m), v]))) (M \<times> params(sh))" |
    "\<delta>\<^sub>2 M _ = undefined"


subsection \<open>\<open>\<delta>\<close>-function\<close>

text \<open>In order to map a program configuration onto all possible successor 
  configurations, we can now merge the results of the $\<open>\<delta>\<close>_1$ and 
  $\<open>\<delta>\<close>_2$-function, thereby establishing the definition of the \<open>\<delta>\<close>-function.
  Note that each application can only result in finitely many successor 
  configurations due to the earlier established properties of the $\<open>\<delta>\<close>_1$ and 
  $\<open>\<delta>\<close>_2$-function.\<close>
  fun
    successors :: "method set \<Rightarrow> config \<Rightarrow> config set" ("\<delta>") where
    "\<delta> M c = \<delta>\<^sub>1 c \<union> \<delta>\<^sub>2 M c"


subsection \<open>Proof Automation\<close>

text \<open>In order to establish an automated proof system for the construction of 
  global traces, we now desire to provide simplification lemmas for the $\<open>\<delta>\<close>_s$
  and $\<open>\<delta>\<close>_2$-function. \par
  We start by providing corresponding simplifications for the $\<open>\<delta>\<close>_s$-function. For
  this purpose, we aim to define a general simplification lemma for each statement of 
  $WL_{EXT}$. This will lay the groundwork for efficient proof derivations, as 
  we can later simply utilize our simplification lemmas when deriving successor 
  configurations, hence avoiding having to deal with the underlying valuation function. 
  Note that $WL_{EXT}$ is non-deterministic due to its notion of concurrency, thus 
  implying that an application of the $\<open>\<delta>\<close>_s$-function may return multiple 
  successor~configurations. \par
  Our simplification lemmas will furthermore assume the concreteness of the trace
  provided as the function argument. This ensures that we only have to deal with 
  trace concretizations after evaluations of input statements. That is the case, because 
  we have already proven that the concretization of a concrete trace under a minimal 
  concretization mapping preserves the original trace. Hence the concretizations for 
  any other statement would not change the composed trace, as no new symbolic variables 
  are introduced. \par
  This assumption is reasonable, and not a problem, as we always concretize 
  all composed traces after every evaluation step of the trace composition, which 
  in turn implies that the resulting trace will always be concrete. Hence, our proof 
  derivation can always be applied, as long as we start our trace composition in a 
  concrete state. Note that we make this assumption in order to greatly reduce the 
  complexity of the following simplification~lemmas.\<close>

  context notes [simp] = consistent_def concrete\<^sub>\<Sigma>_def begin

  lemma \<delta>\<^sub>s_Skip:
    assumes "concrete\<^sub>\<T>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>)" 
    shows "\<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[SKIP]) = {(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[\<nabla>])}"
  proof -
    have "min_conc_map\<^sub>\<T> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) 0 = \<circle>"
      using assms min_conc_map_of_concrete\<^sub>\<T> by presburger
    \<comment> \<open>Considering that \<open>(sh \<leadsto> \<sigma>)\<close> is concrete, it contains no symbolic variables.
       Using the \<open>min_conc_map_of_concrete\<^sub>\<T>\<close> theorem, we can then infer that the minimal 
       concretization mapping of \<open>(sh \<leadsto> \<sigma>)\<close> matches the empty state.\<close>
    hence "trace_conc (min_conc_map\<^sub>\<T> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) 0) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) = sh \<leadsto> State\<llangle>\<sigma>\<rrangle>"
      using assms trace_conc_pr by fastforce
    \<comment> \<open>Utilizing a supplementary theorem, we conclude that the trace concretization of 
       \<open>(sh \<leadsto> \<sigma>)\<close> under its minimal concretization mapping must be \<open>(sh \<leadsto> \<sigma>)\<close> itself.\<close>
    thus ?thesis by auto
    \<comment> \<open>We can now use Isabelle in order to infer the conclusion of the lemma.\<close>
  qed

  lemma \<delta>\<^sub>s_Assign:
    assumes "concrete\<^sub>\<T>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<and> vars\<^sub>A(a) \<subseteq> fmdom'(\<sigma>)" 
    shows "\<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[x := a]) = {((sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<leadsto> State\<llangle>[x \<longmapsto> Exp (val\<^sub>A a \<sigma>)] \<sigma>\<rrangle>, \<lambda>[\<nabla>])}"
  proof -
    have "concrete\<^sub>S (Exp (val\<^sub>A a \<sigma>))"
      using assms concrete_imp\<^sub>A by (metis concrete\<^sub>\<T>.simps(2) sexp.simps(4))
    \<comment> \<open>Due to the concreteness of \<open>\<sigma>\<close>, the evaluation of a under \<open>\<sigma>\<close> must be of 
       concrete nature. This can be inferred using the \<open>concrete_imp\<^sub>A\<close> theorem
       of the base theory.\<close>
    moreover hence "concrete\<^sub>\<T> ((sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<leadsto> State\<llangle>[x \<longmapsto> Exp (val\<^sub>A a \<sigma>)] \<sigma>\<rrangle>)"
      using assms by simp
    \<comment> \<open>Hence the symbolic trace generated by the the assign statement must also 
       be concrete.\<close>
    moreover hence "min_conc_map\<^sub>\<T> ((sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<leadsto> State\<llangle>[x \<longmapsto> Exp (val\<^sub>A a \<sigma>)] \<sigma>\<rrangle>) 0 = \<circle>"
      using assms min_conc_map_of_concrete\<^sub>\<T> by presburger
    \<comment> \<open>Considering that the computed trace is concrete, it must not contain any 
       symbolic variables. Using the \<open>min_conc_map_of_concrete\<^sub>\<T>\<close> theorem, we can then 
       infer that its minimal concretization must be the empty state.\<close>
    ultimately have "trace_conc (min_conc_map\<^sub>\<T> ((sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<leadsto> State\<llangle>[x \<longmapsto> Exp (val\<^sub>A a \<sigma>)] \<sigma>\<rrangle>) 0) ((sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<leadsto> State\<llangle>[x \<longmapsto> Exp (val\<^sub>A a \<sigma>)] \<sigma>\<rrangle>) = ((sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<leadsto> State\<llangle>[x \<longmapsto> Exp (val\<^sub>A a \<sigma>)] \<sigma>\<rrangle>)"
      using assms trace_conc_pr by presburger
    \<comment> \<open>Utilizing a supplementary theorem, we conclude that the trace concretization of 
       the computed trace under its minimal concretization mapping must be 
       the computed trace itself.\<close>
    thus ?thesis by auto
    \<comment> \<open>We can now use Isabelle in order to infer the conclusion of the lemma.\<close>
  qed

text \<open>Applying the $\<open>\<delta>\<close>_s$-function on the If-Branch and the While-Loop can cause
  two distinct results. If the path condition containing the evaluated Boolean
  guard is consistent, the statement enters the true-case. If the path condition
  consisting of the evaluated negated Boolean guard is consistent, the statement will
  enter the false-case. We formalize a simplification lemma for each of these
  cases. \par
  Note that path conditions are fully evaluated during the trace composition, 
  as they are always simplified under state \<open>\<sigma>\<close>, which we guarantee to be of
  concrete~nature.\<close>
  lemma \<delta>\<^sub>s_If\<^sub>T:
    assumes "consistent ({val\<^sub>B b \<sigma>}) \<and> concrete\<^sub>\<T>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>)"
    shows "\<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[IF b THEN S FI]) = {(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S])}"
  proof -
    have "consistent(sval\<^sub>B {val\<^sub>B b \<sigma>} (min_conc_map\<^sub>\<T> (\<langle>\<sigma>\<rangle>) 0))"
      using assms consistent_pr by blast
    \<comment> \<open>As we have already proven in the base theory, the concretization of a 
       concrete expression always preserves the original expression. Note that
       this theorem also holds for consistent path conditions. Considering that we 
       have already assumed the consistency of the path condition containing b   
       (i.e. the true-case), the consistency of the path condition must then be 
       preserved if further simplified under a minimal concretization mapping.\<close>
    moreover have "\<not>consistent(sval\<^sub>B {val\<^sub>B (not b) \<sigma>} (min_conc_map\<^sub>\<T> (\<langle>\<sigma>\<rangle>) 0))"
      using assms by simp
    \<comment> \<open>Hence, the path condition of the continuation trace corresponding to
       the false-case cannot~be~consistent.\<close>
    ultimately have "{cont \<in> (val\<^sub>s (IF b THEN S FI) \<sigma>). consistent(sval\<^sub>B (\<down>\<^sub>p cont) (min_conc_map\<^sub>\<T> (\<down>\<^sub>\<tau> cont) 0))} = { {val\<^sub>B b \<sigma>} \<triangleright> \<langle>\<sigma>\<rangle> \<^item> \<lambda>[S] }"
      using assms by auto
    \<comment> \<open>Thus, the set of consistent continuation traces can only contain the
       continuation trace corresponding to the true-case.\<close>
    moreover have "min_conc_map\<^sub>\<T> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) 0 = \<circle>"
      using assms min_conc_map_of_concrete\<^sub>\<T> by presburger
    \<comment> \<open>Considering that \<open>(sh \<leadsto> \<sigma>)\<close> is concrete, it contains no symbolic variables.
       Using the \<open>min_conc_map_of_concrete\<^sub>\<T>\<close> theorem, we can then infer that the minimal 
       concretization mapping of \<open>(sh \<leadsto> \<sigma>)\<close> matches the empty state.\<close>
    hence "trace_conc (min_conc_map\<^sub>\<T> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) 0) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) = sh \<leadsto> State\<llangle>\<sigma>\<rrangle>"
      using assms trace_conc_pr LAGC_Base.concat.simps(1) by presburger
    \<comment> \<open>Utilizing a supplementary theorem, we conclude that the trace concretization of 
       \<open>(sh \<leadsto> \<sigma>)\<close> under its minimal concretization mapping must be \<open>(sh \<leadsto> \<sigma>)\<close> itself.\<close>
    ultimately show ?thesis by auto
    \<comment> \<open>We can now use Isabelle in order to infer the conclusion of the lemma.\<close>
  qed

  lemma \<delta>\<^sub>s_If\<^sub>F:
    assumes "consistent {(val\<^sub>B (not b) \<sigma>)} \<and> concrete\<^sub>\<T>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>)"
    shows "\<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[IF b THEN S FI]) = {(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[\<nabla>])}"
  proof -
    have "consistent(sval\<^sub>B {val\<^sub>B (not b) \<sigma>} (min_conc_map\<^sub>\<T> (\<langle>\<sigma>\<rangle>) 0))"
      using assms consistent_pr by blast
    \<comment> \<open>As we have already assumed that the path condition containing \<open>not b\<close> 
       (i.e. the false-case) is consistent, the consistency of the path condition must
       then be preserved if further simplified under a minimal concretization mapping.\<close>
    moreover have "\<not>consistent(sval\<^sub>B {val\<^sub>B b \<sigma>} (min_conc_map\<^sub>\<T> (\<langle>\<sigma>\<rangle>) 0))"
      using assms conc_pc_pr\<^sub>B\<^sub>N consistent_def s_value_pr\<^sub>B 
      by (metis bexp.simps(17) singletonI val\<^sub>B.simps(2))
    \<comment> \<open>Hence, the path condition of the continuation trace corresponding to the
       true-case cannot be~consistent.\<close>
    ultimately have "{cont \<in> (val\<^sub>s (IF b THEN S FI) \<sigma>). consistent(sval\<^sub>B (\<down>\<^sub>p cont) (min_conc_map\<^sub>\<T> (\<down>\<^sub>\<tau> cont) 0))} = { {val\<^sub>B (not b) \<sigma>} \<triangleright> \<langle>\<sigma>\<rangle> \<^item> \<lambda>[\<nabla>] }"
      using assms by auto
    \<comment> \<open>Thus, the set of consistent continuation traces can only contain the
       continuation trace corresponding to the false-case.\<close>
    moreover have "min_conc_map\<^sub>\<T> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) 0 = \<circle>"
      using assms min_conc_map_of_concrete\<^sub>\<T> by presburger
    \<comment> \<open>Considering that \<open>(sh \<leadsto> \<sigma>)\<close> is concrete, it contains no symbolic variables.
       Using the \<open>min_conc_map_of_concrete\<^sub>\<T>\<close> theorem, we can then infer that the minimal 
       concretization mapping of \<open>(sh \<leadsto> \<sigma>)\<close> matches the empty state.\<close>
    hence "trace_conc (min_conc_map\<^sub>\<T> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) 0) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) = sh \<leadsto> State\<llangle>\<sigma>\<rrangle>"
      using assms trace_conc_pr LAGC_Base.concat.simps(1) by presburger 
    \<comment> \<open>Utilizing a supplementary theorem, we conclude that the trace concretization of 
       \<open>(sh \<leadsto> \<sigma>)\<close> under its minimal concretization mapping must be \<open>(sh \<leadsto> \<sigma>)\<close> itself.\<close>
    ultimately show ?thesis by auto
    \<comment> \<open>We can now use Isabelle in order to infer the conclusion of the lemma.\<close>
  qed

  lemma \<delta>\<^sub>s_While\<^sub>T:
    assumes "consistent {(val\<^sub>B b \<sigma>)} \<and> concrete\<^sub>\<T>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>)"
    shows "\<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[WHILE b DO S OD]) = {(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S;;WHILE b DO S OD])}"
  proof -
    have "consistent(sval\<^sub>B {val\<^sub>B b \<sigma>} (min_conc_map\<^sub>\<T> (\<langle>\<sigma>\<rangle>) 0))"
      using assms consistent_pr by blast
    \<comment> \<open>As we have already assumed that the path condition containing b 
       (i.e. the true-case) is consistent, the consistency of the path condition must
       then be preserved if further simplified under a minimal concretization mapping.\<close>
    moreover have "\<not>consistent(sval\<^sub>B {val\<^sub>B (not b) \<sigma>} (min_conc_map\<^sub>\<T> (\<langle>\<sigma>\<rangle>) 0))"
      using assms by simp
    \<comment> \<open>Hence, the path condition of the continuation trace corresponding to the
       false-case cannot be~consistent.\<close>
    ultimately have "{cont \<in> (val\<^sub>s (WHILE b DO S OD) \<sigma>). consistent(sval\<^sub>B (\<down>\<^sub>p cont) (min_conc_map\<^sub>\<T> (\<down>\<^sub>\<tau> cont) 0))} = { {val\<^sub>B b \<sigma>} \<triangleright> \<langle>\<sigma>\<rangle> \<^item> \<lambda>[S;;WHILE b DO S OD] }"
      using assms by auto
    \<comment> \<open>Thus, the set of consistent continuation traces can only contain the
       continuation trace corresponding to the true-case.\<close>
    moreover have "min_conc_map\<^sub>\<T> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) 0 = \<circle>"
      using assms min_conc_map_of_concrete\<^sub>\<T> by presburger
    \<comment> \<open>Considering that \<open>(sh \<leadsto> \<sigma>)\<close> is concrete, it contains no symbolic variables.
       Using the \<open>min_conc_map_of_concrete\<^sub>\<T>\<close> theorem, we can then infer that the minimal 
       concretization mapping of \<open>(sh \<leadsto> \<sigma>)\<close> matches the empty state.\<close>
    hence "trace_conc (min_conc_map\<^sub>\<T> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) 0) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) = sh \<leadsto> State\<llangle>\<sigma>\<rrangle>"
      using assms trace_conc_pr by presburger
    \<comment> \<open>Utilizing a supplementary theorem, we conclude that the trace concretization of 
       \<open>(sh \<leadsto> \<sigma>)\<close> under its minimal concretization mapping must be \<open>(sh \<leadsto> \<sigma>)\<close> itself.\<close>
    ultimately show ?thesis by auto
    \<comment> \<open>We can now use Isabelle in order to infer the conclusion of the lemma.\<close>
  qed

  lemma \<delta>\<^sub>s_While\<^sub>F:
    assumes "consistent {(val\<^sub>B (not b) \<sigma>)} \<and> concrete\<^sub>\<T>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>)"
    shows "\<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[WHILE b DO S OD]) = {(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[\<nabla>])}"
  proof -
    have "consistent(sval\<^sub>B {val\<^sub>B (not b) \<sigma>} (min_conc_map\<^sub>\<T> (\<langle>\<sigma>\<rangle>) 0))"
      using assms consistent_pr by blast
    \<comment> \<open>As we have already assumed that the path condition containing \<open>not b\<close> 
       (i.e. the false-case) is consistent, the consistency of the path condition must
       then be preserved if further simplified under a minimal concretization mapping.\<close>
    moreover have "\<not>consistent(sval\<^sub>B {val\<^sub>B b \<sigma>} (min_conc_map\<^sub>\<T> (\<langle>\<sigma>\<rangle>) 0))"
      using assms conc_pc_pr\<^sub>B\<^sub>N consistent_def s_value_pr\<^sub>B 
      by (metis bexp.simps(17) singletonI val\<^sub>B.simps(2))
    \<comment> \<open>Hence, the path condition of the true-case is not consistent.\<close>
    ultimately have "{cont \<in> (val\<^sub>s (WHILE b DO S OD) \<sigma>). consistent(sval\<^sub>B (\<down>\<^sub>p cont) (min_conc_map\<^sub>\<T> (\<down>\<^sub>\<tau> cont) 0))} = { {val\<^sub>B (not b) \<sigma>} \<triangleright> \<langle>\<sigma>\<rangle> \<^item> \<lambda>[\<nabla>] }"
      using assms by auto
    \<comment> \<open>Thus, the set of consistent continuation traces can only contain the
       continuation trace corresponding to the false-case.\<close>
    moreover have "min_conc_map\<^sub>\<T> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) 0 = \<circle>"
      using assms min_conc_map_of_concrete\<^sub>\<T> by presburger
    \<comment> \<open>Considering that \<open>(sh \<leadsto> \<sigma>)\<close> is concrete, it contains no symbolic variables.
       Using the \<open>min_conc_map_of_concrete\<^sub>\<T>\<close> theorem, we can then infer that the minimal 
       concretization mapping of \<open>(sh \<leadsto> \<sigma>)\<close> matches the empty state.\<close>
    hence "trace_conc (min_conc_map\<^sub>\<T> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) 0) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) = sh \<leadsto> State\<llangle>\<sigma>\<rrangle>"
      using assms trace_conc_pr by presburger
    \<comment> \<open>Utilizing a supplementary theorem, we conclude that the trace concretization of 
       \<open>(sh \<leadsto> \<sigma>)\<close> under its minimal concretization mapping must be \<open>(sh \<leadsto> \<sigma>)\<close> itself.\<close>
    ultimately show ?thesis by auto
    \<comment> \<open>We can now use Isabelle in order to infer the conclusion of the lemma.\<close>
  qed

text \<open>The simplification lemma for the sequential command is slightly more complex. 
  We establish that the successor configurations of any sequential statement \<open>S\<^sub>1;;S\<^sub>2\<close> 
  match the successor configurations of \<open>S\<^sub>1\<close> with \<open>S\<^sub>2\<close> appended on all their 
  continuation markers. This allows us to simplify $\<open>\<delta>\<close>_s$-function applications on 
  sequential statements to applications on only its first constituent. We infer 
  this equality in Isabelle by proving both subset~relations.\<close>
  lemma \<delta>\<^sub>s_Seq\<^sub>1:
    assumes "concrete\<^sub>\<T>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>)"
    shows "\<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1;;S\<^sub>2]) 
                \<subseteq> (%c. ((fst c), cont_append (snd c) S\<^sub>2)) ` \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1])"
  proof (subst subset_iff)
    show "\<forall>c. c \<in> \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1;;S\<^sub>2]) 
              \<longrightarrow> c \<in> (%c. ((fst c), cont_append (snd c) S\<^sub>2)) ` \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1])"
    \<comment> \<open>We first use the subset-iff rule in order to rewrite the subset relation into 
       a semantically equivalent~implication.\<close>
    proof (rule allI, rule impI)
      fix c
      \<comment> \<open>We assume \<open>c\<close> to be an arbitrary, but fixed configuration.\<close>
      assume "c \<in> \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1;;S\<^sub>2])"
      \<comment> \<open>We assume that the premise holds, implying that \<open>c\<close> is a successor 
         configuration~of~\<open>S\<^sub>1;;S\<^sub>2\<close>.\<close>
      then obtain \<pi> where assm\<^sub>\<pi>: 
        "c = (trace_conc (min_conc_map\<^sub>\<T> (sh \<cdot> (\<down>\<^sub>\<tau> \<pi>)) 0) (sh \<cdot> (\<down>\<^sub>\<tau> \<pi>)), \<down>\<^sub>\<lambda> \<pi>) 
         \<and> \<pi> \<in> val\<^sub>s (S\<^sub>1;;S\<^sub>2) \<sigma> \<and> consistent(sval\<^sub>B (\<down>\<^sub>p \<pi>) (min_conc_map\<^sub>\<T> (\<down>\<^sub>\<tau> \<pi>) 0))" by auto
      \<comment> \<open>Due to the definition of the $\<open>\<delta>\<close>_s$-function, there must exist a continuation 
         trace \<open>\<pi>\<close> with a consistent path condition generated from \<open>S\<^sub>1;;S\<^sub>2\<close> that can 
         be translated into \<open>c\<close>. We obtain this continuation trace~\<open>\<pi>\<close>.\<close> 
      moreover then obtain \<pi>' where assm\<^sub>\<pi>': 
        "\<pi> = (\<down>\<^sub>p \<pi>') \<triangleright> (\<down>\<^sub>\<tau> \<pi>') \<^item> cont_append (\<down>\<^sub>\<lambda> \<pi>') S\<^sub>2 \<and> \<pi>' \<in> (val\<^sub>s S\<^sub>1 \<sigma>)" by auto
      \<comment> \<open>Aligning with the definition of the valuation function, there must also
         exist a continuation trace \<open>\<pi>'\<close> generated from \<open>S\<^sub>1\<close>, which matches
         \<open>\<pi>\<close>, if we appended \<open>S\<^sub>2\<close> onto its continuation~marker.\<close>
      ultimately have connect: 
        "consistent (sval\<^sub>B (\<down>\<^sub>p \<pi>') (min_conc_map\<^sub>\<T> (\<down>\<^sub>\<tau> \<pi>') 0)) 
         \<and> (\<down>\<^sub>\<tau> \<pi>') = (\<down>\<^sub>\<tau> \<pi>) \<and> (\<down>\<^sub>\<lambda> \<pi>) = cont_append (\<down>\<^sub>\<lambda> \<pi>') S\<^sub>2" by simp
      \<comment> \<open>Considering that the path conditions of \<open>\<pi>\<close> and \<open>\<pi>'\<close> match, both must be 
         consistent. Their symbolic traces also match. The only difference lies
         in the modified continuation~marker.\<close>
      moreover then obtain c' where 
        "c' = (trace_conc (min_conc_map\<^sub>\<T> (sh \<cdot> (\<down>\<^sub>\<tau> \<pi>')) 0) (sh \<cdot> (\<down>\<^sub>\<tau> \<pi>')), \<down>\<^sub>\<lambda> \<pi>')" by auto
      \<comment> \<open>We can then obtain the configuration \<open>c'\<close> translated from the 
         continuation trace~\<open>\<pi>'\<close>.\<close>
      moreover then have "fst(c) = fst(c') \<and> snd(c) = cont_append (snd c') S\<^sub>2"
        by (simp add: assm\<^sub>\<pi> connect)
      \<comment> \<open>Given this information, both \<open>c\<close> and \<open>c'\<close> must match in their symbolic 
         traces. However, \<open>c\<close> additionally appends \<open>S\<^sub>2\<close> onto its continuation~marker.\<close>
      ultimately show "c \<in> (%c. ((fst c), cont_append (snd c) S\<^sub>2)) ` \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1])"
        using assm\<^sub>\<pi>' assm\<^sub>\<pi> image_iff by fastforce
      \<comment> \<open>Thus \<open>c\<close> must match \<open>c'\<close> with an appended \<open>S\<^sub>2\<close> in its continuation marker,
         closing the proof.\<close>
    qed
  qed

  lemma \<delta>\<^sub>s_Seq\<^sub>2:
    assumes "concrete\<^sub>\<T>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>)"
    shows "(%c. ((fst c), cont_append (snd c) S\<^sub>2)) ` \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1]) 
                \<subseteq> \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1;;S\<^sub>2])"
  proof (subst subset_iff)
    show "\<forall>c. c \<in> (%c. ((fst c), cont_append (snd c) S\<^sub>2)) ` \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1]) 
              \<longrightarrow> c \<in> \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1;;S\<^sub>2])"
    \<comment> \<open>We first use the subset-iff rule in order to rewrite the subset relation into 
       a semantically equivalent~implication.\<close> 
    proof (rule allI, rule impI)
      fix c
      \<comment> \<open>We assume \<open>c\<close> to be an arbitrary, but fixed configuration.\<close>
      assume "c \<in> (%c. ((fst c), cont_append (snd c) S\<^sub>2)) ` \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1])"
      \<comment> \<open>We assume that the premise holds, implying that \<open>c\<close> is a successor 
         configuration of \<open>S\<^sub>1\<close> with \<open>S\<^sub>2\<close> appended onto its continuation~marker.\<close>
      then obtain c' where assm\<^sub>c':
        "c' \<in> \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1]) \<and> fst(c') = fst(c) \<and> cont_append (snd c') S\<^sub>2 = snd(c)" by force
      \<comment> \<open>Then there must exist a configuration \<open>c'\<close> that is also a successor 
         configuration of \<open>S\<^sub>1\<close>, matching with \<open>c\<close> in everything except its
         continuation marker. \<open>c'\<close> with \<open>S\<^sub>2\<close> appended on its continuation marker
         matches~configuration~\<open>c\<close>.\<close>
      moreover then obtain \<pi> where assm\<^sub>\<pi>:
        "\<pi> \<in> val\<^sub>s S\<^sub>1 \<sigma> \<and> consistent(sval\<^sub>B (\<down>\<^sub>p \<pi>) (min_conc_map\<^sub>\<T> (\<down>\<^sub>\<tau> \<pi>) 0)) 
         \<and> c' = (trace_conc (min_conc_map\<^sub>\<T> (sh \<cdot> (\<down>\<^sub>\<tau> \<pi>)) 0) (sh \<cdot> (\<down>\<^sub>\<tau> \<pi>)), (\<down>\<^sub>\<lambda> \<pi>))" by auto
      \<comment> \<open>Hence there must exist a continuation trace \<open>\<pi>\<close> with a consistent path 
         condition generated from \<open>S\<^sub>1\<close>, which translates to configuration~\<open>c'\<close>.\<close>
      moreover then obtain \<pi>' where
        "\<pi>' = (\<down>\<^sub>p \<pi>) \<triangleright> (\<down>\<^sub>\<tau> \<pi>) \<^item> cont_append (\<down>\<^sub>\<lambda> \<pi>) S\<^sub>2" by auto
      \<comment> \<open>We then obtain the continuation trace \<open>\<pi>'\<close> which matches \<open>\<pi>\<close> except
         having \<open>S\<^sub>2\<close> additionally appended onto its continuation~marker.\<close>
      ultimately have connect: 
        "\<pi>' \<in> val\<^sub>s (S\<^sub>1;;S\<^sub>2) \<sigma> \<and> consistent(sval\<^sub>B (\<down>\<^sub>p \<pi>') (min_conc_map\<^sub>\<T> (\<down>\<^sub>\<tau> \<pi>') 0)) 
         \<and> (\<down>\<^sub>\<tau> \<pi>') = (\<down>\<^sub>\<tau> \<pi>) \<and> (\<down>\<^sub>\<lambda> \<pi>') = cont_append (\<down>\<^sub>\<lambda> \<pi>) S\<^sub>2" by auto 
      \<comment> \<open>This implies that \<open>\<pi>'\<close> must be a continuation trace with a consistent
         path condition generated from \<open>S\<^sub>1;;S\<^sub>2\<close>. Note that \<open>\<pi>\<close> matches with \<open>\<pi>'\<close>
         in its symbolic trace, but not in its continuation marker.\<close>
      then obtain c'' where assm\<^sub>c'':
        "c'' = (trace_conc (min_conc_map\<^sub>\<T> (sh \<cdot> (\<down>\<^sub>\<tau> \<pi>')) 0) (sh \<cdot> (\<down>\<^sub>\<tau> \<pi>')), \<down>\<^sub>\<lambda> \<pi>')" by auto
      \<comment> \<open>We then obtain the configuration \<open>c''\<close> translated from \<open>\<pi>'\<close>.\<close>
      hence "c = c''" using assm\<^sub>c' assm\<^sub>\<pi> connect by auto
      \<comment> \<open>We can now derive that \<open>c\<close> and \<open>c''\<close> match in both of~their~elements.\<close>
      thus "c \<in> \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1;;S\<^sub>2])" 
        using assm\<^sub>c'' connect image_iff by fastforce
      \<comment> \<open>We know that \<open>c''\<close> is a successor configuration of \<open>S\<^sub>1;;S\<^sub>2\<close>. 
         Considering that \<open>c\<close> matches \<open>c''\<close>, we can finally conclude that 
         \<open>c\<close> must also be a successor configuration of \<open>S\<^sub>1;;S\<^sub>2\<close>, which needed to be 
         proven in the first~place.\<close>
    qed
  qed
        
  lemma \<delta>\<^sub>s_Seq:
    assumes "concrete\<^sub>\<T>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>)"
    shows "\<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1;;S\<^sub>2]) 
                = (%c. ((fst c), cont_append (snd c) S\<^sub>2)) ` \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1])"
    apply (subst set_eq_subset)
    using assms \<delta>\<^sub>s_Seq\<^sub>1 \<delta>\<^sub>s_Seq\<^sub>2 by auto
    \<comment> \<open>We can now use the proof of both subset directions to infer the 
       desired~equality.\<close>

text \<open>The simplification lemma for the local parallelism command can be formalized
  in a similar manner. We establish that the successor configurations of any
  local parallelism command \<open>CO S\<^sub>1 \<parallel> S\<^sub>2 OC\<close> match the successor configurations of 
  \<open>S\<^sub>1\<close> with the local parallelism under \<open>S\<^sub>2\<close> reconstructed in all their continuation
  markers, merged with the successor configurations of \<open>S\<^sub>2\<close> with the local parallelism 
  under \<open>S\<^sub>1\<close> reconstructed in all their continuation markers. This allows us to 
  simplify $\<open>\<delta>\<close>_s$-function applications on local parallelism commands to applications 
  on both of its constituents. We again infer this equality in Isabelle by proving 
  both corresponding subset~relations.\<close>
  lemma \<delta>\<^sub>s_LocPar\<^sub>1:
    assumes "concrete\<^sub>\<T>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>)"
    shows "\<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[CO S\<^sub>1 \<parallel> S\<^sub>2 OC]) 
                \<subseteq> (%c. (fst(c), parallel (snd c) \<lambda>[S\<^sub>2])) ` \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1]) \<union>
                    (%c. (fst(c), parallel \<lambda>[S\<^sub>1] (snd c))) ` \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>2])"
  proof (subst subset_iff)
    show "\<forall>c. c \<in> \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[CO S\<^sub>1 \<parallel> S\<^sub>2 OC]) 
              \<longrightarrow> c \<in> (%c. (fst(c), parallel (snd c) \<lambda>[S\<^sub>2])) ` \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1]) \<union> 
                           (%c. (fst(c), parallel \<lambda>[S\<^sub>1] (snd c))) ` \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>2])"
    \<comment> \<open>We first use the subset-iff rule in order to rewrite the subset relation into 
       a semantically equivalent~implication.\<close>  
    proof (rule allI, rule impI)
      fix c
      \<comment> \<open>We assume \<open>c\<close> to be an arbitrary, but fixed configuration.\<close>
      assume "c \<in> \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[CO S\<^sub>1 \<parallel> S\<^sub>2 OC])"
      \<comment> \<open>We assume that the premise holds, implying that \<open>c\<close> is a successor 
         configuration of the local parallelism command \<open>CO S\<^sub>1 \<parallel> S\<^sub>2 OC\<close>.\<close>
      then obtain \<pi> where assm\<^sub>\<pi>: 
        "c = (trace_conc (min_conc_map\<^sub>\<T> (sh \<cdot> (\<down>\<^sub>\<tau> \<pi>)) 0) (sh \<cdot> (\<down>\<^sub>\<tau> \<pi>)), \<down>\<^sub>\<lambda> \<pi>) 
         \<and> \<pi> \<in> val\<^sub>s (CO S\<^sub>1 \<parallel> S\<^sub>2 OC) \<sigma> \<and> consistent(sval\<^sub>B (\<down>\<^sub>p \<pi>) (min_conc_map\<^sub>\<T> (\<down>\<^sub>\<tau> \<pi>) 0))" by auto
      \<comment> \<open>Due to the definition of the $\<open>\<delta>\<close>_s$-function, there must exist a continuation 
         trace \<open>\<pi>\<close> with a consistent path condition generated from \<open>CO S\<^sub>1 \<parallel> S\<^sub>2 OC\<close> 
         that can be translated into \<open>c\<close>. We obtain this continuation trace~\<open>\<pi>\<close>.\<close> 
      moreover then have "(\<exists>\<pi>'. \<pi> = (\<down>\<^sub>p \<pi>') \<triangleright> (\<down>\<^sub>\<tau> \<pi>') \<^item> parallel (\<down>\<^sub>\<lambda> \<pi>') \<lambda>[S\<^sub>2] \<and> \<pi>' \<in> (val\<^sub>s S\<^sub>1 \<sigma>)) 
         \<or> (\<exists>\<pi>'. \<pi> = (\<down>\<^sub>p \<pi>') \<triangleright> (\<down>\<^sub>\<tau> \<pi>') \<^item> parallel \<lambda>[S\<^sub>1] (\<down>\<^sub>\<lambda> \<pi>') \<and> \<pi>' \<in> (val\<^sub>s S\<^sub>2 \<sigma>))" by auto
      \<comment> \<open>Aligning with the definition of the valuation function, there must exist
         a continuation trace \<open>\<pi>'\<close>, which satisifies one of the following cases: In 
         the first case, \<open>\<pi>'\<close> is generated from \<open>S\<^sub>1\<close>, and matches \<open>\<pi>\<close> when
         reconstructing the local parallelism construct with \<open>S\<^sub>2\<close> in its 
         continuation marker. In the second case, \<open>\<pi>'\<close> is generated from \<open>S\<^sub>2\<close> and 
         matches \<open>\<pi>\<close> when reconstructing the local parallelism construct with \<open>S\<^sub>1\<close> 
         in its continuation~marker.\<close>
      thus "c \<in> ((%c. (fst(c), parallel (snd c) \<lambda>[S\<^sub>2])) ` \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1])) \<union> ((%c. (fst(c), parallel \<lambda>[S\<^sub>1] (snd c))) ` \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>2]))"
      proof (rule disjE)
      \<comment> \<open>We perform a case distinction over those two cases.\<close>
        assume "\<exists>\<pi>'. \<pi> = (\<down>\<^sub>p \<pi>') \<triangleright> (\<down>\<^sub>\<tau> \<pi>') \<^item> parallel (\<down>\<^sub>\<lambda> \<pi>') \<lambda>[S\<^sub>2] \<and> \<pi>' \<in> (val\<^sub>s S\<^sub>1 \<sigma>)"
        \<comment> \<open>Let us assume that the first case holds.\<close>
        moreover then obtain \<pi>' where assm\<^sub>\<pi>':
          "\<pi> = (\<down>\<^sub>p \<pi>') \<triangleright> (\<down>\<^sub>\<tau> \<pi>') \<^item> parallel (\<down>\<^sub>\<lambda> \<pi>') \<lambda>[S\<^sub>2] \<and> \<pi>' \<in> (val\<^sub>s S\<^sub>1 \<sigma>)" by auto
        \<comment> \<open>We then obtain this continuation trace \<open>\<pi>'\<close>.\<close>
        ultimately have connect: 
          "consistent (sval\<^sub>B (\<down>\<^sub>p \<pi>') (min_conc_map\<^sub>\<T> (\<down>\<^sub>\<tau> \<pi>') 0)) 
          \<and> (\<down>\<^sub>\<tau> \<pi>') = (\<down>\<^sub>\<tau> \<pi>) \<and> (\<down>\<^sub>\<lambda> \<pi>) = parallel (\<down>\<^sub>\<lambda> \<pi>') \<lambda>[S\<^sub>2]" 
          using assm\<^sub>\<pi> by auto
        \<comment> \<open>Considering that the path conditions of \<open>\<pi>\<close> and \<open>\<pi>'\<close> match, both must be 
           consistent. Their symbolic traces also match. The only difference lies
           in the modified continuation~marker.\<close>
        moreover then obtain c' where
          "c' = (trace_conc (min_conc_map\<^sub>\<T> (sh \<cdot> (\<down>\<^sub>\<tau> \<pi>')) 0) (sh \<cdot> (\<down>\<^sub>\<tau> \<pi>')), \<down>\<^sub>\<lambda> \<pi>')" by auto
        \<comment> \<open>We can then obtain the configuration \<open>c'\<close> translated from the 
           continuation trace~\<open>\<pi>'\<close>.\<close>
        moreover then have "fst(c) = fst(c') \<and> snd(c) = parallel (snd c') \<lambda>[S\<^sub>2]"
          by (simp add: assm\<^sub>\<pi> connect)
        \<comment> \<open>Given this information, both \<open>c\<close> and \<open>c'\<close> must match in their symbolic 
           traces. However, \<open>c\<close> additionally reconstructs the local parallelism
           with \<open>S\<^sub>2\<close> in its continuation~marker.\<close>
        ultimately have "c \<in> (%c. (fst(c), parallel (snd c) \<lambda>[S\<^sub>2])) ` \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1])"
          using assm\<^sub>\<pi>' assm\<^sub>\<pi> image_iff by fastforce
        \<comment> \<open>Thus configuration \<open>c'\<close> with the local parallelism under \<open>S\<^sub>2\<close> reconstructed
           in its continuation marker must match with \<open>c\<close>.\<close>
        thus "c \<in> (%c. (fst(c), parallel (snd c) \<lambda>[S\<^sub>2])) ` \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1]) \<union> 
                     (%c. (fst(c), parallel \<lambda>[S\<^sub>1] (snd c))) ` \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>2])" by simp
        \<comment> \<open>Hence it must also be in the merged set, thus closing this case.\<close>
      next
        assume "\<exists>\<pi>'. \<pi> = (\<down>\<^sub>p \<pi>') \<triangleright> (\<down>\<^sub>\<tau> \<pi>') \<^item> parallel \<lambda>[S\<^sub>1] (\<down>\<^sub>\<lambda> \<pi>') \<and> \<pi>' \<in> (val\<^sub>s S\<^sub>2 \<sigma>)"
        \<comment> \<open>Let us assume that the second case holds.\<close>
        moreover then obtain \<pi>' where assm\<^sub>\<pi>':
          "\<pi> = (\<down>\<^sub>p \<pi>') \<triangleright> (\<down>\<^sub>\<tau> \<pi>') \<^item> parallel \<lambda>[S\<^sub>1] (\<down>\<^sub>\<lambda> \<pi>') \<and> \<pi>' \<in> (val\<^sub>s S\<^sub>2 \<sigma>)" by auto
        \<comment> \<open>We then obtain this continuation trace \<open>\<pi>'\<close>.\<close>
        ultimately have connect:
          "consistent (sval\<^sub>B (\<down>\<^sub>p \<pi>') (min_conc_map\<^sub>\<T> (\<down>\<^sub>\<tau> \<pi>') 0)) 
          \<and> (\<down>\<^sub>\<tau> \<pi>') = (\<down>\<^sub>\<tau> \<pi>) \<and> (\<down>\<^sub>\<lambda> \<pi>) = parallel \<lambda>[S\<^sub>1] (\<down>\<^sub>\<lambda> \<pi>')" 
          using assm\<^sub>\<pi> by auto
        \<comment> \<open>Considering that the path conditions of \<open>\<pi>\<close> and \<open>\<pi>'\<close> match, both must be 
           consistent. Their symbolic traces also match. The only difference lies
           in the modified continuation~marker.\<close>
        moreover then obtain c' where
          "c' = (trace_conc (min_conc_map\<^sub>\<T> (sh \<cdot> (\<down>\<^sub>\<tau> \<pi>')) 0) (sh \<cdot> (\<down>\<^sub>\<tau> \<pi>')), \<down>\<^sub>\<lambda> \<pi>')" by auto
        \<comment> \<open>We can then obtain the configuration \<open>c'\<close> translated from the 
           continuation trace~\<open>\<pi>'\<close>.\<close>
        moreover then have "fst(c) = fst(c') \<and> snd(c) = parallel \<lambda>[S\<^sub>1] (snd c')"
          by (simp add: assm\<^sub>\<pi> connect)
        \<comment> \<open>Given this information, both \<open>c\<close> and \<open>c'\<close> must match in their symbolic 
           traces. However, \<open>c\<close> additionally reconstructs the local parallelism
           with \<open>S\<^sub>1\<close> in its continuation~marker.\<close>
        ultimately have "c \<in> (%c. (fst(c), parallel \<lambda>[S\<^sub>1] (snd c))) ` \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>2])"
          using assm\<^sub>\<pi>' assm\<^sub>\<pi> image_iff by fastforce
        \<comment> \<open>Thus configuration \<open>c'\<close> with the local parallelism under \<open>S\<^sub>1\<close> reconstructed
           in its continuation marker must match with \<open>c\<close>.\<close>
        thus "c \<in> (%c. (fst(c), parallel (snd c) \<lambda>[S\<^sub>2])) ` \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1]) \<union> 
                      (%c. (fst(c), parallel \<lambda>[S\<^sub>1] (snd c))) ` \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>2])" by simp
        \<comment> \<open>Hence it must also be in the merged set, thus closing the second case.\<close>
      qed
    qed
  qed

  lemma \<delta>\<^sub>s_LocPar\<^sub>2:
    assumes "concrete\<^sub>\<T>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>)"
    shows "(%c. (fst(c), parallel (snd c) \<lambda>[S\<^sub>2])) ` \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1]) \<union> 
               (%c. (fst(c), parallel \<lambda>[S\<^sub>1] (snd c))) ` \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>2]) 
                \<subseteq> \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[CO S\<^sub>1 \<parallel> S\<^sub>2 OC])"
  proof (subst subset_iff)
    show "\<forall>c. c \<in> (%c. (fst(c), parallel (snd c) \<lambda>[S\<^sub>2])) ` \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1]) \<union> 
                          (%c. (fst(c), parallel \<lambda>[S\<^sub>1] (snd c))) ` \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>2]) 
              \<longrightarrow> c \<in> \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[CO S\<^sub>1 \<parallel> S\<^sub>2 OC])"
    \<comment> \<open>We first use the subset-iff rule in order to rewrite the subset relation into 
       a semantically equivalent~implication.\<close>  
    proof (rule allI, rule impI)
      fix c
      \<comment> \<open>We assume \<open>c\<close> to be an arbitrary, but fixed configuration.\<close>
      assume "c \<in> (%c. (fst(c), parallel (snd c) \<lambda>[S\<^sub>2])) ` \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1]) \<union> 
                          (%c. (fst(c), parallel \<lambda>[S\<^sub>1] (snd c))) ` \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>2])"
      \<comment> \<open>We assume that the premise holds, implying that \<open>c\<close> is either a successor 
         configuration of \<open>S\<^sub>1\<close> with the local parallelism under \<open>S\<^sub>2\<close> reconstructed in 
         its continuation marker, or a successor configuration of \<open>S\<^sub>2\<close> with the local 
         parallelism under \<open>S\<^sub>1\<close> reconstructed in its continuation marker.\<close>
      then obtain c' where assm\<^sub>c':
        "(c' \<in> \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1]) \<and> fst(c') = fst(c) \<and> parallel (snd c') \<lambda>[S\<^sub>2] = snd(c)) \<or> 
        (c' \<in> \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>2]) \<and> fst(c') = fst(c) \<and> parallel \<lambda>[S\<^sub>1] (snd c') = snd(c))" 
        by fastforce
      \<comment> \<open>Then there must exist a configuration \<open>c'\<close> that satisfies one of the two
         cases: In the first case, \<open>c'\<close> is a successor configuration of \<open>S\<^sub>1\<close>, whilst
         \<open>c\<close> matches \<open>c'\<close> with the local parallelism under \<open>S\<^sub>2\<close> reconstructed in its 
         continuation marker. In the second case, \<open>c'\<close> is a successor configuration 
         of \<open>S\<^sub>2\<close>, whilst \<open>c\<close> matches \<open>c'\<close> with the local parallelism under \<open>S\<^sub>1\<close> 
         reconstructed in its continuation~marker. \<close>
      thus "c \<in> \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[CO S\<^sub>1 \<parallel> S\<^sub>2 OC])"
      proof (cases)
      \<comment> \<open>We perform a case distinction over those two cases.\<close>
        assume p: "c' \<in> \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1]) \<and> fst(c') = fst(c) \<and> parallel (snd c') \<lambda>[S\<^sub>2] = snd(c)"
        \<comment> \<open>Let us assume that the first case holds.\<close>
        moreover then obtain \<pi> where assm\<^sub>\<pi>:
          "\<pi> \<in> val\<^sub>s S\<^sub>1 \<sigma> \<and> consistent(sval\<^sub>B (\<down>\<^sub>p \<pi>) (min_conc_map\<^sub>\<T> (\<down>\<^sub>\<tau> \<pi>) 0)) 
          \<and> c' = (trace_conc (min_conc_map\<^sub>\<T> (sh \<cdot> (\<down>\<^sub>\<tau> \<pi>)) 0) (sh \<cdot> (\<down>\<^sub>\<tau> \<pi>)), (\<down>\<^sub>\<lambda> \<pi>))" by auto
        \<comment> \<open>Hence there must exist a continuation trace \<open>\<pi>\<close> with a consistent path 
           condition generated from \<open>S\<^sub>1\<close>, which translates to configuration~\<open>c'\<close>.\<close>
        moreover then obtain \<pi>' where
          "\<pi>' = (\<down>\<^sub>p \<pi>) \<triangleright> (\<down>\<^sub>\<tau> \<pi>) \<^item> parallel (\<down>\<^sub>\<lambda> \<pi>) \<lambda>[S\<^sub>2]" by auto
        \<comment> \<open>We then obtain the continuation trace \<open>\<pi>'\<close> which matches with \<open>\<pi>\<close> except
           having the local parallelism under \<open>S\<^sub>2\<close> reconstructed in its 
           continuation~marker.\<close>
        ultimately have connect: 
          "\<pi>' \<in> val\<^sub>s (CO S\<^sub>1 \<parallel> S\<^sub>2 OC) \<sigma> \<and> consistent(sval\<^sub>B (\<down>\<^sub>p \<pi>') (min_conc_map\<^sub>\<T> (\<down>\<^sub>\<tau> \<pi>') 0)) 
          \<and> (\<down>\<^sub>\<tau> \<pi>') = (\<down>\<^sub>\<tau> \<pi>) \<and> (\<down>\<^sub>\<lambda> \<pi>') = parallel (\<down>\<^sub>\<lambda> \<pi>) \<lambda>[S\<^sub>2]" by auto 
        \<comment> \<open>This implies that \<open>\<pi>'\<close> must be a continuation trace with a consistent
           path condition generated from \<open>CO S\<^sub>1 \<parallel> S\<^sub>2 OC\<close>. Note that \<open>\<pi>\<close> matches with 
           \<open>\<pi>'\<close> in its symbolic trace, but not in its continuation~marker.\<close>
        then obtain c'' where assm\<^sub>c'':
          "c'' = (trace_conc (min_conc_map\<^sub>\<T> (sh \<cdot> (\<down>\<^sub>\<tau> \<pi>')) 0) (sh \<cdot> (\<down>\<^sub>\<tau> \<pi>')), \<down>\<^sub>\<lambda> \<pi>')" by auto
        \<comment> \<open>We then obtain the configuration \<open>c''\<close> translated from \<open>\<pi>'\<close>.\<close>
        hence "c = c''" by (metis p assm\<^sub>c' assm\<^sub>\<pi> connect fst_eqD prod.collapse snd_eqD)
        \<comment> \<open>We can now derive that \<open>c\<close> and \<open>c''\<close> match in both of~their~elements.\<close>
        moreover have "c'' \<in> (%c. (trace_conc (min_conc_map\<^sub>\<T> (sh \<cdot> (\<down>\<^sub>\<tau> c)) 0) (sh \<cdot> (\<down>\<^sub>\<tau> c)), \<down>\<^sub>\<lambda> c)) 
          ` {c \<in> val\<^sub>s (CO S\<^sub>1 \<parallel> S\<^sub>2 OC) \<sigma>. consistent(sval\<^sub>B (\<down>\<^sub>p c) (min_conc_map\<^sub>\<T> (\<down>\<^sub>\<tau> c) 0))}"
          using assm\<^sub>c'' connect by blast
        \<comment> \<open>Using the setup of \<open>c''\<close>, we can now infer that \<open>c''\<close> must be a successor 
           configuration of the local parallelism statement \<open>CO S\<^sub>1 \<parallel> S\<^sub>2 OC\<close>.\<close>
        ultimately show "c \<in> \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[CO S\<^sub>1 \<parallel> S\<^sub>2 OC])"
          by auto
        \<comment> \<open>Taking into consideration that \<open>c\<close> matches with \<open>c''\<close>, we can finally 
           conclude that \<open>c\<close> must also be a successor configuration of the local
           parallelism statement \<open>CO S\<^sub>1 \<parallel> S\<^sub>2 OC\<close>, which needed to be proven in the 
           first~place.\<close>
      next
        assume "\<not>(c' \<in> \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1]) \<and> fst(c') = fst(c) \<and> parallel (snd c') \<lambda>[S\<^sub>2] = snd(c))"
        \<comment> \<open>Let us assume the first case does not hold.\<close>
        hence p: "c' \<in> \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>2]) \<and> fst(c') = fst(c) \<and> parallel \<lambda>[S\<^sub>1] (snd c') = snd(c)"
          using assm\<^sub>c' by auto
        \<comment> \<open>Then the second case must hold.\<close>
        moreover then obtain \<pi> where assm\<^sub>\<pi>:
          "\<pi> \<in> val\<^sub>s S\<^sub>2 \<sigma> \<and> consistent(sval\<^sub>B (\<down>\<^sub>p \<pi>) (min_conc_map\<^sub>\<T> (\<down>\<^sub>\<tau> \<pi>) 0)) 
          \<and> c' = (trace_conc (min_conc_map\<^sub>\<T> (sh \<cdot> (\<down>\<^sub>\<tau> \<pi>)) 0) (sh \<cdot> (\<down>\<^sub>\<tau> \<pi>)), (\<down>\<^sub>\<lambda> \<pi>))" by auto
        \<comment> \<open>Hence there must exist a continuation trace \<open>\<pi>\<close> with a consistent path 
           condition generated from \<open>S\<^sub>2\<close>, which translates to configuration~\<open>c'\<close>.\<close>
        moreover then obtain \<pi>' where
          "\<pi>' = (\<down>\<^sub>p \<pi>) \<triangleright> (\<down>\<^sub>\<tau> \<pi>) \<^item> parallel \<lambda>[S\<^sub>1] (\<down>\<^sub>\<lambda> \<pi>)" by auto
        \<comment> \<open>We then obtain the continuation trace \<open>\<pi>'\<close> which matches with \<open>\<pi>\<close> except
           having the local parallelism under \<open>S\<^sub>1\<close> reconstructed in its 
           continuation~marker.\<close>
        ultimately have connect:
          "\<pi>' \<in> val\<^sub>s (CO S\<^sub>1 \<parallel> S\<^sub>2 OC) \<sigma> \<and> consistent(sval\<^sub>B (\<down>\<^sub>p \<pi>') (min_conc_map\<^sub>\<T> (\<down>\<^sub>\<tau> \<pi>') 0)) 
          \<and> (\<down>\<^sub>\<tau> \<pi>') = (\<down>\<^sub>\<tau> \<pi>) \<and> (\<down>\<^sub>\<lambda> \<pi>') = parallel \<lambda>[S\<^sub>1] (\<down>\<^sub>\<lambda> \<pi>)" by auto 
        \<comment> \<open>This implies that \<open>\<pi>'\<close> must be a continuation trace with a consistent
           path condition generated from \<open>CO S\<^sub>1 \<parallel> S\<^sub>2 OC\<close>. Note that \<open>\<pi>\<close> matches with 
           \<open>\<pi>'\<close> in its symbolic trace, but not in its continuation~marker.\<close>
        then obtain c'' where assm\<^sub>c'':
          "c'' = (trace_conc (min_conc_map\<^sub>\<T> (sh \<cdot> (\<down>\<^sub>\<tau> \<pi>')) 0) (sh \<cdot> (\<down>\<^sub>\<tau> \<pi>')), \<down>\<^sub>\<lambda> \<pi>')" by auto
        \<comment> \<open>We then obtain the configuration \<open>c''\<close> translated from \<open>\<pi>'\<close>.\<close>
        hence "c = c''" by (metis p assm\<^sub>c' assm\<^sub>\<pi> connect fst_eqD prod.collapse snd_eqD)
        \<comment> \<open>We can now derive that \<open>c\<close> and \<open>c''\<close> match in both of~their~elements.\<close>
        moreover have "c'' \<in> (%c. (trace_conc (min_conc_map\<^sub>\<T> (sh \<cdot> (\<down>\<^sub>\<tau> c)) 0) (sh \<cdot> (\<down>\<^sub>\<tau> c)), \<down>\<^sub>\<lambda> c)) 
          ` {c \<in> val\<^sub>s (CO S\<^sub>1 \<parallel> S\<^sub>2 OC) \<sigma>. consistent(sval\<^sub>B (\<down>\<^sub>p c) (min_conc_map\<^sub>\<T> (\<down>\<^sub>\<tau> c) 0))}"
          using assm\<^sub>c'' connect by blast
        \<comment> \<open>Using the setup of \<open>c''\<close>, we can now infer that \<open>c''\<close> must be a successor 
           configuration of the local parallelism command \<open>CO S\<^sub>1 \<parallel> S\<^sub>2 OC\<close>.\<close>
        ultimately show "c \<in> \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[CO S\<^sub>1 \<parallel> S\<^sub>2 OC])"
          by auto
        \<comment> \<open>Taking into consideration that \<open>c\<close> matches with \<open>c''\<close>, we can finally 
           conclude that \<open>c\<close> must also be a successor configuration of the local
           parallelism statement \<open>CO S\<^sub>1 \<parallel> S\<^sub>2 OC\<close>, which needed to be proven in the 
           first~place.\<close>
      qed
    qed
  qed

  lemma \<delta>\<^sub>s_LocPar:
    assumes "concrete\<^sub>\<T>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>)"
    shows "\<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[CO S\<^sub>1 \<parallel> S\<^sub>2 OC]) = ((%c. (fst(c), parallel (snd c) \<lambda>[S\<^sub>2])) ` \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>1])) \<union> ((%c. (fst(c), parallel \<lambda>[S\<^sub>1] (snd c))) ` \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S\<^sub>2]))"
    apply (subst set_eq_subset)
    using assms \<delta>\<^sub>s_LocPar\<^sub>1 \<delta>\<^sub>s_LocPar\<^sub>2 by auto
    \<comment> \<open>We can now use the proof of both subset directions to infer the 
       desired~equality.\<close>

text \<open>We next provide the simplification lemmas for the local memory command
  of $WL_{EXT}$. Similar to the valuation function, we choose to provide two distinct 
  rules in order to manage the scope evaluation. The first rule handles the case of 
  having no variable declarations in the local memory command, whilst the second rule 
  simplifies the evaluation of the command with variable~declarations.\<close>
  lemma \<delta>\<^sub>s_Scope\<^sub>A:
    assumes "concrete\<^sub>\<T>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>)" 
    shows "\<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[\<lbrace> \<nu> S \<rbrace>]) = \<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S])"
  proof -
    have "min_conc_map\<^sub>\<T> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) 0 = \<circle>"
      using assms min_conc_map_of_concrete\<^sub>\<T> by presburger
    \<comment> \<open>Considering that \<open>(sh \<leadsto> \<sigma>)\<close> is concrete, it contains no symbolic variables.
       Using the \<open>min_conc_map_of_concrete\<^sub>\<T>\<close> theorem, we can then infer that the minimal 
       concretization mapping of \<open>(sh \<leadsto> \<sigma>)\<close> matches the empty state.\<close>
    hence "trace_conc (min_conc_map\<^sub>\<T> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) 0) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) = sh \<leadsto> State\<llangle>\<sigma>\<rrangle>"
      using assms trace_conc_pr by fastforce
    \<comment> \<open>Utilizing a supplementary theorem, we conclude that the trace concretization of 
       \<open>(sh \<leadsto> \<sigma>)\<close> under its minimal concretization mapping must be \<open>(sh \<leadsto> \<sigma>)\<close> itself.\<close>
    thus ?thesis by auto
    \<comment> \<open>We can now use Isabelle in order to infer the conclusion of this lemma.\<close>
  qed

  lemma \<delta>\<^sub>s_Scope\<^sub>B:
    assumes "concrete\<^sub>\<T>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>)" 
    shows "\<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[\<lbrace> (x;d) S \<rbrace>]) = {((sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<leadsto> State\<llangle>[(vargen \<sigma> 0 100 (''$'' @ x @ ''::Scope'')) \<longmapsto> Exp (Num 0)] \<sigma>\<rrangle>, \<lambda>[\<lbrace> d S \<rbrace> [x \<leftarrow>\<^sub>s (vargen \<sigma> 0 100 (''$'' @ x @ ''::Scope''))] ])}"
  proof -
    have "concrete\<^sub>\<T> ((sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<leadsto> State\<llangle>[(vargen \<sigma> 0 100 (''$'' @ x @ ''::Scope'')) \<longmapsto> Exp (Num 0)] \<sigma>\<rrangle>)"
      using assms by simp
    \<comment> \<open>Due to the concreteness of \<open>(sh \<leadsto> \<sigma>)\<close>, and the fact that 0 is a concrete 
       arithmetic numeral, we conclude that the symbolic trace generated by the the 
       scope statement must be of concrete nature.\<close>
    moreover hence "min_conc_map\<^sub>\<T> ((sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<leadsto> State\<llangle>[(vargen \<sigma> 0 100 (''$'' @ x @ ''::Scope'')) \<longmapsto> Exp (Num 0)] \<sigma>\<rrangle>) 0 = \<circle>"
      using assms min_conc_map_of_concrete\<^sub>\<T> by presburger
    \<comment> \<open>Considering that the computed trace is concrete, it contains no symbolic variables.
       Using the \<open>min_conc_map_of_concrete\<^sub>\<T>\<close> theorem, we can then infer that the minimal 
       concretization mapping must then match the empty state.\<close>
    ultimately have "trace_conc (min_conc_map\<^sub>\<T> ((sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<leadsto> State\<llangle>[(vargen \<sigma> 0 100 (''$'' @ x @ ''::Scope'')) \<longmapsto> Exp (Num 0)] \<sigma>\<rrangle>) 0) ((sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<leadsto> State\<llangle>[(vargen \<sigma> 0 100 (''$'' @ x @ ''::Scope'')) \<longmapsto> Exp (Num 0)] \<sigma>\<rrangle>) = (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<leadsto> State\<llangle>[(vargen \<sigma> 0 100 (''$'' @ x @ ''::Scope'')) \<longmapsto> Exp (Num 0)] \<sigma>\<rrangle>"
      using assms trace_conc_pr by fastforce
    \<comment> \<open>Utilizing a supplementary theorem, we conclude that the trace concretization of 
       the computed trace under its minimal concretization mapping must be the
       computed trace itself.\<close>
    thus ?thesis by auto
    \<comment> \<open>We can now use Isabelle in order to infer the conclusion of this lemma.\<close>
  qed

text \<open>We now desire to setup the simplification lemma for the input statement, which
  is the only $WL_{EXT}$ statement that introduces symbolic variables. Hence, it is
  the only statement in our model, for which the trace concretization is not trivially
  computable. However, note that our lemma does not simplify the actual trace 
  concretization procedure in order to keep the model as modular as possible. During
  the proof automization, the concretization will therefore need to be handled by 
  separate~lemmas.\<close>
  lemma \<delta>\<^sub>s_Input:
    assumes "concrete\<^sub>\<T>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<and> x \<noteq> (vargen \<sigma> 0 100 (''$'' @ x @ ''::Input''))" 
    shows "\<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[INPUT x]) = {(trace_conc ([(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> Exp (Num 0)] \<circle>) ((((sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<leadsto> State\<llangle>[x \<longmapsto> Exp (Var (vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')))] [(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> \<^emph>] \<sigma>\<rrangle>) \<leadsto> Event\<llangle>inpEv, [A (Var (vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')))]\<rrangle>) \<leadsto> State\<llangle>[x \<longmapsto> Exp (Var (vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')))] [(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> \<^emph>] \<sigma>\<rrangle>), \<lambda>[\<nabla>])}"
  proof -
    have "min_conc_map\<^sub>\<Sigma>([(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> \<^emph>] \<sigma>) 0 = [(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> Exp (Num 0)] \<circle>"
      using assms min_conc_map_of_concrete_upd\<^sub>\<Sigma> by simp
    \<comment> \<open>Let us first investigate state \<open>\<sigma>\<close>, which is updated by mapping a fresh
       disambiguated input variable \<open>x'\<close> onto the symbolic value. Using the 
       \<open>min_conc_map_of_concrete_upd\<^sub>\<Sigma>\<close> lemma of the \<open>base-theory\<close>, we can
       infer that the minimal trace concretization mapping of this updated state 
       only maps \<open>x'\<close> onto the arithmetic numeral 0, implying that it is undefined 
       for any other~variable.\<close>
    moreover hence "symb\<^sub>\<Sigma>([(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> \<^emph>] \<sigma>) = {(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input''))}"
      using assms symb\<^sub>\<Sigma>_def by auto
    \<comment> \<open>Due to the concreteness of \<open>\<sigma>\<close>, the updated version of \<open>\<sigma>\<close> only contains
       one singular symbolic variable, which is \<open>x'\<close>.\<close>
    ultimately have "min_conc_map\<^sub>\<Sigma>([x \<longmapsto> Exp (Var (vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')))] [(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> \<^emph>] \<sigma>) 0 = [(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> Exp (Num 0)] \<circle>"
      using assms min_conc_map_of_concrete_no_upd\<^sub>\<Sigma> by (metis singletonD)  
    \<comment> \<open>Let us again update the state, such that \<open>x\<close> maps onto \<open>x'\<close>. Then the earlier
       established minimal concretization mapping does not need to be changed in order 
       to be a valid concretization mapping for the updated state. This holds, because
       the updated variable \<open>x\<close> is not of symbolic nature. Note that we use the
       earlier given \<open>min_conc_map_of_concrete_no_upd\<^sub>\<Sigma>\<close> lemma to infer this
       conclusion.\<close>
    hence "min_conc_map\<^sub>\<T>(((((sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<leadsto> State\<llangle>[x \<longmapsto> Exp (Var (vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')))] [(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> \<^emph>] \<sigma>\<rrangle>) \<leadsto> Event\<llangle>inpEv, [A (Var (vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')))]\<rrangle>) \<leadsto> State\<llangle>[x \<longmapsto> Exp (Var (vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')))] [(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> \<^emph>] \<sigma>\<rrangle>)) 0 = [(vargen \<sigma> 0 100 (''$'' @ x @ ''::Input'')) \<longmapsto> Exp (Num 0)] \<circle>"
      using assms min_conc_map_of_concrete\<^sub>\<T> by (smt (z3) fmadd_empty(1) fmadd_idempotent min_conc_map\<^sub>\<T>.simps(2) min_conc_map\<^sub>\<T>.simps(3))
    \<comment> \<open>The established concretization mapping must then be a valid concretization 
       mapping for the whole trace generated by the input statement.\<close>
    thus ?thesis by fastforce
    \<comment> \<open>We can now use Isabelle in order to infer the conclusion of this lemma.\<close>
  qed

text \<open>Applying the $\<open>\<delta>\<close>_s$-function on the guarded statement can again cause
  two distinct results. If the path condition containing the evaluated Boolean
  guard is consistent, the statement behind the Boolean guard is executed. 
  If the path condition containing the evaluated negated Boolean guard
  is consistent, the statement blocks, implying that no successor configuration
  exists. We formalize a simplification lemma for both~cases. \par\<close>
  lemma \<delta>\<^sub>s_Guard\<^sub>T:
    assumes "consistent ({val\<^sub>B b \<sigma>}) \<and> concrete\<^sub>\<T>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>)"
    shows "\<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[:: b;; S END]) = {(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[S])}"
  proof -
    have "consistent(sval\<^sub>B {val\<^sub>B b \<sigma>} (min_conc_map\<^sub>\<T> (\<langle>\<sigma>\<rangle>) 0))"
      using assms consistent_pr by blast
    \<comment> \<open>As we have already assumed that the path condition containing b 
       (i.e. the true-case) is consistent, the consistency of the path condition must
       then be preserved if further simplified under a minimal concretization mapping.\<close>
    moreover have "\<not>consistent(sval\<^sub>B {val\<^sub>B (not b) \<sigma>} (min_conc_map\<^sub>\<T> (\<langle>\<sigma>\<rangle>) 0))"
      using assms by simp
    \<comment> \<open>Hence, the path condition of the continuation trace corresponding to
       the false-case cannot~be~consistent.\<close>
    ultimately have "{cont \<in> (val\<^sub>s (:: b;; S END) \<sigma>). consistent(sval\<^sub>B (\<down>\<^sub>p cont) (min_conc_map\<^sub>\<T> (\<down>\<^sub>\<tau> cont) 0))} = { {val\<^sub>B b \<sigma>} \<triangleright> \<langle>\<sigma>\<rangle> \<^item> \<lambda>[S] }"
      using assms by auto
    \<comment> \<open>Thus, the set of consistent continuation traces can only contain the
       continuation trace corresponding to the true-case.\<close>
    moreover have "min_conc_map\<^sub>\<T> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) 0 = \<circle>"
      using assms min_conc_map_of_concrete\<^sub>\<T> by presburger
    \<comment> \<open>Considering that \<open>(sh \<leadsto> \<sigma>)\<close> is concrete, it contains no symbolic variables.
       Using the \<open>min_conc_map_of_concrete\<^sub>\<T>\<close> theorem, we can then infer that the minimal 
       concretization mapping of \<open>(sh \<leadsto> \<sigma>)\<close> matches the empty state.\<close>
    hence "trace_conc (min_conc_map\<^sub>\<T> (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) 0) (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) = sh \<leadsto> State\<llangle>\<sigma>\<rrangle>"
      using assms trace_conc_pr by presburger
    \<comment> \<open>Utilizing a supplementary theorem, we conclude that the trace concretization of 
       \<open>(sh \<leadsto> \<sigma>)\<close> under its minimal concretization mapping must be \<open>(sh \<leadsto> \<sigma>)\<close> itself.\<close>
    ultimately show ?thesis by auto
    \<comment> \<open>We can now use Isabelle in order to infer the conclusion of this lemma.\<close>
  qed

  lemma \<delta>\<^sub>s_Guard\<^sub>F:
    assumes "consistent {(val\<^sub>B (not b) \<sigma>)} \<and> concrete\<^sub>\<T>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>)"
    shows "\<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[:: b;; S END]) = {}"
  proof -
    have "consistent(sval\<^sub>B {val\<^sub>B (not b) \<sigma>} (min_conc_map\<^sub>\<T> (\<langle>\<sigma>\<rangle>) 0))"
      using assms consistent_pr by blast
    \<comment> \<open>As we have already assumed that the path condition containing \<open>not b\<close> 
       (i.e. the false-case) is consistent, the consistency of the path condition must
       then be preserved if further simplified under a minimal concretization mapping.\<close>
    moreover have "\<not>consistent(sval\<^sub>B {val\<^sub>B b \<sigma>} (min_conc_map\<^sub>\<T> (\<langle>\<sigma>\<rangle>) 0))"
      using assms conc_pc_pr\<^sub>B\<^sub>N consistent_def s_value_pr\<^sub>B 
      by (metis bexp.simps(17) singletonI val\<^sub>B.simps(2))
    \<comment> \<open>Hence, the path condition corresponding to the true-case cannot be~consistent.\<close>
    ultimately have "{cont \<in> (val\<^sub>s (:: b;; S END) \<sigma>). consistent(sval\<^sub>B (\<down>\<^sub>p cont) (min_conc_map\<^sub>\<T> (\<down>\<^sub>\<tau> cont) 0))} = {}"
      using assms by auto
    \<comment> \<open>This implies that the set of consistent continuation traces must be empty.\<close>
    thus ?thesis by simp
    \<comment> \<open>Thus, no possible successor configuration can exist, concluding this lemma.\<close>
  qed

  lemma \<delta>\<^sub>s_Call:
    assumes "concrete\<^sub>\<T>(sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<and> vars\<^sub>A(a) \<subseteq> fmdom'(\<sigma>)" 
    shows "\<delta>\<^sub>s (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[CALL m a]) = {(((sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<leadsto> Event\<llangle>invEv, lval\<^sub>E [P m, A a] \<sigma>\<rrangle>) \<leadsto> State\<llangle>\<sigma>\<rrangle>, \<lambda>[\<nabla>])}"
  proof -
    have "concrete\<^sub>\<T> (((sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<leadsto> Event\<llangle>invEv, lval\<^sub>E [P m, A a] \<sigma>\<rrangle>) \<leadsto> State\<llangle>\<sigma>\<rrangle>)"
      using assms l_concrete_imp\<^sub>E concrete\<^sub>\<T>.simps vars\<^sub>E.simps 
      by (metis Un_commute Un_empty_right lvars\<^sub>E.simps)
    \<comment> \<open>Due to the premise of this lemma, \<open>\<sigma>\<close> must be of concrete nature. The event
       occurring in the generated trace must also be concrete, as it only contains
       concrete expressions. Hence, we can infer the concreteness of the whole generated
       trace.\<close>
    moreover hence "min_conc_map\<^sub>\<T> (((sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<leadsto> Event\<llangle>invEv, lval\<^sub>E [P m, A a] \<sigma>\<rrangle>) \<leadsto> State\<llangle>\<sigma>\<rrangle>) 0 = \<circle>"
      using assms min_conc_map_of_concrete\<^sub>\<T> by presburger
    \<comment> \<open>Considering that the computed trace is concrete, it contains no symbolic 
       variables. Using the \<open>min_conc_map_of_concrete\<^sub>\<T>\<close> theorem, we can then deduce that 
       the minimal concretization mapping of the computed trace matches the empty state.\<close>
    ultimately have "trace_conc (min_conc_map\<^sub>\<T> (((sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<leadsto> Event\<llangle>invEv, lval\<^sub>E [P m, A a] \<sigma>\<rrangle>) \<leadsto> State\<llangle>\<sigma>\<rrangle>) 0) (((sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<leadsto> Event\<llangle>invEv, lval\<^sub>E [P m, A a] \<sigma>\<rrangle>) \<leadsto> State\<llangle>\<sigma>\<rrangle>) = ((sh \<leadsto> State\<llangle>\<sigma>\<rrangle>) \<leadsto> Event\<llangle>invEv, lval\<^sub>E [P m, A a] \<sigma>\<rrangle>) \<leadsto> State\<llangle>\<sigma>\<rrangle>"
      using assms trace_conc_pr by presburger
    \<comment> \<open>Utilizing a supplementary theorem, we conclude that the trace concretization of 
       the computed trace under its minimal concretization mapping must be the
       computed trace itself.\<close>
    thus ?thesis by fastforce
    \<comment> \<open>We can now use Isabelle in order to infer the conclusion of this lemma.\<close>
  qed

  end

text \<open>Note that we have optimized our $\<open>\<delta>\<close>_2$-function for our code-generation
  objective. However, this results in a weakness of the corresponding proof system, 
  as it cannot derive its function values using its automatically 
  generated simplifications alone. The origin of this problem is the \<open>Set.filter\<close> 
  function, which we use in order to filter out all tuples that are violating the 
  established notion of wellformedness. Isabelle cannot automatically resolve this 
  filtering process using its standard simplifications. \par
  In order to provide a solution for this problem, we therefore propose an alternative
  formalization of the $\<open>\<delta>\<close>_2$-function, which models the filtering process of the
  tuples in a different manner. Instead of using the \<open>Set.filter\<close> function,
  we choose to intersect all tuples of methods and arithmetic method parameters with 
  the set of all wellformed tuples. Note that this is equivalent to the original 
  definition of the $\<open>\<delta>\<close>_2$-function. \par
  However, we cannot replace the original formalization of the $\<open>\<delta>\<close>_2$-function with 
  this alternative definition, as the set of all wellformed tuples of methods and 
  arithmetic expressions (i.e. one of the sets we intersect) is infinite. As we 
  have already explained, this would greatly interfere with our code-generation. 
  Hence, we choose to use the standard definition for the purpose of generating 
  executable code, and this supplementary simplification for ensuring
  automated~reasoning.\<close>
  lemma \<delta>\<^sub>2_simp: "\<delta>\<^sub>2 M (sh \<leadsto> State\<llangle>\<sigma>\<rrangle>, q) = 
          (%(m, v). (((sh \<cdot> (gen_event invREv \<sigma> [P (\<Up>\<^sub>n m), v])) 
                        \<leadsto> State\<llangle>[(vargen \<sigma> 0 100 (''$'' @ (\<Up>\<^sub>n m) @ ''::Param'')) \<longmapsto> Exp (proj\<^sub>A v)] \<sigma>\<rrangle>), 
                    q + {# \<lambda>[(\<Up>\<^sub>s m) [(\<Up>\<^sub>v m) \<leftarrow>\<^sub>s (vargen \<sigma> 0 100 (''$'' @ (\<Up>\<^sub>n m) @ ''::Param''))] ] #})) 
          ` ({(m, v). wellformed (sh \<cdot> (gen_event invREv \<sigma> [P (\<Up>\<^sub>n m), v]))} \<inter> (M \<times> params(sh)))"
    by fastforce

text \<open>In order to finalize the automated proof system of the \<open>\<delta>\<close>-function, we 
  collect all necessary simplifications in a set, and call it the \<open>\<delta>\<close>-system.
  Our proof derivation system then consists of the following lemmas: \begin{description}
  \item[\<open>\<delta>\<^sub>s\<close>-simplifications] These lemmas correspond to our earlier established
    simplifications for the \<open>\<delta>\<^sub>s\<close>-function. Note that they ensure a major 
    speedup of our proof derivations, as we avoid dealing with the underlying
    valuation function.
  \item[\<open>\<delta>\<^sub>2\<close>-simplifications] We also have to add the \<open>\<delta>\<^sub>2\<close> simplification lemma
    in order to ensure that we can automatically compute function values
    of the \<open>\<delta>\<^sub>2\<close>-function.
  \item[Concreteness lemmas] The concreteness lemmas fulfill two distinct
    purposes: \par
    Remember that the simplification lemmas for the \<open>\<delta>\<^sub>s\<close>-function are only
    usable iff we guarantee that the argument trace is of concrete
    nature. Hence, we first need to establish the concreteness of the initial 
    program state (e.g. using the \<open>\<sigma>\<^sub>I_concrete\<close> lemma), so as to ensure that 
    our proof derivation system is even applicable. \par
    Secondly, we provide the \<open>concrete_upd_pr\<^sub>A\<close> lemma of the base theory, so as
    to enable the prover to inductively compute the concreteness of states. Note
    that we do not add the standard simplification of the state concreteness
    notion, as this would greatly reduce our derivation speed.
  \item[Trace Concretization lemmas] Remember that the trace concretization
    procedure of the input command is not automatically resolved by the corresponding 
    \<open>\<delta>\<^sub>s\<close>-simplification. Hence, it becomes indispensable to add the previously proven 
    trace concretization lemmas of the base theory to our proof automation system. 
  \item[Consistency simplifications] Our proof system also needs to resolve
    the consistency notion in order to verify the consistency of path conditions
    during the trace composition. Hence, we choose to add its standard simplification
    to our \<open>\<delta>_system\<close>.
  \item[Supplementary lemmas] Similar to \<open>WL\<close>, we additionally add the
  \<open>eval-nat-numeral\<close> lemma, so as to ensure that the simplifier can freely swap 
  between the Suc/Zero notation and the numeral representation when inferring results 
  from the variable generation function. This is necessary, as we use the \<open>Suc\<close> 
  constructor in its function definition, but numerals when decrementing~the~bound. \par
  We furthermore add several other simplifications: The \<open>fmap_ext\<close> lemma is used in
  order to add a state equality notion to our proof system. The \<open>member_rec\<close> 
  simplification ensures that we can derive list memberships when computing the 
  initial state \<open>\<sigma>\<^sub>I\<close>~\<open>S\<close> for a program S. The \<open>insert_commute\<close> and
  \<open>fmupd_reorder_neq\<close> lemmas additionally guarantee that the prover can freely 
  permute state updates and set elements during the proof derivation.
  \end{description} We additionally remove the normal simplifications of the $\<open>\<delta>\<close>_s$ 
  and $\<open>\<delta>\<close>_2$-function, thereby ensuring that the Isabelle simplifier will later 
  select the \<open>\<delta>\<close>-system when deriving successor~configurations. \par
  Note that there is still room for further improvements in our proof automation
  (e.g. speed-related improvements), which we propose as an idea for further work on
  this~model.\<close>
  lemmas \<delta>_system = 
    \<delta>\<^sub>s_Skip \<delta>\<^sub>s_Assign \<delta>\<^sub>s_If\<^sub>T \<delta>\<^sub>s_If\<^sub>F \<delta>\<^sub>s_While\<^sub>T \<delta>\<^sub>s_While\<^sub>F \<delta>\<^sub>s_Seq  \<comment> \<open>\<open>\<delta>\<^sub>s\<close>-simplifications\<close>
    \<delta>\<^sub>s_LocPar \<delta>\<^sub>s_Scope\<^sub>A \<delta>\<^sub>s_Scope\<^sub>B \<delta>\<^sub>s_Input \<delta>\<^sub>s_Guard\<^sub>T \<delta>\<^sub>s_Guard\<^sub>F \<delta>\<^sub>s_Call \<comment> \<open>\<open>\<delta>\<^sub>s\<close>-simplifications\<close>
    \<delta>\<^sub>2_simp  \<comment> \<open>\<open>\<delta>\<^sub>2\<close>-simplifications\<close>  
    conc_concrete \<sigma>\<^sub>I_concrete concrete_upd_pr\<^sub>A \<comment> \<open>Concreteness lemmas\<close> 
    fmmap_keys_empty fmmap_keys_conc \<comment> \<open>Trace Concretization lemmas\<close>
    fmmap_keys_input fmmap_keys_dom_upd \<comment> \<open>Trace Concretization lemmas\<close>
    consistent_def \<comment> \<open>Consistency simplifications\<close>
    eval_nat_numeral fmap_ext member_rec \<comment> \<open>Supplementary lemmas\<close>
    insert_commute fmupd_reorder_neq \<comment> \<open>Supplementary lemmas\<close>

  declare basic_successors.simps[simp del]
  declare successors\<^sub>2.simps[simp del]



section \<open>Global Trace Semantics\<close>

subsection \<open>Bounded Global Traces\<close>

text \<open>Similar to \<open>WL\<close>, we now desire to setup the transitive closure of the
  trace composition, thus later allowing us to compute global traces in our 
  semantics. For this purpose, we first adapt the computation of n-bounded global 
  traces (\<open>\<Delta>\<^sub>N\<close>-function), which halts the transitive closure after a certain bound 
  is reached. \par
  The formalization of this function is almost identical to the definition for \<open>WL\<close>.
  However, note that the notion of a terminal configuration has drastically changed.
  In \<open>WL\<close>, we were simply able to stop the transitive closure when the reached
  configuration contained an empty continuation marker, as this implied its terminal
  character. In $WL_{EXT}$ however, a configuration containing only empty
  continuation markers must not necessary be terminal, as there may still be 
  pending invocation events. \par
  This in turn implies that we have to axiomatize terminal configurations of $WL_{EXT}$ 
  in a different manner. We choose to call a $WL_{EXT}$ configuration \<open>c\<close> terminal 
  iff the set of its successor configurations is empty (i.e. \<open>\<delta> M c = {}\<close>). This also 
  ensures that the processes corresponding to all called methods have already been 
  created, thus smoothly aligning with our intuition~of~termination.\<close>
  fun
    composition\<^sub>N :: "nat \<Rightarrow> method set \<Rightarrow> config \<Rightarrow> config set" ("\<Delta>\<^sub>N") where
    "\<Delta>\<^sub>N 0 M c = {c}" |
    "\<Delta>\<^sub>N (Suc n) M c = (if \<delta> M c = {} then {c} else \<Union>((%c. \<Delta>\<^sub>N n M c) ` \<delta> M c))" 

text \<open>The definition for n-bounded global traces generated from program \<open>S\<close> called 
  in \<open>\<sigma>\<close> is now straightforward. We simply return all symbolic traces of the 
  configurations received by calling the \<open>\<Delta>\<^sub>N\<close>-function in initial configuration 
  \<open>(\<langle>\<sigma>\<rangle>, {# \<lambda>[S] #})\<close> with bound n.\<close>
  fun
    Traces\<^sub>N :: "program \<Rightarrow> \<Sigma> \<Rightarrow> nat \<Rightarrow> \<T> set" ("Tr\<^sub>N") where
    "Tr\<^sub>N (Program M \<lbrace> S \<rbrace>) \<sigma> n = fst ` \<Delta>\<^sub>N n (set (remdups M)) (\<langle>\<sigma>\<rangle>, {# \<lambda>[S] #})"


subsection \<open>Unbounded Global Traces\<close>

text \<open>The construction of the unbounded global traces (\<open>\<Delta>\<close>-function) is now
  straightforward, as it is almost identical to the definition for \<open>WL\<close>. However,
  note that we again have to adapt the formalization of terminal configurations
  in the condition of the If-case.\<close>
  partial_function (tailrec) composition :: "nat \<Rightarrow> method set \<Rightarrow> config \<Rightarrow> config set" ("\<Delta>") where
    [code]: "\<Delta> n M c = (if \<forall>c \<in> \<Delta>\<^sub>N n M c. \<delta> M c = {} then \<Delta>\<^sub>N n M c else \<Delta> (n + 100) M c)"

text \<open>The construction of unbounded global traces generated from program \<open>S\<close> called 
  in \<open>\<sigma>\<close> is simple. We simply return all symbolic traces of the configurations 
  received by calling the \<open>\<Delta>\<close>-function in initial configuration \<open>(\<langle>\<sigma>\<rangle>, {# \<lambda>[S] #})\<close>. 
  Note that this again requires the \<open>\<Delta>\<close>-function to converge in a
  corresponding~fixpoint.\<close>
  fun
    Traces :: "program \<Rightarrow> \<Sigma> \<Rightarrow> \<T> set" ("Tr") where
    "Tr (Program M \<lbrace> S \<rbrace>) \<sigma> = fst ` \<Delta> 0 (set (remdups M)) (\<langle>\<sigma>\<rangle>, {# \<lambda>[S] #})"


subsection \<open>Proof Automation\<close>

text \<open>Similar to \<open>WL\<close>, we again choose to introduce simplification lemmas for the \<open>\<Delta>\<^sub>N\<close> 
  and \<open>\<Delta>\<close>-function. This decision will later ensure a major speedup when deriving 
  global traces in our proof~system.\<close>
  lemma \<Delta>\<^sub>N_Bound: 
    shows "\<Delta>\<^sub>N 0 M c = {c}"
    by simp

  lemma \<Delta>\<^sub>N_Termination: 
    assumes "\<delta> M c = {}"
    shows"\<Delta>\<^sub>N (Suc n) M c = {c}"
    using assms by simp

  lemma \<Delta>\<^sub>N_Step: 
    assumes "\<not>\<delta> M c = {}"
    shows"\<Delta>\<^sub>N (Suc n) M c = \<Union>((%c. \<Delta>\<^sub>N n M c) ` \<delta> M c)"
    using assms by simp

  lemma \<Delta>_fixpoint_reached:
    assumes "\<forall>c \<in> \<Delta>\<^sub>N n M c. \<delta> M c = {}"
    shows "\<Delta> n M c = \<Delta>\<^sub>N n M c"
    using composition.simps assms by auto

  lemma \<Delta>_iteration:
    assumes "\<not>(\<forall>c \<in> \<Delta>\<^sub>N n M c. \<delta> M c = {})"
    shows "\<Delta> n M c = \<Delta> (n + 100) M c"
    using composition.simps assms by meson

text \<open>In order to finalize the automated proof system of the 
  \<open>\<Delta>\<^sub>N\<close>-function/\<open>\<Delta>\<close>-function, we collect all previously proven lemmas in a set, 
  and call it the \<open>\<Delta>\<^sub>N\<close>-system/\<open>\<Delta>\<close>-system. \par
  Note that we additionally remove the normal function simplifications of the
  \<open>\<Delta>\<^sub>N\<close>-function, such that the Isabelle simplifier will later select our \<open>\<Delta>\<^sub>N\<close>-system
  when deriving global traces. An explicitly defined \<open>partial-function\<close> does not 
  automatically add its simplifications to the simplifier, hence there are no 
  equations to remove for~the~\<open>\<Delta>\<close>-function.\<close>
  lemmas \<Delta>\<^sub>N_system = 
    \<Delta>\<^sub>N_Bound \<Delta>\<^sub>N_Termination \<Delta>\<^sub>N_Step

  lemmas \<Delta>_system = 
    \<Delta>_fixpoint_reached \<Delta>_iteration

  declare composition\<^sub>N.simps[simp del]

text \<open>We now collect the \<open>\<delta>\<close>-system, \<open>\<Delta>\<^sub>N\<close>-system and \<open>\<Delta>\<close>-system in one set,
  naming it the \<open>WL\<^sub>E\<close>-derivation-system. Note that we will later be able to use this
  system for all global trace derivations~in~$WL_{EXT}$.\<close>
  lemmas WL\<^sub>E_derivation_system = 
    \<delta>_system \<Delta>\<^sub>N_system \<Delta>_system


subsection \<open>Trace Derivation Examples\<close>

text \<open>We now take another look at our previously defined example programs for
  $WL_{Ext}$, analyzing their global traces using our proof automation system. \par
  Program \<open>WL_ex\<^sub>1\<close> first opens a new scope that declares a local variable x, which
  is only active inside the corresponding scope. Utilizing the local parallelism
  command, we then non-deterministically assign this variable to either numeral 1 
  or numeral 2. \par
  In order to disambiguate the fresh scope variable, \<open>x\<close> is replaced by the 
  standardized variable \<open>$x::Scope\<close>, which aligns with our convention. Due to the 
  underlying non-determinism of the local parallelism command, two distinct global 
  traces can be generated. The first trace listed below corresponds to the scenario, 
  in which \<open>$x::Scope\<close> is first assigned to 2, before it is assigned to 1. The second
  trace matches the case, in which \<open>$x::Scope\<close> is first assigned to 1, before being
  assigned to numeral 2. Note that this smoothly aligns with our intuition of
  the local parallelism~command.\<close>
  lemma "Tr WL_ex\<^sub>1 (\<sigma>\<^sub>I WL_ex\<^sub>1) =
            {((\<langle>fm []\<rangle> \<leadsto> State \<llangle>fm [(''$x::Scope'', Exp (Num 0))]\<rrangle>) \<leadsto>
            State \<llangle>fm [(''$x::Scope'', Exp (Num 1))]\<rrangle>) \<leadsto>
            State \<llangle>fm [(''$x::Scope'', Exp (Num 2))]\<rrangle>,
            ((\<langle>fm []\<rangle> \<leadsto> State \<llangle>fm [(''$x::Scope'', Exp (Num 0))]\<rrangle>) \<leadsto>
            State \<llangle>fm [(''$x::Scope'', Exp (Num 2))]\<rrangle>) \<leadsto>
            State \<llangle>fm [(''$x::Scope'', Exp (Num 1))]\<rrangle>}"
    by (simp add: WL\<^sub>E_derivation_system WL_ex\<^sub>1_def)

text \<open>Program \<open>WL_ex\<^sub>2\<close> first assigns \<open>x\<close> to numeral 0. It then executes a method call on
  method \<open>foo\<close>, passing it the parameter \<open>x\<close>. The main body then finishes execution
  by assigning \<open>x\<close> to numeral 1. Method \<open>foo\<close> in turn contains only a singular
  atomic statement, which assigns its formal parameter \<open>x\<close> to the numeral 2. \par
  Note that the formal parameter of \<open>foo\<close> will again be renamed for disambiguation
  purposes. The new name \<open>$foo:Param\<close>, as can be seen below, matches our established
  variable convention. Due to the non-deterministic execution of the main body and \<open>foo\<close>, 
  three distinct global traces can be generated. The first and second trace, as inferred 
  below, match the cases, in which the process corresponding to \<open>foo\<close> reacts to the 
  invocation directly after the evaluation of the method call. Afterwards, both the main 
  body and \<open>foo\<close> still have a statement left to execute. Note that both traces differ in 
  which statement is executed first. The third trace matches the scenario, in which the
  process corresponding to \<open>foo\<close> only reacts to the invocation, once the main body
  has finished its execution. As can be seen, even small scale programs like this
  can cause a severe blowup of the trace quantity, which is a direct consequence
  of the underlying non-determinism.\<close>
  lemma "Tr WL_ex\<^sub>2 (\<sigma>\<^sub>I WL_ex\<^sub>2) =
            {(((((((\<langle>fm [(''x'', Exp (Num 0))]\<rangle> \<leadsto> State \<llangle>fm [(''x'', Exp (Num 0))]\<rrangle>) \<leadsto>
        Event \<llangle>invEv,[P ''foo'', A (Num 0)]\<rrangle>) \<leadsto>
       State \<llangle>fm [(''x'', Exp (Num 0))]\<rrangle>) \<leadsto>
      Event \<llangle>invREv,[P ''foo'', A (Num 0)]\<rrangle>) \<leadsto>
     State \<llangle>fm [(''x'', Exp (Num 0))]\<rrangle>) \<leadsto>
    State \<llangle>fm [(''x'', Exp (Num 0)), (''$foo::Param'', Exp (Num 0))]\<rrangle>) \<leadsto>
   State \<llangle>fm [(''x'', Exp (Num 1)), (''$foo::Param'', Exp (Num 0))]\<rrangle>) \<leadsto>
  State \<llangle>fm [(''x'', Exp (Num 1)), (''$foo::Param'', Exp (Num 2))]\<rrangle>,
  (((((((\<langle>fm [(''x'', Exp (Num 0))]\<rangle> \<leadsto> State \<llangle>fm [(''x'', Exp (Num 0))]\<rrangle>) \<leadsto>
        Event \<llangle>invEv,[P ''foo'', A (Num 0)]\<rrangle>) \<leadsto>
       State \<llangle>fm [(''x'', Exp (Num 0))]\<rrangle>) \<leadsto>
      Event \<llangle>invREv,[P ''foo'', A (Num 0)]\<rrangle>) \<leadsto>
     State \<llangle>fm [(''x'', Exp (Num 0))]\<rrangle>) \<leadsto>
    State \<llangle>fm [(''x'', Exp (Num 0)), (''$foo::Param'', Exp (Num 0))]\<rrangle>) \<leadsto>
   State \<llangle>fm [(''x'', Exp (Num 0)), (''$foo::Param'', Exp (Num 2))]\<rrangle>) \<leadsto>
  State \<llangle>fm [(''x'', Exp (Num 1)), (''$foo::Param'', Exp (Num 2))]\<rrangle>,
  (((((((\<langle>fm [(''x'', Exp (Num 0))]\<rangle> \<leadsto> State \<llangle>fm [(''x'', Exp (Num 0))]\<rrangle>) \<leadsto>
        Event \<llangle>invEv,[P ''foo'', A (Num 0)]\<rrangle>) \<leadsto>
       State \<llangle>fm [(''x'', Exp (Num 0))]\<rrangle>) \<leadsto>
      State \<llangle>fm [(''x'', Exp (Num 1))]\<rrangle>) \<leadsto>
     Event \<llangle>invREv,[P ''foo'', A (Num 0)]\<rrangle>) \<leadsto>
    State \<llangle>fm [(''x'', Exp (Num 1))]\<rrangle>) \<leadsto>
   State \<llangle>fm [(''x'', Exp (Num 1)), (''$foo::Param'', Exp (Num 0))]\<rrangle>) \<leadsto>
  State \<llangle>fm [(''x'', Exp (Num 1)), (''$foo::Param'', Exp (Num 2))]\<rrangle>}"
    by (simp add: WL\<^sub>E_derivation_system WL_ex\<^sub>2_def)

text \<open>The intuition behind program \<open>WL_ex\<^sub>3\<close> is simple. It first receives an unknown 
  input variable from another system, and subsequentially increments it by one. \par
  As can be inferred below, this program generates exactly one global trace. A new input 
  variable \<open>$x::Input\<close> is introduced, which is then concretized with the standard value 
  0 during the trace composition. We also insert an input event into the trace, which 
  could - in further extensions of this model - serve as a trace composition 
  interaction~point.\<close>
  lemma "Tr WL_ex\<^sub>3 (\<sigma>\<^sub>I WL_ex\<^sub>3) =
          {(((\<langle>fm [(''x'', Exp (Num 0)), (''$x::Input'', Exp (Num 0))]\<rangle> \<leadsto>
          State \<llangle>fm [(''x'', Exp (Num 0)), (''$x::Input'', Exp (Num 0))]\<rrangle>) \<leadsto>
          Event \<llangle>inpEv,[A (Num 0)]\<rrangle>) \<leadsto>
          State \<llangle>fm [(''x'', Exp (Num 0)), (''$x::Input'', Exp (Num 0))]\<rrangle>) \<leadsto>
          State \<llangle>fm [(''x'', Exp (Num 1)), (''$x::Input'', Exp (Num 0))]\<rrangle>}"
    by (simp add: WL\<^sub>E_derivation_system WL_ex\<^sub>3_def)

text \<open>Note that the trace derivation speed of our proof automation is much slower 
  than for the standard While Language. This is a direct result of the increased 
  language complexity. We therefore propose further speed-related optimizations 
  (i.e. additional simplifications) as an idea for a further extension of~this~model.\<close>


subsection \<open>Code Generation\<close>

text \<open>Due to our focus on the code generation, we can again use the \<open>value\<close> keyword
  to execute code for the global trace construction of arbitrary programs, and
  output the corresponding results in the console. Note that the code execution itself 
  is very performant, and can therefore be used to compute the global traces for any 
  arbitrary program \<open>S\<close> and arbitrary initial state \<open>\<sigma>\<close>. We again propose further work 
  on exports of this code to several other programming languages supported by Isabelle 
  (e.g. Haskell, Scala) as an idea for extending the work of this~thesis.\<close>
  value "Tr WL_ex\<^sub>1 (\<sigma>\<^sub>I WL_ex\<^sub>1)"
  value "Tr WL_ex\<^sub>2 (\<sigma>\<^sub>I WL_ex\<^sub>2)"
  value "Tr WL_ex\<^sub>3 (\<sigma>\<^sub>I WL_ex\<^sub>3)"



section \<open>Trace Equivalence\<close>

text \<open>Similar to \<open>WL\<close>, we again propose a notion of equivalence between programs. 
  We call two programs \<open>S\<close> and \<open>S'\<close> of $WL_{EXT}$ trace equivalent under a given initial 
  state \<open>\<sigma>\<close> iff \<open>S\<close> and \<open>S'\<close> called in \<open>\<sigma>\<close> generate the exact same set of global 
  traces upon termination. Note that the formalization of the inductive predicate
  is identical to the definition for \<open>WL\<close>. \par
  Remember that the \<open>code_pred\<close> keyword is used to generate code for the
  inductive~definition.\<close>
  inductive
    equivalent :: "program \<Rightarrow> program \<Rightarrow> \<Sigma> \<Rightarrow> bool" ("_ \<sim> _ [_]" 80) where
    "\<lbrakk> Tr prog \<sigma> = Tr prog' \<sigma> \<rbrakk> \<Longrightarrow> prog \<sim> prog' [\<sigma>]"
  code_pred equivalent .

text \<open>We again automatically generate inductive simplifications for our trace
  equivalence notion using the \<open>inductive-simps\<close> keyword, thereby adding them to the 
  Isabelle simplifier~equations. This guarantees that we can conduct proofs 
  inferring trace~equivalence.\<close>
  inductive_simps tequivalence: "prog \<sim> prog' [\<sigma>]"

text \<open>Last but not least, we derive an example trace equivalence conclusion
  in Isabelle utilizing the inductive definition~above.\<close>
  lemma "Program [] \<lbrace> CO ''x'' := Num 1 \<parallel> ''x'' := Num 2 OC \<rbrace> \<sim> 
              Program [] \<lbrace> CO ''x'' := Num 2 \<parallel> ''x'' := Num 1 OC \<rbrace> [\<circle>]"
    using tequivalence by (simp add: WL\<^sub>E_derivation_system)

end