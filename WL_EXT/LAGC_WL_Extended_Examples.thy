theory LAGC_WL_Extended_Examples
  imports LAGC_WL_Extended
begin


subsection \<open>Extended While Language ($WL_{EXT}$)\<close>

text \<open>\<open>SKIP\<close>\<close>
definition
  stmt\<^sub>0 :: stmt where
  "stmt\<^sub>0 \<equiv> SKIP"

text \<open>\<open>x1 := 2\<close>\<close>
definition
  stmt\<^sub>1 :: stmt where
  "stmt\<^sub>1 \<equiv> ''x1'' := Num 2"

text \<open>\<open>IF True THEN SKIP FI\<close>\<close>
definition
  stmt\<^sub>2 :: stmt where
  "stmt\<^sub>2 \<equiv> IF (Bool True) THEN SKIP FI"

text \<open>\<open>IF False THEN WHILE (False) DO SKIP OD FI;; z1 := 0\<close>\<close>
definition
  stmt\<^sub>3 :: stmt where
  "stmt\<^sub>3 \<equiv> IF (Bool False) THEN WHILE (Bool False) DO SKIP OD FI;; ''z1'' := Num 0"

text \<open>\<open>WHILE True DO SKIP OD\<close>\<close>
definition
  stmt\<^sub>4 :: stmt where
  "stmt\<^sub>4 \<equiv> WHILE (Bool True) DO SKIP OD"

text \<open>\<open>SKIP; WHILE False DO x0 := 5;; SKIP OD;; SKIP\<close>\<close>
definition
  stmt\<^sub>5 :: stmt where
  "stmt\<^sub>5 \<equiv> (SKIP;;WHILE (Bool False) DO ''x0'' := Num 5;;SKIP OD);;SKIP"

text \<open>\<open>SKIP;; CO x := 2;; y := 5 \<parallel> SKIP;; x := 3 OC\<close>\<close>
definition
  stmt\<^sub>6 :: stmt where
  "stmt\<^sub>6 \<equiv> SKIP;; CO ''x'' := Num 2;; ''y'' := Num 5 \<parallel> SKIP;; ''x'' := Num 3 OC"

text \<open>\<open>x := 2;; {(x; y; \<nu>) x := 5}\<close>\<close>
definition
  stmt\<^sub>7 :: stmt where
  "stmt\<^sub>7 \<equiv> ''x'' := Num 2;; \<lbrace>(''x''; (''y''; \<nu>)) ''x'' := Num 5\<rbrace>"

text \<open>\<open>{\<nu> SKIP}\<close>\<close>
definition
  stmt\<^sub>8 :: stmt where
  "stmt\<^sub>8 \<equiv> \<lbrace>\<nu> SKIP\<rbrace>"

text \<open>\<open>INPUT x;; SKIP\<close>\<close>
definition
  stmt\<^sub>9 :: stmt where
  "stmt\<^sub>9 \<equiv> INPUT ''x'';; SKIP" 

text \<open>\<open>CALL foo (x);; SKIP\<close>\<close>
definition
  stmt\<^sub>1\<^sub>0 :: stmt where
  "stmt\<^sub>1\<^sub>0 \<equiv> CALL ''foo'' (Var ''x'');; SKIP" 

text \<open>\<open>SKIP;; :: (5 \<le> y);; x := 2 END\<close>\<close>
definition
  stmt\<^sub>1\<^sub>1 :: stmt where
  "stmt\<^sub>1\<^sub>1 \<equiv> SKIP;; :: ((Num 5) \<^sub>Rleq (Var ''y''));; ''x'' := Num 2 END" 

text \<open>\<open>Program [] {SKIP}\<close>\<close>
definition
  program\<^sub>0 :: program where
  "program\<^sub>0 \<equiv> Program [] \<lbrace> SKIP \<rbrace>"

text \<open>\<open>Program [(Method foo(x) = (x := y))] {SKIP}\<close>\<close>
definition
  program\<^sub>1 :: program where
  "program\<^sub>1 \<equiv> Program [ 
                  (Method ''foo'' ''x'' \<lbrace> ''x'' := Var ''y'' \<rbrace>)
              ] \<lbrace> SKIP \<rbrace>"

text \<open>\<open>Program [(Method foo(x) = (x := y)), (Method bar(x) = (INPUT y))] {CALL foo(2);; CALL bar(5)}\<close>\<close>
definition
  program\<^sub>2 :: program where
  "program\<^sub>2 \<equiv> Program [
                  (Method ''foo'' ''x'' \<lbrace> ''x'' := Var ''y'' \<rbrace>),
                  (Method ''bar'' ''x'' \<lbrace> INPUT ''y'' \<rbrace>)
              ] \<lbrace> CALL ''foo'' (Num 2);; CALL ''bar'' (Var ''x'') \<rbrace>"

text \<open>\<open>\<sigma>\<^sub>A = [x \<mapsto> 0][y \<mapsto> 0]\<close>\<close>
definition
  \<sigma>\<^sub>A :: \<Sigma> where
  "\<sigma>\<^sub>A \<equiv> fm[
          (''x'', Exp (Num 0)),
          (''y'', Exp (Num 0))
          ]"

lemma "vars\<^sub>s(stmt\<^sub>0) = {}"
  by (simp add: stmt\<^sub>0_def)

lemma "vars\<^sub>s(stmt\<^sub>1) = {''x1''}"
  by (simp add: stmt\<^sub>1_def)

lemma "vars\<^sub>s(stmt\<^sub>2) = {}"
  by (simp add: stmt\<^sub>2_def)

lemma "vars\<^sub>s(stmt\<^sub>3) = {''z1''}"
  by (simp add: stmt\<^sub>3_def)

lemma "vars\<^sub>s(stmt\<^sub>4) = {}"
  by (simp add: stmt\<^sub>4_def)

lemma "vars\<^sub>s(stmt\<^sub>5) = {''x0''}"
  by (simp add: stmt\<^sub>5_def)

lemma "vars\<^sub>s(stmt\<^sub>6) = {''x'', ''y''}"
  by (auto simp add: stmt\<^sub>6_def)

lemma "vars\<^sub>s(stmt\<^sub>7) = {''x''}"
  by (simp add: stmt\<^sub>7_def)

lemma "vars\<^sub>s(stmt\<^sub>8) = {}"
  by (simp add: stmt\<^sub>8_def)

lemma "vars\<^sub>s(stmt\<^sub>9) = {''x''}"
  by (simp add: stmt\<^sub>9_def)

lemma "vars\<^sub>s(stmt\<^sub>1\<^sub>0) = {''x''}"
  by (simp add: stmt\<^sub>1\<^sub>0_def)

lemma "vars\<^sub>s(stmt\<^sub>1\<^sub>1) = {''x'', ''y''}"
  by (simp add: stmt\<^sub>1\<^sub>1_def)

lemma "vars\<^sub>p(program\<^sub>0) = {}"
  by (simp add: program\<^sub>0_def)

lemma "vars\<^sub>p(program\<^sub>1) = {''y''}"
  by (auto simp add: program\<^sub>1_def)

lemma "vars\<^sub>p(program\<^sub>2) = {''x'', ''y''}"
  by (auto simp add: program\<^sub>2_def)

lemma "occ\<^sub>p(program\<^sub>0) = []"
  by (simp add: program\<^sub>0_def)

lemma "occ\<^sub>p(program\<^sub>1) = [''y'']"
  by (simp add: program\<^sub>1_def)

lemma "occ\<^sub>p(program\<^sub>2) = [''y'', ''y'', ''x'']"
  by (simp add: program\<^sub>2_def)

lemma "program\<^sub>0 [''y'' \<leftarrow>\<^sub>p ''z''] = program\<^sub>0"
  by (simp add: program\<^sub>0_def)

lemma "program\<^sub>1 [''y'' \<leftarrow>\<^sub>p ''z''] = 
        Program [ 
              (Method ''foo'' ''x'' \<lbrace> ''x'' := Var ''z'' \<rbrace>)
        ] \<lbrace> SKIP \<rbrace>"
  by (simp add: program\<^sub>1_def)

lemma "program\<^sub>2 [''x'' \<leftarrow>\<^sub>p ''z''] = 
        Program [
                  (Method ''foo'' ''z'' \<lbrace> ''z'' := Var ''y'' \<rbrace>),
                  (Method ''bar'' ''z'' \<lbrace> INPUT ''y'' \<rbrace>)
        ] \<lbrace> CALL ''foo'' (Num 2);; CALL ''bar'' (Var ''z'') \<rbrace>"
  by (simp add: program\<^sub>2_def)

lemma "\<sigma>\<^sub>I program\<^sub>0 = fm []"
  by (simp add: program\<^sub>0_def)

lemma "\<sigma>\<^sub>I program\<^sub>1 = fm [(''y'', Exp (Num 0))]"
  by (simp add: program\<^sub>1_def)

lemma "\<sigma>\<^sub>I program\<^sub>2 = fm [(''x'', Exp (Num 0)), (''y'', Exp (Num 0))]"
  by (simp add: program\<^sub>2_def fmupd_reorder_neq)



subsection \<open>Local Evaluation\<close>

text \<open>\<open>SKIP\<close>\<close>
lemma "val\<^sub>s SKIP \<sigma>\<^sub>A 
      = { {} \<triangleright> \<langle>\<sigma>\<^sub>A\<rangle> \<^item> \<lambda>[\<nabla>] }"
  by simp

text \<open>\<open>x := 1\<close>\<close>
lemma "val\<^sub>s (''x'' := Num 1) \<sigma>\<^sub>A
      = { {} \<triangleright> \<langle>\<sigma>\<^sub>A\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 1)] \<sigma>\<^sub>A\<rrangle> \<^item> \<lambda>[\<nabla>] }"
  by simp

text \<open>\<open>If True THEN x := 1 FI\<close>\<close>
lemma "val\<^sub>s (IF (Bool True) THEN ''x'' := Num 1 FI) \<sigma>\<^sub>A 
      = { {Bool True} \<triangleright> \<langle>\<sigma>\<^sub>A\<rangle> \<^item> \<lambda>[''x'' := Num 1], 
          {Bool False} \<triangleright> \<langle>\<sigma>\<^sub>A\<rangle> \<^item> \<lambda>[\<nabla>] }"
  by simp

text \<open>\<open>WHILE True DO SKIP OD\<close>\<close>
lemma "val\<^sub>s (WHILE (Bool True) DO SKIP OD) \<sigma>\<^sub>A 
      = { {Bool True} \<triangleright> \<langle>\<sigma>\<^sub>A\<rangle> \<^item> \<lambda>[SKIP;;WHILE (Bool True) DO SKIP OD], 
          {Bool False} \<triangleright> \<langle>\<sigma>\<^sub>A\<rangle> \<^item> \<lambda>[\<nabla>] }"
  by simp

text \<open>\<open>SKIP; x := 1\<close>\<close>
lemma "val\<^sub>s (SKIP;;''x'' := Num 1) \<sigma>\<^sub>A 
      = { {} \<triangleright> \<langle>\<sigma>\<^sub>A\<rangle> \<^item> \<lambda>[''x'' := Num 1] }"
  by simp

text \<open>\<open>x := 1; y := x + 1\<close> [\<open>Example 3.1\<close>]\<close>
lemma "val\<^sub>s (''x'' := Num 1;; ''y'' := (Var ''x'') \<^sub>Aadd (Num 1)) \<sigma>\<^sub>A 
      = { {} \<triangleright> \<langle>\<sigma>\<^sub>A\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 1)] \<sigma>\<^sub>A\<rrangle> \<^item> \<lambda>[''y'' := (Var ''x'') \<^sub>Aadd (Num 1)] }"
  by simp

text \<open>\<open>CO x := 1;; y := x + 1 \<parallel> x := 2 OC\<close> [\<open>Example 5.1\<close>]\<close>
lemma "val\<^sub>s (CO ''x'' := Num 1;; ''y'' := ((Var ''x'') \<^sub>Aadd (Num 1)) \<parallel> ''x'' := Num 2 OC) \<sigma>\<^sub>A
      = { {} \<triangleright> \<langle>\<sigma>\<^sub>A\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 1)] \<sigma>\<^sub>A\<rrangle> \<^item> \<lambda>[CO ''y'' := ((Var ''x'') \<^sub>Aadd (Num 1)) \<parallel> ''x'' := Num 2 OC],
          {} \<triangleright> \<langle>\<sigma>\<^sub>A\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 2)] \<sigma>\<^sub>A\<rrangle> \<^item> \<lambda>[''x'' := Num 1;; ''y'' := ((Var ''x'') \<^sub>Aadd (Num 1))] }"
  by (simp add: insert_commute)

text \<open>\<open>CO CO x := 1 \<parallel> x := 2 OC \<parallel> x := 3 OC\<close>\<close>
lemma "val\<^sub>s (CO CO ''x'' := Num 1 \<parallel> ''x'' := Num 2 OC \<parallel> ''x'' := Num 3 OC) \<sigma>\<^sub>A
      = { {} \<triangleright> \<langle>\<sigma>\<^sub>A\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 1)] \<sigma>\<^sub>A\<rrangle> \<^item> \<lambda>[CO ''x'' := Num 2 \<parallel> ''x'' := Num 3 OC],
          {} \<triangleright> \<langle>\<sigma>\<^sub>A\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 2)] \<sigma>\<^sub>A\<rrangle> \<^item> \<lambda>[CO ''x'' := Num 1 \<parallel> ''x'' := Num 3 OC],
          {} \<triangleright> \<langle>\<sigma>\<^sub>A\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 3)] \<sigma>\<^sub>A\<rrangle> \<^item> \<lambda>[CO ''x'' := Num 1 \<parallel> ''x'' := Num 2 OC] }"
  by (simp add: insert_commute)

text \<open>\<open>{ \<nu> x := 1}\<close>\<close>
lemma "val\<^sub>s (\<lbrace> \<nu> ''x'' := Num 1 \<rbrace>) \<sigma>\<^sub>A 
      = { {} \<triangleright> \<langle>\<sigma>\<^sub>A\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 1)] \<sigma>\<^sub>A\<rrangle> \<^item> \<lambda>[\<nabla>] }"
  by simp

text \<open>\<open>{ (x; \<nu>) x := 1;; y := x + 1}\<close>\<close>
lemma "val\<^sub>s (\<lbrace> (''x''; \<nu>) ''x'' := Num 1;; ''y'' := (Var ''x'') \<^sub>Aadd (Num 1) \<rbrace>) \<sigma>\<^sub>A
      = { {} \<triangleright> \<langle>\<sigma>\<^sub>A\<rangle> \<leadsto> State\<llangle>[''$x::Scope'' \<longmapsto> Exp (Num 0)] \<sigma>\<^sub>A\<rrangle> \<^item> \<lambda>[\<lbrace> \<nu> ''$x::Scope'' := Num 1;; ''y'' := (Var ''$x::Scope'') \<^sub>Aadd (Num 1) \<rbrace>] }" 
  by (simp add: \<sigma>\<^sub>A_def eval_nat_numeral)

text \<open>\<open>{ (x; y; \<nu>) { (x; \<nu>) x := y } }\<close>\<close>
lemma "val\<^sub>s (\<lbrace> (''x''; (''y''; \<nu>)) \<lbrace> (''x''; \<nu>) ''x'' := Var ''y'' \<rbrace> \<rbrace>) \<sigma>\<^sub>A
      = { {} \<triangleright> \<langle>\<sigma>\<^sub>A\<rangle> \<leadsto> State\<llangle>[''$x::Scope'' \<longmapsto> Exp (Num 0)] \<sigma>\<^sub>A\<rrangle> \<^item> \<lambda>[\<lbrace> (''y''; \<nu>) \<lbrace> (''$x::Scope''; \<nu>) ''$x::Scope'' := Var ''y'' \<rbrace> \<rbrace>] }" 
  by (simp add: \<sigma>\<^sub>A_def eval_nat_numeral)

text \<open>\<open>INPUT y;; y := 1\<close>\<close>
lemma "val\<^sub>s (INPUT ''y'';; ''y'' := Num 1) \<sigma>\<^sub>A
      = { {} \<triangleright> (((\<langle>\<sigma>\<^sub>A\<rangle> \<leadsto> State\<llangle>[''y'' \<longmapsto> Exp (Var ''$y::Input'')] [''$y::Input'' \<longmapsto> \<^emph>] \<sigma>\<^sub>A\<rrangle>) \<leadsto> Event\<llangle>inpEv, [A (Var ''$y::Input'')]\<rrangle>) \<leadsto> State\<llangle>[''y'' \<longmapsto> Exp (Var ''$y::Input'')] [''$y::Input'' \<longmapsto> \<^emph>] \<sigma>\<^sub>A\<rrangle>) 
          \<^item> \<lambda>[''y'' := Num 1] }"
  by (simp add: \<sigma>\<^sub>A_def eval_nat_numeral)

text \<open>\<open>:: True;; x := 5 END\<close>\<close>
lemma "val\<^sub>s (:: (Bool True);; ''x'' := Num 5 END) \<sigma>\<^sub>A
      = { {Bool True} \<triangleright> \<langle>\<sigma>\<^sub>A\<rangle> \<^item> \<lambda>[''x'' := Num 5] }"
  by (simp add: insert_commute)

text \<open>\<open>CALL foo(x);; SKIP\<close>\<close>
lemma "val\<^sub>s (CALL ''foo'' (Var ''x'');; SKIP) \<sigma>\<^sub>A
     = { {} \<triangleright> (\<langle>\<sigma>\<^sub>A\<rangle> \<leadsto> Event\<llangle>invEv, [P ''foo'', A (Num 0)]\<rrangle>) \<leadsto> State\<llangle>\<sigma>\<^sub>A\<rrangle> \<^item> \<lambda>[SKIP] }"
  by (simp add: \<sigma>\<^sub>A_def)



subsection \<open>Trace Composition\<close>

text \<open>\<open>SKIP\<close>\<close>
lemma "\<delta>\<^sub>s (\<langle>\<circle>\<rangle>, \<lambda>[SKIP]) = {(\<langle>\<circle>\<rangle>, \<lambda>[\<nabla>])}" 
  by (simp add: \<delta>_system)

text \<open>\<open>SKIP;SKIP\<close>\<close>
lemma "\<delta>\<^sub>s (\<langle>\<circle>\<rangle>, \<lambda>[SKIP;;SKIP]) = {(\<langle>\<circle>\<rangle>, \<lambda>[SKIP])}" 
  by (simp add: \<delta>_system)

text \<open>\<open>x := 1\<close>\<close>
lemma "\<delta>\<^sub>s (\<langle>\<circle>\<rangle>, \<lambda>[''x'' := Num 1]) =
       {(\<langle>\<circle>\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 1)] \<circle>\<rrangle>, \<lambda>[\<nabla>])}" 
  by (simp add: \<delta>_system)

text \<open>\<open>IF True THEN x := 1 FI\<close>\<close>
lemma "\<delta>\<^sub>s (\<langle>\<circle>\<rangle>, \<lambda>[IF (Bool True) THEN ''x'' := Num 1 FI]) =
       {(\<langle>\<circle>\<rangle>, \<lambda>[''x'' := Num 1])}" 
  by (simp add: \<delta>_system)

text \<open>\<open>IF False THEN x := 1 FI\<close>\<close>
lemma "\<delta>\<^sub>s (\<langle>\<circle>\<rangle>, \<lambda>[IF (Bool False) THEN ''x'' := Num 1 FI]) =
       {(\<langle>\<circle>\<rangle>, \<lambda>[\<nabla>])}" 
  by (simp add: \<delta>_system)

text \<open>\<open>WHILE True DO x := 2 OD\<close>\<close>
lemma "\<delta>\<^sub>s (\<langle>\<circle>\<rangle>, \<lambda>[WHILE (Bool True) DO ''x'' := Num 2 OD]) =
       {(\<langle>\<circle>\<rangle>, \<lambda>[''x'' := (Num 2);;WHILE (Bool True) DO ''x'' := Num 2 OD])}" 
  by (simp add: \<delta>_system)

text \<open>\<open>WHILE False DO x := 2 OD\<close>\<close>
lemma "\<delta>\<^sub>s (\<langle>\<circle>\<rangle>, \<lambda>[WHILE (Bool False) DO ''x'' := Num 2 OD]) =
       {(\<langle>\<circle>\<rangle>, \<lambda>[\<nabla>])}" 
  by (simp add: \<delta>_system)

text \<open>\<open>x := 1; y := x + 1\<close>\<close>
lemma "\<delta>\<^sub>s (\<langle>\<circle>\<rangle>, \<lambda>[''x'' := Num 1;; ''y'' := (Var ''x'') \<^sub>Aadd (Num 1)]) =
       {(\<langle>\<circle>\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 1)] \<circle>\<rrangle>, \<lambda>[''y'' := (Var ''x'') \<^sub>Aadd (Num 1)])}" 
  by (simp add: \<delta>_system)

text \<open>\<open>CO x := 2 \<parallel> x := 3 OC\<close>\<close>
lemma "\<delta>\<^sub>s (\<langle>\<circle>\<rangle>, \<lambda>[CO ''x'' := Num 2 \<parallel> ''x'' := Num 3 OC]) =
       {(\<langle>\<circle>\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 3)] \<circle>\<rrangle>, \<lambda>[''x'' := Num 2]),
        (\<langle>\<circle>\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 2)] \<circle>\<rrangle>, \<lambda>[''x'' := Num 3])}"
  by (simp add: \<delta>_system) 

text \<open>\<open>CO CO x := 5 \<parallel> x := 6;; SKIP OC \<parallel> x := 4 OC\<close>\<close>
lemma "\<delta>\<^sub>s (\<langle>\<circle>\<rangle>, \<lambda>[CO CO ''x'' := Num 5 \<parallel> ''x'' := Num 6;;SKIP OC \<parallel> ''x'' := Num 4 OC]) =
       {(\<langle>\<circle>\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 4)] \<circle>\<rrangle>, \<lambda>[CO ''x'' := Num 5 \<parallel> ''x'' := Num 6;;SKIP OC]),
        (\<langle>\<circle>\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 6)] \<circle>\<rrangle>, \<lambda>[CO CO ''x'' := Num 5 \<parallel> SKIP OC \<parallel> ''x'' := Num 4 OC]),
        (\<langle>\<circle>\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 5)] \<circle>\<rrangle>, \<lambda>[CO ''x'' := Num 6;;SKIP \<parallel> ''x'' := Num 4 OC])}"
  by (simp add: \<delta>_system) 

text \<open>\<open>\<lbrace> \<nu> SKIP \<rbrace>\<close>\<close>
lemma "\<delta>\<^sub>s (\<langle>\<circle>\<rangle>, \<lambda>[\<lbrace> \<nu> SKIP \<rbrace>]) = {(\<langle>\<circle>\<rangle>, \<lambda>[\<nabla>])}" 
  by (simp add: \<delta>_system)

text \<open>\<open>\<lbrace> (x;(y;\<nu>)) x := 5;; y := 3 \<rbrace>\<close>\<close>
lemma "\<delta>\<^sub>s (\<langle>\<circle>\<rangle>, \<lambda>[\<lbrace> (''x''; (''y''; \<nu>)) ''x'' := Num 5;; ''y'' := Num 3 \<rbrace>]) =
       {(\<langle>\<circle>\<rangle> \<leadsto> State\<llangle>[''$x::Scope'' \<longmapsto> Exp (Num 0)] \<circle>\<rrangle>, \<lambda>[\<lbrace> (''y''; \<nu>) ''$x::Scope'' := Num 5;; ''y'' := Num 3 \<rbrace>])}" 
  by (simp add: \<delta>_system)

text \<open>\<open> INPUT ''x'';; ''x'' := 5\<close>\<close>
lemma "\<delta>\<^sub>s (\<langle>\<circle>\<rangle>, \<lambda>[INPUT ''x'';; ''x'' := Num 5]) =
      {(((\<langle>[''$x::Input'' \<longmapsto> Exp (Num 0)] \<circle>\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 0)] [''$x::Input'' \<longmapsto> Exp (Num 0)] \<circle>\<rrangle>) \<leadsto> Event\<llangle>inpEv, [A (Num 0)]\<rrangle>) \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 0)] [''$x::Input'' \<longmapsto> Exp (Num 0)] \<circle>\<rrangle>, \<lambda>[''x'' := Num 5])}"
  by (simp add: \<delta>_system)

text \<open>\<open>:: (Bool True);; x := 5;; SKIP END\<close>\<close>
lemma "\<delta>\<^sub>s (\<langle>\<circle>\<rangle>, \<lambda>[:: (Bool True);; ''x'' := Num 5;; SKIP END]) =
       {(\<langle>\<circle>\<rangle>, \<lambda>[''x'' := Num 5;; SKIP])}"
  by (simp add: \<delta>_system)

text \<open>\<open>:: (Bool False);; x := 5;; SKIP END\<close>\<close>
lemma "\<delta>\<^sub>s (\<langle>\<circle>\<rangle>, \<lambda>[:: (Bool False);; ''x'' := Num 5;; SKIP END]) = {}"
  by (simp add: \<delta>_system)

text \<open>\<open>CALL foo(2);; SKIP\<close>\<close>
lemma "\<delta>\<^sub>s (\<langle>\<circle>\<rangle>, \<lambda>[CALL ''foo'' (Num 2);; SKIP]) = 
      {((\<langle>\<circle>\<rangle> \<leadsto> Event\<llangle>invEv, [P ''foo'', A (Num 2)]\<rrangle>) \<leadsto> State\<llangle>\<circle>\<rrangle>, \<lambda>[SKIP])}"
  by (simp add: \<delta>_system)

text \<open>\<open>CALL foo(x);; SKIP\<close>\<close>
lemma "\<delta>\<^sub>s (\<langle>[''x'' \<longmapsto> Exp (Num 0)] \<circle>\<rangle>, \<lambda>[CALL ''foo'' (Var ''x'');; SKIP]) = 
      {((\<langle>[''x'' \<longmapsto> Exp (Num 0)] \<circle>\<rangle> \<leadsto> Event\<llangle>invEv, [P ''foo'', A (Num 0)]\<rrangle>) \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 0)] \<circle>\<rrangle>, \<lambda>[SKIP])}"
  by (simp add: \<delta>_system)

context notes [simp] = add_mset_commute begin

text \<open>[\<open>SKIP\<close>]\<close>
lemma "\<delta>\<^sub>1 (\<langle>\<circle>\<rangle>, {# \<lambda>[SKIP] #}) = {(\<langle>\<circle>\<rangle>, {# \<lambda>[\<nabla>] #})}" 
  by (simp add: \<delta>_system) 

text \<open>[\<open>SKIP;SKIP\<close>]\<close>
lemma "\<delta>\<^sub>1 (\<langle>\<circle>\<rangle>, {# \<lambda>[SKIP;;SKIP] #}) = {(\<langle>\<circle>\<rangle>, {# \<lambda>[SKIP] #})}" 
  by (simp add: \<delta>_system) 

text \<open>[\<open>x := 1\<close>]\<close>
lemma "\<delta>\<^sub>1 (\<langle>\<circle>\<rangle>, {# \<lambda>[''x'' := Num 1] #}) =
       {(\<langle>\<circle>\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 1)] \<circle>\<rrangle>, {# \<lambda>[\<nabla>] #})}" 
  by (simp add: \<delta>_system) 

text \<open>[\<open>IF True THEN x := 1 FI\<close>]\<close>
lemma "\<delta>\<^sub>1 (\<langle>\<circle>\<rangle>, {# \<lambda>[IF (Bool True) THEN ''x'' := Num 1 FI] #}) =
       {(\<langle>\<circle>\<rangle>, {# \<lambda>[''x'' := Num 1] #})}" 
  by (simp add: \<delta>_system) 

text \<open>[\<open>IF False THEN x := 1 FI\<close>]\<close>
lemma "\<delta>\<^sub>1 (\<langle>\<circle>\<rangle>, {# \<lambda>[IF (Bool False) THEN ''x'' := Num 1 FI] #}) =
       {(\<langle>\<circle>\<rangle>, {# \<lambda>[\<nabla>] #})}" 
  by (simp add: \<delta>_system) 

text \<open>[\<open>WHILE True DO x := 2 OD\<close>]\<close>
lemma "\<delta>\<^sub>1 (\<langle>\<circle>\<rangle>, {# \<lambda>[WHILE (Bool True) DO ''x'' := Num 2 OD] #}) =
       {(\<langle>\<circle>\<rangle>, {# \<lambda>[''x'' := (Num 2);;WHILE (Bool True) DO ''x'' := Num 2 OD] #})}" 
  by (simp add: \<delta>_system) 

text \<open>[\<open>WHILE False DO x := 2 OD\<close>]\<close>
lemma "\<delta>\<^sub>1 (\<langle>\<circle>\<rangle>, {# \<lambda>[WHILE (Bool False) DO ''x'' := Num 2 OD] #}) =
       {(\<langle>\<circle>\<rangle>, {# \<lambda>[\<nabla>] #})}" 
  by (simp add: \<delta>_system) 

text \<open>[\<open>x := 1; y := x + 1\<close>]\<close>
lemma "\<delta>\<^sub>1 (\<langle>\<circle>\<rangle>, {# \<lambda>[''x'' := Num 1;; ''y'' := (Var ''x'') \<^sub>Aadd (Num 1)] #}) =
       {(\<langle>\<circle>\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 1)] \<circle>\<rrangle>, {# \<lambda>[''y'' := (Var ''x'') \<^sub>Aadd (Num 1)] #})}" 
  by (simp add: \<delta>_system) 

text \<open>\<open>CO CO x := 5 \<parallel> x := 6;; SKIP OC \<parallel> x := 4 OC\<close>\<close>
lemma "\<delta>\<^sub>1 (\<langle>\<circle>\<rangle>, {# \<lambda>[CO CO ''x'' := Num 5 \<parallel> ''x'' := Num 6;;SKIP OC \<parallel> ''x'' := Num 4 OC] #}) =
       {(\<langle>\<circle>\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 4)] \<circle>\<rrangle>, {# \<lambda>[CO ''x'' := Num 5 \<parallel> ''x'' := Num 6;;SKIP OC] #}),
        (\<langle>\<circle>\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 6)] \<circle>\<rrangle>, {# \<lambda>[CO CO ''x'' := Num 5 \<parallel> SKIP OC \<parallel> ''x'' := Num 4 OC] #}),
        (\<langle>\<circle>\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 5)] \<circle>\<rrangle>, {# \<lambda>[CO ''x'' := Num 6;;SKIP \<parallel> ''x'' := Num 4 OC] #})}"
  by (simp add: \<delta>_system) 

text \<open>\<open>\<lbrace> \<nu> SKIP \<rbrace>\<close>\<close>
lemma "\<delta>\<^sub>1 (\<langle>\<circle>\<rangle>, {# \<lambda>[\<lbrace> \<nu> SKIP \<rbrace>] #}) = {(\<langle>\<circle>\<rangle>, {# \<lambda>[\<nabla>] #})}" 
  by (simp add: \<delta>_system)

text \<open>\<open>\<lbrace> (x;(y;\<nu>)) x := 5;; y := 3 \<rbrace>\<close>\<close>
lemma "\<delta>\<^sub>1 (\<langle>\<circle>\<rangle>, {# \<lambda>[\<lbrace> (''x''; (''y''; \<nu>)) ''x'' := Num 5;; ''y'' := Num 3 \<rbrace>] #}) =
       {(\<langle>\<circle>\<rangle> \<leadsto> State\<llangle>[''$x::Scope'' \<longmapsto> Exp (Num 0)] \<circle>\<rrangle>, {# \<lambda>[\<lbrace> (''y''; \<nu>) ''$x::Scope'' := Num 5;; ''y'' := Num 3 \<rbrace>] #})}" 
  by (simp add: \<delta>_system)

text \<open>\<open> INPUT ''x'';; ''x'' := 5\<close>\<close>
lemma "\<delta>\<^sub>1 (\<langle>\<circle>\<rangle>, {# \<lambda>[INPUT ''x'';; ''x'' := Num 5] #}) =
      {(((\<langle>[''$x::Input'' \<longmapsto> Exp (Num 0)] \<circle>\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 0)] [''$x::Input'' \<longmapsto> Exp (Num 0)] \<circle>\<rrangle>) \<leadsto> Event\<llangle>inpEv, [A (Num 0)]\<rrangle>) \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 0)] [''$x::Input'' \<longmapsto> Exp (Num 0)] \<circle>\<rrangle>, {# \<lambda>[''x'' := Num 5] #})}"
  by (simp add: \<delta>_system)

text \<open>\<open>:: (Bool True);; x := 5;; SKIP END\<close>\<close>
lemma "\<delta>\<^sub>1 (\<langle>\<circle>\<rangle>, {# \<lambda>[:: (Bool True);; ''x'' := Num 5;; SKIP END] #}) =
       {(\<langle>\<circle>\<rangle>, {# \<lambda>[''x'' := Num 5;; SKIP] #})}"
  by (simp add: \<delta>_system)

text \<open>\<open>:: (Bool False);; x := 5;; SKIP END\<close>\<close>
lemma "\<delta>\<^sub>1 (\<langle>\<circle>\<rangle>, {# \<lambda>[:: (Bool False);; ''x'' := Num 5;; SKIP END] #}) = {}"
  by (simp add: \<delta>_system)

text \<open>\<open>CALL foo(2);; SKIP\<close>\<close>
lemma "\<delta>\<^sub>1 (\<langle>\<circle>\<rangle>, {# \<lambda>[CALL ''foo'' (Num 2);; SKIP] #}) = 
      {((\<langle>\<circle>\<rangle> \<leadsto> Event\<llangle>invEv, [P ''foo'', A (Num 2)]\<rrangle>) \<leadsto> State\<llangle>\<circle>\<rrangle>, {# \<lambda>[SKIP] #})}"
  by (simp add: \<delta>_system)

text \<open>\<open>CALL foo(x);; SKIP\<close>\<close>
lemma "\<delta>\<^sub>1 (\<langle>[''x'' \<longmapsto> Exp (Num 0)] \<circle>\<rangle>, {# \<lambda>[CALL ''foo'' (Var ''x'');; SKIP] #}) = 
      {((\<langle>[''x'' \<longmapsto> Exp (Num 0)] \<circle>\<rangle> \<leadsto> Event\<llangle>invEv, [P ''foo'', A (Num 0)]\<rrangle>) \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 0)] \<circle>\<rrangle>, {# \<lambda>[SKIP] #})}"
  by (simp add: \<delta>_system)

text \<open>[] [\<open>SKIP\<close>]\<close>
lemma "\<delta>\<^sub>1 (\<langle>\<circle>\<rangle>, {# \<lambda>[\<nabla>], \<lambda>[SKIP] #}) =
       {(\<langle>\<circle>\<rangle>, {# \<lambda>[\<nabla>], \<lambda>[\<nabla>] #})}" 
  by (simp add: \<delta>_system) 

text \<open>[\<open>SKIP\<close>] [\<open>SKIP\<close>]\<close>
lemma "\<delta>\<^sub>1 (\<langle>\<circle>\<rangle>, {# \<lambda>[SKIP], \<lambda>[SKIP] #}) =
       {(\<langle>\<circle>\<rangle>, {# \<lambda>[SKIP], \<lambda>[\<nabla>] #})}" 
  by (simp add: \<delta>_system) 

text \<open>[\<open>x := 1\<close>] [\<open>y := 1 + 1\<close>]\<close>
lemma "\<delta>\<^sub>1 (\<langle>\<circle>\<rangle>, {# \<lambda>[''x'' := Num 1], \<lambda>[''y'' := (Num 1) \<^sub>Aadd (Num 1)] #}) =
       {(\<langle>\<circle>\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 1)] \<circle>\<rrangle>, {# \<lambda>[\<nabla>], \<lambda>[''y'' := (Num 1) \<^sub>Aadd (Num 1)] #}),
        (\<langle>\<circle>\<rangle> \<leadsto> State\<llangle>[''y'' \<longmapsto> Exp (Num 2)] \<circle>\<rrangle>, {# \<lambda>[\<nabla>], \<lambda>[''x'' := Num 1] #})}" 
  by (simp add: \<delta>_system)

text \<open>[\<open>CO x := 2 \<parallel> x := 3 OC\<close>] [\<open>:: False;; x := 5 END\<close>]\<close>
lemma "\<delta>\<^sub>1 (\<langle>\<circle>\<rangle>, {# \<lambda>[CO ''x'' := Num 2 \<parallel> ''x'' := Num 3 OC], \<lambda>[:: (Bool False);; ''x'' := Num 5 END] #}) =
       {(\<langle>\<circle>\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 2)] \<circle>\<rrangle>, {# \<lambda>[''x'' := Num 3], \<lambda>[:: (Bool False);; ''x'' := Num 5 END] #}),
        (\<langle>\<circle>\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 3)] \<circle>\<rrangle>, {# \<lambda>[''x'' := Num 2], \<lambda>[:: (Bool False);; ''x'' := Num 5 END] #})}" 
  by (simp add: \<delta>_system) 

text \<open>[\<open>CO x := 2 \<parallel> x := 3 OC\<close>] [\<open>CO y := 0 \<parallel> SKIP OC\<close>]\<close>
lemma "\<delta>\<^sub>1 (\<langle>\<circle>\<rangle>, {# \<lambda>[CO ''x'' := Num 2 \<parallel> ''x'' := Num 3 OC], \<lambda>[CO ''y'' := Num 0 \<parallel> SKIP OC] #}) =
       {(\<langle>\<circle>\<rangle>, {# \<lambda>[CO ''x'' := Num 2 \<parallel> ''x'' := Num 3 OC], \<lambda>[''y'' := Num 0] #}),
        (\<langle>\<circle>\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 2)] \<circle>\<rrangle>, {# \<lambda>[''x'' := Num 3], \<lambda>[CO ''y'' := Num 0 \<parallel> SKIP OC] #}),
        (\<langle>\<circle>\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 3)] \<circle>\<rrangle>, {# \<lambda>[''x'' := Num 2], \<lambda>[CO ''y'' := Num 0 \<parallel> SKIP OC] #}),
        (\<langle>\<circle>\<rangle> \<leadsto> State\<llangle>[''y'' \<longmapsto> Exp (Num 0)] \<circle>\<rrangle>, {# \<lambda>[CO ''x'' := Num 2 \<parallel> ''x'' := Num 3 OC], \<lambda>[SKIP] #})}" 
  by (simp add: \<delta>_system)

text \<open>[\<open>INPUT x\<close>] [\<open>INPUT y;; SKIP\<close>]\<close>
lemma "\<delta>\<^sub>1 (\<langle>\<circle>\<rangle>, {# \<lambda>[INPUT ''x''], \<lambda>[INPUT ''y'';; SKIP] #}) =
       {(((\<langle>[''$x::Input'' \<longmapsto> Exp (Num 0)] \<circle>\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 0)] [''$x::Input'' \<longmapsto> Exp (Num 0)] \<circle>\<rrangle>) \<leadsto> Event\<llangle>inpEv, [A (Num 0)]\<rrangle>) \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 0)] [''$x::Input'' \<longmapsto> Exp (Num 0)] \<circle>\<rrangle>, {# \<lambda>[\<nabla>], \<lambda>[INPUT ''y'';; SKIP] #}),
        (((\<langle>[''$y::Input'' \<longmapsto> Exp (Num 0)] \<circle>\<rangle> \<leadsto> State\<llangle>[''y'' \<longmapsto> Exp (Num 0)] [''$y::Input'' \<longmapsto> Exp (Num 0)] \<circle>\<rrangle>) \<leadsto> Event\<llangle>inpEv, [A (Num 0)]\<rrangle>) \<leadsto> State\<llangle>[''y'' \<longmapsto> Exp (Num 0)] [''$y::Input'' \<longmapsto> Exp (Num 0)] \<circle>\<rrangle>, {# \<lambda>[INPUT ''x''], \<lambda>[SKIP] #})}" 
  by (simp add: \<delta>_system) 

text \<open>[\<open>\<lbrace> (x; \<nu>) :: (x \<le> y);; x := y \<rbrace>\<close>] [\<open>CALL foo(2)\<close>]\<close>
lemma "\<delta>\<^sub>1 (\<langle>\<circle>\<rangle>, {# \<lambda>[\<lbrace>(''x''; \<nu>) (:: ((Var ''x'') \<^sub>Rleq (Var ''y''));; ''x'' := Var ''y'' END)\<rbrace>], \<lambda>[CALL ''foo'' (Num 2)] #}) =
       {(\<langle>\<circle>\<rangle> \<leadsto> State\<llangle>[''$x::Scope'' \<longmapsto> Exp (Num 0)] \<circle>\<rrangle>, {# \<lambda>[\<lbrace> \<nu> (:: ((Var ''$x::Scope'') \<^sub>Rleq (Var ''y''));; ''$x::Scope'' := Var ''y'' END)\<rbrace>], \<lambda>[CALL ''foo'' (Num 2)] #}),
        ((\<langle>\<circle>\<rangle> \<leadsto> Event\<llangle>invEv, [P ''foo'', A (Num 2)]\<rrangle>) \<leadsto> State\<llangle>\<circle>\<rrangle>, {# \<lambda>[\<lbrace>(''x''; \<nu>) (:: ((Var ''x'') \<^sub>Rleq (Var ''y''));; ''x'' := Var ''y'' END)\<rbrace>], \<lambda>[\<nabla>] #})}" 
  by (simp add: \<delta>_system)

lemma "wellformed \<epsilon>" 
  by simp

lemma "wellformed ((\<langle>\<circle>\<rangle> \<leadsto> Event\<llangle>invEv, [A (Num 2)]\<rrangle>) \<leadsto> State\<llangle>\<circle>\<rrangle>)"
  by simp

lemma "wellformed (((\<langle>\<circle>\<rangle> \<leadsto> Event\<llangle>invEv, [A (Num 2)]\<rrangle>) \<leadsto> State\<llangle>\<circle>\<rrangle>) \<leadsto> Event\<llangle>invREv, [A (Num 2)]\<rrangle>)"
  by simp

lemma "\<not>wellformed ((\<langle>\<circle>\<rangle> \<leadsto> Event\<llangle>invREv, [A (Num 2)]\<rrangle>) \<leadsto> State\<llangle>\<circle>\<rrangle>)"
  by simp

lemma "params (\<langle>\<circle>\<rangle> \<leadsto> Event\<llangle>inpEv, [A (Num 2)]\<rrangle>) = {}"
  by simp

lemma "params (\<langle>\<circle>\<rangle> \<leadsto> Event\<llangle>invEv, [P ''foo'', A (Num 2)]\<rrangle>) = {A (Num 2)}"
  by simp

text \<open>[\<open>SKIP;; SKIP\<close>] [\<open>x := 5\<close>]\<close>
lemma "\<delta>\<^sub>2 {
         (Method ''foo'' ''x'' \<lbrace> SKIP \<rbrace>)
       } (\<langle>\<circle>\<rangle>, {# \<lambda>[SKIP;;SKIP], \<lambda>[''x'' := Num 5] #}) = {}"
  by (simp add: \<delta>_system)

text \<open>[\<open>SKIP;; SKIP\<close>]\<close>
lemma "\<delta>\<^sub>2 {
         (Method ''foo'' ''x'' \<lbrace> ''x'' := Num 5 \<rbrace>),
         (Method ''bar'' ''y'' \<lbrace> ''y'' := Num 5 \<rbrace>)
       } ((\<langle>\<circle>\<rangle> \<leadsto> Event\<llangle>invEv, [P ''foo'', A (Num 2)]\<rrangle>) \<leadsto> State\<llangle>\<circle>\<rrangle>, {# \<lambda>[SKIP;;SKIP] #}) =
       {(((((\<langle>\<circle>\<rangle> \<leadsto> Event\<llangle>invEv, [P ''foo'', A (Num 2)]\<rrangle>) \<leadsto> State\<llangle>\<circle>\<rrangle>) \<leadsto> Event\<llangle>invREv, [P ''foo'', A (Num 2)]\<rrangle>) \<leadsto> State\<llangle>\<circle>\<rrangle>) \<leadsto> State\<llangle>[''$foo::Param'' \<longmapsto> Exp (Num 2)] \<circle>\<rrangle>, {# \<lambda>[SKIP;;SKIP], \<lambda>[''$foo::Param'' := Num 5] #})}"
  by (simp add: \<delta>_system)


text \<open>[\<open>\<nabla>\<close>]\<close>
lemma "\<delta>\<^sub>2 {
         (Method ''foo'' ''x'' \<lbrace> ''x'' := Num 5 \<rbrace>)
       } ((\<langle>\<circle>\<rangle> \<leadsto> Event\<llangle>invEv, [P ''foo'', A (Num 2)]\<rrangle>) \<leadsto> State\<llangle>\<circle>\<rrangle>, {# \<lambda>[\<nabla>] #}) =
       {(((((\<langle>\<circle>\<rangle> \<leadsto> Event\<llangle>invEv, [P ''foo'', A (Num 2)]\<rrangle>) \<leadsto> State\<llangle>\<circle>\<rrangle>) \<leadsto> Event\<llangle>invREv, [P ''foo'', A (Num 2)]\<rrangle>) \<leadsto> State\<llangle>\<circle>\<rrangle>) \<leadsto> State\<llangle>[''$foo::Param'' \<longmapsto> Exp (Num 2)] \<circle>\<rrangle>, {# \<lambda>[\<nabla>], \<lambda>[''$foo::Param'' := Num 5] #})}"
  by (simp add: \<delta>_system)

text \<open>[\<open>SKIP;; SKIP\<close>] [\<open>x := 5\<close>]\<close>
lemma "\<delta> {
         (Method ''foo'' ''x'' \<lbrace> SKIP \<rbrace>)
       } (\<langle>\<circle>\<rangle>, {# \<lambda>[SKIP;;SKIP], \<lambda>[''x'' := Num 5] #}) = 
       {(\<langle>\<circle>\<rangle>, {# \<lambda>[SKIP], \<lambda>[''x'' := Num 5] #}),
        (\<langle>\<circle>\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 5)] \<circle>\<rrangle>, {# \<lambda>[SKIP;;SKIP], \<lambda>[\<nabla>] #})}"
  by (simp add: \<delta>_system)

text \<open>[\<open>SKIP;; SKIP\<close>]\<close>
lemma "\<delta> {
         (Method ''foo'' ''x'' \<lbrace> ''x'' := Num 5 \<rbrace>)
       } ((\<langle>\<circle>\<rangle> \<leadsto> Event\<llangle>invEv, [P ''foo'', A (Num 2)]\<rrangle>) \<leadsto> State\<llangle>\<circle>\<rrangle>, {# \<lambda>[SKIP;;SKIP] #}) =
       {(((((\<langle>\<circle>\<rangle> \<leadsto> Event\<llangle>invEv, [P ''foo'', A (Num 2)]\<rrangle>) \<leadsto> State\<llangle>\<circle>\<rrangle>) \<leadsto> Event\<llangle>invREv, [P ''foo'', A (Num 2)]\<rrangle>) \<leadsto> State\<llangle>\<circle>\<rrangle>) \<leadsto> State\<llangle>[''$foo::Param'' \<longmapsto> Exp (Num 2)] \<circle>\<rrangle>, {# \<lambda>[SKIP;;SKIP], \<lambda>[''$foo::Param'' := Num 5] #}),
        ((\<langle>\<circle>\<rangle> \<leadsto> Event\<llangle>invEv, [P ''foo'', A (Num 2)]\<rrangle>) \<leadsto> State\<llangle>\<circle>\<rrangle>, {# \<lambda>[SKIP] #})}"
  by (simp add: \<delta>_system)

text \<open>[\<open>\<nabla>\<close>]\<close>
lemma "\<delta> {
         (Method ''foo'' ''x'' \<lbrace> ''x'' := Num 5 \<rbrace>)
       } ((\<langle>\<circle>\<rangle> \<leadsto> Event\<llangle>invEv, [P ''foo'', A (Num 2)]\<rrangle>) \<leadsto> State\<llangle>\<circle>\<rrangle>, {# \<lambda>[\<nabla>] #}) =
       {(((((\<langle>\<circle>\<rangle> \<leadsto> Event\<llangle>invEv, [P ''foo'', A (Num 2)]\<rrangle>) \<leadsto> State\<llangle>\<circle>\<rrangle>) \<leadsto> Event\<llangle>invREv, [P ''foo'', A (Num 2)]\<rrangle>) \<leadsto> State\<llangle>\<circle>\<rrangle>) \<leadsto> State\<llangle>[''$foo::Param'' \<longmapsto> Exp (Num 2)] \<circle>\<rrangle>, {# \<lambda>[\<nabla>], \<lambda>[''$foo::Param'' := Num 5] #})}"
  by (simp add: \<delta>_system)

end



subsection \<open>Global Trace Semantics\<close>

text \<open>\<open>SKIP\<close>\<close>
lemma "\<Delta>\<^sub>N 1000 {} (\<langle>\<circle>\<rangle>, {# \<lambda>[SKIP] #}) = {(\<langle>\<circle>\<rangle>, {# \<lambda>[\<nabla>] #})}"
  by (simp add: \<delta>_system \<Delta>\<^sub>N_system)

text \<open>\<open>SKIP; x := 2\<close>\<close>
lemma "\<Delta>\<^sub>N 1000 {} (\<langle>\<circle>\<rangle>, {# \<lambda>[SKIP;;''x'' := Num 2] #}) = {(\<langle>\<circle>\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 2)] \<circle>\<rrangle>, {# \<lambda>[\<nabla>] #})}"
  by (simp add: \<delta>_system \<Delta>\<^sub>N_system)

text \<open>\<open>SKIP; SKIP; SKIP\<close>\<close>
lemma "\<Delta>\<^sub>N 1000 {} (\<langle>\<circle>\<rangle>, {# \<lambda>[SKIP;;(SKIP;;SKIP)] #}) = {(\<langle>\<circle>\<rangle>, {# \<lambda>[\<nabla>] #})}"
  by (simp add: \<delta>_system \<Delta>\<^sub>N_system)

text \<open>\<open>x := 1; x := x + 1\<close>\<close>
lemma "\<Delta>\<^sub>N 1000 {} (\<langle>\<circle>\<rangle>, {# \<lambda>[''x'' := Num 1;; ''x'' := (Var ''x'') \<^sub>Aadd (Num 1)] #}) =
       {((\<langle>\<circle>\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 1)] \<circle>\<rrangle>) \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 2)] \<circle>\<rrangle>, {# \<lambda>[\<nabla>] #})}" 
  by (simp add: \<delta>_system \<Delta>\<^sub>N_system)

text \<open>\<open>x := 1; y := x + 1\<close> [\<open>Example 3.4\<close>]\<close>
lemma "\<Delta>\<^sub>N 1000 {} (\<langle>\<sigma>\<^sub>A\<rangle>, {# \<lambda>[''x'' := Num 1;; ''y'' := (Var ''x'') \<^sub>Aadd (Num 1)] #}) =
       {((\<langle>\<sigma>\<^sub>A\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 1)] \<sigma>\<^sub>A\<rrangle>) \<leadsto> State\<llangle>[''y'' \<longmapsto> Exp (Num 2)] [''x'' \<longmapsto> Exp (Num 1)] \<sigma>\<^sub>A\<rrangle>, {# \<lambda>[\<nabla>] #})}" 
  by (simp add: \<delta>_system \<Delta>\<^sub>N_system \<sigma>\<^sub>A_def)

text \<open>\<open>x := 0; IF (x == 0) THEN x := 2 FI\<close>\<close>
lemma "\<Delta>\<^sub>N 1000 {} (\<langle>\<circle>\<rangle>, {# \<lambda>[''x'' := Num 0;;IF ((Var ''x'') \<^sub>Req (Num 0)) THEN ''x'' := Num 2 FI] #}) =
       {((\<langle>\<circle>\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 0)] \<circle>\<rrangle>) \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 2)] \<circle>\<rrangle>, {# \<lambda>[\<nabla>] #})}"
  by (simp add: \<delta>_system \<Delta>\<^sub>N_system)

text \<open>\<open>x := 0; WHILE (x == 0) DO x := 2 OD\<close>\<close>
lemma "\<Delta>\<^sub>N 1000 {} (\<langle>\<circle>\<rangle>, {# \<lambda>[''x'' := Num 0;;WHILE ((Var ''x'') \<^sub>Req (Num 0)) DO ''x'' := Num 2 OD] #}) =
       {((\<langle>\<circle>\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 0)] \<circle>\<rrangle>) \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 2)] \<circle>\<rrangle>, {# \<lambda>[\<nabla>] #})}"
  by (simp add: \<delta>_system \<Delta>\<^sub>N_system)

text \<open>\<open>x := 4;; y := 1;; WHILE (x \<ge> 2) DO y := y * x;; x := x - 1 OD\<close>\<close>
lemma "\<Delta>\<^sub>N 1000 {} (\<langle>\<circle>\<rangle>, {# \<lambda>[(''x'' := Num 4;; ''y'' := Num 1);; 
              WHILE ((Var ''x'') \<^sub>Rgeq (Num 2)) 
                 DO ''y'' := (Var ''y'') \<^sub>Amul (Var ''x'');; 
                    ''x'' := (Var ''x'') \<^sub>Asub (Num 1) 
                 OD] #}) =
      {((((((((\<langle>fm []\<rangle> \<leadsto> State \<llangle>fm [(''x'', Exp (Num 4))]\<rrangle>) \<leadsto>
         State \<llangle>fm [(''x'', Exp (Num 4)), (''y'', Exp (Num 1))]\<rrangle>) \<leadsto>
        State \<llangle>fm [(''x'', Exp (Num 4)), (''y'', Exp (Num 4))]\<rrangle>) \<leadsto>
       State \<llangle>fm [(''x'', Exp (Num 3)), (''y'', Exp (Num 4))]\<rrangle>) \<leadsto>
      State \<llangle>fm [(''x'', Exp (Num 3)), (''y'', Exp (Num 12))]\<rrangle>) \<leadsto>
     State \<llangle>fm [(''x'', Exp (Num 2)), (''y'', Exp (Num 12))]\<rrangle>) \<leadsto>
    State \<llangle>fm [(''x'', Exp (Num 2)), (''y'', Exp (Num 24))]\<rrangle>) \<leadsto>
   State \<llangle>fm [(''x'', Exp (Num 1)), (''y'', Exp (Num 24))]\<rrangle>,
   {# \<lambda>[\<nabla>] #})}"
  by (simp add: \<delta>_system \<Delta>\<^sub>N_system)

text \<open>\<open>CO CO x := 5 \<parallel> x := 6 OC \<parallel> x := 5;; SKIP OC;; x := 8\<close>\<close>
lemma "\<Delta>\<^sub>N 1000 {} (\<langle>\<circle>\<rangle>, {# \<lambda>[CO CO ''x'' := Num 5 \<parallel> ''x'' := Num 6 OC \<parallel> ''x'' := Num 5;; SKIP OC;; ''x'' := Num 8] #}) =
       {((((\<langle>fm []\<rangle> \<leadsto> State \<llangle>fm [(''x'', Exp (Num 5))]\<rrangle>) \<leadsto> 
          State \<llangle>fm [(''x'', Exp (Num 6))]\<rrangle>) \<leadsto>
         State \<llangle>fm [(''x'', Exp (Num 5))]\<rrangle>) \<leadsto> 
        State \<llangle>fm [(''x'', Exp (Num 8))]\<rrangle>, {# \<lambda>[\<nabla>] #}),
       ((((\<langle>fm []\<rangle> \<leadsto> State \<llangle>fm [(''x'', Exp (Num 5))]\<rrangle>) \<leadsto> 
          State \<llangle>fm [(''x'', Exp (Num 5))]\<rrangle>) \<leadsto>
         State \<llangle>fm [(''x'', Exp (Num 6))]\<rrangle>) \<leadsto>
        State \<llangle>fm [(''x'', Exp (Num 8))]\<rrangle>, {# \<lambda>[\<nabla>] #}),
       ((((\<langle>fm []\<rangle> \<leadsto> State \<llangle>fm [(''x'', Exp (Num 6))]\<rrangle>) \<leadsto> 
          State \<llangle>fm [(''x'', Exp (Num 5))]\<rrangle>) \<leadsto>
         State \<llangle>fm [(''x'', Exp (Num 5))]\<rrangle>) \<leadsto>
        State \<llangle>fm [(''x'', Exp (Num 8))]\<rrangle>, {# \<lambda>[\<nabla>] #})}"
  by (simp add: \<delta>_system \<Delta>\<^sub>N_system)

text \<open>\<open>y := 5;; \<lbrace> (x;\<nu>) x := 2;; \<lbrace> (x;\<nu>) x := 1 \<rbrace>;; y := x \<rbrace> x := y\<close>\<close>
lemma "\<Delta>\<^sub>N 1000 {} (\<langle>\<circle>\<rangle>, {# \<lambda>[(''y'' := Num 5;; \<lbrace> (''x'';\<nu>) (''x'' := Num 2;; \<lbrace> (''x'';\<nu>) ''x'' := Num 1 \<rbrace>);; ''y'' := Var ''x'' \<rbrace>);; ''x'' := Var ''y''] #}) =
      {(((((((\<langle>fm []\<rangle> \<leadsto> State \<llangle>fm [(''y'', Exp (Num 5))]\<rrangle>) \<leadsto>
            State \<llangle>fm [(''y'', Exp (Num 5)), (''$x::Scope'', Exp (Num 0))]\<rrangle>) \<leadsto>
           State \<llangle>fm [(''y'', Exp (Num 5)), (''$x::Scope'', Exp (Num 2))]\<rrangle>) \<leadsto>
          State \<llangle>fm [(''y'', Exp (Num 5)), (''$x::Scope'', Exp (Num 2)), (''$$x::Scope::Scope'', Exp (Num 0))]\<rrangle>) \<leadsto>
         State \<llangle>fm [(''y'', Exp (Num 5)), (''$x::Scope'', Exp (Num 2)), (''$$x::Scope::Scope'', Exp (Num 1))]\<rrangle>) \<leadsto>
        State \<llangle>fm [(''y'', Exp (Num 2)), (''$x::Scope'', Exp (Num 2)), (''$$x::Scope::Scope'', Exp (Num 1))]\<rrangle>) \<leadsto>
       State \<llangle>fm [(''y'', Exp (Num 2)), (''$x::Scope'', Exp (Num 2)), (''$$x::Scope::Scope'', Exp (Num 1)), (''x'', Exp (Num 2))]\<rrangle>, {# \<lambda>[\<nabla>] #})}"
  by (simp add: \<delta>_system \<Delta>\<^sub>N_system)

text \<open>\<open>x := 5;; \<lbrace> (x;\<nu>) x := 2 \<rbrace> \<lbrace> (x;\<nu>) x := 1 \<rbrace>\<close>\<close>
lemma "\<Delta>\<^sub>N 1000 {} (\<langle>\<circle>\<rangle>, {# \<lambda>[(''x'' := Num 5;; \<lbrace> (''x''; \<nu>) ''x'' := Num 2 \<rbrace>);; \<lbrace> (''x''; \<nu>) ''x'' := Num 1 \<rbrace>] #}) =
       {(((((\<langle>fm []\<rangle> \<leadsto> State \<llangle>fm [(''x'', Exp (Num 5))]\<rrangle>) \<leadsto>
           State \<llangle>fm [(''x'', Exp (Num 5)), (''$x::Scope'', Exp (Num 0))]\<rrangle>) \<leadsto>
          State \<llangle>fm [(''x'', Exp (Num 5)), (''$x::Scope'', Exp (Num 2))]\<rrangle>) \<leadsto>
         State \<llangle>fm [(''x'', Exp (Num 5)), (''$x::Scope'', Exp (Num 2)), (''c$x::Scope'', Exp (Num 0))]\<rrangle>) \<leadsto>
        State \<llangle>fm [(''x'', Exp (Num 5)), (''$x::Scope'', Exp (Num 2)), (''c$x::Scope'', Exp (Num 1))]\<rrangle>, {# \<lambda>[\<nabla>] #})}"
  by (simp add: \<delta>_system \<Delta>\<^sub>N_system)

text \<open>\<open>x := 5;; Input x\<close>\<close>
lemma "\<Delta>\<^sub>N 1000 {} (\<langle>\<circle>\<rangle>, {# \<lambda>[''x'' := Num 5;; INPUT ''x''] #}) =
       {((((\<langle>fm [(''$x::Input'', Exp (Num 0))]\<rangle> \<leadsto>
      State \<llangle>fm [(''x'', Exp (Num 5)), (''$x::Input'', Exp (Num 0))]\<rrangle>) \<leadsto>
     State \<llangle>fm [(''x'', Exp (Num 0)), (''$x::Input'', Exp (Num 0))]\<rrangle>) \<leadsto>
    Event \<llangle>inpEv,[A (Num 0)]\<rrangle>) \<leadsto>
   State \<llangle>fm [(''x'', Exp (Num 0)), (''$x::Input'', Exp (Num 0))]\<rrangle>, {# \<lambda>[\<nabla>] #})}"
  by (simp add: \<delta>_system \<Delta>\<^sub>N_system)

text \<open>\<open>x := 5;; Input x;; x := x + 1;; Input x\<close>\<close>
lemma "\<Delta>\<^sub>N 1000 {} (\<langle>\<circle>\<rangle>, {# \<lambda>[((''x'' := Num 5;; INPUT ''x'');; ''x'' := ((Var ''x'') \<^sub>Aadd (Num 1)));; INPUT ''x''] #}) =
      {((((((((\<langle>fm [(''$x::Input'', Exp (Num 0)), (''c$x::Input'', Exp (Num 0))]\<rangle> \<leadsto>
              State \<llangle>fm [(''x'', Exp (Num 5)), (''$x::Input'', Exp (Num 0)), (''c$x::Input'', Exp (Num 0))]\<rrangle>) \<leadsto>
             State \<llangle>fm [(''x'', Exp (Num 0)), (''$x::Input'', Exp (Num 0)), (''c$x::Input'', Exp (Num 0))]\<rrangle>) \<leadsto>
            Event \<llangle>inpEv,[A (Num 0)]\<rrangle>) \<leadsto>
           State \<llangle>fm [(''x'', Exp (Num 0)), (''$x::Input'', Exp (Num 0)), (''c$x::Input'', Exp (Num 0))]\<rrangle>) \<leadsto>
          State \<llangle>fm [(''x'', Exp (Num 1)), (''$x::Input'', Exp (Num 0)), (''c$x::Input'', Exp (Num 0))]\<rrangle>) \<leadsto>
        State \<llangle>fm [(''x'', Exp (Num 0)), (''$x::Input'', Exp (Num 0)), (''c$x::Input'', Exp (Num 0))]\<rrangle>) \<leadsto>
       Event \<llangle>inpEv,[A (Num 0)]\<rrangle>) \<leadsto>
      State \<llangle>fm [(''x'', Exp (Num 0)), (''$x::Input'', Exp (Num 0)), (''c$x::Input'', Exp (Num 0))]\<rrangle>, {# \<lambda>[\<nabla>] #})}"
  by (simp add: \<delta>_system \<Delta>\<^sub>N_system)

text \<open>\<open>x := 0;; CO :: x \<le> 5;; x := 6 \<parallel> x := 6 OC\<close>\<close>
lemma "\<Delta>\<^sub>N 10 {} (\<langle>\<circle>\<rangle>, {# \<lambda>[''x'' := Num 0;; CO :: ((Var ''x'') \<^sub>Rleq (Num 5));; ''x'' := Num 6 END \<parallel> ''x'' := Num 6 OC] #}) =
      {(((\<langle>fm []\<rangle> \<leadsto> State \<llangle>fm [(''x'', Exp (Num 0))]\<rrangle>) \<leadsto> State \<llangle>fm [(''x'', Exp (Num 6))]\<rrangle>) \<leadsto>
           State \<llangle>fm [(''x'', Exp (Num 6))]\<rrangle>, {# \<lambda>[\<nabla>] #}),
       ((\<langle>fm []\<rangle> \<leadsto> State \<llangle>fm [(''x'', Exp (Num 0))]\<rrangle>) \<leadsto> State \<llangle>fm [(''x'', Exp (Num 6))]\<rrangle>, {# \<lambda>[:: ((Var ''x'') \<^sub>Rleq (Num 5));; ''x'' := Num 6 END] #})}"
  by (simp add: \<delta>_system \<Delta>\<^sub>N_system)

text \<open>\<open>SKIP\<close>\<close>
lemma "Tr\<^sub>N (Program [] \<lbrace> SKIP \<rbrace>) \<circle> 1000 = {\<langle>\<circle>\<rangle>}"
  by (simp add: \<delta>_system \<Delta>\<^sub>N_system)

text \<open>\<open>SKIP; x := 2\<close>\<close>
lemma "Tr\<^sub>N (Program [] \<lbrace> SKIP;;''x'' := Num 2 \<rbrace>) \<circle> 1000 = {\<langle>\<circle>\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 2)] \<circle>\<rrangle>}"
  by (simp add: \<delta>_system \<Delta>\<^sub>N_system)

text \<open>\<open>SKIP; SKIP; SKIP\<close>\<close>
lemma "Tr\<^sub>N (Program [] \<lbrace> SKIP;;(SKIP;;SKIP) \<rbrace>) \<circle> 1000 = {\<langle>\<circle>\<rangle>}"
  by (simp add: \<delta>_system \<Delta>\<^sub>N_system)

text \<open>\<open>x := 1; x := x + 1\<close>\<close>
lemma "Tr\<^sub>N (Program [] \<lbrace> ''x'' := Num 1;; ''x'' := (Var ''x'') \<^sub>Aadd (Num 1) \<rbrace>) \<circle> 1000 =
       {(\<langle>\<circle>\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 1)] \<circle>\<rrangle>) \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 2)] \<circle>\<rrangle>}" 
  by (simp add: \<delta>_system \<Delta>\<^sub>N_system)

text \<open>\<open>x := 1; y := x + 1\<close> [\<open>Example 3.4\<close>]\<close>
lemma "Tr\<^sub>N (Program [] \<lbrace> ''x'' := Num 1;; ''y'' := (Var ''x'') \<^sub>Aadd (Num 1) \<rbrace>) \<sigma>\<^sub>A 1000 =
       {(\<langle>\<sigma>\<^sub>A\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 1)] \<sigma>\<^sub>A\<rrangle>) \<leadsto> State\<llangle>[''y'' \<longmapsto> Exp (Num 2)] [''x'' \<longmapsto> Exp (Num 1)] \<sigma>\<^sub>A\<rrangle>}" 
  by (simp add: \<delta>_system \<Delta>\<^sub>N_system \<sigma>\<^sub>A_def)

text \<open>\<open>x := 0; IF (x == 0) THEN x := 2 FI\<close>\<close>
lemma "Tr\<^sub>N (Program [] \<lbrace> ''x'' := Num 0;;IF ((Var ''x'') \<^sub>Req (Num 0)) THEN ''x'' := Num 2 FI \<rbrace>) \<circle> 1000 =
       {(\<langle>\<circle>\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 0)] \<circle>\<rrangle>) \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 2)] \<circle>\<rrangle>}"
  by (simp add: \<delta>_system \<Delta>\<^sub>N_system)

text \<open>\<open>x := 0; WHILE (x == 0) DO x := 2 OD\<close>\<close>
lemma "Tr\<^sub>N (Program [] \<lbrace> ''x'' := Num 0;;WHILE ((Var ''x'') \<^sub>Req (Num 0)) DO ''x'' := Num 2 OD \<rbrace>) \<circle> 1000 =
       {(\<langle>\<circle>\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 0)] \<circle>\<rrangle>) \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 2)] \<circle>\<rrangle>}"
  by (simp add: \<delta>_system \<Delta>\<^sub>N_system)

text \<open>\<open>x := 6;; y := 1;; WHILE (x \<ge> 2) DO y := y * x;; x := x - 1 OD\<close>\<close>
lemma "Tr\<^sub>N (Program [] \<lbrace> (''x'' := Num 6;; ''y'' := Num 1);; 
              WHILE ((Var ''x'') \<^sub>Rgeq (Num 2)) 
                 DO ''y'' := (Var ''y'') \<^sub>Amul (Var ''x'');; 
                    ''x'' := (Var ''x'') \<^sub>Asub (Num 1) 
                 OD \<rbrace>) \<circle> 1000 =
      {(((((((((((\<langle>fm []\<rangle> \<leadsto> State \<llangle>fm [(''x'', Exp (Num 6))]\<rrangle>) \<leadsto>
            State \<llangle>fm [(''x'', Exp (Num 6)), (''y'', Exp (Num 1))]\<rrangle>) \<leadsto>
           State \<llangle>fm [(''x'', Exp (Num 6)), (''y'', Exp (Num 6))]\<rrangle>) \<leadsto>
          State \<llangle>fm [(''x'', Exp (Num 5)), (''y'', Exp (Num 6))]\<rrangle>) \<leadsto>
         State \<llangle>fm [(''x'', Exp (Num 5)), (''y'', Exp (Num 30))]\<rrangle>) \<leadsto>
        State \<llangle>fm [(''x'', Exp (Num 4)), (''y'', Exp (Num 30))]\<rrangle>) \<leadsto>
       State \<llangle>fm [(''x'', Exp (Num 4)), (''y'', Exp (Num 120))]\<rrangle>) \<leadsto>
      State \<llangle>fm [(''x'', Exp (Num 3)), (''y'', Exp (Num 120))]\<rrangle>) \<leadsto>
     State \<llangle>fm [(''x'', Exp (Num 3)), (''y'', Exp (Num 360))]\<rrangle>) \<leadsto>
    State \<llangle>fm [(''x'', Exp (Num 2)), (''y'', Exp (Num 360))]\<rrangle>) \<leadsto>
   State \<llangle>fm [(''x'', Exp (Num 2)), (''y'', Exp (Num 720))]\<rrangle>) \<leadsto>
  State \<llangle>fm [(''x'', Exp (Num 1)), (''y'', Exp (Num 720))]\<rrangle>}"
  by (simp add: \<delta>_system \<Delta>\<^sub>N_system)

text \<open>\<open>CO CO x := 5 \<parallel> x := 6 OC \<parallel> x := 7;; SKIP OC;; x := 8\<close>\<close>
lemma "Tr\<^sub>N (Program [] \<lbrace> CO CO ''x'' := Num 5 \<parallel> ''x'' := Num 6 OC \<parallel> ''x'' := Num 5;; SKIP OC;; ''x'' := Num 8 \<rbrace>) \<circle> 1000 =
       {(((\<langle>fm []\<rangle> \<leadsto> State \<llangle>fm [(''x'', Exp (Num 5))]\<rrangle>) \<leadsto> 
          State \<llangle>fm [(''x'', Exp (Num 6))]\<rrangle>) \<leadsto>
         State \<llangle>fm [(''x'', Exp (Num 5))]\<rrangle>) \<leadsto> 
        State \<llangle>fm [(''x'', Exp (Num 8))]\<rrangle>,
       (((\<langle>fm []\<rangle> \<leadsto> State \<llangle>fm [(''x'', Exp (Num 5))]\<rrangle>) \<leadsto> 
          State \<llangle>fm [(''x'', Exp (Num 5))]\<rrangle>) \<leadsto>
         State \<llangle>fm [(''x'', Exp (Num 6))]\<rrangle>) \<leadsto>
        State \<llangle>fm [(''x'', Exp (Num 8))]\<rrangle>,
       (((\<langle>fm []\<rangle> \<leadsto> State \<llangle>fm [(''x'', Exp (Num 6))]\<rrangle>) \<leadsto> 
          State \<llangle>fm [(''x'', Exp (Num 5))]\<rrangle>) \<leadsto>
         State \<llangle>fm [(''x'', Exp (Num 5))]\<rrangle>) \<leadsto>
        State \<llangle>fm [(''x'', Exp (Num 8))]\<rrangle>}"
  by (simp add: \<delta>_system \<Delta>\<^sub>N_system)

text \<open>\<open>y := 5;; \<lbrace> (x;\<nu>) x := 2;; \<lbrace> (x;\<nu>) x := 1 \<rbrace>;; y := x \<rbrace> x := y\<close>\<close>
lemma "Tr\<^sub>N (Program [] \<lbrace> (''y'' := Num 5;; \<lbrace> (''x'';\<nu>) (''x'' := Num 2;; \<lbrace> (''x'';\<nu>) ''x'' := Num 1 \<rbrace>);; ''y'' := Var ''x'' \<rbrace>);; ''x'' := Var ''y'' \<rbrace>) \<circle> 1000 =
      {((((((\<langle>fm []\<rangle> \<leadsto> State \<llangle>fm [(''y'', Exp (Num 5))]\<rrangle>) \<leadsto>
            State \<llangle>fm [(''y'', Exp (Num 5)), (''$x::Scope'', Exp (Num 0))]\<rrangle>) \<leadsto>
           State \<llangle>fm [(''y'', Exp (Num 5)), (''$x::Scope'', Exp (Num 2))]\<rrangle>) \<leadsto>
          State \<llangle>fm [(''y'', Exp (Num 5)), (''$x::Scope'', Exp (Num 2)), (''$$x::Scope::Scope'', Exp (Num 0))]\<rrangle>) \<leadsto>
         State \<llangle>fm [(''y'', Exp (Num 5)), (''$x::Scope'', Exp (Num 2)), (''$$x::Scope::Scope'', Exp (Num 1))]\<rrangle>) \<leadsto>
        State \<llangle>fm [(''y'', Exp (Num 2)), (''$x::Scope'', Exp (Num 2)), (''$$x::Scope::Scope'', Exp (Num 1))]\<rrangle>) \<leadsto>
       State \<llangle>fm [(''y'', Exp (Num 2)), (''$x::Scope'', Exp (Num 2)), (''$$x::Scope::Scope'', Exp (Num 1)), (''x'', Exp (Num 2))]\<rrangle>}"
  by (simp add: \<delta>_system \<Delta>\<^sub>N_system)

text \<open>\<open>x := 5;; \<lbrace> (x;\<nu>) x := 2 \<rbrace> \<lbrace> (x;\<nu>) x := 1 \<rbrace>\<close>\<close>
lemma "Tr\<^sub>N (Program [] \<lbrace> (''x'' := Num 5;; \<lbrace> (''x''; \<nu>) ''x'' := Num 2 \<rbrace>);; \<lbrace> (''x''; \<nu>) ''x'' := Num 1 \<rbrace> \<rbrace>) \<circle> 1000 =
       {((((\<langle>fm []\<rangle> \<leadsto> State \<llangle>fm [(''x'', Exp (Num 5))]\<rrangle>) \<leadsto>
           State \<llangle>fm [(''x'', Exp (Num 5)), (''$x::Scope'', Exp (Num 0))]\<rrangle>) \<leadsto>
          State \<llangle>fm [(''x'', Exp (Num 5)), (''$x::Scope'', Exp (Num 2))]\<rrangle>) \<leadsto>
         State \<llangle>fm [(''x'', Exp (Num 5)), (''$x::Scope'', Exp (Num 2)), (''c$x::Scope'', Exp (Num 0))]\<rrangle>) \<leadsto>
        State \<llangle>fm [(''x'', Exp (Num 5)), (''$x::Scope'', Exp (Num 2)), (''c$x::Scope'', Exp (Num 1))]\<rrangle>}"
  by (simp add: \<delta>_system \<Delta>\<^sub>N_system)

text \<open>\<open>x := 5;; Input x\<close>\<close>
lemma "Tr\<^sub>N (Program [] \<lbrace> ''x'' := Num 5;; INPUT ''x'' \<rbrace>) \<circle> 1000 =
       {(((\<langle>fm [(''$x::Input'', Exp (Num 0))]\<rangle> \<leadsto>
      State \<llangle>fm [(''x'', Exp (Num 5)), (''$x::Input'', Exp (Num 0))]\<rrangle>) \<leadsto>
     State \<llangle>fm [(''x'', Exp (Num 0)), (''$x::Input'', Exp (Num 0))]\<rrangle>) \<leadsto>
    Event \<llangle>inpEv,[A (Num 0)]\<rrangle>) \<leadsto>
   State \<llangle>fm [(''x'', Exp (Num 0)), (''$x::Input'', Exp (Num 0))]\<rrangle>}"
  by (simp add: \<delta>_system \<Delta>\<^sub>N_system)

text \<open>\<open>x := 5;; Input x;; x := x + 1;; Input x\<close>\<close>
lemma "Tr\<^sub>N (Program [] \<lbrace> ((''x'' := Num 5;; INPUT ''x'');; ''x'' := ((Var ''x'') \<^sub>Aadd (Num 1)));; INPUT ''x'' \<rbrace>) \<circle> 1000 =
      {(((((((\<langle>fm [(''$x::Input'', Exp (Num 0)), (''c$x::Input'', Exp (Num 0))]\<rangle> \<leadsto>
              State \<llangle>fm [(''x'', Exp (Num 5)), (''$x::Input'', Exp (Num 0)), (''c$x::Input'', Exp (Num 0))]\<rrangle>) \<leadsto>
             State \<llangle>fm [(''x'', Exp (Num 0)), (''$x::Input'', Exp (Num 0)), (''c$x::Input'', Exp (Num 0))]\<rrangle>) \<leadsto>
            Event \<llangle>inpEv,[A (Num 0)]\<rrangle>) \<leadsto>
           State \<llangle>fm [(''x'', Exp (Num 0)), (''$x::Input'', Exp (Num 0)), (''c$x::Input'', Exp (Num 0))]\<rrangle>) \<leadsto>
          State \<llangle>fm [(''x'', Exp (Num 1)), (''$x::Input'', Exp (Num 0)), (''c$x::Input'', Exp (Num 0))]\<rrangle>) \<leadsto>
        State \<llangle>fm [(''x'', Exp (Num 0)), (''$x::Input'', Exp (Num 0)), (''c$x::Input'', Exp (Num 0))]\<rrangle>) \<leadsto>
       Event \<llangle>inpEv,[A (Num 0)]\<rrangle>) \<leadsto>
      State \<llangle>fm [(''x'', Exp (Num 0)), (''$x::Input'', Exp (Num 0)), (''c$x::Input'', Exp (Num 0))]\<rrangle>}"
  by (simp add: \<delta>_system \<Delta>\<^sub>N_system)

text \<open>\<open>x := 0;; CO :: x \<le> 5;; x := 6 \<parallel> x := 6 OC\<close>\<close>
lemma "Tr\<^sub>N (Program [] \<lbrace> ''x'' := Num 0;; CO :: ((Var ''x'') \<^sub>Rleq (Num 5));; ''x'' := Num 6 END \<parallel> ''x'' := Num 6 OC \<rbrace>) \<circle> 10 =
      {((\<langle>fm []\<rangle> \<leadsto> State \<llangle>fm [(''x'', Exp (Num 0))]\<rrangle>) \<leadsto> State \<llangle>fm [(''x'', Exp (Num 6))]\<rrangle>) \<leadsto>
           State \<llangle>fm [(''x'', Exp (Num 6))]\<rrangle>,
       (\<langle>fm []\<rangle> \<leadsto> State \<llangle>fm [(''x'', Exp (Num 0))]\<rrangle>) \<leadsto> State \<llangle>fm [(''x'', Exp (Num 6))]\<rrangle>}"
  by (simp add: \<delta>_system \<Delta>\<^sub>N_system)

text \<open>\<open>x := 5;; CALL foo(x);; x := 6\<close>\<close>
lemma "Tr\<^sub>N (Program [
            (Method ''foo'' ''x'' \<lbrace> ''x'' := Num 7 \<rbrace>)
          ] \<lbrace> (''x'' := Num 5;; CALL ''foo'' (Var ''x''));; ''x'' := Num 6 \<rbrace>) \<circle> 1000 =
      {(((((((\<langle>fm []\<rangle> \<leadsto> State \<llangle>fm [(''x'', Exp (Num 5))]\<rrangle>) \<leadsto>
        Event \<llangle>invEv,[P ''foo'', A (Num 5)]\<rrangle>) \<leadsto>
       State \<llangle>fm [(''x'', Exp (Num 5))]\<rrangle>) \<leadsto>
      Event \<llangle>invREv,[P ''foo'', A (Num 5)]\<rrangle>) \<leadsto>
     State \<llangle>fm [(''x'', Exp (Num 5))]\<rrangle>) \<leadsto>
    State \<llangle>fm [(''x'', Exp (Num 5)), (''$foo::Param'', Exp (Num 5))]\<rrangle>) \<leadsto>
   State \<llangle>fm [(''x'', Exp (Num 6)), (''$foo::Param'', Exp (Num 5))]\<rrangle>) \<leadsto>
  State \<llangle>fm [(''x'', Exp (Num 6)), (''$foo::Param'', Exp (Num 7))]\<rrangle>,
  (((((((\<langle>fm []\<rangle> \<leadsto> State \<llangle>fm [(''x'', Exp (Num 5))]\<rrangle>) \<leadsto>
        Event \<llangle>invEv,[P ''foo'', A (Num 5)]\<rrangle>) \<leadsto>
       State \<llangle>fm [(''x'', Exp (Num 5))]\<rrangle>) \<leadsto>
      Event \<llangle>invREv,[P ''foo'', A (Num 5)]\<rrangle>) \<leadsto>
     State \<llangle>fm [(''x'', Exp (Num 5))]\<rrangle>) \<leadsto>
    State \<llangle>fm [(''x'', Exp (Num 5)), (''$foo::Param'', Exp (Num 5))]\<rrangle>) \<leadsto>
   State \<llangle>fm [(''x'', Exp (Num 5)), (''$foo::Param'', Exp (Num 7))]\<rrangle>) \<leadsto>
  State \<llangle>fm [(''x'', Exp (Num 6)), (''$foo::Param'', Exp (Num 7))]\<rrangle>,
  (((((((\<langle>fm []\<rangle> \<leadsto> State \<llangle>fm [(''x'', Exp (Num 5))]\<rrangle>) \<leadsto>
        Event \<llangle>invEv,[P ''foo'', A (Num 5)]\<rrangle>) \<leadsto>
       State \<llangle>fm [(''x'', Exp (Num 5))]\<rrangle>) \<leadsto>
      State \<llangle>fm [(''x'', Exp (Num 6))]\<rrangle>) \<leadsto>
     Event \<llangle>invREv,[P ''foo'', A (Num 5)]\<rrangle>) \<leadsto>
    State \<llangle>fm [(''x'', Exp (Num 6))]\<rrangle>) \<leadsto>
   State \<llangle>fm [(''x'', Exp (Num 6)), (''$foo::Param'', Exp (Num 5))]\<rrangle>) \<leadsto>
  State \<llangle>fm [(''x'', Exp (Num 6)), (''$foo::Param'', Exp (Num 7))]\<rrangle>}"
  by (simp add: \<delta>_system \<Delta>\<^sub>N_system)

text \<open>\<open>SKIP\<close>\<close>
lemma "Tr (Program [] \<lbrace> SKIP \<rbrace>) \<circle> = {\<langle>\<circle>\<rangle>}"
  by (simp add: WL\<^sub>E_derivation_system)

text \<open>\<open>SKIP; x := 2\<close>\<close>
lemma "Tr (Program [] \<lbrace> SKIP;;''x'' := Num 2 \<rbrace>) \<circle> = {\<langle>\<circle>\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 2)] \<circle>\<rrangle>}"
  by (simp add: WL\<^sub>E_derivation_system)

text \<open>\<open>SKIP; SKIP; SKIP\<close>\<close>
lemma "Tr (Program [] \<lbrace> SKIP;;(SKIP;;SKIP) \<rbrace>) \<circle> = {\<langle>\<circle>\<rangle>}"
  by (simp add: WL\<^sub>E_derivation_system)

text \<open>\<open>x := 1; x := x + 1\<close>\<close>
lemma "Tr (Program [] \<lbrace> ''x'' := Num 1;; ''x'' := (Var ''x'') \<^sub>Aadd (Num 1) \<rbrace>) \<circle> =
       {(\<langle>\<circle>\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 1)] \<circle>\<rrangle>) \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 2)] \<circle>\<rrangle>}" 
  by (simp add: WL\<^sub>E_derivation_system)

text \<open>\<open>x := 1; y := x + 1\<close> [\<open>Example 3.4\<close>]\<close>
lemma "Tr (Program [] \<lbrace> ''x'' := Num 1;; ''y'' := (Var ''x'') \<^sub>Aadd (Num 1) \<rbrace>) \<sigma>\<^sub>A =
       {(\<langle>\<sigma>\<^sub>A\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 1)] \<sigma>\<^sub>A\<rrangle>) \<leadsto> State\<llangle>[''y'' \<longmapsto> Exp (Num 2)] [''x'' \<longmapsto> Exp (Num 1)] \<sigma>\<^sub>A\<rrangle>}" 
  by (simp add: WL\<^sub>E_derivation_system \<sigma>\<^sub>A_def)

text \<open>\<open>x := 0; IF (x == 0) THEN x := 2 FI\<close>\<close>
lemma "Tr (Program [] \<lbrace> ''x'' := Num 0;;IF ((Var ''x'') \<^sub>Req (Num 0)) THEN ''x'' := Num 2 FI \<rbrace>) \<circle> =
       {(\<langle>\<circle>\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 0)] \<circle>\<rrangle>) \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 2)] \<circle>\<rrangle>}"
  by (simp add: WL\<^sub>E_derivation_system)

text \<open>\<open>x := 0; WHILE (x == 0) DO x := 2 OD\<close>\<close>
lemma "Tr (Program [] \<lbrace> ''x'' := Num 0;;WHILE ((Var ''x'') \<^sub>Req (Num 0)) DO ''x'' := Num 2 OD \<rbrace>) \<circle> =
       {(\<langle>\<circle>\<rangle> \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 0)] \<circle>\<rrangle>) \<leadsto> State\<llangle>[''x'' \<longmapsto> Exp (Num 2)] \<circle>\<rrangle>}"
  by (simp add: WL\<^sub>E_derivation_system)

text \<open>\<open>x := 4;; y := 1;; WHILE (x \<ge> 2) DO y := y * x;; x := x - 1 OD\<close>\<close>
lemma "Tr (Program [] \<lbrace> (''x'' := Num 4;; ''y'' := Num 1);; 
              WHILE ((Var ''x'') \<^sub>Rgeq (Num 2)) 
                 DO ''y'' := (Var ''y'') \<^sub>Amul (Var ''x'');; 
                    ''x'' := (Var ''x'') \<^sub>Asub (Num 1) 
                 OD \<rbrace>) \<circle> =
      {(((((((\<langle>fm []\<rangle> \<leadsto> State \<llangle>fm [(''x'', Exp (Num 4))]\<rrangle>) \<leadsto>
         State \<llangle>fm [(''x'', Exp (Num 4)), (''y'', Exp (Num 1))]\<rrangle>) \<leadsto>
        State \<llangle>fm [(''x'', Exp (Num 4)), (''y'', Exp (Num 4))]\<rrangle>) \<leadsto>
       State \<llangle>fm [(''x'', Exp (Num 3)), (''y'', Exp (Num 4))]\<rrangle>) \<leadsto>
      State \<llangle>fm [(''x'', Exp (Num 3)), (''y'', Exp (Num 12))]\<rrangle>) \<leadsto>
     State \<llangle>fm [(''x'', Exp (Num 2)), (''y'', Exp (Num 12))]\<rrangle>) \<leadsto>
    State \<llangle>fm [(''x'', Exp (Num 2)), (''y'', Exp (Num 24))]\<rrangle>) \<leadsto>
   State \<llangle>fm [(''x'', Exp (Num 1)), (''y'', Exp (Num 24))]\<rrangle>}"
  by (simp add: WL\<^sub>E_derivation_system)

text \<open>\<open>CO CO x := 5 \<parallel> x := 6 OC \<parallel> x := 7;; SKIP OC;; x := 8\<close>\<close>
lemma "Tr (Program [] \<lbrace> CO CO ''x'' := Num 5 \<parallel> ''x'' := Num 6 OC \<parallel> ''x'' := Num 5;; SKIP OC;; ''x'' := Num 8 \<rbrace>) \<circle> =
       {(((\<langle>fm []\<rangle> \<leadsto> State \<llangle>fm [(''x'', Exp (Num 5))]\<rrangle>) \<leadsto> 
          State \<llangle>fm [(''x'', Exp (Num 6))]\<rrangle>) \<leadsto>
         State \<llangle>fm [(''x'', Exp (Num 5))]\<rrangle>) \<leadsto> 
        State \<llangle>fm [(''x'', Exp (Num 8))]\<rrangle>,
       (((\<langle>fm []\<rangle> \<leadsto> State \<llangle>fm [(''x'', Exp (Num 5))]\<rrangle>) \<leadsto> 
          State \<llangle>fm [(''x'', Exp (Num 5))]\<rrangle>) \<leadsto>
         State \<llangle>fm [(''x'', Exp (Num 6))]\<rrangle>) \<leadsto>
        State \<llangle>fm [(''x'', Exp (Num 8))]\<rrangle>,
       (((\<langle>fm []\<rangle> \<leadsto> State \<llangle>fm [(''x'', Exp (Num 6))]\<rrangle>) \<leadsto> 
          State \<llangle>fm [(''x'', Exp (Num 5))]\<rrangle>) \<leadsto>
         State \<llangle>fm [(''x'', Exp (Num 5))]\<rrangle>) \<leadsto>
        State \<llangle>fm [(''x'', Exp (Num 8))]\<rrangle>}"
  by (simp add: WL\<^sub>E_derivation_system)

text \<open>\<open>y := 5;; \<lbrace> (x;\<nu>) x := 2;; \<lbrace> (x;\<nu>) x := 1 \<rbrace>;; y := x \<rbrace> x := y\<close>\<close>
lemma "Tr (Program [] \<lbrace> (''y'' := Num 5;; \<lbrace> (''x'';\<nu>) (''x'' := Num 2;; \<lbrace> (''x'';\<nu>) ''x'' := Num 1 \<rbrace>);; ''y'' := Var ''x'' \<rbrace>);; ''x'' := Var ''y'' \<rbrace>) \<circle> =
      {((((((\<langle>fm []\<rangle> \<leadsto> State \<llangle>fm [(''y'', Exp (Num 5))]\<rrangle>) \<leadsto>
            State \<llangle>fm [(''y'', Exp (Num 5)), (''$x::Scope'', Exp (Num 0))]\<rrangle>) \<leadsto>
           State \<llangle>fm [(''y'', Exp (Num 5)), (''$x::Scope'', Exp (Num 2))]\<rrangle>) \<leadsto>
          State \<llangle>fm [(''y'', Exp (Num 5)), (''$x::Scope'', Exp (Num 2)), (''$$x::Scope::Scope'', Exp (Num 0))]\<rrangle>) \<leadsto>
         State \<llangle>fm [(''y'', Exp (Num 5)), (''$x::Scope'', Exp (Num 2)), (''$$x::Scope::Scope'', Exp (Num 1))]\<rrangle>) \<leadsto>
        State \<llangle>fm [(''y'', Exp (Num 2)), (''$x::Scope'', Exp (Num 2)), (''$$x::Scope::Scope'', Exp (Num 1))]\<rrangle>) \<leadsto>
       State \<llangle>fm [(''y'', Exp (Num 2)), (''$x::Scope'', Exp (Num 2)), (''$$x::Scope::Scope'', Exp (Num 1)), (''x'', Exp (Num 2))]\<rrangle>}"
  by (simp add: WL\<^sub>E_derivation_system)

text \<open>\<open>x := 5;; \<lbrace> (x;\<nu>) x := 2 \<rbrace> \<lbrace> (x;\<nu>) x := 1 \<rbrace>\<close>\<close>
lemma "Tr (Program [] \<lbrace> (''x'' := Num 5;; \<lbrace> (''x''; \<nu>) ''x'' := Num 2 \<rbrace>);; \<lbrace> (''x''; \<nu>) ''x'' := Num 1 \<rbrace> \<rbrace>) \<circle> =
       {((((\<langle>fm []\<rangle> \<leadsto> State \<llangle>fm [(''x'', Exp (Num 5))]\<rrangle>) \<leadsto>
           State \<llangle>fm [(''x'', Exp (Num 5)), (''$x::Scope'', Exp (Num 0))]\<rrangle>) \<leadsto>
          State \<llangle>fm [(''x'', Exp (Num 5)), (''$x::Scope'', Exp (Num 2))]\<rrangle>) \<leadsto>
         State \<llangle>fm [(''x'', Exp (Num 5)), (''$x::Scope'', Exp (Num 2)), (''c$x::Scope'', Exp (Num 0))]\<rrangle>) \<leadsto>
        State \<llangle>fm [(''x'', Exp (Num 5)), (''$x::Scope'', Exp (Num 2)), (''c$x::Scope'', Exp (Num 1))]\<rrangle>}"
  by (simp add: WL\<^sub>E_derivation_system)

text \<open>\<open>x := 5;; Input x\<close>\<close>
lemma "Tr (Program [] \<lbrace> ''x'' := Num 5;; INPUT ''x'' \<rbrace>) \<circle> =
       {(((\<langle>fm [(''$x::Input'', Exp (Num 0))]\<rangle> \<leadsto>
      State \<llangle>fm [(''x'', Exp (Num 5)), (''$x::Input'', Exp (Num 0))]\<rrangle>) \<leadsto>
     State \<llangle>fm [(''x'', Exp (Num 0)), (''$x::Input'', Exp (Num 0))]\<rrangle>) \<leadsto>
    Event \<llangle>inpEv,[A (Num 0)]\<rrangle>) \<leadsto>
   State \<llangle>fm [(''x'', Exp (Num 0)), (''$x::Input'', Exp (Num 0))]\<rrangle>}"
  by (simp add: WL\<^sub>E_derivation_system)

text \<open>\<open>x := 5;; Input x;; x := x + 1;; Input x\<close>\<close>
lemma "Tr (Program [] \<lbrace> ((''x'' := Num 5;; INPUT ''x'');; ''x'' := ((Var ''x'') \<^sub>Aadd (Num 1)));; INPUT ''x'' \<rbrace>) \<circle> =
      {(((((((\<langle>fm [(''$x::Input'', Exp (Num 0)), (''c$x::Input'', Exp (Num 0))]\<rangle> \<leadsto>
              State \<llangle>fm [(''x'', Exp (Num 5)), (''$x::Input'', Exp (Num 0)), (''c$x::Input'', Exp (Num 0))]\<rrangle>) \<leadsto>
             State \<llangle>fm [(''x'', Exp (Num 0)), (''$x::Input'', Exp (Num 0)), (''c$x::Input'', Exp (Num 0))]\<rrangle>) \<leadsto>
            Event \<llangle>inpEv,[A (Num 0)]\<rrangle>) \<leadsto>
           State \<llangle>fm [(''x'', Exp (Num 0)), (''$x::Input'', Exp (Num 0)), (''c$x::Input'', Exp (Num 0))]\<rrangle>) \<leadsto>
          State \<llangle>fm [(''x'', Exp (Num 1)), (''$x::Input'', Exp (Num 0)), (''c$x::Input'', Exp (Num 0))]\<rrangle>) \<leadsto>
        State \<llangle>fm [(''x'', Exp (Num 0)), (''$x::Input'', Exp (Num 0)), (''c$x::Input'', Exp (Num 0))]\<rrangle>) \<leadsto>
       Event \<llangle>inpEv,[A (Num 0)]\<rrangle>) \<leadsto>
      State \<llangle>fm [(''x'', Exp (Num 0)), (''$x::Input'', Exp (Num 0)), (''c$x::Input'', Exp (Num 0))]\<rrangle>}"
  by (simp add: WL\<^sub>E_derivation_system)

text \<open>\<open>x := 0;; CO :: x \<le> 5;; x := 7 \<parallel> x := 6 OC\<close>\<close>
lemma "Tr (Program [] \<lbrace> ''x'' := Num 0;; CO :: ((Var ''x'') \<^sub>Rleq (Num 5));; ''x'' := Num 7 END \<parallel> ''x'' := Num 6 OC \<rbrace>) \<circle> =
      {((\<langle>fm []\<rangle> \<leadsto> State \<llangle>fm [(''x'', Exp (Num 0))]\<rrangle>) \<leadsto> State \<llangle>fm [(''x'', Exp (Num 6))]\<rrangle>) \<leadsto>
        State \<llangle>fm [(''x'', Exp (Num 7))]\<rrangle>,
       ((\<langle>fm []\<rangle> \<leadsto> State \<llangle>fm [(''x'', Exp (Num 0))]\<rrangle>) \<leadsto> State \<llangle>fm [(''x'', Exp (Num 7))]\<rrangle>) \<leadsto>
        State \<llangle>fm [(''x'', Exp (Num 6))]\<rrangle>,
        (\<langle>fm []\<rangle> \<leadsto> State \<llangle>fm [(''x'', Exp (Num 0))]\<rrangle>) \<leadsto> State \<llangle>fm [(''x'', Exp (Num 6))]\<rrangle>}"
  by (simp add: WL\<^sub>E_derivation_system)

text \<open>\<open>x := 5;; CALL foo(x);; x := 6\<close>\<close>
lemma "Tr (Program [
            (Method ''foo'' ''x'' \<lbrace> ''x'' := Num 7 \<rbrace>),
            (Method ''bar'' ''y'' \<lbrace> ''y'' := Num 8 \<rbrace>)
          ] \<lbrace> (''x'' := Num 5;; CALL ''foo'' (Var ''x''));; ''x'' := Num 6 \<rbrace>) \<circle> =
      {(((((((\<langle>fm []\<rangle> \<leadsto> State \<llangle>fm [(''x'', Exp (Num 5))]\<rrangle>) \<leadsto>
        Event \<llangle>invEv,[P ''foo'', A (Num 5)]\<rrangle>) \<leadsto>
       State \<llangle>fm [(''x'', Exp (Num 5))]\<rrangle>) \<leadsto>
      Event \<llangle>invREv,[P ''foo'', A (Num 5)]\<rrangle>) \<leadsto>
     State \<llangle>fm [(''x'', Exp (Num 5))]\<rrangle>) \<leadsto>
    State \<llangle>fm [(''x'', Exp (Num 5)), (''$foo::Param'', Exp (Num 5))]\<rrangle>) \<leadsto>
   State \<llangle>fm [(''x'', Exp (Num 6)), (''$foo::Param'', Exp (Num 5))]\<rrangle>) \<leadsto>
  State \<llangle>fm [(''x'', Exp (Num 6)), (''$foo::Param'', Exp (Num 7))]\<rrangle>,
  (((((((\<langle>fm []\<rangle> \<leadsto> State \<llangle>fm [(''x'', Exp (Num 5))]\<rrangle>) \<leadsto>
        Event \<llangle>invEv,[P ''foo'', A (Num 5)]\<rrangle>) \<leadsto>
       State \<llangle>fm [(''x'', Exp (Num 5))]\<rrangle>) \<leadsto>
      Event \<llangle>invREv,[P ''foo'', A (Num 5)]\<rrangle>) \<leadsto>
     State \<llangle>fm [(''x'', Exp (Num 5))]\<rrangle>) \<leadsto>
    State \<llangle>fm [(''x'', Exp (Num 5)), (''$foo::Param'', Exp (Num 5))]\<rrangle>) \<leadsto>
   State \<llangle>fm [(''x'', Exp (Num 5)), (''$foo::Param'', Exp (Num 7))]\<rrangle>) \<leadsto>
  State \<llangle>fm [(''x'', Exp (Num 6)), (''$foo::Param'', Exp (Num 7))]\<rrangle>,
  (((((((\<langle>fm []\<rangle> \<leadsto> State \<llangle>fm [(''x'', Exp (Num 5))]\<rrangle>) \<leadsto>
        Event \<llangle>invEv,[P ''foo'', A (Num 5)]\<rrangle>) \<leadsto>
       State \<llangle>fm [(''x'', Exp (Num 5))]\<rrangle>) \<leadsto>
      State \<llangle>fm [(''x'', Exp (Num 6))]\<rrangle>) \<leadsto>
     Event \<llangle>invREv,[P ''foo'', A (Num 5)]\<rrangle>) \<leadsto>
    State \<llangle>fm [(''x'', Exp (Num 6))]\<rrangle>) \<leadsto>
   State \<llangle>fm [(''x'', Exp (Num 6)), (''$foo::Param'', Exp (Num 5))]\<rrangle>) \<leadsto>
  State \<llangle>fm [(''x'', Exp (Num 6)), (''$foo::Param'', Exp (Num 7))]\<rrangle>}"
  by (simp add: WL\<^sub>E_derivation_system)

text \<open>\<open>x := 1;; CALL foo(x+1)\<close>\<close>
lemma "Tr (Program [
            (Method ''foo'' ''x'' \<lbrace> INPUT ''x'' \<rbrace>),
            (Method ''bar'' ''y'' \<lbrace> ''y'' := (Var ''x'') \<rbrace>)
          ] \<lbrace> ''x'' := Num 1;; CALL ''foo'' ((Var ''x'') \<^sub>Asub (Num 1)) \<rbrace>) \<circle> =
      {((((((((\<langle>fm [(''$$foo::Param::Input'', Exp (Num 0))]\<rangle> \<leadsto>
           State \<llangle>fm [(''x'', Exp (Num 1)), (''$$foo::Param::Input'', Exp (Num 0))]\<rrangle>) \<leadsto>
          Event \<llangle>invEv,[P ''foo'', A (Num 0)]\<rrangle>) \<leadsto>
         State \<llangle>fm [(''x'', Exp (Num 1)), (''$$foo::Param::Input'', Exp (Num 0))]\<rrangle>) \<leadsto>
        Event \<llangle>invREv,[P ''foo'', A (Num 0)]\<rrangle>) \<leadsto>
       State \<llangle>fm [(''x'', Exp (Num 1)), (''$$foo::Param::Input'', Exp (Num 0))]\<rrangle>) \<leadsto>
      State \<llangle>fm [(''x'', Exp (Num 1)), (''$foo::Param'', Exp (Num 0)), (''$$foo::Param::Input'', Exp (Num 0))]\<rrangle>) \<leadsto>
     State \<llangle>fm [(''x'', Exp (Num 1)), (''$foo::Param'', Exp (Num 0)), (''$$foo::Param::Input'', Exp (Num 0))]\<rrangle>) \<leadsto>
    Event \<llangle>inpEv,[A (Num 0)]\<rrangle>) \<leadsto>
   State \<llangle>fm [(''x'', Exp (Num 1)), (''$foo::Param'', Exp (Num 0)), (''$$foo::Param::Input'', Exp (Num 0))]\<rrangle>}"
  by (simp add: WL\<^sub>E_derivation_system)

text \<open>\<open>x := 1;; CALL foo(x+1)\<close>\<close>
lemma "Tr (Program [
            (Method ''foo'' ''x'' \<lbrace> INPUT ''x'' \<rbrace>),
            (Method ''bar'' ''y'' \<lbrace> ''y'' := (Var ''x'') \<rbrace>)
          ] \<lbrace> (''x'' := Num 1;; ''y'' := Num 1);; CALL ''foo'' ((Var ''x'') \<^sub>Asub (Num 1)) \<rbrace>) \<circle> =
      {(((((((((\<langle>fm [(''$$foo::Param::Input'', Exp (Num 0))]\<rangle> \<leadsto>
             State \<llangle>fm [(''x'', Exp (Num 1)), (''$$foo::Param::Input'', Exp (Num 0))]\<rrangle>) \<leadsto>
            State \<llangle>fm [(''x'', Exp (Num 1)), (''y'', Exp (Num 1)), (''$$foo::Param::Input'', Exp (Num 0))]\<rrangle>) \<leadsto>
           Event \<llangle>invEv,[P ''foo'', A (Num 0)]\<rrangle>) \<leadsto>
          State \<llangle>fm [(''x'', Exp (Num 1)), (''y'', Exp (Num 1)), (''$$foo::Param::Input'', Exp (Num 0))]\<rrangle>) \<leadsto>
         Event \<llangle>invREv,[P ''foo'', A (Num 0)]\<rrangle>) \<leadsto>
        State \<llangle>fm [(''x'', Exp (Num 1)), (''y'', Exp (Num 1)), (''$$foo::Param::Input'', Exp (Num 0))]\<rrangle>) \<leadsto>
       State \<llangle>fm [(''x'', Exp (Num 1)), (''y'', Exp (Num 1)), (''$foo::Param'', Exp (Num 0)), (''$$foo::Param::Input'', Exp (Num 0))]\<rrangle>) \<leadsto>
      State \<llangle>fm [(''x'', Exp (Num 1)), (''y'', Exp (Num 1)), (''$foo::Param'', Exp (Num 0)), (''$$foo::Param::Input'', Exp (Num 0))]\<rrangle>) \<leadsto>
     Event \<llangle>inpEv,[A (Num 0)]\<rrangle>) \<leadsto>
    State \<llangle>fm [(''x'', Exp (Num 1)), (''y'', Exp (Num 1)), (''$foo::Param'', Exp (Num 0)), (''$$foo::Param::Input'', Exp (Num 0))]\<rrangle>}"
  by (simp add: WL\<^sub>E_derivation_system)


subsection \<open>Code Generation\<close>

text \<open>\<open>SKIP\<close>\<close>
value "Tr\<^sub>N (Program [] \<lbrace> SKIP \<rbrace>) \<circle> 1000"

text \<open>\<open>SKIP; x := 2\<close>\<close>
value "Tr\<^sub>N (Program [] \<lbrace> SKIP;;''x'' := Num 2 \<rbrace>) \<circle> 1000"

text \<open>\<open>SKIP; SKIP; SKIP\<close>\<close>
value "Tr\<^sub>N (Program [] \<lbrace> SKIP;;(SKIP;;SKIP) \<rbrace>) \<circle> 1000"

text \<open>\<open>x := 1; x := x + 1\<close>\<close>
value "Tr\<^sub>N (Program [] \<lbrace> ''x'' := Num 1;; ''x'' := (Var ''x'') \<^sub>Aadd (Num 1) \<rbrace>) \<circle> 1000"

text \<open>\<open>x := 1; y := x + 1\<close> [\<open>Example 3.4\<close>]\<close>
value "Tr\<^sub>N (Program [] \<lbrace> ''x'' := Num 1;; ''y'' := (Var ''x'') \<^sub>Aadd (Num 1) \<rbrace>) \<sigma>\<^sub>A 1000"

text \<open>\<open>x := 0; IF (x == 0) THEN x := 2 FI\<close>\<close>
value "Tr\<^sub>N (Program [] \<lbrace> ''x'' := Num 0;;IF ((Var ''x'') \<^sub>Req (Num 0)) THEN ''x'' := Num 2 FI \<rbrace>) \<circle> 1000"

text \<open>\<open>x := 0; WHILE (x == 0) DO x := 2 OD\<close>\<close>
value "Tr\<^sub>N (Program [] \<lbrace> ''x'' := Num 0;;WHILE ((Var ''x'') \<^sub>Req (Num 0)) DO ''x'' := Num 2 OD \<rbrace>) \<circle> 1000"

text \<open>\<open>x := 6;; y := 1;; WHILE (x \<ge> 2) DO y := y * x;; x := x - 1 OD\<close>\<close>
value "Tr\<^sub>N (Program [] \<lbrace> (''x'' := Num 6;; ''y'' := Num 1);; 
              WHILE ((Var ''x'') \<^sub>Rgeq (Num 2)) 
                 DO ''y'' := (Var ''y'') \<^sub>Amul (Var ''x'');; 
                    ''x'' := (Var ''x'') \<^sub>Asub (Num 1) 
                 OD \<rbrace>) \<circle> 1000"

text \<open>\<open>CO CO x := 5 \<parallel> x := 6 OC \<parallel> x := 7;; SKIP OC;; x := 8\<close>\<close>
value "Tr\<^sub>N (Program [] \<lbrace> CO CO ''x'' := Num 5 \<parallel> ''x'' := Num 6 OC \<parallel> ''x'' := Num 5;; SKIP OC;; ''x'' := Num 8 \<rbrace>) \<circle> 1000"

text \<open>\<open>SKIP\<close>\<close>
value "Tr (Program [] \<lbrace> SKIP \<rbrace>) \<circle>"

text \<open>\<open>SKIP; x := 2\<close>\<close>
value "Tr (Program [] \<lbrace> SKIP;;''x'' := Num 2 \<rbrace>) \<circle>"

text \<open>\<open>SKIP; SKIP; SKIP\<close>\<close>
value "Tr (Program [] \<lbrace> SKIP;;(SKIP;;SKIP) \<rbrace>) \<circle>"

text \<open>\<open>x := 1; x := x + 1\<close>\<close>
value "Tr (Program [] \<lbrace> ''x'' := Num 1;; ''x'' := (Var ''x'') \<^sub>Aadd (Num 1) \<rbrace>) \<circle>"

text \<open>\<open>x := 1; y := x + 1\<close> [\<open>Example 3.4\<close>]\<close>
value "Tr (Program [] \<lbrace> ''x'' := Num 1;; ''y'' := (Var ''x'') \<^sub>Aadd (Num 1) \<rbrace>) \<sigma>\<^sub>A"

text \<open>\<open>x := 0; IF (x == 0) THEN x := 2 FI\<close>\<close>
value "Tr (Program [] \<lbrace> ''x'' := Num 0;;IF ((Var ''x'') \<^sub>Req (Num 0)) THEN ''x'' := Num 2 FI \<rbrace>) \<circle>"

text \<open>\<open>x := 0; WHILE (x == 0) DO x := 2 OD\<close>\<close>
value "Tr (Program [] \<lbrace> ''x'' := Num 0;;WHILE ((Var ''x'') \<^sub>Req (Num 0)) DO ''x'' := Num 2 OD \<rbrace>) \<circle>"

text \<open>\<open>x := 4;; y := 1;; WHILE (x \<ge> 2) DO y := y * x;; x := x - 1 OD\<close>\<close>
value "Tr (Program [] \<lbrace> (''x'' := Num 4;; ''y'' := Num 1);; 
              WHILE ((Var ''x'') \<^sub>Rgeq (Num 2)) 
                 DO ''y'' := (Var ''y'') \<^sub>Amul (Var ''x'');; 
                    ''x'' := (Var ''x'') \<^sub>Asub (Num 1) 
                 OD \<rbrace>) \<circle>"

text \<open>\<open>CO CO x := 5 \<parallel> x := 6 OC \<parallel> x := 7;; SKIP OC;; x := 8\<close>\<close>
value "Tr (Program [] \<lbrace> CO CO ''x'' := Num 5 \<parallel> ''x'' := Num 6 OC \<parallel> ''x'' := Num 5;; SKIP OC;; ''x'' := Num 8 \<rbrace>) \<circle>"

text \<open>\<open>y := 5;; \<lbrace> (x;\<nu>) x := 2;; \<lbrace> (x;\<nu>) x := 1 \<rbrace>;; y := x \<rbrace> x := y\<close>\<close>
value "Tr (Program [] \<lbrace> (''y'' := Num 5;; \<lbrace> (''x'';\<nu>) (''x'' := Num 2;; \<lbrace> (''x'';\<nu>) ''x'' := Num 1 \<rbrace>);; ''y'' := Var ''x'' \<rbrace>);; ''x'' := Var ''y'' \<rbrace>) \<circle>"

text \<open>\<open>x := 5;; \<lbrace> (x;\<nu>) x := 2 \<rbrace> \<lbrace> (x;\<nu>) x := 1 \<rbrace>\<close>\<close>
value "Tr (Program [] \<lbrace> (''x'' := Num 5;; \<lbrace> (''x''; \<nu>) ''x'' := Num 2 \<rbrace>);; \<lbrace> (''x''; \<nu>) ''x'' := Num 1 \<rbrace> \<rbrace>) \<circle>"

text \<open>\<open>x := 5;; Input x\<close>\<close>
value "Tr (Program [] \<lbrace> ''x'' := Num 5;; INPUT ''x'' \<rbrace>) \<circle>"

text \<open>\<open>x := 5;; Input x;; x := x + 1;; Input x\<close>\<close>
value "Tr (Program [] \<lbrace> ((''x'' := Num 5;; INPUT ''x'');; ''x'' := ((Var ''x'') \<^sub>Aadd (Num 1)));; INPUT ''x'' \<rbrace>) \<circle>"

text \<open>\<open>x := 0;; CO :: x \<le> 5;; x := 7 \<parallel> x := 6 OC\<close>\<close>
value "Tr (Program [] \<lbrace> ''x'' := Num 0;; CO :: ((Var ''x'') \<^sub>Rleq (Num 5));; ''x'' := Num 7 END \<parallel> ''x'' := Num 6 OC \<rbrace>) \<circle>"

text \<open>\<open>x := 5;; CALL foo(x);; x := 6\<close>\<close>
value "Tr (Program [
            (Method ''foo'' ''x'' \<lbrace> ''x'' := Num 7 \<rbrace>)
          ] \<lbrace> (''x'' := Num 5;; CALL ''foo'' (Var ''x''));; ''x'' := Num 6 \<rbrace>) \<circle>"

text \<open>\<open>x := 1;; CALL foo(x+1)\<close>\<close>
value "Tr (Program [
            (Method ''foo'' ''x'' \<lbrace> INPUT ''x'';; CALL ''bar'' (Var ''x'') \<rbrace>),
            (Method ''bar'' ''y'' \<lbrace> ''y'' := (Var ''x'') \<rbrace>)
          ] \<lbrace> ''x'' := Num 1;; CALL ''foo'' ((Var ''x'') \<^sub>Asub (Num 1)) \<rbrace>) \<circle>"

text \<open>\<open>CALL foo(1);; Input y\<close>\<close>
value "Tr (Program [
            (Method ''foo'' ''x'' \<lbrace> INPUT ''x'';; CALL ''bar'' (Var ''x'') \<rbrace>),
            (Method ''bar'' ''y'' \<lbrace> :: (Bool True);; ''y'' := Num 2 END \<rbrace>)
          ] \<lbrace> CALL ''foo'' (Num 1);; INPUT ''y'' \<rbrace>) \<circle>"

text \<open>\<open>x := 0;; CALL bar(1);; x := 1\<close>\<close>
value "Tr (Program [
            (Method ''foo'' ''x'' \<lbrace> INPUT ''x'';; CALL ''bar'' (Var ''x'') \<rbrace>),
            (Method ''bar'' ''y'' \<lbrace> :: ((Var ''x'') \<^sub>Rgeq (Num 1));; ''y'' := Num 2 END \<rbrace>)
          ] \<lbrace> (''x'' := Num 0;; CALL ''bar'' (Num 1));; ''x'' := Num 1 \<rbrace>) \<circle>"

text \<open>\<open>CALL bar(1);; CALL bar(1)\<close>\<close>
value "Tr (Program [
            (Method ''bar'' ''y'' \<lbrace> :: (Bool True);; ''y'' := Num 2 END \<rbrace>)
          ] \<lbrace> CALL ''bar'' (Num 1);; CALL ''bar'' (Num 1) \<rbrace>) \<circle>"

text \<open>\<open>y := 0;; CO :: y \<ge> 1;; x := 5 \<parallel> y := 2 OC\<close>\<close>
value "Tr (Program [] \<lbrace> ''y'' := Num 0;; CO :: ((Var ''y'') \<^sub>Rgeq (Num 1));; ''x'' := Num 5 END \<parallel> ''y'' := Num 2 OC \<rbrace>) \<circle>"



subsection \<open>Trace Equivalence\<close>

text \<open>SKIP \<sim> (SKIP;;SKIP)\<close>
lemma "(Program [] \<lbrace> SKIP \<rbrace>) \<sim> (Program [] \<lbrace> SKIP;;SKIP \<rbrace>) [\<circle>]"
  using tequivalence by (simp add: WL\<^sub>E_derivation_system)

text \<open>(IF (True) THEN x := 2 FI) \<sim> (x := 2;; SKIP)\<close>
lemma "(Program [] \<lbrace> IF (Bool True) THEN ''x'' := Num 2 FI \<rbrace>) \<sim> (Program [] \<lbrace> ''x'' := Num 2;;SKIP \<rbrace>) [\<circle>]"
  using tequivalence by (simp add: WL\<^sub>E_derivation_system)

text \<open>(IF (False) THEN x := 2 FI) \<sim> (x := 2;; SKIP)\<close>
lemma "\<not>((Program [] \<lbrace> IF (Bool False) THEN ''x'' := Num 2 FI \<rbrace>) \<sim> (Program [] \<lbrace> ''x'' := Num 2;;SKIP \<rbrace>) [\<circle>])"
  using tequivalence by (simp add: WL\<^sub>E_derivation_system)

text \<open>(IF x == 1 THEN x := 0) \<sim> (x := 0)\<close>
lemma "\<not>((Program [] \<lbrace> IF ((Var ''x'') \<^sub>Req (Num 1)) THEN ''x'' := Num 0 FI \<rbrace>) \<sim> (Program [] \<lbrace> ''x'' := Num 0 \<rbrace>) [[''x'' \<longmapsto> Exp (Num 0)] \<circle>])"
  using tequivalence by (simp add: WL\<^sub>E_derivation_system)

text \<open>(IF x == 1 THEN x := 0) \<sim> (x := 0)\<close>
lemma "(Program [] \<lbrace> IF ((Var ''x'') \<^sub>Req (Num 1)) THEN ''x'' := Num 0 FI \<rbrace>) \<sim> (Program [] \<lbrace> ''x'' := Num 0 \<rbrace>) [[''x'' \<longmapsto> Exp (Num 1)] \<circle>]"
  using tequivalence by (simp add: WL\<^sub>E_derivation_system)

text \<open>(x := 4;; WHILE (x \<ge> 1) DO x := x - 1 OD) \<sim> (x := 4;; x := 3;; x := 2;; x := 1;; x := 0)\<close>
lemma "(Program [] \<lbrace> ''x'' := Num 4;; WHILE ((Var ''x'') \<^sub>Rgeq (Num 1)) DO ''x'' := (Var ''x'') \<^sub>Asub (Num 1) OD \<rbrace>) 
        \<sim> (Program [] \<lbrace> (((''x'' := Num 4;;''x'' := Num 3);; ''x'' := Num 2);; ''x'' := Num 1);; ''x'' := Num 0 \<rbrace>) [\<circle>]"
  using tequivalence by (simp add: WL\<^sub>E_derivation_system)

text \<open>(CALL bar(5);; SKIP) \<sim> (SKIP;; CALL bar(5))\<close>
lemma "(Program [(Method ''bar'' ''x'' \<lbrace> ''x'' := Num 5 \<rbrace>)] \<lbrace> CALL ''bar'' (Num 5);; SKIP \<rbrace>) 
        \<sim> (Program [(Method ''bar'' ''x'' \<lbrace> ''x'' := Num 5 \<rbrace>)] \<lbrace> SKIP;; CALL ''bar'' (Num 5)\<rbrace>) [\<circle>]"
  using tequivalence by (simp add: WL\<^sub>E_derivation_system)

value "(Program [] \<lbrace> SKIP \<rbrace>) \<sim> (Program [] \<lbrace> SKIP;;SKIP \<rbrace>) [\<circle>]"
value "(Program [] \<lbrace> IF (Bool True) THEN ''x'' := Num 2 FI \<rbrace>) \<sim> (Program [] \<lbrace> ''x'' := Num 2;;SKIP \<rbrace>) [\<circle>]"
value "(Program [] \<lbrace> IF (Bool False) THEN ''x'' := Num 2 FI \<rbrace>) \<sim> (Program [] \<lbrace> ''x'' := Num 2;;SKIP \<rbrace>) [\<circle>]"
value "(Program [] \<lbrace> IF ((Var ''x'') \<^sub>Req (Num 1)) THEN ''x'' := Num 0 FI \<rbrace>) \<sim> (Program [] \<lbrace> ''x'' := Num 0 \<rbrace>) [[''x'' \<longmapsto> Exp (Num 0)] \<circle>]"
value "(Program [] \<lbrace> IF ((Var ''x'') \<^sub>Req (Num 1)) THEN ''x'' := Num 0 FI \<rbrace>) \<sim> (Program [] \<lbrace> ''x'' := Num 0 \<rbrace>) [[''x'' \<longmapsto> Exp (Num 1)] \<circle>]"
value "(Program [] \<lbrace> ''x'' := Num 4;; WHILE ((Var ''x'') \<^sub>Rgeq (Num 1)) DO ''x'' := (Var ''x'') \<^sub>Asub (Num 1) OD \<rbrace>) 
        \<sim> (Program [] \<lbrace> (((''x'' := Num 4;;''x'' := Num 3);; ''x'' := Num 2);; ''x'' := Num 1);; ''x'' := Num 0 \<rbrace>) [\<circle>]"
value "(Program [(Method ''bar'' ''x'' \<lbrace> ''x'' := Num 5 \<rbrace>)] \<lbrace> CALL ''bar'' (Num 5);; SKIP \<rbrace>) 
        \<sim> (Program [(Method ''bar'' ''x'' \<lbrace> ''x'' := Num 5 \<rbrace>)] \<lbrace> SKIP;; CALL ''bar'' (Num 5)\<rbrace>) [\<circle>]"

end