#Mechanization of LAGC Semantics in Isabelle

Formalization of LAGC semantics in the popular theorem proving environment Isabelle/HOL.

Bachelor Thesis Project by Niklas Heidler